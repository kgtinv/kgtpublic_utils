﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.Common.Tests
{
    [TestFixture]
    public class CircularBufferTests
    {
        private bool Throws(Action action)
        {
            try
            {
                action();
            }
            catch (Exception)
            {
                return true;
            }

            return false;
        }

        [Test]
        public void CircularBuffer_EmptyBuffer_PropertiesAndMethodsThrow()
        {
            ICircularBuffer<int> circBuff = new CircularBuffer<int>(5);
            circBuff.Clear();
            Assert.IsFalse(circBuff.IsFull);
            Assert.IsTrue(Throws(() => { var v = circBuff.Newest; }));
        }

        [Test]
        public void CircularBuffer_SingleItem_NewstAndIndexing()
        {
            ICircularBuffer<int> circBuff = new CircularBuffer<int>(5);
            circBuff.Enqueue(2);
            Assert.AreEqual(2, circBuff.Newest);
            Assert.AreEqual(2, circBuff[0]);

            Assert.IsTrue(Throws(() => { var v = circBuff[1]; }));

            var arr = circBuff.ToArray();

            Assert.AreEqual(1, arr.Length);
            Assert.AreEqual(2, arr[0]);
        }

        [Test]
        public void CircularBuffer_MultipleItemsBeyondCapacity_NewstAndIndexing()
        {
            ICircularBuffer<int> circBuff = new CircularBuffer<int>(3);
            circBuff.Enqueue(1);
            circBuff.Enqueue(2);
            circBuff.Enqueue(3);
            circBuff.Enqueue(4);
            circBuff.Enqueue(5);
            circBuff.Enqueue(6);
            circBuff.Enqueue(7);
            circBuff.Enqueue(8);
            Assert.IsTrue(circBuff.IsFull);
            Assert.AreEqual(8, circBuff.Newest);
            Assert.AreEqual(6, circBuff.Oldest);
            Assert.AreEqual(8, circBuff[0]);
            Assert.AreEqual(7, circBuff[1]);
            Assert.AreEqual(6, circBuff[2]);

            Assert.IsTrue(Throws(() => { var v = circBuff[3]; }));

            var arr = circBuff.ToArray();

            Assert.AreEqual(3, arr.Length);
            Assert.AreEqual(6, arr[0]);
            Assert.AreEqual(7, arr[1]);
            Assert.AreEqual(8, arr[2]);

            circBuff.ClearOldestN(2);
            circBuff.Enqueue(9);

            Assert.IsFalse(circBuff.IsFull);
            Assert.AreEqual(9, circBuff.Newest);
            Assert.AreEqual(8, circBuff.Oldest);
            Assert.AreEqual(9, circBuff[0]);
            Assert.AreEqual(8, circBuff[1]);

            Assert.IsTrue(Throws(() => { var v = circBuff[2]; }));

            arr = circBuff.ToArray();

            Assert.AreEqual(2, arr.Length);
            Assert.AreEqual(8, arr[0]);
            Assert.AreEqual(9, arr[1]);
        }

        [Test]
        public void CircularBuffer_ClearNewestItemTest()
        {
            ICircularBuffer<int> circBuff = new CircularBuffer<int>(3);
            circBuff.Clear();
            Assert.IsFalse(circBuff.IsFull);
            Assert.IsTrue(circBuff.IsEmpty);
            circBuff.ClearNewestItem();
            Assert.IsFalse(circBuff.IsFull);
            Assert.IsTrue(circBuff.IsEmpty);

            circBuff.Enqueue(5);
            Assert.AreEqual(5, circBuff.Newest);
            Assert.AreEqual(5, circBuff.Oldest);
            Assert.AreEqual(5, circBuff[0]);
            circBuff.ClearNewestItem();
            Assert.IsFalse(circBuff.IsFull);
            Assert.IsTrue(circBuff.IsEmpty);

            circBuff.Enqueue(6);
            Assert.AreEqual(6, circBuff.Newest);
            Assert.AreEqual(6, circBuff.Oldest);
            Assert.AreEqual(6, circBuff[0]);

            circBuff.Enqueue(1);
            circBuff.Enqueue(2);
            circBuff.Enqueue(3);
            circBuff.Enqueue(4);
            circBuff.Enqueue(5);
            circBuff.Enqueue(6);
            circBuff.Enqueue(7);
            circBuff.Enqueue(8);
            Assert.IsTrue(circBuff.IsFull);
            Assert.AreEqual(8, circBuff.Newest);
            Assert.AreEqual(6, circBuff.Oldest);
            Assert.AreEqual(8, circBuff[0]);
            Assert.AreEqual(7, circBuff[1]);
            Assert.AreEqual(6, circBuff[2]);

            circBuff.ClearNewestItem();
            Assert.IsFalse(circBuff.IsFull);
            Assert.IsFalse(circBuff.IsEmpty);
            Assert.AreEqual(7, circBuff.Newest);
            Assert.AreEqual(6, circBuff.Oldest);
            Assert.AreEqual(7, circBuff[0]);
            Assert.AreEqual(6, circBuff[1]);
            Assert.IsTrue(Throws(() => { var v = circBuff[2]; }));

            circBuff.Enqueue(10);
            Assert.IsTrue(circBuff.IsFull);
            Assert.AreEqual(10, circBuff.Newest);
            Assert.AreEqual(6, circBuff.Oldest);
            Assert.AreEqual(10, circBuff[0]);
            Assert.AreEqual(7, circBuff[1]);
            Assert.AreEqual(6, circBuff[2]);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.Common.Tests
{
    [TestFixture]
    public class TimeIntervalTests
    {
        [Test]
        public void SingleDayInterval_Between_Utc()
        {
            SingleDayTimeInterval ti = new SingleDayTimeInterval(TimeSpan.FromHours(1), TimeSpan.FromHours(13),
                TimeZoneCode.UTC);
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 2, 22, 22)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 1, 0, 0)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 0)));

            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 0, 22, 22)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 1)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 23, 59, 59)));
        }

        [Test]
        public void SingleDayInterval_Inversed_Between_Utc()
        {
            SingleDayTimeInterval ti = new SingleDayTimeInterval(TimeSpan.FromHours(13), TimeSpan.FromHours(1),
                TimeZoneCode.UTC);
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 2, 22, 22)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 1, 0, 0)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 0)));

            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 0, 22, 22)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 1)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 23, 59, 59)));
        }

        public static DateTime FirstMonday(int year)
        {
            int day = 0;

            while ((new DateTime(year, 01, ++day)).DayOfWeek != DayOfWeek.Monday) ;

            return new DateTime(year, 01, day);
        }

        [Test]
        public void SingleWeekInterval_Between_Utc()
        {
            SingleWeekTimeInterval ti = new SingleWeekTimeInterval(TimeSpan.Parse("02.01:00:00"),
                TimeSpan.Parse("05.13:00:00"),
                TimeZoneCode.UTC);

            DateTime first2017Monday = FirstMonday(2017);

            Assert.IsFalse(ti.IsWithinBounds(first2017Monday));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(1)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(2)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(3)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(4)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(5)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(6)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(7)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(8)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(9)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(10)));

            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(2).AddHours(1)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(2).AddHours(2)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(5).AddHours(12)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(5).AddHours(13)));

            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(5).AddHours(14)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(6).AddHours(12)));
        }

        [Test]
        public void SingleWeekInterval_Inversed_Between_Utc()
        {
            SingleWeekTimeInterval ti = new SingleWeekTimeInterval(TimeSpan.Parse("05.13:00:00"),
                TimeSpan.Parse("02.01:00:00"),
                TimeZoneCode.UTC);

            DateTime first2017Monday = FirstMonday(2017);

            Assert.IsTrue(ti.IsWithinBounds(first2017Monday));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(1)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(2)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(3)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(4)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(5)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(6)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(7)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(8)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(9)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(10)));

            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(2).AddHours(1)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(2).AddHours(2)));
            Assert.IsFalse(ti.IsWithinBounds(first2017Monday.AddDays(5).AddHours(12)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(5).AddHours(13)));

            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(5).AddHours(14)));
            Assert.IsTrue(ti.IsWithinBounds(first2017Monday.AddDays(6).AddHours(12)));
        }

        [Test]
        public void SingleDayInterval_Between_Cet()
        {
            SingleDayTimeInterval ti = new SingleDayTimeInterval(TimeSpan.FromHours(1), TimeSpan.FromHours(13),
                TimeZoneCode.CET);
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 0, 0, 0)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 2, 22, 22)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 1, 0, 0)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 12, 0, 0)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 0)));

            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 0, 22, 22)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 12, 0, 1)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 1)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 23, 59, 59)));

            SingleDayTimeInterval ti2 = new SingleDayTimeInterval(TimeSpan.FromMinutes(30), TimeSpan.FromHours(13),
                TimeZoneCode.CET);

            //overflow to the next day
            Assert.IsTrue(ti2.IsWithinBounds(new DateTime(2017, 1, 1, 23, 59, 59)));
        }

        [Test]
        public void SingleDayInterval_Inversed_Between_Cet()
        {
            SingleDayTimeInterval ti = new SingleDayTimeInterval(TimeSpan.FromHours(13), TimeSpan.FromHours(1),
                TimeZoneCode.CET);
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 0, 0, 0)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 2, 22, 22)));
            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 1, 0, 0)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 12, 0, 0)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 0)));

            Assert.IsFalse(ti.IsWithinBounds(new DateTime(2017, 1, 1, 0, 22, 22)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 12, 0, 1)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 13, 0, 1)));
            Assert.IsTrue(ti.IsWithinBounds(new DateTime(2017, 1, 1, 23, 59, 59)));

            SingleDayTimeInterval ti2 = new SingleDayTimeInterval(TimeSpan.FromHours(13), TimeSpan.FromMinutes(30),
                TimeZoneCode.CET);

            //overflow to the next day
            Assert.IsFalse(ti2.IsWithinBounds(new DateTime(2017, 1, 1, 23, 59, 59)));
        }

        [Test]
        public void SingleDayInterval_AddSpan_Utc()
        {
            SingleDayTimeInterval ti = new SingleDayTimeInterval(TimeSpan.FromHours(1), TimeSpan.FromHours(14),
                TimeZoneCode.UTC);

            //single day
            Assert.AreEqual(
                new DateTime(2017, 1, 1, 1, 0, 0), ti.AddSpan(new DateTime(2017, 1, 1, 0, 0, 0), TimeSpan.FromHours(1)));
            Assert.AreEqual(
                new DateTime(2017, 1, 1, 15, 0, 0), ti.AddSpan(new DateTime(2017, 1, 1, 14, 0, 0), TimeSpan.FromHours(1)));
            Assert.AreEqual(
                new DateTime(2017, 1, 1, 15, 0, 0), ti.AddSpan(new DateTime(2017, 1, 1, 2, 0, 0), TimeSpan.FromHours(1)));

            //cross day
            Assert.AreEqual(
                new DateTime(2017, 1, 2, 0, 30, 0), ti.AddSpan(new DateTime(2017, 1, 1, 23, 30, 0), TimeSpan.FromHours(1)));
            Assert.AreEqual(
                new DateTime(2017, 1, 2, 01, 00, 0), ti.AddSpan(new DateTime(2017, 1, 1, 10, 00, 0), TimeSpan.FromHours(11)));
            Assert.AreEqual(
                new DateTime(2017, 1, 2, 01, 00, 0), ti.AddSpan(new DateTime(2017, 1, 1, 23, 00, 0), TimeSpan.FromHours(2)));
            Assert.AreEqual(
                new DateTime(2017, 1, 2, 23, 00, 0), ti.AddSpan(new DateTime(2017, 1, 1, 23, 00, 0), TimeSpan.FromHours(11)));

            //multi days
            Assert.AreEqual(
                new DateTime(2017, 1, 2, 1, 0, 0), ti.AddSpan(new DateTime(2017, 1, 1, 0, 0, 0), TimeSpan.FromHours(12)));
            Assert.AreEqual(
                new DateTime(2017, 1, 10, 15, 0, 0), ti.AddSpan(new DateTime(2017, 1, 1, 14, 0, 0), TimeSpan.FromHours(100)));
            Assert.AreEqual(
                new DateTime(2017, 1, 3, 15, 0, 0), ti.AddSpan(new DateTime(2017, 1, 1, 2, 0, 0), TimeSpan.FromHours(23)));
        }

        [Test]
        public void SingleDayInterval_Distance_Utc()
        {
            SingleDayTimeInterval ti = new SingleDayTimeInterval(TimeSpan.FromHours(1), TimeSpan.FromHours(14),
                TimeZoneCode.UTC);

            Assert.AreEqual(TimeSpan.FromHours(1),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 0, 0, 0), new DateTime(2017, 1, 1, 1, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(1),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 1, 0, 0), new DateTime(2017, 1, 1, 0, 0, 0)));

            Assert.AreEqual(TimeSpan.FromHours(1),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 14, 0, 0), new DateTime(2017, 1, 1, 15, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(1),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 15, 0, 0), new DateTime(2017, 1, 1, 14, 0, 0)));

            Assert.AreEqual(TimeSpan.FromHours(0),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 2, 0, 0), new DateTime(2017, 1, 1, 5, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(0),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 5, 0, 0), new DateTime(2017, 1, 1, 2, 0, 0)));

            Assert.AreEqual(TimeSpan.FromHours(1),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 2, 0, 0), new DateTime(2017, 1, 1, 15, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(1),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 15, 0, 0), new DateTime(2017, 1, 1, 2, 0, 0)));


            /////////// 
            /// Cases crossing day border

            Assert.AreEqual(TimeSpan.FromHours(1),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 30, 0), new DateTime(2017, 1, 2, 0, 30, 0)));
            Assert.AreEqual(TimeSpan.FromHours(1),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 0, 30, 0), new DateTime(2017, 1, 1, 23, 30, 0)));

            Assert.AreEqual(TimeSpan.FromHours(11),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 10, 00, 0), new DateTime(2017, 1, 2, 09, 00, 0)));
            Assert.AreEqual(TimeSpan.FromHours(11),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 09, 00, 0), new DateTime(2017, 1, 1, 10, 00, 0)));

            Assert.AreEqual(TimeSpan.FromHours(2),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 00, 0), new DateTime(2017, 1, 2, 10, 00, 0)));
            Assert.AreEqual(TimeSpan.FromHours(2),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 10, 00, 0), new DateTime(2017, 1, 1, 23, 00, 0)));

            Assert.AreEqual(TimeSpan.FromHours(11),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 00, 0), new DateTime(2017, 1, 2, 23, 00, 0)));
            Assert.AreEqual(TimeSpan.FromHours(11),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 23, 00, 0), new DateTime(2017, 1, 1, 23, 00, 0)));


            /////////// 
            /// Cases crossing multiple days border

            Assert.AreEqual(TimeSpan.FromHours(12),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 30, 0), new DateTime(2017, 1, 3, 0, 30, 0)));
            Assert.AreEqual(TimeSpan.FromHours(12),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 3, 0, 30, 0), new DateTime(2017, 1, 1, 23, 30, 0)));
            Assert.AreEqual(TimeSpan.FromHours(23),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 30, 0), new DateTime(2017, 1, 4, 0, 30, 0)));
            Assert.AreEqual(TimeSpan.FromHours(23),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 4, 0, 30, 0), new DateTime(2017, 1, 1, 23, 30, 0)));
            Assert.AreEqual(TimeSpan.FromHours(34),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 30, 0), new DateTime(2017, 1, 5, 0, 30, 0)));
            Assert.AreEqual(TimeSpan.FromHours(34),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 5, 0, 30, 0), new DateTime(2017, 1, 1, 23, 30, 0)));

            Assert.AreEqual(TimeSpan.FromHours(55),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 10, 00, 0), new DateTime(2017, 1, 6, 09, 00, 0)));
            Assert.AreEqual(TimeSpan.FromHours(55),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 6, 09, 00, 0), new DateTime(2017, 1, 1, 10, 00, 0)));

            Assert.AreEqual(TimeSpan.FromHours(12),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 0, 0, 0), new DateTime(2017, 1, 2, 1, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(12),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 1, 0, 0), new DateTime(2017, 1, 1, 0, 0, 0)));
        }


        [Test]
        public void SingleDayInterval_Inversed_Distance_Utc()
        {
            SingleDayTimeInterval ti = new SingleDayTimeInterval(TimeSpan.FromHours(14), TimeSpan.FromHours(1),
                TimeZoneCode.UTC);

            Assert.AreEqual(TimeSpan.FromHours(0),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 0, 0, 0), new DateTime(2017, 1, 1, 1, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(0),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 1, 0, 0), new DateTime(2017, 1, 1, 0, 0, 0)));

            Assert.AreEqual(TimeSpan.FromHours(0),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 14, 0, 0), new DateTime(2017, 1, 1, 15, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(0),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 15, 0, 0), new DateTime(2017, 1, 1, 14, 0, 0)));

            Assert.AreEqual(TimeSpan.FromHours(3),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 2, 0, 0), new DateTime(2017, 1, 1, 5, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(3),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 5, 0, 0), new DateTime(2017, 1, 1, 2, 0, 0)));

            Assert.AreEqual(TimeSpan.FromHours(12),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 2, 0, 0), new DateTime(2017, 1, 1, 15, 0, 0)));
            Assert.AreEqual(TimeSpan.FromHours(12),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 15, 0, 0), new DateTime(2017, 1, 1, 2, 0, 0)));


            /////////// 
            /// Cases crossing day border

            Assert.AreEqual(TimeSpan.FromHours(0),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 30, 0), new DateTime(2017, 1, 2, 0, 30, 0)));
            Assert.AreEqual(TimeSpan.FromHours(0),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 0, 30, 0), new DateTime(2017, 1, 1, 23, 30, 0)));

            Assert.AreEqual(TimeSpan.FromHours(12),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 10, 00, 0), new DateTime(2017, 1, 2, 09, 00, 0)));
            Assert.AreEqual(TimeSpan.FromHours(12),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 09, 00, 0), new DateTime(2017, 1, 1, 10, 00, 0)));

            Assert.AreEqual(TimeSpan.FromHours(9),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 00, 0), new DateTime(2017, 1, 2, 10, 00, 0)));
            Assert.AreEqual(TimeSpan.FromHours(9),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 10, 00, 0), new DateTime(2017, 1, 1, 23, 00, 0)));

            Assert.AreEqual(TimeSpan.FromHours(13),
               ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 1, 23, 00, 0), new DateTime(2017, 1, 2, 23, 00, 0)));
            Assert.AreEqual(TimeSpan.FromHours(13),
                ti.DateTimesDistanceExcludingInterval(new DateTime(2017, 1, 2, 23, 00, 0), new DateTime(2017, 1, 1, 23, 00, 0)));
        }



        [Test]
        public void SingleWeekInterval_Distance_Cet()
        {
            SingleWeekTimeInterval ti = new SingleWeekTimeInterval(TimeSpan.Parse("04.22:00:00"), TimeSpan.Parse("06.22:00:00"),
                TimeZoneCode.CET);

            DateTime fridayPriorDst = new DateTime(2017, 10, 27);
            DateTime sundayAfterDst = new DateTime(2017, 10, 29);

            Assert.AreEqual(TimeSpan.FromMinutes(30),
                ti.DateTimesDistanceExcludingInterval(fridayPriorDst.Add(new TimeSpan(19, 45, 0)),
                    sundayAfterDst.Add(new TimeSpan(21, 15, 0))));

            Assert.AreEqual(TimeSpan.FromMinutes(90),
                ti.DateTimesDistanceExcludingInterval(fridayPriorDst.Add(new TimeSpan(7, 19, 45, 0)),
                    sundayAfterDst.Add(new TimeSpan(7, 21, 15, 0))));

            Assert.AreEqual(TimeSpan.FromMinutes(90),
                ti.DateTimesDistanceExcludingInterval(fridayPriorDst.Add(new TimeSpan(19, 45, 0)).AddDays(-7),
                    sundayAfterDst.Add(new TimeSpan(21, 15, 0)).AddDays(-7)));
        }
    }
}

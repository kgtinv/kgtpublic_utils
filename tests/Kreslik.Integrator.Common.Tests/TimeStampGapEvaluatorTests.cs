﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.Common.Tests
{
    [TestFixture]
    public class TimeStampGapEvaluatorTests
    {
        [Test]
        public void TimeStepGapEvaluator_BasicForwardStepsGapTests()
        {
            TimeStepGapEvaluator tg = new TimeStepGapEvaluator(TimeSpan.FromMinutes(5), null);
            DateTime dtBase = new DateTime(2017,5, 4, 9, 30, 25);
            Assert.IsTrue(tg.IsPreceededByGap(dtBase));
            Assert.IsFalse(tg.IsPreceededByGap(dtBase.AddMinutes(5)));
            Assert.IsFalse(tg.IsPreceededByGap(dtBase.AddMinutes(9)));
            Assert.IsTrue(tg.IsPreceededByGap(dtBase.AddMinutes(15)));
            Assert.IsFalse(tg.IsPreceededByGap(dtBase.AddMinutes(16)));
        }

        [Test]
        public void TimeStepGapEvaluator_BasicBackwardStepsGapTests()
        {
            TimeStepGapEvaluator tg = new TimeStepGapEvaluator(TimeSpan.FromMinutes(5), null);
            DateTime dtBase = new DateTime(2017, 5, 4, 9, 30, 25);
            Assert.IsTrue(tg.IsPreceededByGap(dtBase));
            Assert.IsFalse(tg.IsPreceededByGap(dtBase.AddMinutes(-5)));
            Assert.IsFalse(tg.IsPreceededByGap(dtBase.AddMinutes(-9)));
            Assert.IsTrue(tg.IsPreceededByGap(dtBase.AddMinutes(-15)));
            Assert.IsFalse(tg.IsPreceededByGap(dtBase.AddMinutes(-16)));
        }

        [Test]
        public void TimeStepGapEvaluator_ForwardStepsWithRolloverGapTests()
        {
            TimeStepGapEvaluator tg = new TimeStepGapEvaluator(TimeSpan.FromMinutes(5), new TimeInterval[]
            {
                new SingleDayTimeInterval(TimeSpan.Parse("16:59:30"), TimeSpan.Parse("17:05:00"), TimeZoneCode.UTC), 
            });
            DateTime dtBase = new DateTime(2017, 5, 4, 16, 30, 00);
            DateTime dtRolloverStart = new DateTime(2017, 5, 4, 16, 59, 30);
            DateTime dtRolloverEnd = new DateTime(2017, 5, 4, 17, 05, 00);
            Assert.IsTrue(tg.IsPreceededByGap(dtRolloverStart.AddMinutes(-3)));
            Assert.IsFalse(tg.IsPreceededByGap(dtRolloverEnd.AddMinutes(+2)));

            Assert.IsTrue(tg.IsPreceededByGap(dtRolloverStart.AddHours(1).AddMinutes(-3)));
            Assert.IsTrue(tg.IsPreceededByGap(dtRolloverEnd.AddHours(1).AddMinutes(+2)));
        }
    }
}

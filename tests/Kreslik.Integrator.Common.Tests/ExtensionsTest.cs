﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.Common.Tests
{

    [TestFixture]
    public class ExtensionsTest
    {
        [Test]
        public void XDocumentHelpersTest_SortsBasicXml()
        {
            string xmlTest1 = "<myXml>ddd</myXml>";
            string sortedXmlTest1 = XDocumentHelpers.SortXmlStringElements(xmlTest1);
            Assert.AreEqual(xmlTest1, sortedXmlTest1);

            string xmlSingleNestLevelTest = @"
<myXml>
    <b>ddd</b>
    <C>aaa</C>
    <a>bbb</a>
</myXml>";

            string xmlSingleNestLevelExpectedResult = @"
<myXml>
    <C>aaa</C>
    <a>bbb</a>
    <b>ddd</b>
</myXml>";

            string xmlSingleNestLevelResult = XDocumentHelpers.SortXmlStringElements(xmlSingleNestLevelTest);
            Assert.IsTrue(xmlSingleNestLevelExpectedResult.EqualsDisregardWhitespaces(xmlSingleNestLevelResult));
        }

        [Test]
        public void XDocumentHelpersTest_SortsMultiLevelXml()
        {
            string xmlTest = @"
<myXml>
    <b>
        <num1>a</num1>
        <num0>b</num0>
		<num5>c</num5>
    </b>
    <C>
		<num1>a</num1>
        <num0>
			<x>10</x>
			<a>1</a>
		</num0>
		<num5>c</num5>
	</C>
    <a>bbb</a>
</myXml>";

            string xmlExpectedResult = @"
<myXml>
    <C>
		<num0>
			<a>1</a>
			<x>10</x>
		</num0>
		<num1>a</num1>
		<num5>c</num5>
	</C>
	<a>bbb</a>
    <b>
		<num0>b</num0>
        <num1>a</num1>
		<num5>c</num5>
    </b>
</myXml>";

            string xmlResult = XDocumentHelpers.SortXmlStringElements(xmlTest);
            Assert.IsTrue(xmlExpectedResult.EqualsDisregardWhitespaces(xmlResult));
        }
    }
}

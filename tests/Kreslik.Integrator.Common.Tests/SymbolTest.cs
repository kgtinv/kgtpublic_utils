﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.Common.Tests
{
    [TestFixture]
    public class SymbolTest
    {
        public class SymbolTestClass
        {
            public Symbol Symbol { get; set; }
        }


        [Test]
        public void Symbol_BasicComparisons()
        {
            Symbol s1 = Symbol.EUR_USD;
            Symbol s2 = Symbol.AUD_JPY;

            if (s1 == s2)
            {
                Assert.Fail("BUG: #1");
            }

            if (s1 != s2)
            {
            }
            else
            {
                Assert.Fail("BUG: #2");
            }

            if (s1 == Symbol.NULL)
            {
                Assert.Fail("BUG: #3");
            }

            if (s1 == Symbol.AUD_JPY)
            {
                Assert.Fail("BUG: #4");
            }

            if (Symbol.AUD_JPY == s2)
            {
            }
            else
            {
                Assert.Fail("BUG: #5");
            }

            if (s2 == Symbol.AUD_JPY)
            {
            }
            else
            {
                Assert.Fail("BUG: #6");
            }

            if (s1 == Symbol.EUR_USD)
            {
            }
            else
            {
                Assert.Fail("BUG: #7");
            }

            if (s1.Equals(Symbol.EUR_USD))
            {
            }
            else
            {
                Assert.Fail("BUG: #8");
            }

            if (s2 != Symbol.NULL)
            {
            }
            else
            {
                Assert.Fail("BUG: #9");
            }

            if (s2 != Symbol.EUR_JPY)
            {
            }
            else
            {
                Assert.Fail("BUG: #10");
            }

            SymbolTestClass stc = new SymbolTestClass();
            stc.Symbol = Symbol.EUR_USD;
            if (stc.Symbol != Symbol.EUR_USD)
            {
                Assert.Fail("BUG: #12");
            }

            stc.Symbol = Symbol.ZAR_MXN;
            if (stc.Symbol != Symbol.ZAR_MXN)
            {
                Assert.Fail("BUG: #13");
            }


            Symbol s3 = s2;
            if (s3 != Symbol.AUD_JPY)
            {
                Assert.Fail("BUG: #14");
            }

        }

        [Test]
        public void Symbol_Conversions()
        {
            Symbol s1 = Symbol.EUR_USD;
            Symbol s2 = (Symbol) (int) s1;

            Assert.AreEqual(s1, s2);
            Assert.AreEqual(s1.ToString(), "EUR/USD");
            Assert.AreEqual((string)s1, "EUR/USD");
            Assert.AreEqual(s1.ShortName, "EURUSD");
            Assert.AreEqual(s2.ToString(), "EUR/USD");
            Assert.AreEqual(s2.ShortName, "EURUSD");

            Assert.AreEqual(s2.BaseCurrency, Currency.EUR);
            Assert.AreEqual(s2.TermCurrency, Currency.USD);

            Symbol s3 = (Symbol)(string)s1;
            Assert.AreEqual(s1, s3);
            Assert.AreEqual(s3.ToString(), "EUR/USD");
        }

        [Test]
        public void Symbol_UninitializedComparisons()
        {
            SymbolTestClass stc = new SymbolTestClass();

            if (stc.Symbol != Symbol.NULL)
            {
                Assert.Fail("BUG: #11");
            }

            Symbol nullSym = new Symbol();
            if (nullSym != Symbol.NULL)
            {
                Assert.Fail("BUG: #15");
            }

        }

        [Test]
        public void Symbol_ToIntConversions()
        {
            Assert.AreEqual(-1, (int) Symbol.NULL);
            Assert.AreEqual(0, (int) Symbol.AUD_CAD);

            Assert.AreEqual(Symbol.ValuesCount, Symbol.Values.Count());
            Assert.AreEqual(Symbol.ValuesCount, Symbol.StringValues.Count());

            int i = 0;
            foreach (Symbol symbol in Symbol.Values)
            {
                Assert.AreEqual(i, (int) symbol);
                Assert.AreEqual((Symbol)i, symbol);
                i++;
            }

            i = 0;
            foreach (string symbolString in Symbol.StringValues)
            {
                Assert.AreEqual((Symbol)i, (Symbol)symbolString);
                Assert.AreEqual((Symbol)i, (Symbol)symbolString.Replace("/", string.Empty));
                Assert.AreEqual(((Symbol)i).ShortName, symbolString.Replace("/", string.Empty));
                i++;
            }
        }

        [Test]
        public void Symbol_NullTest()
        {
            Symbol nullSym = new Symbol();
            Assert.AreEqual("NULLSymbol", nullSym.ToString());
            Assert.AreEqual("NULLSymbol", nullSym.ShortName);
            Assert.AreEqual("NULLSymbol", (string)nullSym);
            Assert.Throws<InvalidCastException>(() => { Symbol s = (Symbol) (string) nullSym; });
        }

        [Test]
        public void Currency_ToIntConversions()
        {
            Assert.AreEqual(-1, (int)Currency.NULL);
            Assert.AreEqual(0, (int)Currency.AED);

            Assert.AreEqual(Currency.ValuesCount, Currency.Values.Count());

            int i = 0;
            foreach (Currency currency in Currency.Values)
            {
                Assert.AreEqual(i, (int)currency);
                Assert.AreEqual((Currency)i, currency);
                Assert.AreEqual((Currency)currency.ToString(), currency);
                i++;
            }
        }

        [Test]
        public void Counterparty_ToIntConversions()
        {
            Assert.AreEqual(-1, (int)Counterparty.NULL);
            Assert.AreEqual(0, (int)Counterparty.CRS);
            Assert.AreEqual(0, (int)BankCounterparty.CRS);

            Assert.AreEqual(Counterparty.ValuesCount, Counterparty.Values.Count());
            Assert.AreEqual(BankCounterparty.ValuesCount, BankCounterparty.Values.Count());

            int i = 0;
            foreach (Counterparty cpt in Counterparty.Values)
            {
                Assert.AreEqual(i, (int)cpt);
                Assert.AreEqual((Counterparty)i, cpt);
                Assert.AreEqual((Counterparty)cpt.ToString(), cpt);
                i++;
            }

            i = 0;
            foreach (BankCounterparty bankCounterparty in BankCounterparty.Values)
            {
                Assert.AreEqual(i, (int)bankCounterparty);
                Assert.AreEqual((BankCounterparty)(Counterparty)i, bankCounterparty);
                Assert.AreEqual((BankCounterparty)bankCounterparty.ToString(), bankCounterparty);

                Assert.AreEqual((Counterparty)i, bankCounterparty);
                Assert.AreEqual((Counterparty)bankCounterparty.ToString(), bankCounterparty);

                Assert.AreEqual((Counterparty)bankCounterparty, bankCounterparty);
                i++;
            }
        }
        
    }
}

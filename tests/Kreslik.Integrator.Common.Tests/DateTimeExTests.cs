﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.Common.Tests
{
    [TestFixture]
    public class DateTimeExTests
    {
        [Test]
        public void GetBusinessDaysTest_RangeOfDays()
        {
            DateTime dtBase = DateTime.UtcNow;

            for (int startDayIdx = 0; startDayIdx < 7; startDayIdx++)
            {
                for (int endDayIdx = 0; endDayIdx < 30; endDayIdx++)
                {
                    DateTime startDateMidnight = dtBase.Date.AddDays(startDayIdx);
                    DateTime endDateMidnight = dtBase.Date.AddDays(startDayIdx + endDayIdx);

                    CompareBusinessDaysDiff(startDateMidnight, endDateMidnight);
                    CompareBusinessDaysDiff(startDateMidnight.AddHours(5), endDateMidnight.AddHours(5));
                    CompareBusinessDaysDiff(startDateMidnight.AddHours(5), endDateMidnight.AddHours(8));
                    CompareBusinessDaysDiff(startDateMidnight.AddHours(8), endDateMidnight.AddHours(5));
                }
            }
        }

        [Test]
        public void SpecificDates_Diff_Test()
        {
            DateTime start = DateTime.Parse("2017-10-13");
            DateTime end = DateTime.Parse("2017-10-16");

            CompareBusinessDaysDiff(start, end);
            CompareBusinessDaysDiff(start.AddHours(5), end.AddHours(5));
            CompareBusinessDaysDiff(start.AddHours(5), end.AddHours(8));
            CompareBusinessDaysDiff(start.AddHours(8), end.AddHours(5));
        }

        private void CompareBusinessDaysDiff(DateTime start, DateTime end)
        {
            int actualBusinessDaysBetweenMidnights = DateTimeEx.GetBusinessDays(start,
                        end);
            int expectedBusinessDaysBetweenMidnights = GetBussinessDays(start, end);

            Assert.AreEqual(expectedBusinessDaysBetweenMidnights, actualBusinessDaysBetweenMidnights, $"Days between {start} ({start.DayOfWeek}) and {end} ({end.DayOfWeek})");
        }

        private int GetBussinessDays(DateTime start, DateTime end)
        {
            int businessDays = 0;
            DateTime current = start;
            while (end.Date > current.Date)
            {
                current += TimeSpan.FromDays(1);
                if (current.IsWorkDay())
                {
                    businessDays++;
                }
            }

            //if (end.Date > start.Date && end.IsWorkDay() && start.IsWeekend())
            //{
            //    businessDays++;
            //}

            return businessDays;
        }


        [Test]
        public void AddBusinessDaysTest_RangeOfDays()
        {
            DateTime dtBase = DateTime.UtcNow;

            for (int startDayIdx = 0; startDayIdx < 7; startDayIdx++)
            {
                for (int daysCount = 0; daysCount < 30; daysCount++)
                {
                    DateTime startDateMidnight = dtBase.Date.AddDays(startDayIdx);

                    AssertBusinessDaysDiff(startDateMidnight, daysCount);
                    AssertBusinessDaysDiff(startDateMidnight.AddHours(5), daysCount);
                    AssertBusinessDaysDiff(startDateMidnight.AddHours(8), daysCount);
                }
            }
        }

        [Test]
        public void SpecificDates_Add_Test()
        {
            DateTime start = DateTime.Parse("2017-10-13");
            int daysCount = 1;

            AssertBusinessDaysDiff(start, daysCount);
            AssertBusinessDaysDiff(start.AddHours(5), daysCount);
            AssertBusinessDaysDiff(start.AddHours(8), daysCount);
        }

        private void AssertBusinessDaysDiff(DateTime startDate, int businessDaysToAdd)
        {
            DateTime endDate = startDate.AddBusinessDays(businessDaysToAdd);

            Assert.AreEqual(startDate.TimeOfDay, endDate.TimeOfDay);
            int expectedDiff = GetBussinessDays(startDate, endDate);
            Assert.AreEqual(businessDaysToAdd, expectedDiff,
                $"{businessDaysToAdd} business days from {startDate} ({startDate.DayOfWeek})  calculated as {endDate} ({endDate.DayOfWeek})");
        }
    }
}

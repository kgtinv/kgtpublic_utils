﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;


namespace Kreslik.Integrator.Common.Tests
{
    internal class TestingWorkItem
    {
        public ManualResetEvent MRE = new ManualResetEvent(false);

        public void DummyAction()
        {
            MRE.WaitOne();
        }
    }

    [TestFixture]
    public class ThreadPoolExTests
    {

        public class ThreadPoolExUnderTests : ThreadPoolEx
        {
            internal ThreadPoolExUnderTests(IThreadPoolExPerformanceCounters counters, ILogger logger)
                : base(counters, logger)
            { }

            internal ThreadPoolExUnderTests(int threadsReservedForHighestPriorityTask, int maximumThreadsNum,
                IThreadPoolExPerformanceCounters counters, ILogger logger)
                : base(threadsReservedForHighestPriorityTask, maximumThreadsNum, counters, logger)
            { }

            private ManualResetEventSlim _popingItemsBlocker = new ManualResetEventSlim(false);

            public void BlockItmesPoping()
            {
                this.OnPopingNextItem = _popingItemsBlocker.Wait;
                _popingItemsBlocker.Reset();
            }

            public void ResumeItmesPoping()
            {
                _popingItemsBlocker.Set();
            }
        }

        [Test]
        public void TimerTaskFlagUtils_AllBackAndForthConversions()
        {
            TimerTaskFlagUtils.TestSelf();
        }

        [Test]
        public void ThreadPoolEx_ScheduleWorkItem_Executed()
        {
            TestLogger testLogger = new TestLogger(Assert.Fail);
            testLogger.FailTestOnUnexpectedLogs = true;
            var counters = new DummyThreadPoolExPerformanceCounters();
            ThreadPoolExUnderTests threadPool = new ThreadPoolExUnderTests(1, 3, counters, testLogger);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.AvailableThreadsCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);

            //wait here first so that the new threads can start and not get blocked
            Thread.Sleep(50);
            threadPool.BlockItmesPoping();
            TestingWorkItem twe = new TestingWorkItem();
            threadPool.QueueWorkItem(new WorkItem(twe.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Normal));
            //give waiting thread time to pick the task
            Thread.Sleep(10);
            Assert.AreEqual(1, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);
            Assert.AreEqual(1, counters.AvailableThreadsCount);
            threadPool.ResumeItmesPoping();
            Thread.Sleep(20);

            Assert.AreEqual(1, counters.AvailableThreadsCount);

            twe.MRE.Set();
            Thread.Sleep(20);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.AvailableThreadsCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);
        }

        [Test, Timeout(2000)]
        public void ThreadPoolEx_ScheduleMultipleHighPriWorkItem_AllExecuted()
        {
            TestLogger testLogger = new TestLogger(Assert.Fail);
            testLogger.FailTestOnUnexpectedLogs = true;
            var counters = new DummyThreadPoolExPerformanceCounters();
            ThreadPoolExUnderTests threadPool = new ThreadPoolExUnderTests(1, 4, counters, testLogger);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(2, counters.AvailableThreadsCount);
            Assert.AreEqual(2, counters.TotalThreadsCount);

            //wait here first so that the new threads can start and not get blocked
            Thread.Sleep(50);
            threadPool.BlockItmesPoping();
            TestingWorkItem twe1 = new TestingWorkItem();
            TestingWorkItem twe2 = new TestingWorkItem();
            TestingWorkItem twe3 = new TestingWorkItem();
            TestingWorkItem twe4 = new TestingWorkItem();
            threadPool.QueueWorkItem(new WorkItem(twe1.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest));
            //give the running thread chance to start and then decrement the count of available threads
            Thread.Sleep(10);
            Assert.AreEqual(1, counters.AvailableThreadsCount);
            if (counters.TotalThreadsCount > 2)
            {
                testLogger.Log(LogLevel.Fatal, "Unexpected! Total threads count: " + counters.TotalThreadsCount);
            }
            Assert.AreEqual(2, counters.TotalThreadsCount);
            threadPool.QueueWorkItem(new WorkItem(twe2.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest));
            //give the newly spawned thread chance to start and then decrement the count of available threads
            Thread.Sleep(10);
            Assert.AreEqual(0, counters.AvailableThreadsCount);
            if (counters.TotalThreadsCount > 3)
            {
                testLogger.Log(LogLevel.Fatal, "Unexpected! Total threads count: " + counters.TotalThreadsCount);
            }
            Assert.AreEqual(3, counters.TotalThreadsCount);
            threadPool.QueueWorkItem(new WorkItem(twe3.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest));
            //give the newly spawned thread chance to start and then decrement the count of available threads
            Thread.Sleep(10);
            Assert.AreEqual(0, counters.AvailableThreadsCount);
            Assert.AreEqual(4, counters.TotalThreadsCount);
            Task.Factory.StartNew(
                () =>
                    threadPool.QueueWorkItem(new WorkItem(twe4.DummyAction, TimerTaskFlagUtils.WorkItemPriority.Highest)),
                //passing cancellation token to prevent thread hijack - see http://stackoverflow.com/questions/12245935/is-task-factory-startnew-guaranteed-to-use-another-thread-than-the-calling-thr
                new CancellationToken());
            Thread.Sleep(50);
            //No synchronous processing - all queued
            Assert.AreEqual(4, counters.QueuedTasksCount);
            Assert.AreEqual(4, counters.TotalThreadsCount);
            Assert.AreEqual(0, counters.AvailableThreadsCount);
            threadPool.ResumeItmesPoping();
            Thread.Sleep(50);
            Assert.AreEqual(0, counters.AvailableThreadsCount);

            twe1.MRE.Set();
            twe2.MRE.Set();
            twe3.MRE.Set();
            twe4.MRE.Set();
            Thread.Sleep(50);

            Assert.AreEqual(0, counters.QueuedTasksCount);
            Assert.AreEqual(4, counters.AvailableThreadsCount);
            Assert.AreEqual(4, counters.TotalThreadsCount);
        }
    }
}

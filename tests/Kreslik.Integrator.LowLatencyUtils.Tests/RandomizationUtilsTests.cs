﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using NUnit.Framework;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class RandomizationUtilsTests
    {
        [Test]
        public void CustomRandomGeneratorDistributionTest()
        {
            int iterations = 1000000;
            int sequenceLength = 10;
            int tolerancePercent = 5;

            Random rnd = RandomizationUtils.Random;
            int[] distributions = new int[sequenceLength];
            for (int iter = 0; iter < iterations; iter++)
            {
                distributions[rnd.Next(sequenceLength)]++;
            }

            int expectedIncidence = iterations / sequenceLength;
            double toleranceDelta = tolerancePercent / 100.0 * expectedIncidence;

            for (int idxInner = 0; idxInner < sequenceLength; idxInner++)
            {
                Assert.AreEqual(expectedIncidence, distributions[idxInner], toleranceDelta);
            }
        }

        [Test]
        public void CreateFisherYatesShuffelSequence_DistributionTest()
        {
            int iterations = 500000;
            int sequenceLength = 10;
            int tolerancePercent = 5;

            int[][] distributionMatrix =
                Enumerable.Range(0, sequenceLength).Select(i => new int[sequenceLength]).ToArray();

            for (int iteration = 0; iteration < iterations; iteration++)
            {
                var seq = RandomizationUtils.CreateFisherYatesShuffleSequence /*SkeetShuffle*/(sequenceLength);
                Assert.False(seq.HasDuplicities(), "Random sequence should not have any duplicities");
                for (int idx = 0; idx < sequenceLength; idx++)
                {
                    distributionMatrix[idx][seq[idx]]++;
                }
            }

            int expectedIncidence = iterations / sequenceLength;
            double toleranceDelta = tolerancePercent / 100.0 * expectedIncidence;

            for (int idxOuter = 0; idxOuter < sequenceLength; idxOuter++)
            {
                for (int idxInner = 0; idxInner < sequenceLength; idxInner++)
                {
                    Assert.AreEqual(expectedIncidence, distributionMatrix[idxOuter][idxInner], toleranceDelta);
                }
            }
        }

        [Test]
        public void ExpandBlocksFromStartIndexesTest()
        {
            IReadOnlyList<int> blockSizes;
            var result = RandomizationUtils.ExpandBlocksFromStartIndexes(new[] {2, 8, -2, 20, 0}, 3, 0, 20, 12, out blockSizes);
            Assert.AreEqual(new[] {2,3,4, 8,9,10, 0, 20, 0,1,2}, result);
            Assert.AreEqual(new[] { 3, 3, 1, 1, 3 }, blockSizes);
        }

        [Test]
        public void PerformBlockRandomizationTest()
        {
            IReadOnlyList<int> blockSizes1;
            IReadOnlyList<int> blockSizes2;
            IReadOnlyList<int> blockSizes3;
            IReadOnlyList<int> blockSizes4;

            var result1 = RandomizationUtils.PerformBlockRandomization(new[] { 0, 1, 2, 3, 5, 8, 9, 10 }, 4, true, true, out blockSizes1);
            var result2 = RandomizationUtils.PerformBlockRandomization(new[] { 0, 1, 2, 3, 5, 8, 9, 10 }, 4, false, true, out blockSizes2);
            var result3 = RandomizationUtils.PerformBlockRandomization(new[] { 0, 1, 2, 3, 5, 8, 9, 10 }, 4, true, false, out blockSizes3);
            var result4 = RandomizationUtils.PerformBlockRandomization(new[] { 0, 1, 2, 3, 5, 8, 9, 10 }, 4, false, false, out blockSizes4);
        }

    }
}

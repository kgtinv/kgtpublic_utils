﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils;
using Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils.IndexesProvider;
using NUnit.Framework;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class SparseBitmapSetTests
    {
        [Test]
        public void SparseBitmapSet_GetFirstMatchingIndexTest()
        {
            int rowsCount = 1500;
            int metricABucketsCount = 4;
            int metricBBucketsCount = 1;
            int metricCBucketsCount = 2;

            bool[][] metricAflags = Enumerable.Range(0, metricABucketsCount).Select(i => new bool[rowsCount]).ToArray();
            bool[][] metricBflags = Enumerable.Range(0, metricBBucketsCount).Select(i => new bool[rowsCount]).ToArray();
            bool[][] metricCflags = Enumerable.Range(0, metricCBucketsCount).Select(i => new bool[rowsCount]).ToArray();

            metricAflags[0][100] = true;
            metricAflags[0][200] = true;
            metricAflags[0][300] = true;
            metricAflags[0][699] = true;

            for (int idx = 900; idx < 1000; idx++)
            {
                metricAflags[1][idx] = true;
            }

            for (int idx = 195; idx < 295; idx++)
            {
                metricAflags[2][idx] = true;
            }

            for (int idx = 650; idx < 702; idx++)
            {
                metricAflags[2][idx] = true;
            }

            metricAflags[2][800] = true;

            //4th bucket empty

            metricBflags[0][11] = true;
            metricBflags[0][199] = true;
            metricBflags[0][201] = true;
            metricBflags[0][591] = true;
            metricBflags[0][699] = true;

            for (int idx = 100; idx < 200; idx++)
            {
                metricCflags[0][idx] = true;
            }

            metricCflags[0][500] = true;

            for (int idx = 600; idx < 800; idx++)
            {
                metricCflags[0][idx] = true;
            }

            for (int idx = 0; idx < rowsCount; idx += 2)
            {
                metricCflags[1][idx] = true;
            }

            var matericABitmaps = metricAflags.Select(flags => new SparseBitmap(flags)).ToArray();
            var matericBBitmaps = metricBflags.Select(flags => new SparseBitmap(flags)).ToArray();
            var matericCBitmaps = metricCflags.Select(flags => new SparseBitmap(flags)).ToArray();

            SparseBitmapSet bitmapSet = new SparseBitmapSet(new[] {matericABitmaps, matericBBitmaps, matericCBitmaps});

            Assert.AreEqual(199, bitmapSet.GetFirstMatchingIndex(new BasicIndexesProvider(new byte[] { 0, 0, 0 }, new byte[] { 3, 0, 1 })));
            Assert.AreEqual(699, bitmapSet.GetFirstMatchingIndex(new BasicIndexesProvider(new byte[] {0, 0, 0}, new byte[] {1, 0, 1})));
            Assert.AreEqual(-1, bitmapSet.GetFirstMatchingIndex(new BasicIndexesProvider(new byte[] { 3, 0, 0 }, new byte[] { 3, 0, 1 })));
            Assert.AreEqual(-1, bitmapSet.GetFirstMatchingIndex(new BasicIndexesProvider(new byte[] { 0, 0, 1 }, new byte[] { 3, 0, 1 })));
        }

        [Test]
        public void SparseBitmapSet_GetMatchingIndexesTest()
        {
            int rowsCount = 1500;
            int metricABucketsCount = 4;
            int metricBBucketsCount = 1;
            int metricCBucketsCount = 2;

            bool[][] metricAflags = Enumerable.Range(0, metricABucketsCount).Select(i => new bool[rowsCount]).ToArray();
            bool[][] metricBflags = Enumerable.Range(0, metricBBucketsCount).Select(i => new bool[rowsCount]).ToArray();
            bool[][] metricCflags = Enumerable.Range(0, metricCBucketsCount).Select(i => new bool[rowsCount]).ToArray();

            metricAflags[0][100] = true;
            metricAflags[0][200] = true;
            metricAflags[0][300] = true;
            metricAflags[0][699] = true;

            for (int idx = 900; idx < 1000; idx++)
            {
                metricAflags[1][idx] = true;
            }

            for (int idx = 195; idx < 295; idx++)
            {
                metricAflags[2][idx] = true;
            }

            for (int idx = 650; idx < 702; idx++)
            {
                metricAflags[2][idx] = true;
            }

            metricAflags[2][800] = true;

            //4th bucket empty

            metricBflags[0][11] = true;
            metricBflags[0][199] = true;
            metricBflags[0][201] = true;
            metricBflags[0][591] = true;
            metricBflags[0][699] = true;

            for (int idx = 100; idx < 200; idx++)
            {
                metricCflags[0][idx] = true;
            }

            metricCflags[0][500] = true;

            for (int idx = 600; idx < 800; idx++)
            {
                metricCflags[0][idx] = true;
            }

            for (int idx = 0; idx < rowsCount; idx += 2)
            {
                metricCflags[1][idx] = true;
            }

            var matericABitmaps = metricAflags.Select(flags => new SparseBitmap(flags)).ToArray();
            var matericBBitmaps = metricBflags.Select(flags => new SparseBitmap(flags)).ToArray();
            var matericCBitmaps = metricCflags.Select(flags => new SparseBitmap(flags)).ToArray();

            SparseBitmapSet bitmapSet = new SparseBitmapSet(new[] { matericABitmaps, matericBBitmaps, matericCBitmaps });

            Assert.AreEqual(new[] {199, 699}, bitmapSet.GetMatchingIndexes(new BasicIndexesProvider(new byte[] { 0, 0, 0 }, new byte[] { 3, 0, 1 })));
            Assert.AreEqual(new[] { 699 }, bitmapSet.GetMatchingIndexes(new BasicIndexesProvider(new byte[] { 0, 0, 0 }, new byte[] { 1, 0, 1 })));
            Assert.AreEqual(new int[] {}, bitmapSet.GetMatchingIndexes(new BasicIndexesProvider(new byte[] { 3, 0, 0 }, new byte[] { 3, 0, 1 })));
            Assert.AreEqual(new int[] { }, bitmapSet.GetMatchingIndexes(new BasicIndexesProvider(new byte[] { 0, 0, 1 }, new byte[] { 3, 0, 1 })));
        }

        [Test]
        public void SparseBitmapSet_ImmediateMatchTest()
        {
            int rowsCount = 1500;
            int metricABucketsCount = 3;
            int metricBBucketsCount = 1;
            int metricCBucketsCount = 2;

            bool[][] metricAflags = Enumerable.Range(0, metricABucketsCount).Select(i => new bool[rowsCount]).ToArray();
            bool[][] metricBflags = Enumerable.Range(0, metricBBucketsCount).Select(i => new bool[rowsCount]).ToArray();
            bool[][] metricCflags = Enumerable.Range(0, metricCBucketsCount).Select(i => new bool[rowsCount]).ToArray();

            metricAflags[0][70] = true;
            metricAflags[0][699] = true;

            for (int idx = 900; idx < 1000; idx++)
            {
                metricAflags[1][idx] = true;
            }

            //3rd bucket empty

            metricBflags[0][2] = true;
            metricBflags[0][70] = true;
            metricBflags[0][591] = true;
            metricBflags[0][699] = true;

            for (int idx = 100; idx < 200; idx++)
            {
                metricCflags[0][idx] = true;
            }

            metricCflags[0][500] = true;

            for (int idx = 600; idx < 800; idx++)
            {
                metricCflags[0][idx] = true;
            }

            for (int idx = 0; idx < rowsCount; idx += 2)
            {
                metricCflags[1][idx] = true;
            }

            var matericABitmaps = metricAflags.Select(flags => new SparseBitmap(flags)).ToArray();
            var matericBBitmaps = metricBflags.Select(flags => new SparseBitmap(flags)).ToArray();
            var matericCBitmaps = metricCflags.Select(flags => new SparseBitmap(flags)).ToArray();

            SparseBitmapSet bitmapSet = new SparseBitmapSet(new[] { matericABitmaps, matericBBitmaps, matericCBitmaps });

            Assert.AreEqual(70, bitmapSet.GetFirstMatchingIndex(new BasicIndexesProvider(new byte[] { 0, 0, 0 }, new byte[] { 2, 0, 1 })));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Moq;
using NUnit.Framework;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class SingleWorkerQueueTests
    {
        [Test]
        public void SingleWorkerQueue_ctorTest()
        {
            var producer = new Mock<IWorkQueueProducer<int>>();

            ILogger testLogger = new TestLogger(Assert.Fail);
            SingleWorkerQueue<int> wq = new SingleWorkerQueue<int>(producer.Object,
                SingleWorkerQueue<int>.SingleWorkerQueueBehavior.BlockingIsNotAllowed_DataCanBeThrownAway, true, testLogger, "testQueue");

            AutoResetEvent consumerCompletelyDoneEvent = new AutoResetEvent(false);
            

            int numItemsReturned = 0;
            wq.SingleItemAvailable += i => { numItemsReturned++; };
            int queueClosed = 0;
            wq.QueueClosedWithNoMoreData += () => { queueClosed++; };
            wq.QueueClosedWithNoMoreData += () => consumerCompletelyDoneEvent.Set();

            wq.CloseQueue();

            Assert.IsTrue(consumerCompletelyDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            Assert.AreEqual(0, numItemsReturned, "Expected zero calls to SingleItemAvailable");
            Assert.AreEqual(1, queueClosed, "Expected 1 calls to QueueClosedWithNoMoreData");

            Assert.Throws<Exception>(() => wq = new SingleWorkerQueue<int>(producer.Object,
                SingleWorkerQueue<int>.SingleWorkerQueueBehavior.BlockingIsNotAllowed_DataCanBeThrownAway, true,
                testLogger, 5, "testQueue"));
        }

        [Test]
        public void SingleWorkerQueue_ConsumingInBatchesTest()
        {
            ManualResetEventSlim consumerSingleItemBlockingEvent = new ManualResetEventSlim(true);
            ManualResetEventSlim consumerBatchBlockingEvent = new ManualResetEventSlim(true);
            var producer = new Mock<IWorkQueueProducer<int>>();


            TestLogger testLogger = new TestLogger(Assert.Fail, LogLevel.Error);
            testLogger.FailTestOnUnexpectedLogs = true;
            SingleWorkerQueue<int> wq = new SingleWorkerQueue<int>(producer.Object,
                SingleWorkerQueue<int>.SingleWorkerQueueBehavior.BlockingIsNotAllowed_DataCanBeThrownAway, true, testLogger, 4, "testQueue");

            int noQueueClosedCallsNum = 0;
            AutoResetEvent consumerCompletelyDoneEvent = new AutoResetEvent(false);
            wq.QueueClosedWithNoMoreData += () =>
            {
                noQueueClosedCallsNum++;
                consumerCompletelyDoneEvent.Set();
            };

            AutoResetEvent consumerBatchPreEvent = new AutoResetEvent(false);
            AutoResetEvent consumerSingleItemDoneEvent = new AutoResetEvent(false);
            AutoResetEvent consumerBatchDoneEvent = new AutoResetEvent(false);

            int noQueuedDataCallsNum = 0;
            wq.NoQueuedData += () =>
            {
                noQueuedDataCallsNum++;
                //Interlocked.Increment(ref noQueuedDataCallsNum);
                consumerBatchPreEvent.Set();
                consumerBatchBlockingEvent.Wait();
                consumerBatchDoneEvent.Set();
            };

            List<int> itemsCalled = new List<int>();
            int singleItemCallsNum = 0;

            //simulate processing pressure + signal once processing is done
            wq.SingleItemAvailable += item =>
            {
                itemsCalled.Add(item);
                singleItemCallsNum++;
                //Interlocked.Increment(ref singleItemCallsNum);
                consumerSingleItemBlockingEvent.Wait();
                consumerSingleItemDoneEvent.Set();
            };

            int writtingWrappedCallsNum = 0;
            wq.WrittingWrappedAndPreviousDataMightBeInconsistent += () =>
            {
                writtingWrappedCallsNum++;
            };


            int i = 0;
            //start blocking
            consumerBatchBlockingEvent.Reset();
            producer.Raise(p => p.NewWorkAvailable += null, i);

            Assert.IsTrue(consumerSingleItemDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            //the initial resume is now not causing wrapping
            Assert.AreEqual(0, writtingWrappedCallsNum);

            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);
            Assert.AreEqual(1, singleItemCallsNum);

            Assert.IsTrue(consumerBatchPreEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            //it was called, but did not return
            Assert.AreEqual(1, noQueuedDataCallsNum);

            //now add other items - those will pile up
            i++;
            producer.Raise(p => p.NewWorkAvailable += null, i);
            i++;
            producer.Raise(p => p.NewWorkAvailable += null, i);
            //give chance to consumer thread - this is to show it's blocked
            Thread.Sleep(10);

            //consumer hasn't progressed at all
            Assert.AreEqual(1, singleItemCallsNum);
            Assert.AreEqual(1, noQueuedDataCallsNum);

            //make sure we have something to wait for
            consumerBatchPreEvent.Reset();
            //unlock consumer and wait for it to catch up
            consumerBatchBlockingEvent.Set();
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.IsTrue(consumerSingleItemDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.IsTrue(consumerBatchPreEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            Assert.AreEqual(3, singleItemCallsNum);
            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);
            Assert.AreEqual(i - 1, itemsCalled[itemsCalled.Count - 2]);
            Assert.AreEqual(2, noQueuedDataCallsNum);


            //start blocking
            consumerBatchBlockingEvent.Reset();
            //make sure we can wait till the item is done
            consumerSingleItemDoneEvent.Reset();
            //this will be processed immediately
            i++;
            producer.Raise(p => p.NewWorkAvailable += null, i);
            //wait for entering batch signal (and it will block in this method)
            Assert.IsTrue(consumerSingleItemDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(4, singleItemCallsNum);
            Assert.AreEqual(3, noQueuedDataCallsNum);

            //pile up 4 items (max capacity)
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);

            //give chance to consumer thread - this is to show it's blocked
            Thread.Sleep(10);

            //nothing happened
            Assert.AreEqual(4, singleItemCallsNum);
            Assert.AreEqual(3, noQueuedDataCallsNum);
            //overflow did not happen
            Assert.AreEqual(0, writtingWrappedCallsNum);

            //make sure we reset at the correct time
            Assert.IsTrue(consumerBatchPreEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            //make sure we will wait for signal after setting the unblocking one
            consumerBatchDoneEvent.Reset();
            //unlock consumer and wait for it to catch up
            consumerBatchBlockingEvent.Set();
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            //make sure the reader thread picks there is new data piled
            Thread.Sleep(15);

            //refresh data
            InterlockedEx.Read(ref noQueuedDataCallsNum);
            //Verify all data were pumped
            Assert.AreEqual(8, InterlockedEx.Read(ref singleItemCallsNum));
            Assert.AreEqual(4, noQueuedDataCallsNum);
            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);
            Assert.AreEqual(i - 1, itemsCalled[itemsCalled.Count - 2]);
            Assert.AreEqual(i - 2, itemsCalled[itemsCalled.Count - 3]);
            Assert.AreEqual(i - 3, itemsCalled[itemsCalled.Count - 4]);
            Assert.AreEqual(8, singleItemCallsNum);

            wq.CloseQueue();

            Assert.IsTrue(consumerCompletelyDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(1, noQueueClosedCallsNum);
        }

        [Test]
        public void SingleWorkerQueue_OverflowTest()
        {
            ManualResetEventSlim consumerSingleItemBlockingEvent = new ManualResetEventSlim(true);
            ManualResetEventSlim consumerBatchBlockingEvent = new ManualResetEventSlim(true);
            var producer = new Mock<IWorkQueueProducer<int>>();

            ILogger testLogger = new TestLogger(Assert.Fail, LogLevel.Fatal);
            //ILogger testLogger = LogFactory.Instance.GetLogger("tstLgr2");
            SingleWorkerQueue<int> wq = new SingleWorkerQueue<int>(producer.Object,
                SingleWorkerQueue<int>.SingleWorkerQueueBehavior.BlockingIsNotAllowed_DataCanBeThrownAway, true, testLogger, 4, "testQueue");

            int noQueueClosedCallsNum = 0;
            AutoResetEvent consumerCompletelyDoneEvent = new AutoResetEvent(false);
            wq.QueueClosedWithNoMoreData += () =>
            {
                noQueueClosedCallsNum++;
                consumerCompletelyDoneEvent.Set();
            };

            AutoResetEvent consumerSingleItemDoneEvent = new AutoResetEvent(false);
            AutoResetEvent consumerBatchDoneEvent = new AutoResetEvent(false);
            AutoResetEvent consumerBatchCallStartedEvent = new AutoResetEvent(false);

            int noQueuedDataCallsNum = 0;
            wq.NoQueuedData += () =>
            {
                consumerBatchCallStartedEvent.Set();
                noQueuedDataCallsNum++;
                //Interlocked.Increment(ref noQueuedDataCallsNum);
                consumerBatchBlockingEvent.Wait();
                consumerBatchDoneEvent.Set();
            };

            List<int> itemsCalled = new List<int>();
            int singleItemCallsNum = 0;

            //simulate processing pressure + signal once processing is done
            //System.Diagnostics.Debug.WriteLine("Test - subscribing");
            wq.SingleItemAvailable += item =>
            {
                //Debug.WriteLine("BLAAAAH");

                itemsCalled.Add(item);
                singleItemCallsNum++;
                //Interlocked.Increment(ref singleItemCallsNum);
                consumerSingleItemBlockingEvent.Wait();
                consumerSingleItemDoneEvent.Set();
            };
            //System.Diagnostics.Debug.WriteLine("Test - subscribed");

            int writtingWrappedCallsNum = 0;
            wq.WrittingWrappedAndPreviousDataMightBeInconsistent += () =>
            {
                writtingWrappedCallsNum++;
            };

            //producer.Raise(p => p.NewWorkAvailable += null, 5);
            //Thread.Sleep(100);
            //singleItemCallsNum = 0;
            //noQueuedDataCallsNum = 0;

            int i = 0;
            //start blocking
            consumerBatchBlockingEvent.Reset();
            //Thread.Sleep(5);
            producer.Raise(p => p.NewWorkAvailable += null, i);

            //InterlockedEx.Read(ref singleItemCallsNum);
            Assert.IsTrue(consumerSingleItemDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            //the initial resume is now not causing wrapping
            Assert.AreEqual(0, writtingWrappedCallsNum);

            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);
            Assert.AreEqual(1, singleItemCallsNum);
            //it was called, but did not return. 
            //WARNING: We are just waiting for single item to be processed - but NoQueuedData might not yet been called
            // - thats why we have this extra event here
            Assert.IsTrue(consumerBatchCallStartedEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(1, noQueuedDataCallsNum);

            //now add other items - those will pile up
            //pile up 6 items (2 over capacity)
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            //give chance to consumer thread - this is to show it's blocked
            Thread.Sleep(10);

            //consumer hasn't progressed at all
            Assert.AreEqual(1, singleItemCallsNum);
            Assert.AreEqual(1, noQueuedDataCallsNum);
            //overflow not yet detected and messaged by consumer
            Assert.AreEqual(0, writtingWrappedCallsNum);

            //postpone the WrittingWrappedAndPreviousDataMightBeInconsistent
            // to be able to verfiy that no events were pumped before this event
            AutoResetEvent overflowDetectionPostponingEvent = new AutoResetEvent(false);
            AutoResetEvent overflowAnnounceStartedEvent = new AutoResetEvent(false);
            wq.WrittingWrappedAndPreviousDataMightBeInconsistent += () =>
            {
                overflowAnnounceStartedEvent.Set();
                overflowDetectionPostponingEvent.WaitOne();
            };

            //unlock consumer and wait for it to catch up
            consumerBatchBlockingEvent.Set();
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.IsTrue(overflowAnnounceStartedEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            //overflow detected and messaged by consumer
            Assert.AreEqual(1, writtingWrappedCallsNum);
            //no other data pumped (they were cleared) BEFORE
            Assert.AreEqual(1, singleItemCallsNum);
            Assert.AreEqual(1, noQueuedDataCallsNum);


            //unblock WrittingWrappedAndPreviousDataMightBeInconsistent to see other events
            overflowDetectionPostponingEvent.Set();
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(3, singleItemCallsNum);
            Assert.AreEqual(2, noQueuedDataCallsNum);

            //last to overflown items were captured
            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);
            Assert.AreEqual(i - 1, itemsCalled[itemsCalled.Count - 2]);


            //Send next data - control they are being received fine
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            Assert.AreEqual(4, singleItemCallsNum);
            Assert.AreEqual(3, noQueuedDataCallsNum);
            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);



            wq.CloseQueue();

            Assert.IsTrue(consumerCompletelyDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(1, noQueueClosedCallsNum);
        }


        [Test]
        public void SingleWorkerQueue_OverflowFewTimes()
        {
            ManualResetEventSlim consumerSingleItemBlockingEvent = new ManualResetEventSlim(true);
            ManualResetEventSlim consumerBatchBlockingEvent = new ManualResetEventSlim(true);
            var producer = new Mock<IWorkQueueProducer<int>>();

            ILogger testLogger = new TestLogger(Assert.Fail, LogLevel.Fatal);
            //ILogger testLogger = LogFactory.Instance.GetLogger("tstLgr3");
            SingleWorkerQueue<int> wq = new SingleWorkerQueue<int>(producer.Object,
                SingleWorkerQueue<int>.SingleWorkerQueueBehavior.BlockingIsNotAllowed_DataCanBeThrownAway, true, testLogger, 4, "testQueue");

            int noQueueClosedCallsNum = 0;
            AutoResetEvent consumerCompletelyDoneEvent = new AutoResetEvent(false);
            wq.QueueClosedWithNoMoreData += () =>
            {
                noQueueClosedCallsNum++;
                consumerCompletelyDoneEvent.Set();
            };

            AutoResetEvent consumerSingleItemDoneEvent = new AutoResetEvent(false);
            AutoResetEvent consumerBatchDoneEvent = new AutoResetEvent(false);
            AutoResetEvent consumerBatchCallStartedEvent = new AutoResetEvent(false);

            int noQueuedDataCallsNum = 0;
            wq.NoQueuedData += () =>
            {
                consumerBatchCallStartedEvent.Set();
                noQueuedDataCallsNum++;
                //Interlocked.Increment(ref noQueuedDataCallsNum);
                consumerBatchBlockingEvent.Wait();
                consumerBatchDoneEvent.Set();
            };

            List<int> itemsCalled = new List<int>();
            int singleItemCallsNum = 0;

            //simulate processing pressure + signal once processing is done
            wq.SingleItemAvailable += item =>
            {
                itemsCalled.Add(item);
                singleItemCallsNum++;
                //Interlocked.Increment(ref singleItemCallsNum);
                consumerSingleItemBlockingEvent.Wait();
                consumerSingleItemDoneEvent.Set();
            };

            int writtingWrappedCallsNum = 0;
            wq.WrittingWrappedAndPreviousDataMightBeInconsistent += () =>
            {
                writtingWrappedCallsNum++;
            };

            int i = 0;
            //start blocking
            consumerBatchBlockingEvent.Reset();
            producer.Raise(p => p.NewWorkAvailable += null, i);

            Assert.IsTrue(consumerSingleItemDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            //the initial resume is now not causing wrapping
            Assert.AreEqual(0, writtingWrappedCallsNum);

            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);
            Assert.AreEqual(1, singleItemCallsNum);
            //it was called, but did not return. 
            Assert.IsTrue(consumerBatchCallStartedEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(1, noQueuedDataCallsNum);

            //now add other items - those will pile up
            //pile up 10 items (twice + 2 over capacity)
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            //give chance to consumer thread - this is to show it's blocked
            Thread.Sleep(10);

            //consumer hasn't progressed at all
            Assert.AreEqual(1, singleItemCallsNum);
            Assert.AreEqual(1, noQueuedDataCallsNum);
            //overflow not yet detected and messaged by consumer
            Assert.AreEqual(0, writtingWrappedCallsNum);

            //postpone the WrittingWrappedAndPreviousDataMightBeInconsistent
            // to be able to verfiy that no events were pumped before this event
            AutoResetEvent overflowDetectionPostponingEvent = new AutoResetEvent(false);
            AutoResetEvent overflowAnnounceStartedEvent = new AutoResetEvent(false);
            wq.WrittingWrappedAndPreviousDataMightBeInconsistent += () =>
            {
                overflowAnnounceStartedEvent.Set();
                overflowDetectionPostponingEvent.WaitOne();
            };

            //unlock consumer and wait for it to catch up
            consumerBatchBlockingEvent.Set();
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.IsTrue(overflowAnnounceStartedEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            //overflow detected and messaged by consumer
            Assert.AreEqual(1, writtingWrappedCallsNum);
            //no other data pumped (they were cleared) BEFORE
            Assert.AreEqual(1, singleItemCallsNum);
            Assert.AreEqual(1, noQueuedDataCallsNum);


            //unblock WrittingWrappedAndPreviousDataMightBeInconsistent to see other events
            overflowDetectionPostponingEvent.Set();
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(3, singleItemCallsNum);
            Assert.AreEqual(2, noQueuedDataCallsNum);

            //last to overflown items were captured
            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);
            Assert.AreEqual(i - 1, itemsCalled[itemsCalled.Count - 2]);


            //Send next data - control they are being received fine
            producer.Raise(p => p.NewWorkAvailable += null, ++i);
            Assert.IsTrue(consumerBatchDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");

            Assert.AreEqual(4, singleItemCallsNum);
            Assert.AreEqual(3, noQueuedDataCallsNum);
            Assert.AreEqual(i, itemsCalled[itemsCalled.Count - 1]);


            wq.CloseQueue();

            Assert.IsTrue(consumerCompletelyDoneEvent.WaitOne(TimeSpan.FromSeconds(1)), "Test finished by timeout");
            Assert.AreEqual(1, noQueueClosedCallsNum);
        }

    }
}

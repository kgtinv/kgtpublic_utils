﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using NUnit.Framework;
using Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils;
using ArraySearchHelpers = Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils.SparseBitmap.ArraySearchHelpers;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class SparseBitmapTests
    {
        [Test]
        public void SparseBitmap_Construction()
        {
            List<bool> bools = Enumerable.Repeat(false, 500).ToList();

            SparseBitmap sb = new SparseBitmap(bools);
            var bitmapEnumerator = sb.GetEnumerator();
            Assert.IsFalse(bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(0));
            Assert.AreEqual(-1, bitmapEnumerator.CurrentIndex);
            try
            {
                var v = bitmapEnumerator.CurrentValue;
                Assert.Fail("Exception should have been thrown");
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("Cannot access CurrentValue"));
            }
        }

        [Test]
        public void SparseBitmap_ConstructionAndSome1Flags()
        {
            List<bool> bools = Enumerable.Repeat(false, 10000).ToList();

            bools[100] = true;

            for (int i = 1000; i < 2000; i++)
            {
                bools[i] = true;
            }

            for (int i = 2002; i < 3000; i += 2)
            {
                bools[i] = true;
            }

            //create nonuniform pattern
            for (int i = 2100; i < 3000; i += 63)
            {
                bools[i] = true;
            }

            bools[5000] = true;

            SparseBitmap sb = new SparseBitmap(bools);
            var bitmapEnumerator = sb.GetEnumerator();
            Assert.IsTrue(bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(0));
            Assert.AreEqual(100 / BitsHelper.BitsInUlong, bitmapEnumerator.CurrentIndex);

            //The hunderth bit
            var val = bitmapEnumerator.CurrentValue;
            Assert.AreEqual(100 % BitsHelper.BitsInUlong, val.GetFirstSetBitIdx());

            //The first few bits of thousandth bit
            Assert.IsTrue(bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(bitmapEnumerator.CurrentIndex + 1));
            Assert.AreEqual(1000 / BitsHelper.BitsInUlong, bitmapEnumerator.CurrentIndex);
            val = bitmapEnumerator.CurrentValue;
            bool[] expectedFlags = new bool[BitsHelper.BitsInUlong];
            for (int idx = 1000 % BitsHelper.BitsInUlong; idx < BitsHelper.BitsInUlong; idx++)
            {
                expectedFlags[idx] = true;
            }
            Assert.AreEqual(expectedFlags, val.GetSetBits());

            //all the words full of 1 between indexes 1000 and 2000
            int currentExpectedIndex = bitmapEnumerator.CurrentIndex;
            for (int idx = 1000 / BitsHelper.BitsInUlong + 1; idx < 2000 / BitsHelper.BitsInUlong; idx++)
            {
                currentExpectedIndex++;
                Assert.IsTrue(bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(bitmapEnumerator.CurrentIndex + 1));
                Assert.AreEqual(currentExpectedIndex, bitmapEnumerator.CurrentIndex);
                val = bitmapEnumerator.CurrentValue;
                Assert.IsTrue(val.IsAllOnes());
            }

            //The bits on border of 1000-2000 and 2002-3000
            Assert.IsTrue(bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(bitmapEnumerator.CurrentIndex + 1));
            Assert.AreEqual(currentExpectedIndex+1, bitmapEnumerator.CurrentIndex);
            val = bitmapEnumerator.CurrentValue;

            expectedFlags = new bool[BitsHelper.BitsInUlong];
            int reminderOneBitsCount = 2000 % BitsHelper.BitsInUlong;
            for (int idx = 0; idx < reminderOneBitsCount; idx++)
            {
                expectedFlags[idx] = true;
            }

            for (int idx = 2002 % BitsHelper.BitsInUlong; idx < BitsHelper.BitsInUlong; idx++)
            {
                if(idx % 2 == 0) 
                    expectedFlags[idx] = true;
            }

            Assert.AreEqual(expectedFlags, val.GetSetBits());

            bitmapEnumerator.Reset();

            bool isMoved = bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(0);
            for (int startIdx = 0; startIdx < bools.Count; startIdx += BitsHelper.BitsInUlong)
            {
                expectedFlags = new bool[BitsHelper.BitsInUlong];
                for (int flagIdx = 0; flagIdx < BitsHelper.BitsInUlong; flagIdx++)
                {
                    if (startIdx + flagIdx < bools.Count)
                    {
                        expectedFlags[flagIdx] = bools[startIdx + flagIdx];
                    }
                }
                if (expectedFlags.ToBitmapFragment(0).HasSomeOnes())
                {
                    Assert.IsTrue(isMoved);
                    Assert.AreEqual(startIdx / BitsHelper.BitsInUlong, bitmapEnumerator.CurrentIndex);
                    Assert.AreEqual(expectedFlags, bitmapEnumerator.CurrentValue.GetSetBits());
                    isMoved = bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(bitmapEnumerator.CurrentIndex + 1);
                }
            }

            Assert.False(isMoved);
        }

        [Test]
        public void SparseBitmap_AllEvenIndexes()
        {
            int rowsCount = 1500;
            bool[] bools = new bool[rowsCount];

            for (int idx = 0; idx < rowsCount; idx += 2)
            {
                bools[idx] = true;
            }

            SparseBitmap_ContentTestHelper(bools);
        }

        [Test]
        public void SparseBitmap_AllOddIndexes()
        {
            int rowsCount = 1500;
            bool[] bools = new bool[rowsCount];

            for (int idx = 1; idx < rowsCount; idx += 2)
            {
                bools[idx] = true;
            }

            SparseBitmap_ContentTestHelper(bools);
        }

        [Test]
        public void SparseBitmap_RandomIndexes()
        {
            int rowsCount = 1500;
            bool[] bools = new bool[rowsCount];

            Random rnd = new Random();
            for (int idx = 1; idx < rowsCount; idx += 2)
            {
                bools[idx] = rnd.Next(3) == 0;
            }

            SparseBitmap_ContentTestHelper(bools);
        }

        [Test]
        public void SparseBitmap_LastWordPopulated()
        {
            int rowsCount = 200;
            bool[] bools = new bool[rowsCount];

            bools[195] = true;
            bools[199] = true;

            SparseBitmap_ContentTestHelper(bools);
        }

        [Test]
        public void SparseBitmap_OnePriorLastWordPopulated()
        {
            int rowsCount = 200;
            bool[] bools = new bool[rowsCount];

            bools[100] = true;
            bools[109] = true;

            SparseBitmap_ContentTestHelper(bools);
        }

        [Test]
        public void SparseBitmap_NothingSet()
        {
            int rowsCount = 1500;
            bool[] bools = new bool[rowsCount];

            SparseBitmap_ContentTestHelper(bools);
        }

        [Test]
        public void SparseBitmap_AllSet()
        {
            int rowsCount = 1500;
            bool[] bools = Enumerable.Repeat(true, rowsCount).ToArray();

            SparseBitmap_ContentTestHelper(bools);
        }

        private void SparseBitmap_ContentTestHelper(bool[] bools)
        {
            int rowsCount = bools.Length;

            bool[] expectedFlags;
            SparseBitmap sb = new SparseBitmap(bools);
            var bitmapEnumerator = sb.GetEnumerator();

            bool isMoved = bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(0);
            for (int startIdx = 0; startIdx < bools.Length; startIdx += BitsHelper.BitsInUlong)
            {
                expectedFlags = new bool[BitsHelper.BitsInUlong];
                for (int flagIdx = 0; flagIdx < BitsHelper.BitsInUlong; flagIdx++)
                {
                    if (startIdx + flagIdx < bools.Length)
                    {
                        expectedFlags[flagIdx] = bools[startIdx + flagIdx];
                    }
                }
                if (expectedFlags.ToBitmapFragment(0).HasSomeOnes())
                {
                    Assert.IsTrue(isMoved);
                    Assert.AreEqual(startIdx / BitsHelper.BitsInUlong, bitmapEnumerator.CurrentIndex);
                    Assert.AreEqual(expectedFlags, bitmapEnumerator.CurrentValue.GetSetBits());
                    isMoved = bitmapEnumerator.MoveToNextNonzeroDataIndexGreaterOrEqual(bitmapEnumerator.CurrentIndex + 1);
                }
            }

            Assert.False(isMoved);
        }

        [Test]
        public void GetClosestLowerOrEqualArrayIndex_Test()
        {
            Assert.AreEqual(1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] {5, 7, 9}, 8, 0, 0, 3));
            Assert.AreEqual(1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 8, 0, 1, 2));
            Assert.AreEqual(1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 8, 0, 1, 3));
            Assert.AreEqual(2, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 10, 0, 0, 3));
            Assert.AreEqual(2, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 9, 0, 0, 3));
            Assert.AreEqual(-1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 4, 0, 0, 3));
            Assert.AreEqual(0, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 5, 0, 0, 5));
            Assert.AreEqual(0, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 6, 0, 0, 3));
            Assert.AreEqual(1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 7, 0, 0, 3));
            Assert.AreEqual(1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9 }, 8, 0, 0, 3));

            Assert.AreEqual(-1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 4, 0, 0, 4));
            Assert.AreEqual(0, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 5, 0, 0, 4));
            Assert.AreEqual(0, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 6, 0, 0, 4));
            Assert.AreEqual(1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 7, 0, 0, 4));
            Assert.AreEqual(1, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 8, 0, 0, 4));
            Assert.AreEqual(2, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 9, 0, 0, 4));
            Assert.AreEqual(2, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 10, 0, 0, 4));
            Assert.AreEqual(3, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 11, 0, 0, 4));
            Assert.AreEqual(3, ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(new[] { 5, 7, 9, 11 }, 12, 0, 0, 4));
        }

        [Test]
        public void GetInclusiveOrNextInterval_Test()
        {
            var intervals1 = new[] {new Interval(1, 2), new Interval(5, 8), new Interval(9, 10)};
            Assert.AreEqual(1, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 2, 0, 5));
            Assert.AreEqual(1, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 2, 1, 5));
            Assert.AreEqual(1, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 2, 0, 2));

            Assert.AreEqual(0, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 0, 0, 3));
            Assert.AreEqual(0, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 1, 0, 3));

            Assert.AreEqual(1, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 4, 0, 3));
            Assert.AreEqual(1, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 5, 0, 3));
            Assert.AreEqual(1, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 7, 0, 3));

            Assert.AreEqual(2, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 8, 0, 3));
            Assert.AreEqual(2, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 9, 0, 3));
            Assert.AreEqual(-1, ArraySearchHelpers.GetInclusiveOrNextInterval(intervals1, 10, 0, 3));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class AtomicSizeTests
    {
        [Test]
        public void IsSameSignAndCloserToZeroThanTests_NothingCloserToZeroThanZero()
        {
            Assert.IsFalse(AtomicSize.ZERO.IsSameSignAndCloserToZeroThan(AtomicSize.ZERO));
            Assert.IsFalse(AtomicSize.FromDecimal(0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.ZERO));
            Assert.IsFalse(AtomicSize.FromDecimal(-0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.ZERO));
        }

        [Test]
        public void IsSameSignAndCloserToZeroThanTests_ZeroCloserToZeroThanNonzero()
        {
            Assert.IsTrue(AtomicSize.ZERO.IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.001m)));
            Assert.IsTrue(AtomicSize.ZERO.IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.001m)));
        }

        [Test]
        public void IsSameSignAndCloserToZeroThanTests_SameSignComparisons()
        {
            //same size
            Assert.IsFalse(AtomicSize.FromDecimal(0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(-0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.001m)));

            //further
            Assert.IsFalse(AtomicSize.FromDecimal(0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.0001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(-0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.0001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(1000m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(-1000m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.001m)));

            //closer
            Assert.IsTrue(AtomicSize.FromDecimal(0.0001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.001m)));
            Assert.IsTrue(AtomicSize.FromDecimal(-0.0001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.001m)));
            Assert.IsTrue(AtomicSize.FromDecimal(0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(1000m)));
            Assert.IsTrue(AtomicSize.FromDecimal(-0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-1000m)));
        }

        [Test]
        public void IsSameSignAndCloserToZeroThanTests_DiffernetSignComparisons_AllFalse()
        {
            //same size
            Assert.IsFalse(AtomicSize.FromDecimal(-0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.001m)));

            //further
            Assert.IsFalse(AtomicSize.FromDecimal(-0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.0001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.0001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(-1000m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(1000m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.001m)));

            //closer
            Assert.IsFalse(AtomicSize.FromDecimal(-0.0001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(0.001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(0.0001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-0.001m)));
            Assert.IsFalse(AtomicSize.FromDecimal(-0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(1000m)));
            Assert.IsFalse(AtomicSize.FromDecimal(0.001m).IsSameSignAndCloserToZeroThan(AtomicSize.FromDecimal(-1000m)));
        }

        [Test]
        public void OperatorStarDecimal()
        {
            Assert.AreEqual(AtomicSize.FromDecimal(0.5m * 0.5m), AtomicSize.FromDecimal(0.5m)*0.5m);
            Assert.AreEqual(AtomicSize.FromDecimal(-0.5m * 0.5m), AtomicSize.FromDecimal(-0.5m) * 0.5m);
            Assert.AreEqual(AtomicSize.FromDecimal(0.5m * -0.5m), AtomicSize.FromDecimal(0.5m) * -0.5m);

            Assert.AreEqual(AtomicSize.FromDecimal(5m * 5m), AtomicSize.FromDecimal(5m) * 5m);
            Assert.AreEqual(AtomicSize.FromDecimal(-5m * 5m), AtomicSize.FromDecimal(-5m) * 5m);
            Assert.AreEqual(AtomicSize.FromDecimal(5m * -5m), AtomicSize.FromDecimal(5m) * -5m);
        }

        [Test]
        public void OperatorStarAtomicSize()
        {
            Assert.AreEqual(AtomicSize.FromDecimal(0.5m * 0.5m), AtomicSize.FromDecimal(0.5m) * AtomicSize.FromDecimal(0.5m));
            Assert.AreEqual(AtomicSize.FromDecimal(-0.5m * 0.5m), AtomicSize.FromDecimal(-0.5m) * AtomicSize.FromDecimal(0.5m));
            Assert.AreEqual(AtomicSize.FromDecimal(0.5m * -0.5m), AtomicSize.FromDecimal(0.5m) * AtomicSize.FromDecimal(-0.5m));

            Assert.AreEqual(AtomicSize.FromDecimal(5m * 5m), AtomicSize.FromDecimal(5m) * AtomicSize.FromDecimal(5m));
            Assert.AreEqual(AtomicSize.FromDecimal(-5m * 5m), AtomicSize.FromDecimal(-5m) * AtomicSize.FromDecimal(5m));
            Assert.AreEqual(AtomicSize.FromDecimal(5m * -5m), AtomicSize.FromDecimal(5m) * AtomicSize.FromDecimal(-5m));
        }
    }
}

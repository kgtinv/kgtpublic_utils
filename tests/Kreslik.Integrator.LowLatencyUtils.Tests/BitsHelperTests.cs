﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils;
using NUnit.Framework;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class BitsHelperTests
    {
        [Test]
        public void ToBitmapFromIList_Test()
        {
            List<ulong> words = BitsHelper.ToBitmap(new[] {false, true, false, true, false, false, true, true}).ToList();
            Assert.AreEqual(1, words.Count);
            Assert.IsTrue(BitsHelper.HasSomeOnes(words[0]));
            Assert.IsFalse(BitsHelper.IsAllOnes(words[0]));
            Assert.IsFalse(BitsHelper.IsAllZeros(words[0]));
            Assert.AreEqual(1, words[0].GetFirstSetBitIdx());
            Assert.AreEqual(7, words[0].GetLastSetBitIdx());

            words = BitsHelper.ToBitmap(new[] { false, false, false, true, false, false, false, false }).ToList();
            Assert.AreEqual(3, words[0].GetFirstSetBitIdx());
            Assert.AreEqual(3, words[0].GetLastSetBitIdx());
        }

        [Test]
        public void ToBitmapFromEnumerable_Test()
        {
            List<ulong> words = BitsHelper.ToBitmap((IEnumerable<bool>) new[] { false, true, false, true, false, false, true, true }).ToList();
            Assert.AreEqual(1, words.Count);
            Assert.IsTrue(BitsHelper.HasSomeOnes(words[0]));
            Assert.IsFalse(BitsHelper.IsAllOnes(words[0]));
            Assert.IsFalse(BitsHelper.IsAllZeros(words[0]));
            Assert.AreEqual(1, words[0].GetFirstSetBitIdx());
        }

        [Test]
        public void FailingSearch_Test()
        {
            ulong intersectionWord = 9223372036854775808;
            Assert.AreEqual(63, intersectionWord.GetFirstSetBitIdx());
            Assert.AreEqual(63, intersectionWord.GetFirstSetBitIdx(63));
            Assert.AreEqual(-1, intersectionWord.GetFirstSetBitIdx(64));
            Assert.AreEqual(63, intersectionWord.GetLastSetBitIdx());
        }

    }
}

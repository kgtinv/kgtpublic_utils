﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Kreslik.Integrator.LowLatencyUtils.Tests
{
    [TestFixture]
    public class MeanTrackerUnsynchronizedTests 
    {
        [Test]
        public void MeanTrackerUnsynchronized_ctorTest()
        {
            Assert.Throws(typeof (Exception), () => new MeanTrackerUnsynchronized(0));
            Assert.Throws(typeof(Exception), () => new MeanTrackerUnsynchronized(1));
            Assert.Throws(typeof(Exception), () => new MeanTrackerUnsynchronized(3));
            Assert.Throws(typeof(Exception), () => new MeanTrackerUnsynchronized(5));
            Assert.Throws(typeof(Exception), () => new MeanTrackerUnsynchronized(12));

            var a = new MeanTrackerUnsynchronized(2);
            var b = new MeanTrackerUnsynchronized(8);
            var c = new MeanTrackerUnsynchronized(64);

            Assert.Throws(typeof(Exception), () => new MeanTrackerTimeWindowedUnsynchronized(0, TimeSpan.Zero));
            Assert.Throws(typeof(Exception), () => new MeanTrackerTimeWindowedUnsynchronized(1, TimeSpan.Zero));
            Assert.Throws(typeof(Exception), () => new MeanTrackerTimeWindowedUnsynchronized(3, TimeSpan.Zero));
            Assert.Throws(typeof(Exception), () => new MeanTrackerTimeWindowedUnsynchronized(5, TimeSpan.Zero));
            Assert.Throws(typeof(Exception), () => new MeanTrackerTimeWindowedUnsynchronized(12, TimeSpan.Zero));

            var a1 = new MeanTrackerTimeWindowedUnsynchronized(2, TimeSpan.Zero);
            var b1 = new MeanTrackerTimeWindowedUnsynchronized(8, TimeSpan.Zero);
            var c1 = new MeanTrackerTimeWindowedUnsynchronized(64, TimeSpan.Zero);
        }

        [Test]
        public void MeanTrackerUnsynchronized_TwoItemsMean()
        {
            var meanTracker = new MeanTrackerUnsynchronized(2);
            Assert.AreEqual(0m, meanTracker.Mean.ToDecimal());
            Assert.IsFalse(meanTracker.IsFull);

            meanTracker.InsertNext(new AtomicDecimal(3));
            Assert.AreEqual(1.5m, meanTracker.Mean.ToDecimal());
            Assert.IsFalse(meanTracker.IsFull);

            meanTracker.InsertNext(new AtomicDecimal(1));
            Assert.AreEqual(2m, meanTracker.Mean.ToDecimal());
            Assert.IsTrue(meanTracker.IsFull);

            meanTracker.InsertNext(new AtomicDecimal(2));
            Assert.AreEqual(1.5m, meanTracker.Mean.ToDecimal());
            Assert.IsTrue(meanTracker.IsFull);
        }

        [Test]
        public void MeanTrackerUnsynchronized_64ItemsMean()
        {
            var meanTracker = new MeanTrackerUnsynchronized(64);
            Assert.AreEqual(0m, meanTracker.Mean.ToDecimal());
            Assert.IsFalse(meanTracker.IsFull);

            for (int i = 0; i < 64; i++)
            {
                meanTracker.InsertNext(0);
            }

            Assert.IsTrue(meanTracker.IsFull);

            for (int i = 1; i <= 64; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(i));
                Assert.AreEqual(i*(1 + i) / 2m / 64m, meanTracker.Mean.ToDecimal());
                Assert.IsTrue(meanTracker.IsFull);
            }

        }

        [Test]
        public void MeanTrackerTimeWindowedUnsynchronized_TwoItemsMeanAndNoTimeConstraint()
        {
            var meanTracker = new MeanTrackerTimeWindowedUnsynchronized(2, TimeSpan.FromHours(1));
            Assert.AreEqual(0m, meanTracker.Mean.ToDecimal());

            meanTracker.ResetCachedMean();
            meanTracker.InsertNext(new AtomicDecimal(3), DateTime.UtcNow);
            Assert.AreEqual(3m, meanTracker.Mean.ToDecimal());

            meanTracker.ResetCachedMean();
            meanTracker.InsertNext(new AtomicDecimal(1), DateTime.UtcNow);
            Assert.AreEqual(2m, meanTracker.Mean.ToDecimal());

            meanTracker.ResetCachedMean();
            meanTracker.InsertNext(new AtomicDecimal(2), DateTime.UtcNow);
            Assert.AreEqual(1.5m, meanTracker.Mean.ToDecimal());
        }

        [Test]
        public void MeanTrackerTimeWindowedUnsynchronized_TwoItemsMeanAndSomeTimeConstraint()
        {
            var meanTracker = new MeanTrackerTimeWindowedUnsynchronized(2, TimeSpan.FromMinutes(50));
            Assert.AreEqual(0m, meanTracker.Mean.ToDecimal());

            meanTracker.ResetCachedMean();
            meanTracker.InsertNext(new AtomicDecimal(3), DateTime.UtcNow);
            Assert.AreEqual(3m, meanTracker.Mean.ToDecimal());
            //no items - last known value
            Assert.AreEqual(3m, meanTracker.GetMean(DateTime.UtcNow.AddHours(2)).ToDecimal());

            //no need to reset
            meanTracker.InsertNext(new AtomicDecimal(1), DateTime.UtcNow.AddHours(3));
            Assert.AreEqual(1m, meanTracker.GetMean(DateTime.UtcNow.AddHours(3)).ToDecimal());

            meanTracker.InsertNext(new AtomicDecimal(2), DateTime.UtcNow.AddHours(4));
            Assert.AreEqual(2m, meanTracker.GetMean(DateTime.UtcNow.AddHours(4)).ToDecimal());
        }


        [Test]
        public void MeanTrackerTimeWindowedUnsynchronized_64ItemsMeanNoTimeConstraint()
        {
            var meanTracker = new MeanTrackerTimeWindowedUnsynchronized(64, TimeSpan.FromMinutes(59));


            for (int i = 1; i <= 64; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(i), DateTime.UtcNow);
                Assert.AreEqual(i * (1 + i) / 2m / i, meanTracker.Mean.ToDecimal());
                if(i != 64)
                    meanTracker.ResetCachedMean();
            }

            for (int i = 0; i < 10; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(1000), DateTime.UtcNow.AddHours(2));
                Assert.AreEqual(64 * (1 + 64) / 2m / 64, meanTracker.Mean.ToDecimal());
            }

            //No value - last known mean
            Assert.AreEqual(64 * (1 + 64) / 2m / 64, meanTracker.GetMean(DateTime.UtcNow.AddDays(1)).ToDecimal());

            meanTracker.ResetCachedMean();

            Assert.AreEqual(1000m, meanTracker.GetMean(DateTime.UtcNow.AddHours(2)).ToDecimal());

            for (int i = 0; i < 9; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(1), DateTime.UtcNow.AddHours(3));
            }

            Assert.AreEqual(1m, meanTracker.GetMean(DateTime.UtcNow.AddHours(3)).ToDecimal());


            for (int i = 0; i < 60; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(123), DateTime.UtcNow.AddHours(4));
            }

            Assert.AreEqual(123m, meanTracker.GetMean(DateTime.UtcNow.AddHours(4)).ToDecimal());


            for (int i = 0; i < 60; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(321), DateTime.UtcNow.AddHours(5));
            }

            Assert.AreEqual(321m, meanTracker.GetMean(DateTime.UtcNow.AddHours(5)).ToDecimal());
        }


        [Test]
        public void MeanTrackerTimeWindowedUnsynchronized_NullifyingItemTest()
        {
            var meanTracker = new MeanTrackerTimeWindowedUnsynchronized(64, TimeSpan.FromMinutes(59));


            for (int i = 1; i <= 64; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(i), DateTime.UtcNow);
                Assert.AreEqual(i * (1 + i) / 2m / i, meanTracker.Mean.ToDecimal());
                if (i != 64)
                    meanTracker.ResetCachedMean();
            }

            meanTracker.MarkNullItem(DateTime.UtcNow);
            Assert.AreEqual(64 * (1 + 64) / 2m / 64, meanTracker.Mean.ToDecimal());
            Assert.AreEqual(0, meanTracker.GetMean(DateTime.UtcNow.AddHours(1)).ToDecimal());
            Assert.AreEqual(0, meanTracker.GetMean(DateTime.UtcNow.AddHours(2)).ToDecimal());


            for (int i = 0; i < 10; i++)
            {
                meanTracker.InsertNext(new AtomicDecimal(1000), DateTime.UtcNow.AddHours(2));
                //we recalc - 2 to 64 and 1000
                Assert.AreEqual((63 * (2 + 64) / 2m + 1000)/ 64, meanTracker.Mean.ToDecimal());
            }

            //No value - last known mean
            Assert.AreEqual((63 * (2 + 64) / 2m + 1000) / 64, meanTracker.GetMean(DateTime.UtcNow.AddDays(1)).ToDecimal());
        }
    }
}

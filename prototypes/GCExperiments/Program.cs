﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.NMbgDevIO;

namespace GCExperiments
{
    class Program
    {


        //benchmarks for monitoring memory garbage created
        static void Main(string[] args)
        {
			BuffersInlining bi = new BuffersInlining();
            bi.PerformTests();

            long bytesBefore = GC.GetTotalMemory(false);
		
            //TimestampsBenchmark();
            //WriteToFileBenchmark();
            //needs to be started in 2 separat processes - as client and as a server
            //NamedPipeWcfBenchmark();
            //NamedPipeBanchmark(false);
            //NamedPipeBanchmark(true);


            //StringTests();


            //RunContinuousPhaseWithStatistics(CachedExceptionTest);

            //ObjectPoolTest();

            //Console.ReadKey();
        }

        private static void RunContinuousPhaseWithStatistics(Action action)
        {
            long totalMemoryBefore = GC.GetTotalMemory(false);
            int gen0CollectionsBefore = GC.CollectionCount(0);

            action();

            long totalMemoryAfter = GC.GetTotalMemory(false);
            int gen0CollectionsAfter = GC.CollectionCount(0);

            Console.WriteLine("Memory consumed by test: {0}, GC0 triggered by test: {1}",
                              totalMemoryAfter - totalMemoryBefore, gen0CollectionsAfter - gen0CollectionsBefore);
        }

        private static void ObjectPoolTest()
        {
            GCFreeObjectPool<object> pool = new GCFreeObjectPool<object>(() => new object(), o => { }, 10);

            RunContinuousPhaseWithStatistics(() =>
                {

                    for (int i = 0; i < 6; i++)
                    {
                        object x =null;
                        for (int j = 0; j < i + 4; j++)
                        {
                            x = pool.Fetch();
                        }

                        for (int j = 0; j < i + 4; j++)
                        {
                            pool.Return(x);
                        }
                    }
                });

            Console.ReadKey();
        }

        //Keep polling timestamp from Meinberg
        private static void TimestampsBenchmark()
        {
            NMbgTimerNoGC timer;

            try
            {
                timer = NMbgTimerNoGC.GetAsynchronousTimer();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when creating timer, will use the basic one");
                Console.WriteLine(e);
                timer = NMbgTimerNoGC.GetBasicTimerWithoutMeibergClock();
            }


            NMbgDateTime dateTime = new NMbgDateTime();
            bool end = false;

            Task t = Task.Factory.StartNew(() =>
                {
                    while (!end)
                    {
                        timer.GetUtcNow(ref dateTime);
                    }
                }
                );

            Console.ReadKey();
            end = true;

            t.Wait();

            //make sure that optimizer cannot optimize the datetime variable out - so using it
            if (dateTime.Ticks < 1)
            {
                Console.WriteLine(dateTime);
            }

            return;
        }

        //Generate 10 ~1GB files, to see if memory garbage is created
        private static void WriteToFileBenchmark()
        {
            byte[] bytes =
                Encoding.Default.GetBytes(
                    "dffdo   pdfldfdfk kodf opftrtt r rttrtrtrtt didf opifopdfiodfp idf opiof ifl ;l;l ;l ;l;gkfljklj tjkjtjtijrtijrtijritjo pif opfiopdf iopf i dfopio pif opif of io iopiopi opi op iop i fopdfi opi\r\n");

            string[] files = new string[] { "File1", "File2", "File3", "File4", "File5", "File6", "File7", "File8", "File9", "File10" };

            for (int j = 0; j < 10; j++)
            {

                Stream _stream = File.Open(
                    files[j],
                    FileMode.Append,
                    FileAccess.Write,
                    FileShare.Read);

                for (int i = 0; i < 10000000; i++)
                {
                    _stream.Write(bytes, 0, bytes.Length);
                }

                _stream.Flush();
                _stream.Close();
            }
        }

        #region WCF Named Pipes test

        [ServiceContract]
        public interface IService
        {
            [OperationContract()]
            void ReceiveData(char[] data);
        }

        [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
        public class ServiceClass : IService
        {
            public void ReceiveData(char[] data)
            {

            }
        }

        public class ServiceProxy : System.ServiceModel.ClientBase<IService>, IService
        {
            public ServiceProxy(Binding binding, EndpointAddress remoteAddress)
                : base(binding, remoteAddress)
            {
            }

            public void ReceiveData(char[] data)
            {
                Channel.ReceiveData(data);
            }
        }

        public static void WcfNamedPipeServerTest()
        {
            Uri listenAddress = new Uri("net.pipe://localhost/foobar");
            Binding binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);

            ServiceHost serviceHost = new ServiceHost(new ServiceClass(), listenAddress);
            serviceHost.AddServiceEndpoint(typeof(IService), binding, "");

            serviceHost.Open();

            Console.ReadKey();

            serviceHost.Close();
        }

        public static void WcfNamedPipeClientTest(char[] charsToSend)
        {
            ServiceProxy serviceProxy = new ServiceProxy(new NetNamedPipeBinding(NetNamedPipeSecurityMode.None),
                                                         new EndpointAddress(new Uri("net.pipe://localhost/foobar")));


            bool end = false;

            Task t = Task.Factory.StartNew(() =>
            {
                while (!end)
                {
                    serviceProxy.ReceiveData(charsToSend);
                }
            }
                );

            Console.ReadKey();
            end = true;

            t.Wait();
        }

        private static void NamedPipeWcfBenchmark()
        {
            Console.WriteLine("s for server, c for client");
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.S:
                    WcfNamedPipeServerTest();
                    break;
                case ConsoleKey.C:
                    char[] chars = "dffdo   pdfldfdfk kodf opftrtt r rttrtrtrtt didf opifopdfiodfp idf opiof ifl ;l;l ;l ;l;gkfljklj tjkjtjtijrtijrtijritjo pif opfiopdf iopf i dfopio pif opif of io iopiopi opi op iop i fopdfi opi\r\n".ToCharArray();
                    WcfNamedPipeClientTest(chars);
                    break;
                default:
                    Console.WriteLine("Wrong choice");
                    return;
            }

            Console.WriteLine("Finished test");
        }

        #endregion /WCF Named Pipes test

        public static void NamedPipeServerTest()
        {
            char[] buffer = new char[1024];

            var server = new NamedPipeServerStream("PipesOfPiece");
            server.WaitForConnection();
            StreamReader reader = new StreamReader(server);

            bool end = false;

            Task t = Task.Factory.StartNew(() =>
            {
                while (!end)
                {
                    int read = reader.Read(buffer, 0, 1024);
                }
            }
                );

            Console.ReadKey();
            end = true;

            t.Wait();
        }

        public static void NamedPipeClientTest(char[] charsToSend)
        {
            var client = new NamedPipeClientStream("PipesOfPiece");
            client.Connect();
            StreamWriter writer = new StreamWriter(client);


            bool end = false;

            Task t = Task.Factory.StartNew(() =>
            {
                while (!end)
                {
                    writer.Write(charsToSend);
                }
            }
                );

            Console.ReadKey();
            end = true;

            t.Wait();
        }

        public static void NamedPipeServerThroughputTest()
        {
            char[] buffer = new char[1024];

            var server = new NamedPipeServerStream("PipesOfPiece");
            server.WaitForConnection();
            StreamReader reader = new StreamReader(server);

            Stopwatch sw = new Stopwatch();

            //warmup receiving
            int read = reader.Read(buffer, 0, 1024);


            sw.Start();
            for (int i = 0; i < 1000000; i++)
            {
                read = reader.Read(buffer, 0, 1024);
            }
            sw.Stop();

            Console.WriteLine("1GB received, duration: {0}", sw.Elapsed);
            Console.ReadKey();
        }

        public static void NamedPipeClientThroughputTest(char[] charsToSend)
        {
            var client = new NamedPipeClientStream("PipesOfPiece");
            client.Connect();
            StreamWriter writer = new StreamWriter(client);

            Stopwatch sw = new Stopwatch();

            //warmup sending
            writer.Write(charsToSend);

            sw.Start();
            for (int i = 0; i < 1000000; i++)
            {
                //charsToSend[i] = (char) i;
                writer.Write(charsToSend);
                writer.Flush();
            }
            sw.Stop();

            Console.WriteLine("1GB sent, duration: {0}", sw.Elapsed);
            Console.ReadKey();
        }

        public static void NamedPipeServerThroughputTest_MessageSizes()
        {
            char[] buffer = new char[1024];

            var server = new NamedPipeServerStream("PipesOfPiece");
            server.WaitForConnection();
            StreamReader reader = new StreamReader(server);

            Stopwatch sw = new Stopwatch();
            int bytesToReceive = 1024 * 1000000;
            char[] numToChars = new char[2];
            int currentBytes;

            //warmup receiving
            int read = reader.Read(buffer, 0, 1024);
            //Console.WriteLine(read);

            sw.Start();
            do
            {
                reader.Read(numToChars, 0, 2);
                currentBytes = ToInt32(numToChars);
                read = reader.Read(buffer, 0, currentBytes);

                bytesToReceive -= (currentBytes + 2);
            } while (bytesToReceive > 0);
            sw.Stop();

            Console.WriteLine("1GB received, duration: {0}", sw.Elapsed);
            Console.ReadKey();
        }

        [System.Security.SecuritySafeCritical]
        public static unsafe char[] GetChars(int value, char[] chars)
        {
            //TODO: if needed to use accross machines then
            //  this should also use BitConverter.IsLittleEndian to detect little/big endian
            //  and order bytes appropriately
            
            fixed (char* numPtr = chars)
                *(int*)numPtr = value;
            return chars;
        }

        [System.Security.SecuritySafeCritical]
        public static unsafe int ToInt32(char[] value)
        {
            //TODO: if needed to use accross machines then
            //  this should also use BitConverter.IsLittleEndian to detect little/big endian
            //  and order bytes appropriately

            fixed (char* numPtr = value)
                return *(int*)numPtr;
        }

        public static void NamedPipeClientThroughputTest_MessageSizes(char[] charsToSend)
        {
            var client = new NamedPipeClientStream("PipesOfPiece");
            client.Connect();
            StreamWriter writer = new StreamWriter(client);

            Stopwatch sw = new Stopwatch();
            Random rand = new Random();
            int bytesToSend = 1024*1000000;
            char[] numToChars = new char[2];
            int currentBytes;

            //warmup sending
            writer.Write(charsToSend);
            writer.Flush();

            sw.Start();
            do
            {
                currentBytes = rand.Next(512, 1025);
                writer.Write(GetChars(currentBytes, numToChars));
                writer.Flush();
                //charsToSend[i] = (char) i;
                writer.Write(charsToSend, 0, currentBytes);
                writer.Flush();

                bytesToSend -= (currentBytes + 2);
            } while (bytesToSend > 0);
            sw.Stop();

            Console.WriteLine("1GB sent, duration: {0}", sw.Elapsed);
            Console.ReadKey();
        }

        //public static void NamedPipeServerTest2()
        //{
        //    char[] buffer = new char[1024];

        //    var server = new NamedPipeServerStream("PipesOfPiece");
        //    server.WaitForConnection();
        //    StreamReader reader = new StreamReader(server);

        //    Stopwatch sw = new Stopwatch();

        //    //warmup receiving
        //    int read = reader.Read();


        //    sw.Start();
        //    for (int i = 0; i < 1000000; i++)
        //    {
        //        read = reader.Read();
        //    }
        //    sw.Stop();

        //    Console.WriteLine("1GB received, duration: {0}", sw.Elapsed);
        //    Console.ReadKey();
        //}

        //public static void NamedPipeClientTest2(char[] charsToSend)
        //{
        //    var client = new NamedPipeClientStream("PipesOfPiece");
        //    client.Connect();
        //    StreamWriter writer = new StreamWriter(client);

        //    Stopwatch sw = new Stopwatch();

        //    //warmup sending
        //    writer.Write(5);

        //    sw.Start();
        //    for (int i = 0; i < 1000000; i++)
        //    {
        //        //charsToSend[i] = (char) i;
        //        writer.Write(i);
        //        writer.Flush();
        //    }
        //    sw.Stop();

        //    Console.WriteLine("1GB sent, duration: {0}", sw.Elapsed);
        //    Console.ReadKey();
        //}


        private static void NamedPipeBanchmark(bool measureThroughput)
        {
            Console.WriteLine("s for server, c for client");
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.S:
                    if (measureThroughput) NamedPipeServerThroughputTest_MessageSizes();
                    else NamedPipeServerTest();
                    break;
                case ConsoleKey.C:
                    char[] chars = Enumerable.Repeat('c', 1024).ToArray();
                    "dffdo   pdfldfdfk kodf opftrtt r rttrtrtrtt didf opifopdfiodfp idf opiof ifl ;l;l ;l ;l;gkfljklj tjkjtjtijrtijrtijritjo pif opfiopdf iopf i dfopio pif opif of io iopiopi opi op iop i fopdfi opi\r\n".ToCharArray().CopyTo(chars, 0);
                    if (measureThroughput) NamedPipeClientThroughputTest_MessageSizes(chars);
                    else NamedPipeClientTest(chars);
                    break;
                default:
                    Console.WriteLine("Wrong choice");
                    return;
            }

            Console.WriteLine("Finished test");
        }

        //example of in place mutating
        public static unsafe void ToUpper(string str)
        {
            fixed (char* pfixed = str)
                for (char* p = pfixed; *p != 0; p++)
                    *p = char.ToUpper(*p);
        }

        //source: http://www.codeproject.com/Articles/3377/Strings-UNDOCUMENTED
        public static unsafe int GetLength(string s)
        {
            // This function is redundant, because it accomplishes 
            // the same role as s.Length
            // but it does demonstrate some of the precautions 
            // that must be taken to 
            // recover the length variable
            fixed (char* p = s)
            {
                int* plength = (int*)p - 1;
                int length = *plength & 0x3fffffff;
                Debug.Assert(length == s.Length);
                return length;
            }
        }

        public static unsafe void OverrideStringContent(string s, char[] newContent)
        {
            //Here we should somehow controll that newContent.Lenght < capacity
            //Probably enforce by accepting just special wrapping struct

            fixed (char* strPtr = s)
            {
                fixed (char* charPtr = newContent)
                {
                    //char* srcCharPtr = strPtr;
                    for (char* dstCharPtr = charPtr, srcCharPtr = strPtr; *dstCharPtr != 0; dstCharPtr++, srcCharPtr++)
                        *srcCharPtr = *dstCharPtr;
                }


                int* strPtrToInt = (int*)strPtr;
                strPtrToInt[-1] = newContent.Length;

                //and also the terminating null
                strPtr[newContent.Length] = '\0';

                //THIS DOESN'T WORK :-| - cannot make string to point elsewhere, as the char array is colocated with the string itself
                //char** stringCharPtr = &strPtr;

                //fixed (char* charPtr = charsToCopy)
                //{
                //    *stringCharPtr = charPtr;
                //}


                //SAFER version of the code - make sure we really alter the content of internal char buffer
                // it however DOESN'T WORK either :-| as .NET doesn't allow taking pointers to singl char

                //char firstChar = (char)typeof(string).GetField("m_firstChar",
                //            System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
                //                                       .GetValue(s);

                //fixed (char* charPtr = &firstChar)
                //{
                //    *(charPtr + 2) = 'X';
                //}
            }
        }

        public static unsafe bool TryOverrideStringContentV2(string s, char[] newContent)
        {
            if (s.Length < newContent.Length)
                return false;

            fixed (char* strPtr = s)
            {
                fixed (char* charPtr = newContent)
                {
                    //char* srcCharPtr = strPtr;
                    for (char* dstCharPtr = charPtr, srcCharPtr = strPtr; *dstCharPtr != 0; dstCharPtr++, srcCharPtr++)
                        *srcCharPtr = *dstCharPtr;
                }

                //DO NOT change length - otherwise runtime (GC especially) can go crazy 
                //int* strPtrToInt = (int*)strPtr;
                //strPtrToInt[-1] = newContent.Length;

                //instead of that, nullify the rest
                for (int idx = newContent.Length; idx < s.Length; idx++)
                    strPtr[idx] = '\0';
            }

            return true;
        }

        public static unsafe bool TryOverrideStringContentV2(string s, byte[] newContent, int startIdx, int endIdx)
        {
            if (s.Length < endIdx - startIdx)
                return false;

            fixed (char* strPtr = s)
            {
                //DO NOT change length - otherwise runtime (GC especially) can go crazy 
                //int* strPtrToInt = (int*)strPtr;
                //strPtrToInt[-1] = newContent.Length;

                //instead of that, nullify the rest
                for (int idx = endIdx - startIdx; idx < s.Length; idx++)
                    strPtr[idx] = '\0';

                fixed (byte* bytePtr = newContent)
                {
                    //char* srcCharPtr = strPtr;
                    byte* dstCharPtr = bytePtr + startIdx;
                    char* srcCharPtr = strPtr;
                    for (; startIdx <= endIdx; startIdx++, dstCharPtr++, srcCharPtr++)
                        *srcCharPtr = (char)*dstCharPtr;
                }
            }

            return true;
        }

        public static void StringMutationTest1_Dangerous()
        {
            string test1 = "dfdfRTcsd";
            string test2 = "HHHHHHHHH";
            char[] chars = "jsdkfTRRYYHHTJTYUtulkolkoiko  ro roptoprotpeoptoept45049604909 klf kl54p][;//./;''[]['fg]  pltp ppRR".ToCharArray();
            //This works but it is VERY VERY dangerous!!!
            OverrideStringContent(test1, chars);
            //This will cause AccessViolationException as test2 memory is corrupted
            Console.WriteLine(test2);
        }

        public static void StringMutationTest2_Dangerous()
        {
            string testA = "00000000000000000000000000000000000000000000000000000000000000000";
            string testB = "55555555555555555555555555555555555555555555555555555555555555555";
            string testC = "99999999999999999999999999999999999999999999999999999999999999999";

            OverrideStringContent(testA, "ABCDEFGH".ToCharArray());
            OverrideStringContent(testB, "XYZFN".ToCharArray());
            OverrideStringContent(testC, "123456789".ToCharArray());

            //Unfortunately event this fails - as runtime counts on the initial length of the strings :-|
            //So mutating strings i a very very very dangerous!!!
            GC.Collect();
        }

        public static void StringMutationTest3()
        {
            string testA = "00000000000000000000000000000000000000000000000000000000000000000";
            string testB = "55555555555555555555555555555555555555555555555555555555555555555";
            string testC = "99999999999999999999999999999999999999999999999999999999999999999";

            TryOverrideStringContentV2(testA, "2013-11-11 12:22:23".ToCharArray());
            TryOverrideStringContentV2(testB, "15549,656800".ToCharArray());
            TryOverrideStringContentV2(testC, "123456789".ToCharArray());

            //We kept the length intact -sp we are safe here
            GC.Collect();

            //Parse functions ignore the '/0' chars - so we can still reuse strings for parsing
            int i = int.Parse(testC);
            DateTime dt = DateTime.Parse(testA);
            decimal dc = decimal.Parse(testB);
        }

        public static void StringTests()
        {
            StringMutationTest1_Dangerous();
            StringMutationTest2_Dangerous();
            StringMutationTest3();
        }


        private static void FuncToThrowA(Exception e)
        {
            throw e;
        }

        private static void FuncToThrowB(Exception e)
        {
            FuncToThrowA(e);
        }

        public static void CachedExceptionTest()
        {
            Exception cachedEx = new Exception();

            for (int i = 0; i < 100000; )
            {
                try
                {
                    if (i%3 == 0)
                    {
                        FuncToThrowB(cachedEx);
                    }
                    else
                    {
                        FuncToThrowA(cachedEx);
                    }
                }
                catch (Exception e)
                {
                    i++;
                }
            }
            
        }

    }
}

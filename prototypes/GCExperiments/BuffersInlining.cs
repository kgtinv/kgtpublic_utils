﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GCExperiments
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public class BasicClassWithMarshallInlinedArray
    {
        private int _i;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        private byte[] _pseudoinlinedArray;

        public int I { get { return this._i; } }
        public string Array { get { return Encoding.ASCII.GetString(this._pseudoinlinedArray).TrimEnd('\0'); } }

        public BasicClassWithMarshallInlinedArray()
        {
            //Array need to be created (Elsewhere!)
            _pseudoinlinedArray = new byte[64];
        }

        public void SetNewData(int i, string asciiString)
        {
            this._i = i;

            //some check goes here

            for (int strIdx = 0; strIdx < asciiString.Length; strIdx++)
            {
                if (asciiString[strIdx] > byte.MaxValue)
                    throw new Exception(string.Format("Passed ascii string has nonascii char at position {0}. {1}",
                                                      strIdx, asciiString));

                _pseudoinlinedArray[strIdx] = (byte) asciiString[strIdx];
            }

            for (int bufferIdx = asciiString.Length; bufferIdx < _pseudoinlinedArray.Length; bufferIdx++)
            {
                _pseudoinlinedArray[bufferIdx] = (byte) 0;
            }
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public unsafe class BasicClassWithFixedInlinedArray
    {
        private const int ARRAY_SIZE = 64;

        private int _i;
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private FixedBuffer _inlinedArray;

        public int I { get { return this._i; } }

        public string Array
        {
            get
            {
                byte[] bytes = new byte[ARRAY_SIZE];

                unsafe
                {
                    // Pin the buffer to a fixed location in memory. 
                    fixed (byte* bytePtr = this._inlinedArray._buffer)
                    {
                        for (int i = 0; i < ARRAY_SIZE; i++)
                        {
                            bytes[i] = *(bytePtr + i);
                        }
                    }
                }

                return Encoding.ASCII.GetString(bytes).TrimEnd('\0');
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)] 
        private struct FixedBuffer
        {
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public fixed byte _buffer[ARRAY_SIZE];
        }

        public BasicClassWithFixedInlinedArray()
        {
            //nothing needed - fixed array is initialized
        }

        public void SetNewData(int i, string asciiString)
        {
            this._i = i;

            //some check goes here


            fixed (byte* bytePtr = this._inlinedArray._buffer)
            {


                for (int strIdx = 0; strIdx < asciiString.Length; strIdx++)
                {
                    if (asciiString[strIdx] > byte.MaxValue)
                        throw new Exception(string.Format("Passed ascii string has nonascii char at position {0}. {1}",
                                                          strIdx, asciiString));

                    bytePtr[strIdx] = (byte) asciiString[strIdx];
                }

                for (int bufferIdx = asciiString.Length; bufferIdx < ARRAY_SIZE; bufferIdx++)
                {
                    bytePtr[bufferIdx] = (byte) 0;
                }

            }
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct FixedByteArray64
    {
        private const int ARRAY_SIZE = 64;
        private fixed byte _buffer[ARRAY_SIZE];

        public void SetBuffer(string asciiString)
        {
            //Some checks...

            fixed (byte* bytePtr = this._buffer)
            {
                for (int strIdx = 0; strIdx < asciiString.Length; strIdx++)
                {
                    if (asciiString[strIdx] > byte.MaxValue)
                        throw new Exception(string.Format("Passed ascii string has nonascii char at position {0}. {1}",
                                                          strIdx, asciiString));

                    bytePtr[strIdx] = (byte)asciiString[strIdx];
                }

                for (int bufferIdx = asciiString.Length; bufferIdx < ARRAY_SIZE; bufferIdx++)
                {
                    bytePtr[bufferIdx] = (byte)0;
                }

            }
        }

        public string ContentAsString
        {
            get
            {
                byte[] bytes = new byte[ARRAY_SIZE];

                unsafe
                {
                    // Pin the buffer to a fixed location in memory. 
                    fixed (byte* bytePtr = this._buffer)
                    {
                        for (int i = 0; i < ARRAY_SIZE; i++)
                        {
                            bytes[i] = *(bytePtr + i);
                        }
                    }
                }

                return Encoding.ASCII.GetString(bytes).TrimEnd('\0');
            }
        }
    }


    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class ClassThatCanBeSafeWithFixedInlinedArray
    {
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.I1)]
        private FixedByteArray64 _inlinedArray;
        private int _i;

        //with this it won't work!!! as fixed size arrays doesn't work correctly with nonblittable types
        //private decimal _d;
        

        public int I { get { return this._i; } }

        public string Array
        {
            get { return this._inlinedArray.ContentAsString; }
        }

        public void SetNewData(int i, string asciiString)
        {
            this._i = i;

            //some check goes here


            this._inlinedArray.SetBuffer(asciiString);
        }
    }

    public class BuffersInlining
    {
        public void PerformTests()
        {
            TestBasicClassWithMarshallInlinedArray();
            TestBasicClassWithFixedInlinedArray();
            TestClassThatCanBeSafeWithFixedInlinedArray();
        }


        public void TestBasicClassWithMarshallInlinedArray()
        {
            BasicClassWithMarshallInlinedArray entry = new BasicClassWithMarshallInlinedArray();
            entry.SetNewData(5, "FooBar");
            byte[] bytes = new byte[100];
            AppendTypeIntoBuffer(bytes, entry);
            entry.SetNewData(20, "HHHH");

            BasicClassWithMarshallInlinedArray newEntry = new BasicClassWithMarshallInlinedArray();
            ReadType(bytes, newEntry);


            string s = "FooBar blaaaaah fdfkdlkldk lkdkldkflkfd";

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 10000000; i++)
            {
                entry.SetNewData(5, s);
                AppendTypeIntoBuffer(bytes, entry);
                ReadType(bytes, newEntry);
            }
            sw.Stop();

            Console.WriteLine("{0} (TestBasicClassWithMarshallInlinedArray)", sw.Elapsed);

        }

        public void TestBasicClassWithFixedInlinedArray()
        {
            BasicClassWithFixedInlinedArray entry = new BasicClassWithFixedInlinedArray();
            entry.SetNewData(5, "FooBar blaaaaah fdfkdlkldk lkdflkdlfkldfkldkflkfd");
            byte[] bytes = new byte[100];
            AppendTypeIntoBuffer(bytes, entry);
            entry.SetNewData(20, "HHHH");

            BasicClassWithFixedInlinedArray newEntry = new BasicClassWithFixedInlinedArray();
            ReadType(bytes, newEntry);


            string s = "FooBar blaaaaah fdfkdlkldk lkdkldkflkfd";

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 10000000; i++)
            {
                entry.SetNewData(5, s);
                AppendTypeIntoBuffer(bytes, entry);
                ReadType(bytes, newEntry);
            }
            sw.Stop();

            Console.WriteLine("{0} (TestBasicClassWithFixedInlinedArray)", sw.Elapsed);
        }

        public void TestClassThatCanBeSafeWithFixedInlinedArray()
        {
            ClassThatCanBeSafeWithFixedInlinedArray entry = new ClassThatCanBeSafeWithFixedInlinedArray();
            entry.SetNewData(5, "FooBar blaaaaah fdfkdlkldk lkdflkdlfkldfkldkflkfd");
            byte[] bytes = new byte[100];
            AppendTypeIntoBuffer(bytes, entry);
            entry.SetNewData(20, "HHHH");

            ClassThatCanBeSafeWithFixedInlinedArray newEntry = new ClassThatCanBeSafeWithFixedInlinedArray();
            ReadType(bytes, newEntry);


            string s = "FooBar blaaaaah fdfkdlkldk lkdkldkflkfd";

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 10000000; i++)
            {
                entry.SetNewData(5, s);
                AppendTypeIntoBuffer(bytes, entry);
                ReadType(bytes, newEntry);
            }
            sw.Stop();

            Console.WriteLine("{0} (TestClassThatCanBeSafeWithFixedInlinedArray)", sw.Elapsed);
        }

        public static void AppendTypeIntoBuffer<T>(byte[] buffer, T value)
        {
            int typeSize = Marshal.SizeOf(typeof(T));

            if (typeSize > buffer.Length)
                throw new Exception("Not enaugh space in buffer");

            unsafe
            {
                fixed (byte* b = &buffer[0])
                {
                    IntPtr p = new IntPtr(b);
                    Marshal.StructureToPtr(value, p, false);
                }
            }
        }

        public static T ReadType<T>(byte[] buffer, T uninitializedInstance) where T : class
        {
            int typeSize = Marshal.SizeOf(typeof(T));

            if (buffer.Length < typeSize)
                throw new Exception("Buffer contains less data than what's needed for the type");

            unsafe
            {
                fixed (byte* b = &buffer[0])
                {
                    IntPtr p = new IntPtr(b);

                    //this throws! doesn't accept value arguments
                    Marshal.PtrToStructure(p, uninitializedInstance);
                }
            }

            return uninitializedInstance;
        }
    }
}

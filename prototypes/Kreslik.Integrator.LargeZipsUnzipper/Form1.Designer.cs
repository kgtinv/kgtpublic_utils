﻿namespace Kreslik.Integrator.LargeZipsUnzipper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtZipFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDestinationFolderName = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panelUnzipping = new System.Windows.Forms.Panel();
            this.lblUnzipping = new System.Windows.Forms.Label();
            this.chkBxUnzipMode = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            this.panelUnzipping.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Zip Files (*.zip)|*.zip";
            // 
            // txtZipFileName
            // 
            this.txtZipFileName.Location = new System.Drawing.Point(129, 24);
            this.txtZipFileName.Name = "txtZipFileName";
            this.txtZipFileName.Size = new System.Drawing.Size(322, 20);
            this.txtZipFileName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Zip Archive:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(467, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Unzip to folder:";
            // 
            // txtDestinationFolderName
            // 
            this.txtDestinationFolderName.Location = new System.Drawing.Point(129, 75);
            this.txtDestinationFolderName.Name = "txtDestinationFolderName";
            this.txtDestinationFolderName.Size = new System.Drawing.Size(322, 20);
            this.txtDestinationFolderName.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(467, 73);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Search";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(554, 126);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Unzip";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panelUnzipping
            // 
            this.panelUnzipping.Controls.Add(this.lblUnzipping);
            this.panelUnzipping.Location = new System.Drawing.Point(4, 2);
            this.panelUnzipping.Name = "panelUnzipping";
            this.panelUnzipping.Size = new System.Drawing.Size(635, 159);
            this.panelUnzipping.TabIndex = 6;
            this.panelUnzipping.Visible = false;
            // 
            // lblUnzipping
            // 
            this.lblUnzipping.AutoSize = true;
            this.lblUnzipping.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblUnzipping.Location = new System.Drawing.Point(107, 58);
            this.lblUnzipping.Name = "lblUnzipping";
            this.lblUnzipping.Size = new System.Drawing.Size(99, 25);
            this.lblUnzipping.TabIndex = 0;
            this.lblUnzipping.Text = "Unzipping";
            // 
            // chkBxUnzipMode
            // 
            this.chkBxUnzipMode.AutoSize = true;
            this.chkBxUnzipMode.Checked = true;
            this.chkBxUnzipMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBxUnzipMode.Location = new System.Drawing.Point(29, 131);
            this.chkBxUnzipMode.Name = "chkBxUnzipMode";
            this.chkBxUnzipMode.Size = new System.Drawing.Size(100, 17);
            this.chkBxUnzipMode.TabIndex = 7;
            this.chkBxUnzipMode.Text = "UnzippingMode";
            this.chkBxUnzipMode.UseVisualStyleBackColor = true;
            this.chkBxUnzipMode.CheckedChanged += new System.EventHandler(this.chkBxUnzipMode_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 161);
            this.Controls.Add(this.panelUnzipping);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtDestinationFolderName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtZipFileName);
            this.Controls.Add(this.chkBxUnzipMode);
            this.Name = "Form1";
            this.Text = "LargeZipsUnzipper";
            this.panelUnzipping.ResumeLayout(false);
            this.panelUnzipping.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtZipFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDestinationFolderName;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Panel panelUnzipping;
        private System.Windows.Forms.Label lblUnzipping;
        private System.Windows.Forms.CheckBox chkBxUnzipMode;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog2;
    }
}


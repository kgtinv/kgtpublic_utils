﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighResolutionTimestampCalibrationTest
{
    class Program
    {
        static void Main(string[] args)
        {

             ConsoleKeyInfo key = new ConsoleKeyInfo();
                bool notChosen = true;

                while (notChosen)
                {
                    Console.WriteLine("HighResolutionDatetime test. Press (S) to start server, (C) to start client");
                    key = Console.ReadKey(true);

                    notChosen = false;
                    switch (key.Key)
                    {
                        case ConsoleKey.S:
                            ServerTest();
                            break;
                        case ConsoleKey.C:
                            ClientTest();
                            break;

                        default:
                            notChosen = true;
                            Console.WriteLine("Unrecognized option: [{0}]", key.KeyChar);
                            break;
                    }
                }

            Console.WriteLine("Will exit after keypress");
            Console.ReadKey();
        }

        static void ServerTest()
        {
            DateTimeService dts = new DateTimeService();
            dts.StartListening();
            Console.WriteLine("Press any key to exit server");
            Console.ReadKey();
        }

        static void ClientTest()
        {
            Console.WriteLine("Enter IP address to connect to:");
            string ip = Console.ReadLine();
            DateTimeServiceClient dtc = new DateTimeServiceClient(ip);
            dtc.RunTest();
        }
    }
}

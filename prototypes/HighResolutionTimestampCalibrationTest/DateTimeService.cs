﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace HighResolutionTimestampCalibrationTest
{
     [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    public class DateTimeService: IDateTimeService
     {
         private ILogger _logger = LogFactory.Instance.GetLogger("DateTimeTest");

        List<TimeSpan> _clientOffsets = new List<TimeSpan>();

        public DateTime GetServerTime()
        {
            return HighResolutionDateTime.UtcNow;
        }

        public void ReceiveClientTime(DateTime clientTime)
        {
            _clientOffsets.Add(clientTime - HighResolutionDateTime.UtcNow);
        }


        private ServiceHost _serviceHost;

        public bool StartListening()
        {
            Uri listenAddress;
            Binding binding;

                    listenAddress =
                        new Uri("net.tcp://localhost:55555/DateTimeService");
                    binding = new NetTcpBinding(SecurityMode.None);

            _serviceHost = new ServiceHost(this, listenAddress);
            _serviceHost.AddServiceEndpoint(typeof(IDateTimeService), binding, "");
            this._logger.Log(LogLevel.Info, "Starting service endpoint that will listen on {0}", listenAddress);

            try
            {
                _serviceHost.Open();
            }
            catch (AddressAlreadyInUseException e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Only one instance of IntegratorServer(Proxy) can be run on one machine.");
                return false;
            }

            return true;
        }


        public void CleanDeltasCache()
        {
            _clientOffsets.Clear();
        }

        public TimeSpan GetServerDelta()
        {
            return TimeSpan.FromTicks((long)_clientOffsets.Select(dl => Math.Abs(dl.Ticks)).Average());
        }
     }
}

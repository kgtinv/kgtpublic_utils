﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace HighResolutionTimestampCalibrationTest
{
    [ServiceContract]
    public interface IDateTimeService
    {
        [OperationContract()]
        DateTime GetServerTime();

        [OperationContract()]
        void ReceiveClientTime(DateTime clientTime);

        [OperationContract()]
        void CleanDeltasCache();

        [OperationContract()]
        TimeSpan GetServerDelta();
    }
}

// CLRHostingTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <windows.h>
#include <metahost.h>
#include <mscoree.h>
#include <corerror.h>
#include <comdef.h>
#pragma comment(lib, "mscoree.lib")


void RunExecutable(TCHAR* fileName, LPCWSTR typeName, LPCWSTR startMethod, LPCWSTR runtimeVersion)
{
	ICLRMetaHost       *pMetaHost       = nullptr;
	ICLRMetaHostPolicy *pMetaHostPolicy = nullptr;
	ICLRRuntimeHost    *pRuntimeHost    = nullptr;
	ICLRRuntimeInfo    *pRuntimeInfo    = nullptr;
	HRESULT hr;


	hr = CLRCreateInstance(CLSID_CLRMetaHost, IID_ICLRMetaHost,
		(LPVOID*)&pMetaHost);
	hr = pMetaHost->GetRuntime(runtimeVersion, IID_PPV_ARGS(&pRuntimeInfo));
	if (FAILED(hr))
	{
		wprintf_s(L"Failed to start .Net runtime %s\n", runtimeVersion);
		goto cleanup;
	}

	hr = pRuntimeInfo->GetInterface(CLSID_CLRRuntimeHost,IID_PPV_ARGS(&pRuntimeHost));         

	ICLRControl* pCLRControl = nullptr;
	hr = pRuntimeHost->GetCLRControl(&pCLRControl);


	/****** GC customization **********/

	hr = pRuntimeInfo->SetDefaultStartupFlags(STARTUP_FLAGS::STARTUP_SERVER_GC | STARTUP_FLAGS::STARTUP_CONCURRENT_GC /*| STARTUP_FLAGS::STARTUP_HOARD_GC_VM*/, NULL);
   if(FAILED(hr))
   {
	   wprintf_s(L"Failed to set startup flags. %d", hr);
      goto cleanup;
   }

   ICLRGCManager2 *pCLRGCManager = NULL;
   hr = pCLRControl->GetCLRManager(IID_ICLRGCManager2, (void**) &pCLRGCManager);
   if(FAILED(hr))
   {
		wprintf_s(L"Failed to get GC manager. %d", hr);
		goto cleanup;
   }


   //
   // SEGMENT SIZE >= 10*1024*1024*1024 leads to OOM in ICLRRuntimeHost::Start
   //  lower values are OK
   //
   SIZE_T segmentSize = (SIZE_T) 10*1024*1024*1024; // This is ok: 9*1024*1024*1024 + 9*1024*1024;
   SIZE_T maxGen0Size = 20000000000;
   wprintf_s(L"Setting segment Size set to %Iu and maxGen0Size to %Iu", segmentSize, maxGen0Size);
   hr = pCLRGCManager->SetGCStartupLimitsEx(segmentSize, maxGen0Size);
   if(FAILED(hr))
   {
	   wprintf_s(L"Failed to set GC startup limits. %d", hr);
      goto cleanup;
   }


   /****** End of GC customization **********/

	wprintf(L"Running runtime version: %s\n", runtimeVersion);
	wprintf_s(L"--- Start ---\n");
	hr = pRuntimeHost->Start();
	if(FAILED(hr))
	{
		wprintf_s(L"Failed to Start. %d", hr);
		goto cleanup;
	}


	TCHAR totalPath[512];
	TCHAR* assemblyFileName = nullptr;

	// AppDomain.ExecuteAssembly needs an absolute path
	// Therefore we need to convert the relative ones
	DWORD len = GetFullPathName(fileName, sizeof(totalPath) / sizeof(TCHAR), totalPath, &assemblyFileName);

	DWORD returnValue = 0;
	// Executing public static Start(string arg)
	hr = pRuntimeHost->ExecuteInDefaultAppDomain(totalPath, typeName, startMethod, L"", &returnValue);
	if(FAILED(hr))
	{
		wprintf_s(L"Failed to ExecuteInDefaultAppDomain. %d", hr);
		goto cleanup;
	}

	wprintf_s(L"--- End ---\n");
	hr = pRuntimeHost->Stop();

	cleanup:
	if (pRuntimeInfo != nullptr)
	{
		pRuntimeInfo->Release();
		pRuntimeInfo = nullptr;
	}
	if (pRuntimeHost != nullptr)
	{
		pRuntimeHost->Release();
		pRuntimeHost = nullptr;
	}
	if (pMetaHost != nullptr)
	{
		pMetaHost->Release();
		pMetaHost = nullptr;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc != 4)
	{
		wprintf_s(L"Usage: CLRHostingTest <path to exe> <typename> <start method>\n");
		return 0;
	}

	//L"SampleApp1.Program", L"Start"
	RunExecutable(argv[1], argv[2], argv[3], L"v4.0.30319");

	return 0;
}


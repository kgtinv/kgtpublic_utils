﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace CLRgcTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Program t = new Program();
            t.Run();
        }

        public static int Start(string arg)
        {
            Program t = new Program();
            t.Run();
            return 0;
        }

        private List<List<byte[]>> _permanentLoad = new List<List<byte[]>>();
        private int _itemsAdded = 0;

        private List<byte[]> _reusableLoad = new List<byte[]>();

        private const bool TEST_REUSABLE_LOAD = true;

        private void AddLod(byte[] bytes)
        {
            if (TEST_REUSABLE_LOAD)
            {
                if (_itemsAdded % 1024 == 0)
                    _reusableLoad = new List<byte[]>();

                _reusableLoad.Add(bytes);
            }
            else
            {
                if (_itemsAdded % 1024 == 0)
                    _permanentLoad.Add(new List<byte[]>());

                _permanentLoad[_itemsAdded / 1024].Add(bytes);
            }

            _itemsAdded++;
        }

        public void Run()
        {
            Console.WriteLine(
                "Press 'I' for test under (I)nteractive GC mode, press 'S' for test under (S)ustainableLowLatency mode and 'L' for test under (L)owLatency mode");

            GCLatencyMode requestedMode;

            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.I:
                    requestedMode = GCLatencyMode.Interactive;
                    break;
                case ConsoleKey.S:
                    requestedMode = GCLatencyMode.SustainedLowLatency;
                    break;
                case ConsoleKey.L:
                    requestedMode = GCLatencyMode.LowLatency;
                    break;
                default:
                    Console.WriteLine("Unknown option specified");
                    return;
            }

            GCSettings.LatencyMode = requestedMode;
            Console.WriteLine("Current GC mode: {0}, Is server GC: {1}",
                              GCSettings.LatencyMode, GCSettings.IsServerGC);

            RunAllocations(4);

            Console.ReadKey();
        }

        private void RunAllocations(int gen0CollectionsCountToExit)
        {
            long[] totalBytesCountBeforeGC = new long[gen0CollectionsCountToExit];
            long[] totalBytesCountAfterGC = new long[gen0CollectionsCountToExit];
            
            int initialGen0CollCount = GC.CollectionCount(0);
            int initialGen2CollCount = GC.CollectionCount(2);

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();

            try
            {
                gen0CollectionsCountToExit += initialGen0CollCount;
                int currentGen0CollCount = initialGen0CollCount;
                int lastGen0CollectionsCount = initialGen0CollCount;

                while (currentGen0CollCount < gen0CollectionsCountToExit)
                {
                    //DateTime before = DateTime.UtcNow;
                    long currentlyAllocatedBytes = GC.GetTotalMemory(false);
                    AddLod(new byte[5000]);

                    
                    currentGen0CollCount = GC.CollectionCount(0);
                    if (currentGen0CollCount != lastGen0CollectionsCount)
                    {
                        totalBytesCountBeforeGC[currentGen0CollCount - initialGen0CollCount -1] = currentlyAllocatedBytes;
                        totalBytesCountAfterGC[currentGen0CollCount - initialGen0CollCount -1] = GC.GetTotalMemory(false);
                        lastGen0CollectionsCount = currentGen0CollCount;
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                Console.WriteLine("Out of memory.");
            }

            Console.WriteLine("Before test started there were {0} Gen0 collections", initialGen0CollCount);
 
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < totalBytesCountBeforeGC.Length; i++)
            {
                sb.AppendFormat("Before[B]:\t{0} \tAfter[B]:\t{1}{2}", totalBytesCountBeforeGC[i],
                                totalBytesCountAfterGC[i],Environment.NewLine);
            }

            Console.WriteLine("Test experienced Gen0 collections at following memory utilizations:{0}{1}",
                              Environment.NewLine, sb.ToString());
        }
    }
}

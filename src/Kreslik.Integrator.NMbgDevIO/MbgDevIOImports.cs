﻿namespace Kreslik.Integrator.NMbgDevIO
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.InteropServices;

    internal static class MbgDevIOImports
    {
        /// <summary>
        /// This method makes sure that a native library needed by all platform invokes is loaded
        /// </summary>
        /// <exception cref="NMbgTimerException">Thrown when the required Meinberg dll is missing on the system</exception>
        public static void Initialize()
        {
            string unused;
            Initialize(out unused, true);
        }

        public static bool TryInitialize(out string error)
        {
            return Initialize(out error, false);
        }

        private static bool Initialize(out string error, bool throwOnError)
        {
            error = null;
            bool success = false;
            Type paretType = null;
            try
            {
                paretType = MethodBase.GetCurrentMethod().DeclaringType;
                Marshal.PrelinkAll(paretType);
                success = true;
            }
            catch (DllNotFoundException e)
            {
                error = string.Format(System.Globalization.CultureInfo.CurrentCulture,
                                      "Initialization of {0} failed due to an exception: {1}", paretType, e.ToString());
                if (throwOnError)
                {
                    throw new NMbgTimerException(error, e);
                }
            }

            return success;
        }

        public const short MBGDEVIO_VERSION = (short)530;

        //
        // Following definitions were added as example of wrapping handle and error codes
        //
        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true, EntryPoint = "mbg_open_device")]
        public static extern NMbgDevIOSafeHandle OpenDevice(int i);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true, EntryPoint = "mbg_close_device")]
        public static extern void CloseDevice(ref IntPtr handle);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true, EntryPoint = "mbg_get_hr_time_cycles")]
        [System.Security.SuppressUnmanagedCodeSecurity()]
        private static extern int GetHrTimeCyclesInternal(NMbgDevIOSafeHandle handle, ref PCPS_HR_TIME_CYCLES p);

        public static void GetHrTimeCycles(NMbgDevIOSafeHandle handle, ref PCPS_HR_TIME_CYCLES p)
        {
            int returnVal = GetHrTimeCyclesInternal(handle, ref p);
            if (returnVal != 0)
            {
                int errorCode = Marshal.GetLastWin32Error();
                throw new NMbgTimerException(string.Format(CultureInfo.CurrentCulture,
                                      "Call to mbg_get_hr_time_cycles failed. returned code: {0}, last error code: {1}",
                                      returnVal, errorCode));
            }
        }
        //
        //
        //

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_find_devices();

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_open_device(int i);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_close_device(ref int h);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_get_time(int h, ref PCPS_TIME t);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_dev_has_hr_time(int h, ref int f);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_get_hr_time(int h, ref PCPS_HR_TIME t);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_get_hr_time_cycles(int h, ref PCPS_HR_TIME_CYCLES p);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_get_hr_time_comp(int h, ref PCPS_HR_TIME t, ref int l);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_dev_has_ucap(int h, ref int f);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_clr_ucap_buff(int h);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_get_ucap_entries(int h, ref PCPS_UCAP_ENTRIES p);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_get_ucap_event(int h, ref PCPS_HR_TIME t);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_dev_is_gps(int h, ref int f);

        [DllImport("mbgdevio", CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mbg_get_gps_pos(int h, ref MBG_RCVR_POS p);

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PCPS_TIME
        {
            public byte sec100;
            public byte sec;
            public byte min;
            public byte hour;
            public byte mday;
            public byte wday;
            public byte month;
            public byte year;
            public byte status;
            public byte signal;
            public byte offs_utc;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PCPS_TIME_STAMP
        {
            public int sec;
            public uint frac;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PCPS_HR_TIME
        {
            public PCPS_TIME_STAMP tstamp;
            public int utc_offs;
            public short status;
            public byte signal;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PCPS_HR_TIME_CYCLES
        {
            public long cycles;
            public PCPS_HR_TIME t;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PCPS_UCAP_ENTRIES
        {
            public int used;
            public int max;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MBG_POS_XYZ
        {
            public double x;
            public double y;
            public double z;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MBG_POS_LLA
        {
            public double lat;
            public double lon;
            public double alt;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MBG_DMS
        {
            public short prefix;
            public short deg;
            public short min;
            public double sec;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MBG_RCVR_POS
        {
            public MBG_POS_XYZ xyz;
            public MBG_POS_LLA lla;
            public MBG_DMS longitude;
            public MBG_DMS latitude;
            public short ellipsoid;
        }
    }
}

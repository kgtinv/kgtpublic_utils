﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Kreslik.Integrator.NMbgDevIO
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.InteropServices;
    using System.Threading;

    public interface ITimerApi
    {
        Func<long> QueryAccurateTimeStampFunc { get; }
        Func<DateTime> GetAccurateTimeStampFunc { get; }
    }

    public class NMbgTimer : ITimerApi
    {
        //TODO: comment
        //private static readonly DateTime _epochTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static readonly long _epochTimeTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
        private NMbgDevIOSafeHandle _deviceHandle;

        private const long FRACS_PER_SECOND = uint.MaxValue + 1L;
        private const uint TICKS_PER_SECOND = 10000000;
        private const uint TICKS_PER_MINUTE = 60*TICKS_PER_SECOND;

        public const int _MAX_POLL_INTERVAL_MS = 10;
        public const int _TIMERS_RESOLUTION_THRESHOLD_MS = 1;

        //polling thread periodically refreshes it's internal timestamps and switches the bool switch
        // indicating which timestamp is refreshed, that way calling code can never use a timestamp
        // that is neing refreshed in the meantime (it may only use slightly outdated timestamp, but
        // it compesates aging with PerformanceCounter value anyway)

        private readonly long _performanceCounterFrequency;

        private bool _isAsynchronous;
        private long _timeStampATicks;
        private long _timeStampATakenAtCyclesValue;
        private long _timeStampBTicks;
        private long _timeStampBTakenAtCyclesValue;
        private bool _isTimeStampATheNewerOne = false;


        private bool _noDeviceAccess = false;
        private static DateTime _startTime = DateTime.UtcNow;
        private static Stopwatch _stopWatch = Stopwatch.StartNew();

        private Timer _refreshLocalTimeTimer;// = new Timer(RefreshLocalHighResTimer, null, TimeSpan.FromMinutes(30), TimeSpan.FromMinutes(30));

        private static void RefreshLocalHighResTimer(object _)
        {
            _startTime = DateTime.UtcNow;
            _stopWatch = Stopwatch.StartNew();
        }

        //Factory methods

        public static NMbgTimer GetTimer()
        {
            return new NMbgTimer(0, true, false);
        }

        public static NMbgTimer GetTimer(uint deviceIdToUse)
        {
            return new NMbgTimer(deviceIdToUse, false, false);
        }

        public static NMbgTimer GetAsynchronousTimer()
        {
            return new NMbgTimer(0, true, true);
        }

        public static NMbgTimer GetAsynchronousTimer(uint deviceIdToUse)
        {
            return new NMbgTimer(deviceIdToUse, false, true);
        }

        public static NMbgTimer GetBasicTimerWithoutMeibergClock()
        {
            return new NMbgTimer();
        }

        private NMbgTimer()
        {
            _noDeviceAccess = true;
            _isAsynchronous = false;
            IsAccurateDateTimeAvailable = false;
            _refreshLocalTimeTimer = new Timer(RefreshLocalHighResTimer, null, TimeSpan.FromMinutes(30), TimeSpan.FromMinutes(30));
        }

        private bool _isAccurateDateTimeAvailable;
        public bool IsAccurateDateTimeAvailable
        {
            get { return this._isAccurateDateTimeAvailable; }

            private set
            {
                if (value != this._isAccurateDateTimeAvailable && this.AccurateDateTimeAvailabilityChanged != null)
                {
                    try
                    {
                        this.AccurateDateTimeAvailabilityChanged(value);
                    }
                    catch (Exception e)
                    {
                        OnUnhandledAsynchronousException(e, true);
                    }  
                }
                this._isAccurateDateTimeAvailable = value;
            }
        }

        public event Action<bool> AccurateDateTimeAvailabilityChanged;

        public Func<long> QueryAccurateTimeStampFunc
        {
            get
            {
                return this.PollDeviceReturningTimestampTicks;
            }
        }

        public Func<DateTime> GetAccurateTimeStampFunc { get { return this.GetUtcNow; } }

        private NMbgTimer(uint deviceIdToUse, bool deviceIdNotSpecified, bool isAsynchronous)
        {
            //Stopwatch.Frequency below is more direct equivalent of P/Invoke that we would need to do:
            //
            //if (PerformanceCounterImports.QueryPerformanceFrequency(out _performanceCounterFrequency) == false)
            //{
            //    // high-performance counter not supported
            //    throw new NMbgTimerException("PerformanceCounter not supported on this system.");
            //}
            //
            // We might also want to throw if Stopwatch.IsHighPerformance != true and Async timer is requested
            _performanceCounterFrequency = Stopwatch.Frequency;

            MbgDevIOImports.Initialize();

            int devicesNum = MbgDevIOImports.mbg_find_devices();

            if (devicesNum <= 0)
            {
                throw new NMbgTimerException("No Meinberg device found on this system.");
            }

            if (deviceIdNotSpecified && devicesNum > 1)
            {
                throw new NMbgTimerException("There are more then one Meinberg device on this system, you need to specify id of device to be used");
            }

            if (devicesNum < deviceIdToUse + 1)
            {
                throw new NMbgTimerException("There are less Meinberg devices on this system then specified by the id");
            }

            _deviceHandle = MbgDevIOImports.OpenDevice((int)deviceIdToUse);
            if (_deviceHandle.IsInvalid)
            {
                int errorCode = Marshal.GetLastWin32Error();
                throw new NMbgTimerException(string.Format(CultureInfo.CurrentCulture,
                                                           "Opening of Meinberg device failed. Last error code: {0}",
                                                           errorCode));
            }

            //Sanity check that the timer returns synchronized time
            // if not - then the following call will throw and ctor will be aborted
            //this.GetUtcTimeStampTicksFromDevice(ref this._timeStampATicks, ref this._timeStampATakenAtCyclesValue);
            //_isTimeStampATheNewerOne = true;

            //this.IsAccurateDateTimeAvailable = true;

            //_isAsynchronous = isAsynchronous;

            this.MarkClockOutOfSyncAndTryResync();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private DateTime GetUtcNow()
        {
            return this.UtcNow;
        }

        private static readonly long MIN_TICKS = DateTime.MinValue.Ticks;
        private static readonly long MAX_TICKS = DateTime.MaxValue.Ticks;

        /// <summary>
        /// Returns current UTC timestamp. If this timer was created as Asynchronous, than this call is non-blocking
        /// </summary>
        public DateTime UtcNow
        {
            get
            {
                if (_isAsynchronous)
                {
                    long timeStampRequestedAt = Stopwatch.GetTimestamp();
                    long timeStampTakenAt;
                    long timeStampTicks;
                    if (_isTimeStampATheNewerOne)
                    {
                        timeStampTakenAt = _timeStampATakenAtCyclesValue;
                        timeStampTicks = _timeStampATicks;
                    }
                    else
                    {
                        timeStampTakenAt = _timeStampBTakenAtCyclesValue;
                        timeStampTicks = _timeStampBTicks;
                    }

                    long ticksDelay = (timeStampRequestedAt - timeStampTakenAt) * TICKS_PER_SECOND /
                                          _performanceCounterFrequency;
                    long nowTicks = timeStampTicks + ticksDelay;

                    if (nowTicks >= MIN_TICKS && nowTicks <= MAX_TICKS /*&& ticksDelay >= 0*/)
                        return new DateTime(nowTicks);
                    else
                    {
                        if (_gapInPolingDetected != null)
                            _gapInPolingDetected(nowTicks, nowTicks);
                        return DateTime.UtcNow;
                    }
                }
                else if(_noDeviceAccess)
                {
                    return _startTime.AddTicks(_stopWatch.Elapsed.Ticks);
                }
                else
                {
                    long ticks = 0;
                    long unused = 0;
                    GetUtcTimeStampTicksFromDevice(ref ticks, ref unused);
                    return new DateTime(ticks);
                }
            }
        }

        public event Action<Exception, bool> UnhandledAsynchronousException;
        public event Action<string, bool> NewInformationAvailable;

        private long NoDeviceAccessGetTimestampTicks()
        {
            return _startTime.Ticks + _stopWatch.Elapsed.Ticks;
        }

        private void OnUnhandledAsynchronousException(Exception e, bool isFatal)
        {
            if (UnhandledAsynchronousException != null)
                UnhandledAsynchronousException(e, isFatal);
        }

        private void OnNewInformationAvailable(string message, bool isFatal)
        {
            if (NewInformationAvailable != null)
                NewInformationAvailable(message, isFatal);
        }

        private void MarkClockInSync()
        {
            _isTimeStampATheNewerOne = true;
            this.IsAccurateDateTimeAvailable = true;
            _isAsynchronous = true;
            _noDeviceAccess = false;
            _exceptionsCount = 0;
            _nextResyncEarliestAfterUtc = DateTime.UtcNow.Add(TimeSpan.FromMinutes(30));
            Interlocked.Exchange(ref this._resyncingClock, 0);
        }

        private static TimeSpan[] _retryIntervals = new TimeSpan[] { TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(30) };
        private DateTime _nextResyncEarliestAfterUtc;
        private int _resyncingClock = 0;
        //important! Do not controll return value from GetUtcTimeStampTicksFromDevice here
        // as it's expected that clock will jump forward (or even backwards) from last correctly polled stamp (as we are not able to poll - that's why we execute this)
        private void MarkClockOutOfSyncAndTryResync()
        {
            if(Interlocked.CompareExchange(ref this._resyncingClock, 1, 0) == 1)
                return;

            try
            {
                this.GetUtcTimeStampTicksFromDevice(ref this._timeStampATicks, ref this._timeStampATakenAtCyclesValue);
                MarkClockInSync();
                return;
            }
            catch (Exception)
            { }

            RefreshLocalHighResTimer(null);
            this.IsAccurateDateTimeAvailable = false;
            _noDeviceAccess = true;
            _isAsynchronous = false;

            Task.Factory.StartNew(
                ()
                    =>
                {
                    //emails flood protection during Mbg clock vibrating it's sync state
                    if (DateTime.UtcNow < _nextResyncEarliestAfterUtc)
                    {
                        this.OnNewInformationAvailable("Detected clock off-sync too early after previous resync - waiting a bit to attempt to resync", false);
                        Thread.Sleep(TimeSpan.FromMinutes(30));
                    }

                    bool hasIssues = true;
                    int retries = 0;
                    DateTime started = DateTime.UtcNow;
                    do
                    {
                        RefreshLocalHighResTimer(null);

                        try
                        {
                            this.GetUtcTimeStampTicksFromDevice(ref this._timeStampATicks, ref this._timeStampATakenAtCyclesValue);
                            hasIssues = false;
                        }
                        catch (Exception e)
                        {
                            //after 0, 15, 60 then each 240 minutes
                            bool isFatal = retries == 6 || retries % 8 == 0;
                            OnUnhandledAsynchronousException(e, isFatal);
                            retries++;
                            TimeSpan nextRetryInterval = _retryIntervals[Math.Min(retries - 1, _retryIntervals.Length - 1)];
                            this.OnNewInformationAvailable(
                                string.Format(
                                    "Meinberg clock was not able to resynchronize (started trying on {0:yyyy-MM-dd HH:mm:ss.fffffff} UTC, attempts up till now: {1}) - will try in next retry interval ({2}). Running on local clock now",
                                    started, retries, nextRetryInterval), isFatal);
                            Thread.Sleep(nextRetryInterval);
                        }

                    } while (hasIssues);

                    MarkClockInSync();

                    this.OnNewInformationAvailable(string.Format("Meinberg clock resynchronized after being in off-sync mode from {0:yyyy-MM-dd HH:mm:ss.fffffff}", started), false);

                }, TaskCreationOptions.LongRunning)
                .ContinueWith(t => OnUnhandledAsynchronousException(t.Exception, true), TaskContinuationOptions.OnlyOnFaulted);

            //force caches refresh
            Thread.Yield();
        }

        private int _exceptionsCount = 0;
        private long PollDeviceReturningTimestampTicks()
        {
            if (!_isAsynchronous)
            {
                return this.NoDeviceAccessGetTimestampTicks();
            }

            long timeStampTakenAtCycles = 0;
            long timeStampTicks = 0;

            bool stampOk = false;
            try
            {
                stampOk = this.GetUtcTimeStampTicksFromDevice(ref timeStampTicks, ref timeStampTakenAtCycles);
            }
            catch (Exception e)
            {
                if (_exceptionsCount % 1024 == 0)
                {
                    OnUnhandledAsynchronousException(e, true);
                }
                _exceptionsCount++;

                if (_exceptionsCount > 1024)
                    MarkClockOutOfSyncAndTryResync();
            }

            if (!stampOk)
            {
                this.IsAccurateDateTimeAvailable = false;
                timeStampTicks = this.UtcNow.Ticks;
            }
            else if (_isTimeStampATheNewerOne)
            {
                _timeStampBTakenAtCyclesValue = timeStampTakenAtCycles;
                _timeStampBTicks = timeStampTicks;
                _isTimeStampATheNewerOne = false;
            }
            else
            {
                _timeStampATakenAtCyclesValue = timeStampTakenAtCycles;
                _timeStampATicks = timeStampTicks;
                _isTimeStampATheNewerOne = true;
            }

            return timeStampTicks;
        }

        private long _lastPolledTicks = 0;//long.MaxValue;
        private long _minTicksToFireCallback = long.MaxValue;
        private Action<long, long> _gapInPolingDetected; 

        public bool RegisterHandlerForGapsInPolling(TimeSpan minimumGapToInformAbout,
                                                    Action<long, long> detectedGapHandler)
        {
            if (!_isAsynchronous)
                return false;

            if (minimumGapToInformAbout.TotalMilliseconds <= _MAX_POLL_INTERVAL_MS)
                return false;

            _minTicksToFireCallback = minimumGapToInformAbout.Ticks;
            _gapInPolingDetected = detectedGapHandler;

            return true;
        }

        public TimeSpan TimestampsPollingInterval
        {
            get { return TimeSpan.FromMilliseconds(_MAX_POLL_INTERVAL_MS); }
        }

        private bool GetUtcTimeStampTicksFromDevice(ref long timesTampTicks, ref long pcCtrTicks)
        {
            MbgDevIOImports.PCPS_HR_TIME_CYCLES timeStruct = new MbgDevIOImports.PCPS_HR_TIME_CYCLES();

            //here is the point where we need to safe the perf counter value to find out card access latency
            long pcCtrTicksLocal = Stopwatch.GetTimestamp();

            MbgDevIOImports.GetHrTimeCycles(_deviceHandle, ref timeStruct);

            //DCF77 clock running on xtal
            //  GPS receiver has not verified its position
            if ((timeStruct.t.status & MbgDevs.PCPS_FREER) == MbgDevs.PCPS_FREER)
            {
                throw new NMbgTimerException("Meinberg clock are NOT synchronized (status contains PCPS_FREER bit).");
            }

            //Invalid time because battery was disconnected.
            //
            // IRIG receiver cards may set the PCPS_INVT bit also if the card's ref time offset 
            // to UTC has not yet been configured, or if the on-board date does not match the 
            // day-of-year number of the incoming IRIG signal. The reason for this is to avoid 
            // accepting wrong times from the IRIG signal due to the ambiguity of the time and
            // day-of-year from the IRIG signal.
            if ((timeStruct.t.status & MbgDevs.PCPS_INVT) == MbgDevs.PCPS_INVT)
            {
                throw new NMbgTimerException(
                    "Meinberg clock are likely returning wrong time due to battery disconnection or some misconfiguration (status contains PCPS_INVT bit).");
            }

            if ((timeStruct.t.status & MbgDevs.PCPS_UTC) != MbgDevs.PCPS_UTC)
            {
                throw new NMbgTimerException(
                    "Meinberg clock are likely returning local time instead of UTC due to misconfiguration (status doesn't contain PCPS_UTC bit).");
            }


            //Latency of the call (spend by OS, actuall card access is negligable)
            long ticksLatency = (timeStruct.cycles - pcCtrTicksLocal) * TICKS_PER_SECOND / _performanceCounterFrequency;

            long timesTampTicksLocal = _epochTimeTicks + timeStruct.t.tstamp.sec * TICKS_PER_SECOND;

            //Ticks are retunred in continuously increasing fromat from 0x0 to 0xFFFFFFFF
            // refer to Meinberg API principles doc for details
            timesTampTicksLocal += ((long)timeStruct.t.tstamp.frac) * TICKS_PER_SECOND / FRACS_PER_SECOND;

            //Ticks could have overflowen to the next second and so we can have negative here
            // but thats OK - DateTime will handle negative arguments
            timesTampTicksLocal -= ticksLatency;

            bool jumpingFarIntoFuture = false;
            if (timesTampTicksLocal - _lastPolledTicks > _minTicksToFireCallback 
                //in case we called this method from resync thread than ignore errors - since it might have been long from last timestamp read
                && !_noDeviceAccess)
            {
                jumpingFarIntoFuture = timesTampTicksLocal - _lastPolledTicks > TICKS_PER_MINUTE;
                if (!jumpingFarIntoFuture && _gapInPolingDetected != null)
                {
                    //Deffer the asynchronousness to callee. It is dangerous (what if callee blocks),
                    // but this deffer the cost of creating closures to callee - here we just copy args to stack.
                    _gapInPolingDetected(_lastPolledTicks, timesTampTicksLocal);
                }
            }

            bool goingBackwards = _lastPolledTicks > timesTampTicksLocal;
            bool hasErrorThatShouldDisregardGivenStamp = goingBackwards || jumpingFarIntoFuture;
            if (hasErrorThatShouldDisregardGivenStamp
                //in case we called this method from resync thread than ignore errors - since it might have been long from last timestamp read
                && !_noDeviceAccess)
            {
                if(_gapInPolingDetected != null)
                    _gapInPolingDetected(_lastPolledTicks, timesTampTicksLocal);
            }
            else
            {
                //Volatile.Write(ref _lastPolledTicks, timesTampTicksLocal);
                //Volatile.Write(ref timesTampTicks, timesTampTicksLocal);
                //Volatile.Write(ref pcCtrTicks, pcCtrTicksLocal);
                _lastPolledTicks = timesTampTicksLocal;
                timesTampTicks = timesTampTicksLocal;
                pcCtrTicks = pcCtrTicksLocal;
            }

            return !hasErrorThatShouldDisregardGivenStamp;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    [Serializable]
    public class BufferSegmentUtilsException : Exception
    {
        public BufferSegmentUtilsException()
        { }

        public BufferSegmentUtilsException(string message)
            : base(message)
        { }

        public BufferSegmentUtilsException(string message, Exception inner)
            : base(message, inner)
        { }
    }

    public static class BufferSegmentUtils
    {
        private const byte _POINT = 0x2E; //.
        private const byte _COLON = 0x3A; //:
        private const byte _DASH = 0x2D; //-
        private const byte _ZERO = 0x30; //0
        public const byte THREE = 0x33; //3
        public const byte FIVE = 0x35; //5

        public const byte SOH = 0x01; //SOH
        private const byte _EQUALS_SIGN = 0x3D; //=
        public const byte TAGS_SEPARATOR = SOH; //SOH
        public const byte VALS_SEPARATOR = _EQUALS_SIGN; //SOH



        public static int ToInt(this BufferSegmentBase buffer)
        {
            int result = 0;
            for (int idx = 0; idx < buffer.ContentSize; idx++)
            {
                result *= 10;
                int currentDigit = buffer[idx] - _ZERO;
                if (currentDigit < 0 || currentDigit > 9)
                    throw new BufferSegmentUtilsException("ToInt: Invalid digit inside buffersegment: " + buffer.GetContentAsAsciiString());
                result += currentDigit;
            }

            return result;
        }

        public static bool FindByte(this BufferSegmentBase buffer, byte @byte, ref int idx)
        {
            while (idx < buffer.ContentSize)
            {
                if (buffer[idx] == @byte)
                    return true;
                idx++;
            }

            return false;
        }

        public static bool FindByteSequence(this BufferSegmentBase buffer, byte[] byteSequence, ref int idx)
        {
            while (idx <= buffer.ContentSize - byteSequence.Length)
            {
                int innerIdx = 0;
                for (; innerIdx < byteSequence.Length && buffer[idx + innerIdx] == byteSequence[innerIdx]; innerIdx++)
                { }
                if (innerIdx == byteSequence.Length)
                    return true;

                idx++;
            }

            return false;
        }

        public static int ToInt(this BufferSegmentBase buffer, ref int idx)
        {
            int result = 0;
            for (; idx < buffer.ContentSize; idx++)
            {
                int currentDigit = buffer[idx] - _ZERO;
                if (currentDigit < 0 || currentDigit > 9)
                    return result;

                result *= 10;
                result += currentDigit;
            }

            return result;
        }

        public static decimal ToDecimal(this BufferSegmentBase buffer)
        {
            long result = 0;
            byte scale = 0;
            bool pointEncountered = false;
            bool negative = buffer[0] == _DASH;
            for (int idx = negative ? 1 : 0; idx < buffer.ContentSize; idx++)
            {
                byte b = buffer[idx];

                if (b == _POINT)
                {
                    if (pointEncountered)
                        throw new BufferSegmentUtilsException("ToDecimal: two points in decimal: " + buffer.GetContentAsAsciiString());
                    pointEncountered = true;
                }
                else
                {
                    result *= 10;
                    int currentDigit = buffer[idx] - _ZERO;
                    if (currentDigit < 0 || currentDigit > 9)
                        throw new BufferSegmentUtilsException("ToDecimal: Invalid digit inside buffersegment: " + buffer.GetContentAsAsciiString());
                    result += currentDigit;

                    if (pointEncountered)
                        scale++;
                }
            }

            //low, mid, high, isnegative, scale
            return new decimal((int)(result & uint.MaxValue), (int)(result >> 32), 0, negative, scale).Normalize();
        }

        private const ulong _MAX_VALUE_BEFORE_ADITION = ulong.MaxValue - 9;
        public static AtomicDecimal ToAtomicDecimal(this BufferSegmentBase buffer)
        {
            ulong result = 0;
            byte scale = 0;
            bool pointEncountered = false;
            for (int idx = 0; idx < buffer.ContentSize; idx++)
            {
                byte b = buffer[idx];

                if (b == _POINT)
                {
                    if (pointEncountered)
                        throw new BufferSegmentUtilsException("ToDecimal: two points in decimal: " + buffer.GetContentAsAsciiString());
                    pointEncountered = true;
                }
                else
                {
                    if (pointEncountered)
                        scale++;

                    int currentDigit = buffer[idx] - _ZERO;
                    if (currentDigit < 0 || currentDigit > 9)
                        throw new BufferSegmentUtilsException("ToDecimal: Invalid digit inside buffersegment: " + buffer.GetContentAsAsciiString());

                    if (scale > AtomicDecimal.CONST_SCALE)
                    {
                        if (currentDigit != 0)
                        {
                            throw new BufferSegmentUtilsException("ToDecimal: To large scale of buffersegment: " + buffer.GetContentAsAsciiString());
                        }
                        scale--;
                    }
                    else
                    {
                        result *= 10;
                    }

                    if (result > _MAX_VALUE_BEFORE_ADITION)
                    {
                        throw new BufferSegmentUtilsException("ToDecimal: To large number in buffersegment: " + buffer.GetContentAsAsciiString());
                    }

                    result += (ulong)currentDigit;
                }
            }

            for (int i = scale; i < AtomicDecimal.CONST_SCALE; i++)
            {
                result *= 10;
            }

            return new AtomicDecimal(result);
        }

        public static DateTime ToDateTime(this BufferSegmentBase buffer)
        {
            if (buffer.ContentSize != 17 && buffer.ContentSize != 21)
                throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment size: " + buffer.GetContentAsAsciiString());

            int year, month, day, hour, minute, second, millisecond = 0;

            year = buffer[0] * 1000 + buffer[1] * 100 + buffer[2] * 10 +
                   buffer[3] - 1111 * _ZERO;
            month = buffer[4] * 10 + buffer[5] - 11 * _ZERO;
            day = buffer[6] * 10 + buffer[7] - 11 * _ZERO;

            if (buffer[8] != _DASH)
                throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());

            hour = buffer[9] * 10 + buffer[10] - 11 * _ZERO;

            if (buffer[11] != _COLON)
                throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());

            minute = buffer[12] * 10 + buffer[13] - 11 * _ZERO;

            if (buffer[14] != _COLON)
                throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());

            second = buffer[15] * 10 + buffer[16] - 11 * _ZERO;

            if (buffer.ContentSize == 21)
            {
                if (buffer[17] != _POINT)
                    throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());

                millisecond = buffer[18] * 100 + buffer[19] * 10 + buffer[20] - 111 * _ZERO;
            }

            return new DateTime(year, month, day, hour, minute, second, millisecond, DateTimeKind.Utc);
        }

        //"hh:mm:ss.fff"
        public static TimeSpan ToTimeOnly(this BufferSegmentBase buffer)
        {
            if (buffer.ContentSize != 8 && buffer.ContentSize != 12)
                throw new BufferSegmentUtilsException("ToTimeOnly: Invalid buffersegment size: " + buffer.GetContentAsAsciiString());


            int hour, minute, second, millisecond = 0;

            hour = buffer[0] * 10 + buffer[1] - 11 * _ZERO;

            if (buffer[2] != _COLON)
                throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());

            minute = buffer[3] * 10 + buffer[4] - 11 * _ZERO;

            if (buffer[5] != _COLON)
                throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());

            second = buffer[6] * 10 + buffer[7] - 11 * _ZERO;

            if (buffer.ContentSize == 12)
            {
                if (buffer[8] != _POINT)
                    throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());

                millisecond = buffer[9] * 100 + buffer[10] * 10 + buffer[11] - 111 * _ZERO;
            }

            return new TimeSpan(0, hour, minute, second, millisecond);
        }

        //"yyyyMMdd", "yyyy-MM-dd"
        public static DateTime ToDateOnly(this BufferSegmentBase buffer)
        {
            if (buffer.ContentSize != 8 && buffer.ContentSize != 10)
                throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment size: " + buffer.GetContentAsAsciiString());

            int year, month, day = 0;

            year = buffer[0] * 1000 + buffer[1] * 100 + buffer[2] * 10 +
                   buffer[3] - 1111 * _ZERO;

            int idx = 4;

            if (buffer.ContentSize == 10)
            {
                if (buffer[idx++] != _DASH)
                    throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " + buffer.GetContentAsAsciiString());
            }

            month = buffer[idx] * 10 + buffer[idx + 1] - 11 * _ZERO;

            idx += 2;

            if (buffer.ContentSize == 10)
            {
                if (buffer[idx++] != _DASH)
                    throw new BufferSegmentUtilsException("ToDateTime: Invalid buffersegment format: " +
                                                          buffer.GetContentAsAsciiString());
            }

            day = buffer[idx] * 10 + buffer[idx + 1] - 11 * _ZERO;

            return new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);
        }

        //"yyyyMMdd", "yyyy-MM-dd"
        public static bool TryToDateOnly(this BufferSegmentBase buffer, out DateTime? dateTime)
        {
            dateTime = null;

            if (buffer.ContentSize != 8 && buffer.ContentSize != 10)
                return false;

            int year, month, day = 0;

            year = buffer[0] * 1000 + buffer[1] * 100 + buffer[2] * 10 +
                   buffer[3] - 1111 * _ZERO;

            int idx = 4;

            if (buffer.ContentSize == 10)
            {
                if (buffer[idx++] != _DASH)
                    return false;
            }

            month = buffer[idx] * 10 + buffer[idx + 1] - 11 * _ZERO;

            idx += 2;

            if (buffer.ContentSize == 10)
            {
                if (buffer[idx++] != _DASH)
                    return false;
            }

            day = buffer[idx] * 10 + buffer[idx + 1] - 11 * _ZERO;

            dateTime = new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);
            return true;
        }

        public static void CopyToClearingRest(this BufferSegmentBase fromBuffer, byte[] toBuffer)
        {
            int sourceBufferSize = fromBuffer == null ? 0 : fromBuffer.ContentSize;

            if(fromBuffer != null)
                Buffer.BlockCopy(fromBuffer.Buffer, fromBuffer.ContentStart, toBuffer, 0, sourceBufferSize);
            Array.Clear(toBuffer, sourceBufferSize, toBuffer.Length - sourceBufferSize);
        }

        public static void CopyToClearingRest(this byte[] fromBuffer, byte[] toBuffer)
        {
            int sourceBufferSize = fromBuffer == null ? 0 : fromBuffer.Length;

            if (fromBuffer != null)
                Buffer.BlockCopy(fromBuffer, 0, toBuffer, 0, sourceBufferSize);
            Array.Clear(toBuffer, sourceBufferSize, toBuffer.Length - sourceBufferSize);
        }

        public static readonly byte[] EMPTY = new byte[0];

        public static byte[] ToAsciiBuffer(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return EMPTY;

            return System.Text.Encoding.ASCII.GetBytes(str);
        }

        public static string GetContentAsAsciiString(this byte[] buffer)
        {
            return System.Text.Encoding.ASCII.GetString(buffer);
        }
    }
}

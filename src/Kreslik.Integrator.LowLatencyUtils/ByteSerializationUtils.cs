﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public static class ByteSerializationUtils
    {
        public static unsafe string FloatToHex(float value)
        {
            return (*(int*)(float*)&value).ToString("X");
        }

        public static unsafe int FloatToIntMemCast(float value)
        {
            return *(int*)(float*)&value;
        }

        public static unsafe float FloatFromHex(string value)
        {
            int valAsInt = int.Parse(value, System.Globalization.NumberStyles.HexNumber);
            return *(float*)(int*)&valAsInt;
        }

        public static unsafe float FloatFromIntMemCast(int value)
        {
            return *(float*)(int*)&value;
        }

        static ByteSerializationUtils()
        {
            if (!BitConverter.IsLittleEndian)
                throw new Exception("ByteSerializationUtils is being run on big endian architecture - this is not supported!");
        }

        public static void AppendLong(byte[] buffer, ref int bufferFreeSpaceStart, long value)
        {
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStart])
                {
                    //interpret the array as longs array and copy there
                    *(long*)b = value;
                }
            }
            bufferFreeSpaceStart += sizeof(long);
        }

        public static unsafe void AppendLong(ref byte* buffer, long value)
        {
            *(long*)buffer = value;
            buffer += sizeof(long);
        }

        public static unsafe long ExtractLong(ref byte* buffer)
        {
            long value = *(long*)buffer;
            buffer += sizeof(long);
            return value;
        }

        public static long ExtractLong(byte[] buffer, ref int bufferFreeSpaceStart)
        {
            int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
            bufferFreeSpaceStart += sizeof(long);
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStartLocal])
                {
                    //interpret the array as longs and copy from there
                    return *(long*)b;
                }
            }
        }

        public static void AppendULong(byte[] buffer, ref int bufferFreeSpaceStart, ulong value)
        {
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStart])
                {
                    //interpret the array as longs array and copy there
                    *(ulong*)b = value;
                }
            }
            bufferFreeSpaceStart += sizeof(ulong);
        }

        public static unsafe void AppendULong(ref byte* buffer, ulong value)
        {
            *(ulong*)buffer = value;
            buffer += sizeof(ulong);
        }

        public static unsafe ulong ExtractULong(ref byte* buffer)
        {
            ulong value = *(ulong*)buffer;
            buffer += sizeof(ulong);
            return value;
        }

        public static ulong ExtractULong(byte[] buffer, ref int bufferFreeSpaceStart)
        {
            int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
            bufferFreeSpaceStart += sizeof(ulong);
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStartLocal])
                {
                    //interpret the array as longs and copy from there
                    return *(ulong*)b;
                }
            }
        }

        public static void AppendInt(byte[] buffer, ref int bufferFreeSpaceStart, int value)
        {
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStart])
                {
                    //interpret the array as longs array and copy there
                    *(int*)b = value;
                }
            }
            bufferFreeSpaceStart += sizeof(int);
        }

        public static unsafe void AppendInt(ref byte* buffer, int value)
        {
            *(int*)buffer = value;
            buffer += sizeof(int);
        }

        public static unsafe void AppendIntNullable(ref byte* buffer, int? value)
        {
            AppendBool(ref buffer, value.HasValue);
            AppendInt(ref buffer, value.HasValue ? value.Value : 0);
        }

        public static unsafe int ExtractInt(ref byte* buffer)
        {
            int value = *(int*)buffer;
            buffer += sizeof(int);
            return value;
        }

        public static unsafe int? ExtractIntNullable(ref byte* buffer)
        {
            bool hasValue = ExtractBool(ref buffer);
            int value = ExtractInt(ref buffer);
            return hasValue ? (int?)value : (int?)null;
        }

        public static int ExtractInt(byte[] buffer, ref int bufferFreeSpaceStart)
        {
            int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
            bufferFreeSpaceStart += sizeof(int);
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStartLocal])
                {
                    //interpret the array as longs and copy from there
                    return *(int*)b;
                }
            }
        }

        public static void AppendUInt(byte[] buffer, ref int bufferFreeSpaceStart, uint value)
        {
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStart])
                {
                    //interpret the array as longs array and copy there
                    *(uint*)b = value;
                }
            }
            bufferFreeSpaceStart += sizeof(uint);
        }

        public static unsafe void AppendUInt(ref byte* buffer, uint value)
        {
            *(uint*)buffer = value;
            buffer += sizeof(uint);
        }

        public static unsafe uint ExtractUInt(ref byte* buffer)
        {
            uint value = *(uint*)buffer;
            buffer += sizeof(uint);
            return value;
        }

        public static uint ExtractUInt(byte[] buffer, ref int bufferFreeSpaceStart)
        {
            int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
            bufferFreeSpaceStart += sizeof(uint);
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStartLocal])
                {
                    //interpret the array as longs and copy from there
                    return *(uint*)b;
                }
            }
        }

        public static void AppendDecimal(byte[] buffer, ref int bufferFreeSpaceStart, decimal value)
        {
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStart])
                {
                    //interpret the array as longs array and copy there
                    *(decimal*)b = value;
                }
            }
            bufferFreeSpaceStart += sizeof(decimal);
        }

        public static unsafe void AppendDecimal(ref byte* buffer, decimal value)
        {
            *(decimal*)buffer = value;
            buffer += sizeof(decimal);
        }

        public static unsafe void AppendDecimalNullable(ref byte* buffer, decimal? value)
        {
            AppendBool(ref buffer, value.HasValue);
            AppendDecimal(ref buffer, value.HasValue ? value.Value : 0m);
        }

        public static unsafe decimal ExtractDecimal(ref byte* buffer)
        {
            decimal value = *(decimal*)buffer;
            buffer += sizeof(decimal);
            return value;
        }

        public static unsafe decimal? ExtractDecimalNullable(ref byte* buffer)
        {
            bool hasValue = ExtractBool(ref buffer);
            decimal value = ExtractDecimal(ref buffer);
            return hasValue ? (decimal?)value : (decimal?)null;
        }

        public static decimal ExtractDecimal(byte[] buffer, ref int bufferFreeSpaceStart)
        {
            int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
            bufferFreeSpaceStart += sizeof(decimal);
            unsafe
            {
                fixed (byte* b = &buffer[bufferFreeSpaceStartLocal])
                {
                    return *(decimal*)b;
                }
            }
        }

        public static unsafe void AppendGuid(ref byte* buffer, Guid value)
        {
            *(Guid*)buffer = value;
            buffer += sizeof(Guid);
        }

        public static unsafe Guid ExtractGuid(ref byte* buffer)
        {
            Guid value = *(Guid*)buffer;
            buffer += sizeof(Guid);
            return value;
        }

        public static void AppendAsciiString(byte[] buffer, ref int bufferFreeSpaceStart, string value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                buffer[bufferFreeSpaceStart++] = (byte)value[i];
            }
        }

        //
        // This is almost 2 times faster - probably due to skipping array bounds check in unsafe
        //                      but it pins so can hurt GC
        //
        //public static void AppendAsciiStringPtrs(byte[] buffer, ref int bufferFreeSpaceStart, string value)
        //{
        //    unsafe
        //    {
        //        fixed (char* c = value)
        //        {
        //            for (int i = 0; i < value.Length; i++)
        //            {
        //                buffer[bufferFreeSpaceStart++] = (byte)c[i];
        //            }
        //        }
        //    }
        //}

        public static unsafe void AppendAsciiString(ref byte* buffer, string value, int length)
        {
            int lengthToBeSerialized = Math.Min(value == null ? 0 : value.Length, length);
            for (int i = 0; i < lengthToBeSerialized; i++)
            {
                *buffer = (byte)value[i];
                buffer++;
            }

            NullifyBuffer(ref buffer, length - lengthToBeSerialized);
        }

        public static unsafe void NullifyBuffer(ref byte* buffer, int segmentLengthToNullify)
        {
            if(segmentLengthToNullify <= 0)
                return;

            byte* nullifyEnd = buffer + segmentLengthToNullify;
            for (;buffer < nullifyEnd; buffer++)
            {
                *buffer = 0;
            }
        }

        public static unsafe void AppendAsciiString(ref byte* buffer, byte[] value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                *buffer = value[i];
                buffer++;
            }
        }

        public static unsafe string ExtractAsciiString(ref byte* buffer, int stringLength)
        {
            string str = (*buffer) == 0 ? null : new string((sbyte*)buffer, 0, stringLength, Encoding.ASCII).TrimEnd('\0');
            buffer += stringLength;
            return str;
        }

        public static string ExtractAsciiString(byte[] buffer, ref int bufferFreeSpaceStart, int stringLength)
        {
            int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
            bufferFreeSpaceStart += stringLength;
            return Encoding.ASCII.GetString(buffer, bufferFreeSpaceStartLocal, stringLength).TrimEnd('\0');
        }

        public static void AppendBool(byte[] buffer, ref int bufferFreeSpaceStart, bool value)
        {
            buffer[bufferFreeSpaceStart] = value ? (byte)1 : (byte)0;
            bufferFreeSpaceStart += 1;
        }

        public static unsafe void AppendBool(ref byte* buffer, bool value)
        {
            AppendByte(ref buffer, value ? (byte)1 : (byte)0);
        }

        public static unsafe bool ExtractBool(ref byte* buffer)
        {
            return ExtractByte(ref buffer) == (byte) 1;
        }

        public static bool ExtractBool(byte[] buffer, ref int bufferFreeSpaceStart)
        {
            int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
            bufferFreeSpaceStart += 1;
            return buffer[bufferFreeSpaceStartLocal] == (byte)1;
        }


        public static unsafe void AppendByte(ref byte* buffer, byte value)
        {
            *buffer = value;
            buffer += sizeof(byte);
        }

        public static unsafe void AppendByteNullable(ref byte* buffer, byte? value)
        {
            AppendBool(ref buffer, value.HasValue);
            AppendByte(ref buffer, value.HasValue ? value.Value : (byte) 0);
        }

        public static unsafe byte ExtractByte(ref byte* buffer)
        {
            byte value = *buffer;
            buffer += sizeof(byte);
            return value;
        }

        public static unsafe byte? ExtractByteNullable(ref byte* buffer)
        {
            bool hasValue = ExtractBool(ref buffer);
            byte value = ExtractByte(ref buffer);
            return hasValue ? (byte?)value : (byte?)null;
        }

        public static unsafe void AppendSymbol(ref byte* buffer, Symbol value)
        {
            AppendByte(ref buffer, (byte)value);
        }

        public static unsafe void AppendSymbolNullable(ref byte* buffer, Symbol? value)
        {
            AppendByteNullable(ref buffer, value.HasValue ? (byte?)value.Value : (byte?)null);
        }

        public static unsafe Symbol ExtractSymbol(ref byte* buffer)
        {
            return (Symbol)ExtractByte(ref buffer);
        }

        public static unsafe Symbol? ExtractSymbolNullable(ref byte* buffer)
        {
            bool hasValue = ExtractBool(ref buffer);
            byte value = ExtractByte(ref buffer);
            return hasValue ? (Symbol)value : (Symbol?)null;
        }

        public static unsafe void AppendCounterparty(ref byte* buffer, Counterparty value)
        {
            AppendByte(ref buffer, (byte)value);
        }

        public static unsafe Counterparty ExtractCounterparty(ref byte* buffer)
        {
            return (Counterparty)ExtractByte(ref buffer);
        }

        public static unsafe void AppendDateTime(ref byte* buffer, DateTime value)
        {
            *(DateTime*)buffer = value;
            buffer += sizeof(DateTime);
        }

        public static unsafe void AppendDateTimeNullable(ref byte* buffer, DateTime? value)
        {
            AppendBool(ref buffer, value.HasValue);
            AppendDateTime(ref buffer, value.HasValue ? value.Value : DateTime.MinValue);
        }

        public static unsafe DateTime ExtractDateTime(ref byte* buffer)
        {
            DateTime value = *(DateTime*)buffer;
            buffer += sizeof(DateTime);
            return value;
        }

        public static unsafe DateTime? ExtractDateTimeNullable(ref byte* buffer)
        {
            bool hasValue = ExtractBool(ref buffer);
            DateTime value = ExtractDateTime(ref buffer);
            return hasValue ? (DateTime?)value : (DateTime?)null;
        }

        //
        //Following unfortunately does not work
        //
        //public static void AppendStruct<T>(byte[] buffer, ref int bufferFreeSpaceStart, T value) where T: struct
        //{
        //    unsafe
        //    {
        //        fixed (byte* b = &buffer[bufferFreeSpaceStart])
        //        {
        //            //interpret the array as longs array and copy there
        //            *(T*)b = value;
        //        }
        //    }
        //    bufferFreeSpaceStart += sizeof(long);
        //}

        //public static T ExtractStruct<T>(byte[] buffer, ref int bufferFreeSpaceStart) where T : struct
        //{
        //    int bufferFreeSpaceStartLocal = bufferFreeSpaceStart;
        //    bufferFreeSpaceStart += sizeof(long);
        //    unsafe
        //    {
        //        fixed (byte* b = &buffer[bufferFreeSpaceStartLocal])
        //        {
        //            //interpret the array as longs and copy from there
        //            return *(T*)b;
        //        }
        //    }
        //}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public interface IMeanTracker
    {
        AtomicDecimal Mean { get; }
    }

    public class MeanTrackerUnsynchronized : IMeanTracker
    {
        private AtomicDecimal[] _data; //circualar buffer with the data
        private int _dataIdx; //current position in circualar buffer
        private readonly int _maxSize;
        private readonly int _mask;
        private readonly int _powerOf2;

        private static bool IsPowerOfTwo(long x)
        {
            return (x & (x - 1)) == 0;
        }

        public MeanTrackerUnsynchronized(int size)
        {
            if (size < 2 || !IsPowerOfTwo(size))
                throw new Exception("Mean tracker must have size which is power of 2, greater or equal to 2");

            this._maxSize = size;
            this._mask = size - 1;
            this._data = new AtomicDecimal[_maxSize];
            this._dataIdx = 0;

            while (size >> _powerOf2 > 1)
            {
                _powerOf2++;
            }
        }

        public void InsertNext(AtomicDecimal item)
        {
            if (_dataIdx == _maxSize - 1)
                IsFull = true;

            //cannot simpify to '+=' doe to requirement to subtract smaller from larger
            this._meanUndivided = (this._meanUndivided + item) - _data[_dataIdx];

            _data[_dataIdx] = item;

            _dataIdx = (_dataIdx + 1) & _mask;
        }

        public bool IsFull { get; private set; }

        private AtomicDecimal _meanUndivided;
        public AtomicDecimal Mean { get { return this._meanUndivided.DivideByPowerOf2(_powerOf2); } }
    }

    public class MeanTrackerTimeWindowedUnsynchronized : IMeanTracker
    {
        private AtomicDecimal[] _data; //circualar buffer with the data
        private DateTime[] _dataTimeStamps;
        private int _dataIdx; //current position in circualar buffer
        //private readonly int _maxSize;
        private readonly int _mask;
        private readonly int _powerOf2;
        private readonly TimeSpan _maxMeanAge;

        private static bool IsPowerOfTwo(long x)
        {
            return (x & (x - 1)) == 0;
        }

        public MeanTrackerTimeWindowedUnsynchronized(int size, TimeSpan maxMeanAge)
        {
            if (size < 2 || !IsPowerOfTwo(size))
                throw new Exception("Mean tracker must have size which is power of 2, greater or equal to 2");

            //this._maxSize = size;
            this._mask = size - 1;
            this._data = new AtomicDecimal[size];
            this._dataTimeStamps = new DateTime[size];
            this._dataIdx = 0;
            this._maxMeanAge = maxMeanAge;

            while (size >> _powerOf2 > 1)
            {
                _powerOf2++;
            }
        }

        private DateTime _nullifySpreadAfterUtc = DateTime.MaxValue;
        public void MarkNullItem(DateTime itemTimeStamp)
        {
            _nullifySpreadAfterUtc = itemTimeStamp + _maxMeanAge;
        }

        public void InsertNext(AtomicDecimal item, DateTime itemTimeStamp)
        {
            int dataIdxLocal = _dataIdx;
            _data[dataIdxLocal] = item;
            _dataTimeStamps[dataIdxLocal] = itemTimeStamp;
            _nullifySpreadAfterUtc = DateTime.MaxValue;

            _dataIdx = (_dataIdx + 1) & _mask;
        }

        //we need this to be AtomicDecimal to avoid torn reads and nead for synchronization
        private AtomicDecimal _meanCached;
        private DateTime _meanCachedGoodUntil;
        public AtomicDecimal Mean { get { return this.GetMean(HighResolutionDateTime.UtcNow); } }

        public void ResetCachedMean()
        {
            this._meanCachedGoodUntil = DateTime.MinValue;
            this._meanCached = AtomicDecimal.Zero;
        }

        public AtomicDecimal GetMean(DateTime utcNow)
        {
            if (_nullifySpreadAfterUtc <= utcNow)
            {
                //make sure that once new item is inserted the mean is recalculated
                ResetCachedMean();
                return AtomicDecimal.Zero;
            }

            //its ok if two threads execute internal of this method concurrently
            // and it's still likely much faster then blocking one of those two threads (and certainly faster for non-contended cases)
            if (_meanCachedGoodUntil <= utcNow)
            {
                DateTime maxAcceptedAge = utcNow - _maxMeanAge;

                AtomicDecimal sum = AtomicDecimal.Zero;
                int currentDataIdxLocal = _dataIdx;
                int idx = currentDataIdxLocal;
                bool allItemsTooOld = true;
                do
                {
                    if (_dataTimeStamps[idx] >= maxAcceptedAge)
                    {
                        allItemsTooOld = false;
                        break;
                    }
                    idx = (idx + 1) & _mask;
                } while (idx != currentDataIdxLocal);

                if (!allItemsTooOld)
                {
                    int cnt = currentDataIdxLocal - idx;
                    if (cnt <= 0)
                        cnt += _mask + 1; //mask + 1 == size

                    for (int idxOffset = 0; idxOffset < cnt; idxOffset++)
                    {
                        sum += _data[(idx + idxOffset) & _mask];
                    }

                    _meanCached = AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(sum.ToDecimal() / cnt);
                }
                _meanCachedGoodUntil = utcNow + _maxMeanAge;
            }

            return _meanCached;
        }
    }
}

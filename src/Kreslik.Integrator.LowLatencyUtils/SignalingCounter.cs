﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public interface ISignalingCounter: IDisposable
    {
        void AtomicIncrement();
        void AtomicDecrement();
        int Count { get; }
        int CountAtomic { get; }
        bool WaitTillZero(TimeSpan timeOut);
    }

    public class SignalingCounterWithMasterCounter : UncollectableMarshalByRefObject, ISignalingCounter, IDisposable
    {
        private readonly ISignalingCounter _currentCounter;
        private readonly ISignalingCounter _masterCounter;

        public SignalingCounterWithMasterCounter(ISignalingCounter currentCounter, ISignalingCounter masterCounter)
        {
            _currentCounter = currentCounter;
            _masterCounter = masterCounter;
        }

        public SignalingCounterWithMasterCounter(ISignalingCounter masterCounter)
            :this(new SignalingCounter(), masterCounter)
        { }

        public void AtomicIncrement()
        {
            _currentCounter.AtomicIncrement();
            _masterCounter.AtomicIncrement();
        }

        public void AtomicDecrement()
        {
            _currentCounter.AtomicDecrement();
            _masterCounter.AtomicDecrement();
        }

        public int Count => _currentCounter.Count;
        public int CountAtomic => _currentCounter.CountAtomic;
        public bool WaitTillZero(TimeSpan timeOut)
        {
            return _currentCounter.WaitTillZero(timeOut);
        }

        public int MasterCount => _masterCounter.Count;

        //we cannot dispose the master here - as it's shared
        public void Dispose()
        {
            _currentCounter.Dispose();
        }
    }

    public class SignalingCounter: UncollectableMarshalByRefObject, ISignalingCounter
    {
        private int _counter;
        private ManualResetEventSlim _mres;

        public void AtomicIncrement()
        {
            if (Interlocked.Increment(ref this._counter) > 0 && _mres != null)
            {
                SetMrStateThreadSafe();
            }
        }

        public void AtomicSetCount(int count)
        {
            if (Interlocked.Exchange(ref this._counter, count) > 0 && _mres != null)
            {
                SetMrStateThreadSafe();
            }
        }

        public void ZeroOut()
        {
            AtomicSetCount(0);
        }

        public void AtomicDecrement()
        {
            if (Interlocked.Decrement(ref this._counter) <= 0 && _mres != null)
            {
                SetMrStateThreadSafe();
            }
        }

        private void SetMrStateThreadSafe()
        {
            if (_mres == null)
            {
                return;
            }

            //need to lock MRE to prevent ABA (interlockeds are not enough - since we increment and decrement simultaneously)
            lock (_mres)
            {
                bool isZero = this.CountAtomic <= 0;
                if (isZero)
                {
                    _mres.Set();
                }
                else
                {
                    _mres.Reset();
                }
            }
        }

        public int Count { get { return this._counter; } }

        public int CountAtomic
        {
            get { return InterlockedEx.Read(ref this._counter); }
        }

        public bool WaitTillZero(TimeSpan timeOut)
        {
            if (CountAtomic > 0)
            {
                _mres = GetOrCreateEvent();
                return _mres.Wait(timeOut);
            }

            return true;
        }

        private ManualResetEventSlim GetOrCreateEvent()
        {
            if (_mres == null)
            {
                //need to lock instance - to prevent multiple MREs per object
                lock (this)
                {
                    _mres = new ManualResetEventSlim(false);
                    SetMrStateThreadSafe();
                }
            }

            return _mres;
        }

        public WaitHandle WaitHandle
        {
            get { return GetOrCreateEvent().WaitHandle; }
        }

        public void Dispose()
        {
            _mres?.Dispose();
        }
    }
}

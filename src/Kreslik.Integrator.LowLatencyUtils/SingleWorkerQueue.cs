﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public interface IWorkQueueProducer<T>
    {
        event Action<T> NewWorkAvailable;
        event Action DataDiscontinuity;
    }

    public interface IWorkQueueHandle<T>
    {
        event Action<T> SingleItemAvailable;
        event Action NoQueuedData;
        event Action WrittingWrappedAndPreviousDataMightBeInconsistent;
        event Action DataDiscontinuity;
        event Action QueueClosedWithNoMoreData;
        void ClearDiscontinuityFlag();
        string Identity { get; }
    }


    //multiple writters, single reader
    //struct constraint is solely for performance reasons - not needed otherwise
    public class SingleWorkerQueue<T> : IWorkQueueHandle<T> where T : struct
    {
        private const int _DEFAULT_BUFFER_LENGTH = 1024;

        //must be a power of 2
        private readonly int _bufferLength = 1024;
        private readonly int _bufferLengthMask;
        private readonly T[] _ticksArray;
        private readonly IWorkQueueProducer<T> _producer;
        private readonly ILogger _logger;
        private long _lastWriteClaimedIndex = -1;
        private long _lastWritePublishedIndex = -1;
        private long _lastReadIndex = -1;
        private readonly ManualResetEventSlim _dataAvailableEvent = new ManualResetEventSlim(false);
        private readonly ManualResetEventSlim _dataReadEvent = null;
        private bool _done = false;
        private bool _writtingWrapped = false;
        private bool _writtingPausedResumed = false;
        private event Action<T> _consumeSingleItemAction;
        private bool _hasJustSingleThreadedProducer;

        private readonly string _identifier;
        public string Identity { get { return _identifier; } }

        public enum SingleWorkerQueueBehavior
        {
            AllInputDataMustBePumped_BlockingIsAllowed,
            BlockingIsNotAllowed_DataCanBeThrownAway
        }

        /// <summary>
        /// Ticks queue, serializing multithreaded producers and invoking events on consumer on a single thread
        /// </summary>
        /// <param name="producer">Multithreaded producer producing ticks</param>
        /// <param name="hasJustSingleThreadedProducer">Indicating whether this queue has just single threaded producer</param>
        /// <param name="queueBehavior">queueu behavior</param>
        public SingleWorkerQueue(IWorkQueueProducer<T> producer, SingleWorkerQueueBehavior queueBehavior, bool hasJustSingleThreadedProducer, ILogger logger, string identifier)
            : this(producer, queueBehavior, hasJustSingleThreadedProducer, logger, _DEFAULT_BUFFER_LENGTH, identifier)
        { }

        /// <summary>
        /// Ticks queue, serializing multithreaded producers and invoking events on consumer on a single thread
        /// </summary>
        /// <param name="producer">Multithreaded producer producing ticks</param>
        /// <param name="consumer">Singlehreaded consumer receiving ticks</param>
        /// <param name="queueBehavior">queueu behavior</param>
        /// <param name="hasJustSingleThreadedProducer">Indicating whether this queue has just single threaded producer</param>
        /// <param name="bufferLength">Length of ring buffer; must be positive power of 2</param>
        public SingleWorkerQueue(IWorkQueueProducer<T> producer, SingleWorkerQueueBehavior queueBehavior, bool hasJustSingleThreadedProducer, ILogger logger, int bufferLength, string identifier)
        {
            if (!IsPowerOfTwo(bufferLength))
                throw new Exception("SingleWorkerQueue - length must be power of two");
            //This would be full Disruptor then
            // We need readers wait till the item is fully published, but publisher might increment writer index and then write in different order, so writerindexforreader cannot be easily set
            if(!hasJustSingleThreadedProducer)
                throw new Exception("Queue is not yet coded for multithreaded producers");
            this._bufferLength = bufferLength;
            this._bufferLengthMask = bufferLength - 1;
            this._ticksArray = new T[_bufferLength];
            this._hasJustSingleThreadedProducer = hasJustSingleThreadedProducer;
            this._identifier = identifier;

            if (queueBehavior == SingleWorkerQueueBehavior.AllInputDataMustBePumped_BlockingIsAllowed)
            {
                this._dataReadEvent = new ManualResetEventSlim(false);
            }
            
            this._producer = producer;
            this._logger = logger;

            Task.Factory.StartNew(ConsumingMethod, TaskCreationOptions.LongRunning)
                .ContinueWith(
                    t =>
                    {
                        this._logger.LogException(LogLevel.Fatal, t.Exception,
                            "Consuming of data from SingleWorkerQueue [{0}] aborted by unexpected exception - data won't be processed anymore",
                            _identifier);
                        this.CloseQueue();
                    }, TaskContinuationOptions.OnlyOnFaulted);

            //this is so that the worker thread can spin off before we'd try to subscribe to events and got the wrapping event
            Thread.Sleep(10);
        }

        private void PausePumpingData()
        {
            this._producer.NewWorkAvailable -= ProducerOnNewItemAvailable;
            _isPausingInProcess = 1;
            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(1), () =>
            {
                if (InterlockedEx.CheckAndFlipState(ref _isPausingInProcess, 0, 1))
                {
                    _writtingWrapped = true;
                }
            });
        }

        private int _isPausingInProcess = 0;
        public void ClearDiscontinuityFlag()
        {
            _writtingWrapped = false;
            _isPausingInProcess = 0;
        }

        private void ResumePumpingData()
        {
            _writtingPausedResumed = true;
            //quick resume - we are fine
            _isPausingInProcess = 0;
            this._producer.NewWorkAvailable += ProducerOnNewItemAvailable;
        }

        //public void ClearDiscontinuityFlag()
        //{
        //    _writtingWrapped = false;
        //}

        public void CloseQueue()
        {
            this._logger.Log(LogLevel.Info, "SingleWorkerQueue [{0}] close requested", _identifier);

            this._producer.NewWorkAvailable -= ProducerOnNewItemAvailable;
            this._done = true;
            this._dataAvailableEvent.Set();
        }

        //private int _numOfPendingProducersUpdatesInCurrentBatch;

        private void ProducerOnNewItemAvailable(T item)
        {
            long nextWriteIndexLocal =
                this._hasJustSingleThreadedProducer
                    ? ++_lastWriteClaimedIndex
                    : Interlocked.Increment(ref _lastWriteClaimedIndex);

            //Interlocked.Increment(ref this._numOfPendingProducersUpdatesInCurrentBatch);

            //Writing overflown
            if (nextWriteIndexLocal > _lastReadIndex + _bufferLength
                &&
                nextWriteIndexLocal > Interlocked.Read(ref _lastReadIndex) + _bufferLength)
            {
                if (this._dataReadEvent != null)
                {
                    //Wait to be able to write next item
                    while (nextWriteIndexLocal >= Interlocked.Read(ref _lastReadIndex) + _bufferLength)
                    {
                        this._dataReadEvent.Wait();
                        this._dataReadEvent.Reset();
                    }
                }
                else
                {
                    //setting the whole buffer to empty - 'deleting' whole buffer (as opposed to skipping, where
                    //  we would need to change readers index. This way it's changed only from reader thread)
                    Interlocked.Exchange(ref _lastWriteClaimedIndex, nextWriteIndexLocal - _bufferLength);
                    _writtingWrapped = true;
                }
            }

            _ticksArray[nextWriteIndexLocal & _bufferLengthMask] = item;

            ////however her we might lost earlier updates with higher id; so solution would be locking
            //// because we cannot simply interlocked read the highest index - as it can change in the meantime 
            //if (Interlocked.Decrement(ref this._numOfPendingProducersUpdatesInCurrentBatch) == 0)

            _lastWritePublishedIndex = nextWriteIndexLocal;
            _dataAvailableEvent.Set();
        }

        private void ConsumingMethod()
        {
            do
            {
                //the Reset in previous cycle could mask the close signal
                if (!_done && !_writtingWrapped)
                    _dataAvailableEvent.Wait();
                _dataAvailableEvent.Reset();

                if (_writtingWrapped)
                {
                    _writtingWrapped = false;
                    if (_writtingPausedResumed)
                        this._logger.Log(LogLevel.Warn,
                            "SingleWorkerQueue [{0}] overflowing detected pausing-resuming of processing. This means processing will be forcefully missing some data", _identifier);
                    else
                        this._logger.Log(LogLevel.Fatal,
                            "SingleWorkerQueue [{0}] overflowing detected (and recovered) - this means processing is too slow and will be forcefully missing some data", _identifier);
                    _writtingPausedResumed = false;
                    this.OnWrittingWrappedAndPreviousDataMightBeInconsistent();
                    //force refresh write idx
                    Interlocked.Read(ref _lastWritePublishedIndex);
                }

                //can be just read from local cache - as event waiting will force caches resync
                long lastWritePublishedIndexLocal = _lastWritePublishedIndex;              
                while (_lastReadIndex < lastWritePublishedIndexLocal)
                {
                    //need to refresh this for each item - as subscription can change between two items
                    var consumeSingleItemActionLocal = this._consumeSingleItemAction;

                    if (consumeSingleItemActionLocal != null)
                        consumeSingleItemActionLocal(_ticksArray[(_lastReadIndex + 1) & _bufferLengthMask]);
                    _lastReadIndex++;

                    //if in blocking mode, signal that there are no queued data for each datapoint
                    // this is to push data further the pipeline immediately
                    if (this._dataReadEvent != null)
                        this.OnNoQueuedData();
                }

                //if in blocking mode, signal that some capacity was freed
                if (this._dataReadEvent != null)
                    this._dataReadEvent.Set();

                //This should be read in interlocked call. However, that would degrade performance of all reads,
                // so we rather allow posibility of NoQueuedData being called after overwritte happened, but it'll
                // be immediately followed by WrittingWrappedAndPreviousDataMightBeInconsistent call
                if (_lastWritePublishedIndex >= lastWritePublishedIndexLocal && !_writtingWrapped
                    &&
                    //We already called this for each item if in blocking mode
                    this._dataReadEvent == null)
                    this.OnNoQueuedData();

            } while (!_done);

            this._logger.Log(LogLevel.Info, "SingleWorkerQueue [{0}] stopped pumping data to consumer", _identifier);

            this.OnQueueClosedWithNoMoreData();
        }

        private static bool IsPowerOfTwo(long x)
        {
            return (x & (x - 1)) == 0;
        }

        public void OverwriteConsumingMethod(Action<T> oldConsumeSingleItemAction, Action<T> newConsumeSingleItemAction)
        {
            this.SingleItemAvailable += newConsumeSingleItemAction;
            this.SingleItemAvailable -= oldConsumeSingleItemAction;
        }

        public event Action<T> SingleItemAvailable
        {
            add
            {
                bool needsResume = this._consumeSingleItemAction == null;

                this._consumeSingleItemAction -= value;
                this._consumeSingleItemAction += value;

                if (needsResume)
                {
                    //System.Diagnostics.Debug.WriteLine("RESUMING");
                    this._logger.Log(LogLevel.Trace, "Resuming the SingleWorkQueue [{0}] as at least one consumer is subscribed", _identifier);
                    this.ResumePumpingData();
                }
            }

            remove
            {
                this._consumeSingleItemAction -= value;

                if (this._consumeSingleItemAction == null)
                {
                    this._logger.Log(LogLevel.Trace, "Pausing SingleWorkerQueue [{0}] as none consumer is subscribed", _identifier);
                    this.PausePumpingData();
                }
            }
        }

        public event Action NoQueuedData;

        private void OnNoQueuedData()
        {
            if (this.NoQueuedData != null)
                this.NoQueuedData();
        }

        public event Action WrittingWrappedAndPreviousDataMightBeInconsistent;

        private void OnWrittingWrappedAndPreviousDataMightBeInconsistent()
        {
            if (this.WrittingWrappedAndPreviousDataMightBeInconsistent != null)
                this.WrittingWrappedAndPreviousDataMightBeInconsistent();
        }

        public event Action QueueClosedWithNoMoreData;
        
        private void OnQueueClosedWithNoMoreData()
        {
            if (this.QueueClosedWithNoMoreData != null)
                this.QueueClosedWithNoMoreData();
        }

        ~SingleWorkerQueue()
        {
            this._logger.Log(LogLevel.Warn, "WorkerQueue [{0}] disposed", this._identifier);
        }

        public event Action DataDiscontinuity
        {
            add { this._producer.DataDiscontinuity += value; }
            remove { this._producer.DataDiscontinuity -= value; }
        }
    }
}

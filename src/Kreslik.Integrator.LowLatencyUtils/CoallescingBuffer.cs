﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public interface ICoallescingItem
    {
        //For Venue ToBs (Cpt = Cpt - BanpoolsCnt) - Cpt*(Symbols*2) + Symbol + side*Symbols
        //For BanpoolTob - cpt = 0
        //For BankPool All prices - ?
        //Ticker
        int CoallescingIndex { get; }
    }

    public interface IBlockingBuffer<T>
    {
        void CompleteAdding();
        int Count { get; }
        int DroppedItems { get; }
        void Add(T item);
        event Action<Exception> ExceptionDuringDispatchingOccured;
    }

    public interface ICoallescingBlockingBuffer<T> : IBlockingBuffer<T> where T : class, ICoallescingItem
    { }

    public abstract class BlockingBufferBase<T>
    {
        private bool _usePriceObjectsPooling;
        private Action<T> _itemConsumingAction;
        protected ILogger _logger;
        private string _identity;

        protected BlockingBufferBase(Action<T> itemConsumingAction, bool usePriceObjectsPooling, string identity, ILogger logger)
        {
            this._itemConsumingAction = itemConsumingAction;
            this._usePriceObjectsPooling = usePriceObjectsPooling;
            this._logger = logger;
            this._identity = identity;
        }


        protected void CompleteAddingInternal()
        {
            //prevent double releasing
            _itemConsumingAction = this.NoOp;
        }

        private void NoOp(T item) { }

        protected void ReleaseItemIfNeeded(T item)
        {
            if (this._usePriceObjectsPooling)
            {
                IPooledObject pooledObject = item as IPooledObject;
                if (pooledObject != null)
                    pooledObject.Release();
            }
        }

        protected void AcquireItemIfNeeded(T item)
        {
            if (this._usePriceObjectsPooling)
            {
                IPooledObject pooledObject = item as IPooledObject;
                if (pooledObject != null)
                    pooledObject.Acquire();
            }
        }

        protected void ItemConsumingActionWrapped(T item)
        {
            try
            {
                this._itemConsumingAction(item);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e,
                                                      "ClientGatewayEventsDispatcher {0} experienced unhandled exception",
                                                      this._identity);

                if (ExceptionDuringDispatchingOccured != null)
                {
                    try
                    {
                        ExceptionDuringDispatchingOccured(e);
                    }
                    catch (Exception ex)
                    {
                        this._logger.LogException(LogLevel.Fatal, ex,
                                                                "ClientGatewayEventsDispatcher experienced unhandled exception - during handling another unhandled exception");
                    }
                }
            }

            ReleaseItemIfNeeded(item);
        }

        public event Action<Exception> ExceptionDuringDispatchingOccured;
    }

    public class CoallescingBlockingBuffer<T> : BlockingBufferBase<T>, ICoallescingBlockingBuffer<T> where T : class, ICoallescingItem
    {
        private BlockingCollection<int> _itemIndexes = new BlockingCollection<int>(new ConcurrentQueue<int>());
        private T[] _items;

        public CoallescingBlockingBuffer(int maxIndex, Action<T> itemConsumingAction, bool usePriceObjectsPooling, string identity, ILogger logger)
            :base(itemConsumingAction, usePriceObjectsPooling, identity, logger)
        {
            _items = new T[maxIndex];
            Task.Factory.StartNew(ProcessItems, TaskCreationOptions.LongRunning).ObserveException();
        }

        public void CompleteAdding()
        {
            base.CompleteAddingInternal();
            _itemIndexes.CompleteAdding();
        }

        public int Count { get { return _itemIndexes.Count; } }

        public int DroppedItems { get; private set; }

        public void Add(T item)
        {
            int idx = item.CoallescingIndex;
            if (idx < 0 || idx > _items.Length)
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Remoting item cannot be processed as index is unexpected [{0}]: {1}" + idx, item);
                return;
            }

            AcquireItemIfNeeded(item);

            T itemOverwritten = Interlocked.Exchange(ref _items[item.CoallescingIndex], item);
            if (itemOverwritten == null)
            {
                _itemIndexes.Add(item.CoallescingIndex);
            }
            else
            {
                this.DroppedItems++;
                ReleaseItemIfNeeded(itemOverwritten);
            }
        }

        private void ProcessItems()
        {
            foreach (int itemIndex in _itemIndexes.GetConsumingEnumerable())
            {
                T item = Interlocked.Exchange(ref _items[itemIndex], null);
                if (item != null)
                {
                    this.ItemConsumingActionWrapped(item);
                }
                else
                {
                    //ERROR!
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Remoting item null - unexpected. Index processed: " + itemIndex);
                }
            }
        }
    }


    public class OrderedBlockingBuffer<T> : BlockingBufferBase<T>, IBlockingBuffer<T>
    {
        private BlockingCollection<T> _items = new BlockingCollection<T>(new ConcurrentQueue<T>());

        public OrderedBlockingBuffer(Action<T> itemConsumingAction, bool usePriceObjectsPooling, string identity, ILogger logger)
            :base(itemConsumingAction, usePriceObjectsPooling, identity, logger)
        {
            Task.Factory.StartNew(ProcessItems, TaskCreationOptions.LongRunning);
        }

        public void CompleteAdding()
        {
            base.CompleteAddingInternal();
            _items.CompleteAdding();
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public int DroppedItems { get { return 0; } }

        public void Add(T item)
        {
            AcquireItemIfNeeded(item);
            _items.Add(item);
        }

        private void ProcessItems()
        {
            foreach (T item in _items.GetConsumingEnumerable())
            {
                base.ItemConsumingActionWrapped(item);
            }
        }
    }
    
}

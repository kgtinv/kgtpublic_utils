﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public static class RandomizationUtils
    {
        //Source:
        //http://stackoverflow.com/questions/22336451/how-can-i-use-rngcryptoserviceprovider-to-create-a-random-password-based-on-my-p
        public class CryptoRandom : Random
        {
            private RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();
            private byte[] _uint32Buffer = new byte[4];
            public CryptoRandom() { }
            public CryptoRandom(Int32 ignoredSeed) { }
            public override Int32 Next()
            {
                _rng.GetBytes(_uint32Buffer);
                return BitConverter.ToInt32(_uint32Buffer, 0) & 0x7FFFFFFF;
            }
            public override Int32 Next(Int32 maxValue)
            {
                if (maxValue < 0) throw new ArgumentOutOfRangeException("maxValue");
                return Next(0, maxValue);
            }
            public override Int32 Next(Int32 minValue, Int32 maxValue)
            {
                if (minValue > maxValue) throw new ArgumentOutOfRangeException("minValue");
                if (minValue == maxValue) return minValue;
                Int64 diff = maxValue - minValue;
                while (true)
                {
                    _rng.GetBytes(_uint32Buffer);
                    UInt32 rand = BitConverter.ToUInt32(_uint32Buffer, 0);
                    Int64 max = (1 + (Int64)UInt32.MaxValue);
                    Int64 remainder = max % diff;
                    if (rand < max - remainder)
                    {
                        return (Int32)(minValue + (rand % diff));
                    }
                }
            }
            public override double NextDouble()
            {
                _rng.GetBytes(_uint32Buffer);
                UInt32 rand = BitConverter.ToUInt32(_uint32Buffer, 0);
                return rand / (1.0 + UInt32.MaxValue);
            }
            public override void NextBytes(byte[] buffer)
            {
                if (buffer == null) throw new ArgumentNullException("buffer");
                _rng.GetBytes(buffer);
            }
        }

        public class SequenceCountingRandom : Random
        {
            public SequenceCountingRandom()
                :base()
            { }

            public SequenceCountingRandom(int seed)
                :base(seed)
            { }

            public int CallsCount { get; private set; }

            public void ResetCallsCount()
            {
                CallsCount = 0;
            }

            public override Int32 Next()
            {
                CallsCount++;
                return base.Next();
            }
            public override Int32 Next(Int32 maxValue)
            {
                CallsCount++;
                return base.Next(maxValue);
            }
            public override Int32 Next(Int32 minValue, Int32 maxValue)
            {
                CallsCount++;
                return base.Next(minValue, maxValue);
            }
            public override double NextDouble()
            {
                CallsCount++;
                return base.NextDouble();
            }
            public override void NextBytes(byte[] buffer)
            {
                CallsCount++;
                base.NextBytes(buffer);
            }
        }

        public class RandomWrapper : Random
        {
            private Random _rnd;
            private Random _checkpointStateRandom;
            private int? _reproducibleSeed = null;

            public RandomWrapper(Random rnd)
            {
                _rnd = rnd;
            }

            public void StartSeparateRandomSequence()
            {
                _checkpointStateRandom = _checkpointStateRandom ?? _rnd;
                _rnd = _reproducibleSeed == null ? new Random() : new Random(_reproducibleSeed.Value);
            }

            public void StopSeparateRandomSequence()
            {
                if (_checkpointStateRandom != null)
                {
                    _rnd = _checkpointStateRandom;
                }
                _checkpointStateRandom = null;
            }

            public void SetReproducibleRandomSeed(int seed)
            {
                _rnd = new Random(seed);
                _reproducibleSeed = seed;
            }

            public override Int32 Next()
            {
                return _rnd.Next();
            }
            public override Int32 Next(Int32 maxValue)
            {
                return _rnd.Next(maxValue);
            }
            public override Int32 Next(Int32 minValue, Int32 maxValue)
            {
                return _rnd.Next(minValue, maxValue);
            }
            public override double NextDouble()
            {
                return _rnd.NextDouble();
            }
            public override void NextBytes(byte[] buffer)
            {
                _rnd.NextBytes(buffer);
            }
        }

        public class SeparateSequenceController: IDisposable
        {
            public SeparateSequenceController()
            {
                StartSeparateRandomSequence();
            }

            public void Dispose()
            {
                StopSeparateRandomSequence();
            }
        }

        public static IDisposable CreateSeparateRandomSequenceBlock()
        {
            return new SeparateSequenceController();
        }

        public static void StartSeparateRandomSequence()
        {
            _rnd.StartSeparateRandomSequence();
        }

        public static void StopSeparateRandomSequence()
        {
            _rnd.StopSeparateRandomSequence();
        }


        private static readonly RandomWrapper _rnd = new RandomWrapper(new CryptoRandom());
        public static Random Random => _rnd;

        //public static void SetReproducibleRandom(Random random) { Random = random; }
        public static void SetReproducibleRandomSeed(int seed)
        {
            _rnd.SetReproducibleRandomSeed(seed);
        }

        public static IReadOnlyList<int> CreateFisherYatesShuffleSequence(int exclusiveUpperBound)
        {
            return CreateFisherYatesShuffleSequence(0, exclusiveUpperBound);
        }

        //Simpliest approach
        //However - beware of this approach!! - it is highly biased (towards smaller numbers)
        //public static IReadOnlyList<int> CreateRandomShuffeledSequence(int inclusiveLowerBound, int exclusiveUpperBound)
        //{
        //    Random rand = new Random(RandomizationUtils.Random.Next());
        //    return
        //        Enumerable.Range(inclusiveLowerBound, exclusiveUpperBound - inclusiveLowerBound)
        //            .OrderBy(x => rand.Next())
        //            .ToArray();
        //}


        public static IReadOnlyList<int> CreateFisherYatesShuffleSequence(int inclusiveLowerBound, int exclusiveUpperBound)
        {
            int[] array = Enumerable.Range(inclusiveLowerBound, exclusiveUpperBound - inclusiveLowerBound).ToArray();
            Random rand = Random;

            for (int unshuffeledIndex = 0; unshuffeledIndex < exclusiveUpperBound - 1; unshuffeledIndex++)
            {
                int swapIndex = rand.Next(unshuffeledIndex, exclusiveUpperBound);
                array.Swap(unshuffeledIndex, swapIndex);
            }

            return array;
        }

        public static void Swap(this IList<int> list, int indexA, int indexB)
        {
            int tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        public static void Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        public static IList<T> PerformFisherYatesShuffle<T>(this IList<T> list)
        {
            Random rand = Random;

            for (int unshuffeledIndex = 0; unshuffeledIndex < list.Count - 1; unshuffeledIndex++)
            {
                int swapIndex = rand.Next(unshuffeledIndex, list.Count);
                list.Swap(unshuffeledIndex, swapIndex);
            }

            return list;
        }

        public static IReadOnlyList<T> PerformRandomSelection<T>(this IReadOnlyList<T> list)
        {
            return PerformRandomSelection(list, list.Count);
        }

        public static IReadOnlyList<T> PerformRandomSelection<T>(this IReadOnlyList<T> list, int resultSize)
        {
            return
                Enumerable.Repeat(list.Count, resultSize).Select(i => list[RandomizationUtils.Random.Next(i)]).ToList();
        }

        public static IReadOnlyList<T> PerformBlockRandomization<T>(this IReadOnlyList<T> list, int minibatchSize,
            bool aligned, bool shuffle, out IReadOnlyList<int> blockSizes)
        {
            return
                PerformBlockRandomization(list.Count, minibatchSize, aligned, shuffle, out blockSizes)
                    .Select(i => list[i])
                    .ToList();
        }

        public static IReadOnlyList<int> PerformBlockRandomization(
            int indexesCount, 
            int minibatchSize, 
            bool aligned, 
            bool shuffle,
            out IReadOnlyList<int> blockSizes)
        {
            int blocksCount;
            List<int> blockStarts;

            if (aligned)
            {
                //aligned radnomization
                blocksCount = (indexesCount - 1) / minibatchSize + 1;
                blockStarts =
                    Enumerable.Range(0, blocksCount).Select(blockId => blockId * minibatchSize).ToList();
            }
            else
            {
                //nonaligned randomization
                blocksCount = indexesCount + minibatchSize - 1;
                blockStarts =
                    Enumerable.Range(-(minibatchSize - 1), indexesCount - 1).ToList();
            }

            //now perform shuffle or random select
            if (shuffle)
            {
                blockStarts.PerformFisherYatesShuffle();
            }
            else
            {
                blockStarts = blockStarts.PerformRandomSelection(blocksCount).ToList();
            }

            return blockStarts.ExpandBlocksFromStartIndexes(minibatchSize, 0, indexesCount - 1, indexesCount, out blockSizes);
        }

        public static IReadOnlyList<int> ExpandBlocksFromStartIndexes(this IReadOnlyList<int> blockStartIndexes,
            int blockSize, int inclusiveMinIndex, int inclusiveMaxIndex, int maxItems, out IReadOnlyList<int> blockSizes)
        {
            List<int> expandedIndexes = new List<int>(blockStartIndexes.Count * blockSize);
            List<int> blockSizesLocal = new List<int>(blockStartIndexes.Count);

            foreach (int blockStartIndex in blockStartIndexes)
            {
                int itemsAdded = 0;
                for (int idx = Math.Max(inclusiveMinIndex, blockStartIndex); idx < Math.Min(inclusiveMaxIndex + 1, blockStartIndex + blockSize); idx++)
                {
                    if (expandedIndexes.Count >= maxItems)
                    {
                        break;
                    }
                    expandedIndexes.Add(idx);
                    itemsAdded++;
                }
                if (itemsAdded > 0)
                {
                    blockSizesLocal.Add(itemsAdded);
                }
            }

            blockSizes = blockSizesLocal;
            return expandedIndexes;
        }

        public static void PerformSynchronousFisherYatesShuffle<T>(IList<T> list1, IList<T> list2)
        {
            if (list1.Count != list2.Count)
            {
                throw new Exception(string.Format("Cannot shuffle given lists in parallel as they contain different number of items ({0} vs {1})",
                    list1.Count, list2.Count));
            }

            Random rand = Random;

            for (int unshuffeledIndex = 0; unshuffeledIndex < list1.Count - 1; unshuffeledIndex++)
            {
                int swapIndex = rand.Next(unshuffeledIndex, list1.Count);
                list1.Swap(unshuffeledIndex, swapIndex);
                list2.Swap(unshuffeledIndex, swapIndex);
            }
        }

        //we need this static - so we do not define new and new randoms that in quick succession (<15ms) can have same seed
        private static readonly Random _alternateSequenceRandom = new CryptoRandom();
        /// <summary>
        /// Generates random string of choosen length with characters from: {[A-Z], [a-z], [0-9], '+', '=', '_'}
        ///  (so Base64 encoding just with '/' replaced by '_')
        /// </summary>
        /// <param name="length">length of string to be generated</param>
        /// <returns>Random string of chosen length</returns>
        public static string CreateRandomHash(int length)
        {
            const int eachStringCharEncodesBites = 6; // 2^6 = 64
            const int eachByteHasBits = 8;
            const double bytesNumNeededForSingleStringChar = eachStringCharEncodesBites / (double) eachByteHasBits;

            int randomBytesNeeded = (int) Math.Ceiling(length * bytesNumNeededForSingleStringChar);

            byte[] randomBytes = new byte[randomBytesNeeded];
            /*RandomizationUtils.Random*/
            _alternateSequenceRandom.NextBytes(randomBytes);
            //Base64: A-Z a-z 0-9 +, /, =
            //We are replacing '/' to get valid path
            var randomBase64String = Convert.ToBase64String(randomBytes).Replace('/','_');
            return randomBase64String.Substring(0, length);
        }
    }
}

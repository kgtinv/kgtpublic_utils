﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{

    public interface IBufferPool<T> : IExtendedObjectPool<T> where T : BufferSegmentItem
    {
        int SegmentSize { get; }
    }

    public class BufferPool : BufferPool<BufferSegmentItem>
    {
        public BufferPool(int segmentSize, int numberOfSegmentsToPool)
            : base(
                segmentSize, numberOfSegmentsToPool, PooledBufferSegmentItem.CreateNewPooled,
                PooledBufferSegmentItem.CreateNewUnpooled, PooledBufferSegmentItem.IsPooled,
                PooledBufferSegmentItem.ClearBufferSegmentItem, "BufferPoolForDC")
        { }

        private class PooledBufferSegmentItem : BufferSegmentItem
        {
            internal static void ClearBufferSegmentItem(BufferSegmentItem item)
            {
                item.ClearContent();
                //this is not needed, but might help to 'fail fast'
                Array.Clear(item.Buffer, item.SegmentStart, item.SegmentSize);
            }

            internal static bool IsPooled(BufferSegmentItem item)
            {
                return item is PooledBufferSegmentItem;
            }

            internal static BufferSegmentItem CreateNewPooled(Byte[] buffer, int segmentStart, int segmentSize)
            {
                return new PooledBufferSegmentItem(buffer, segmentStart, segmentSize);
            }

            internal static BufferSegmentItem CreateNewUnpooled(int segmentSize)
            {
                return new BufferSegmentItem(new byte[segmentSize], 0, segmentSize);
            }

            private PooledBufferSegmentItem(Byte[] buffer, int segmentStart, int segmentSize)
                : base(buffer, segmentStart, segmentSize)
            {
            }
        }
    }

    public class BufferPool<T> : ObjectPoolExtender<T>, IBufferPool<T> where T: BufferSegmentItem
    {
        public BufferPool(int segmentSize, int numberOfSegmentsToPool, Func<byte[], int, int, T> pooledItemCreator,
            Func<int, T> unpooledItemCreator, Func<T, bool> isPooledItem, Action<T> invalidateAction, string bufferPoolName)
            : base(
                GetBasicObjectPool(segmentSize, numberOfSegmentsToPool, pooledItemCreator, invalidateAction, bufferPoolName),
                () => unpooledItemCreator(segmentSize),
                isPooledItem)
        {
            this.SegmentSize = segmentSize;
        }

        private static IObjectPool<T> GetBasicObjectPool(int segmentSize, int numberOfSegmentsToPool,
            Func<byte[], int, int, T> pooledItemCreator, Action<T> invalidateAction, string bufferPoolName)
        {
            var sharedBuffer = new byte[segmentSize*numberOfSegmentsToPool];
            return
                new GCFreeObjectPool<T>(
                    Enumerable.Range(0, numberOfSegmentsToPool)
                        .Select(
                            segmentId =>
                                pooledItemCreator(sharedBuffer, segmentId*segmentSize, segmentSize)),
                    invalidateAction, bufferPoolName);
        }

        public int SegmentSize { get; private set; }
    }

    public interface IExtendedObjectPool<T>
    {
        int NumberOfUnpooledItemsInUse { get; }
        int NumberOfPooledItemsInUse { get; }
        int TotalNumberOfUnpooledItemsCreated { get; }
        int TotalPoolSize { get; }
        T Fetch();
        int TryFetchBatch(T[] itemsToPopulate, int requestedBatchSize);
        void Return(T item);
    }

    public class ObjectPoolExtender<T> : IExtendedObjectPool<T>
    {
        private IObjectPool<T> _objectPool;
        private Func<T> _unpooledItemFactory;
        private Func<T, bool> _isPooledItem;
        private int _numberOfUnpooledItemsInUse = 0;
        private int _totalNumberOfUnpooledItemsCreated = 0;
        public ObjectPoolExtender(IObjectPool<T> objectPool, Func<T> unpooledItemFactory, Func<T, bool> isPooledItem)
        {
            this._objectPool = objectPool;
            this._unpooledItemFactory = unpooledItemFactory;
            this._isPooledItem = isPooledItem;
        }

        public int NumberOfUnpooledItemsInUse { get { return this._numberOfUnpooledItemsInUse; } }
        public int NumberOfPooledItemsInUse { get { return this._objectPool.NumberOfUsedItems; } }
        public int TotalNumberOfUnpooledItemsCreated { get { return this._totalNumberOfUnpooledItemsCreated; } }

        public int TotalPoolSize
        {
            get { return this._objectPool.TotalPoolSize; }
        }

        public T Fetch()
        {
            T item;
            if (!this._objectPool.TryFetch(out item))
            {
                //create a new one
                item = this._unpooledItemFactory();
                Interlocked.Increment(ref this._numberOfUnpooledItemsInUse);
                Interlocked.Increment(ref this._totalNumberOfUnpooledItemsCreated);
            }

            return item;
        }

        public int TryFetchBatch(T[] itemsToPopulate, int requestedBatchSize)
        {
            int returnedBatchSize;
            if ((returnedBatchSize = this._objectPool.TryFetchBatch(itemsToPopulate, requestedBatchSize)) == 0)
            {
                returnedBatchSize++;
                itemsToPopulate[0] = this._unpooledItemFactory();
                Interlocked.Increment(ref this._numberOfUnpooledItemsInUse);
                Interlocked.Increment(ref this._totalNumberOfUnpooledItemsCreated);
            }

            return returnedBatchSize;
        }

        public void Return(T item)
        {
            if (this._isPooledItem(item))
            {
                //item.ClearContent();
                ////this is not needed, but might help to 'fail fast'
                //Array.Clear(item.Buffer, item.SegmentStart, this.SegmentSize);
                this._objectPool.Return(item);
            }
            else
            {
                Interlocked.Decrement(ref this._numberOfUnpooledItemsInUse);
                //not pooled item should be just thrown away
            }
        }
    }


    public interface IObjectPool<T>
    {
        /// <summary>
        /// Indicates whether the pool is empty.
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// Gets the number of items currently available in the pool.
        /// </summary>
        int NumberOfAvailableItems { get; }

        int TotalPoolSize { get; }

        /// <summary>
        /// Gets the number of items currently used from the pool.
        /// </summary>
        int NumberOfUsedItems { get; }

        bool TryFetch(out T item);

        int TryFetchBatch(T[] itemsToPopulate, int requestedBatchSize);

        T Fetch();
        void Return(T item);
    }


    [Serializable]
    public class ObjectPoolException : Exception
    {
        public enum ReasonStatus
        {
            PoolIsFull,
            PoolIsEmpty
        }

        public ObjectPoolException(ReasonStatus reason) { this.Reason = reason; }
        public ObjectPoolException(string message, ReasonStatus reason) : base(message) { this.Reason = reason; }
        public ObjectPoolException(string message, Exception inner, ReasonStatus reason) : base(message, inner) { this.Reason = reason; }
        protected ObjectPoolException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public ReasonStatus Reason { get; private set; }
    }

#if POOLS_INSTRUMENTATION
    [DataContract]
    //[KnownType(typeof(PriceUpdateEventArgs))]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public abstract class TimedObjectBase
    {
        public DateTime TimeStampUtc { get; private set; }


        public void Stamp()
        {
            this.TimeStampUtc = DateTime.UtcNow;
        }

        public void Clear()
        {
            this.TimeStampUtc = DateTime.MinValue;;
        }
    }
#endif

    //this needs to be external to the generic class as that one crate different type for different
    // type parameters and so have more static variables
    public static class PoolsCounter
    {
        public static int InstancesCount;
    }

    public class GCFreeObjectPool<T> : IObjectPool<T> 
#if POOLS_INSTRUMENTATION
//        where T : TimedObjectBase
#endif
    {
        private readonly Action<T> _releaseItemMethod;

        //LIFO usage is more friendly to caches - therefore stack
        private T[] _items;

#if POOLS_INSTRUMENTATION
        private List<T> _takenItems = new List<T>();
        //this can be used to capture source stack traces
        //private List<Tuple<T, StackTrace>> _takenItems2 = new List<Tuple<T, StackTrace>>();
        private SafeTimer _t;
        private string _poolName;
#endif

        //needs to be accessed thrue Interlocked methods to ensure memory bariers
        // Marking volatile wouldn't be enaugh (as _currentSize++ is translated to multiple instructions) 
        private int _currentSize = 0;

        private readonly int _initialSize;

        //Spin lock is a lock free user-mode synchronization mechanism
        // To avoid memory allocations and ABA problems (see eg. http://www.codeproject.com/Articles/190200/Locking-free-synchronization)
        // See comments inside TryFetch_WRONG for more detailed info
        private SpinLock _lock = new SpinLock(false);

        public GCFreeObjectPool(Func<T> factoryMethod, Action<T> releaseItemMethod, int initialSize, string poolName)
        {
            _releaseItemMethod = releaseItemMethod;

            _items = new T[initialSize];
            for (int i = 0; i < initialSize; i++)
            {
                _items[i] = factoryMethod();
            }
            _currentSize = initialSize;
            _initialSize = initialSize;

#if POOLS_INSTRUMENTATION
            _t = new SafeTimer(CheckTimestamps, TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(10));
            this._poolName = poolName + "_" + Interlocked.Increment(ref PoolsCounter.InstancesCount);
            LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "GCFreeObjectPool compiled with \'POOLS_INSTRUMENTATION\' - this will have severe impact on performance");            
#endif
        }

        public GCFreeObjectPool(IEnumerable<T> items, Action<T> releaseItemMethod, string poolName)
        {
            _releaseItemMethod = releaseItemMethod;

            _items = items.ToArray();

            _currentSize = _items.Length;
            _initialSize = _items.Length;

#if POOLS_INSTRUMENTATION
            _t = new SafeTimer(CheckTimestamps, TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(10));
            this._poolName = poolName + "_" + Interlocked.Increment(ref PoolsCounter.InstancesCount);
            LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "GCFreeObjectPool compiled with \'POOLS_INSTRUMENTATION\' - this will have severe impact on performance");
#endif
        }

        /// <summary>
        /// Provides information about pool emptiness
        /// This is concurrent method so it can be used only is information
        /// </summary>
        public bool IsEmpty
        {
            get { return Volatile.Read(ref _currentSize) == 0; }
        }

        /// <summary>
        /// Provides information about pool fulness
        /// This is concurrent method so it can be used only is information
        /// </summary>
        public bool IsFull
        {
            get { return Volatile.Read(ref _currentSize) == _initialSize; }
        }

        public int NumberOfAvailableItems
        {
            get { return Volatile.Read(ref _currentSize); }
        }

        public int NumberOfUsedItems
        {
            get { return _initialSize - Volatile.Read(ref _currentSize); }
        }

        public int TotalPoolSize { get { return _initialSize; } }

        /// <summary>
        /// Attempts to fetch next item from the pool, returns false if pool is empty
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool TryFetch(out T item)
        {
            //this read might be satisfied from the local cache
            int sizeLocal = this._currentSize - 1;

            if (sizeLocal < 0)
            {
                item = default(T);
                return false;
            }

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                sizeLocal = --this._currentSize;
                if (sizeLocal < 0)
                {
                    item = default(T);
                    this._currentSize++;
                    return false;
                }

                item = this._items[sizeLocal];
                //this is for fail fast reasons; however it's more performant not to perform writes after reads
                //this._items[sizeLocal] = default(T);

#if POOLS_INSTRUMENTATION
                MarkItemAsTaken(item);
#endif
            }
            finally
            {
                if (lockTaken)
                    _lock.Exit();
            }

            return true;
        }

        public int TryFetchBatch(T[] itemsToPopulate, int requestedBatchSize)
        {
            //this read might be satisfied from the local cache
            int sizeLocal = this._currentSize - 1;

            if (sizeLocal < 0)
            {
                return 0;
            }

            int returnedBatchSize;

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                if (this._currentSize < requestedBatchSize)
                    returnedBatchSize = this._currentSize;
                else
                    returnedBatchSize = requestedBatchSize;

                this._currentSize -= returnedBatchSize;

                Array.Copy(_items, this._currentSize, itemsToPopulate, 0, returnedBatchSize);

#if POOLS_INSTRUMENTATION
                foreach (T fetchedItem in itemsToPopulate.Take(returnedBatchSize))
                {
                    MarkItemAsTaken(fetchedItem);
                }
#endif

                //this is for fail fast reasons; however it's more performant not to perform writes after reads
                //Array.Clear(_items, _currentSize, returnedBatchSize);
            }
            finally
            {
                if (lockTaken)
                    _lock.Exit();
            }

            return returnedBatchSize;
        }

        //public bool TryFetch_WRONG(out T item)
        //{
        //    int sizeLocal = this._currentSize - 1;

        //    if (sizeLocal < 0)
        //    {
        //        item = default(T);
        //        return false;
        //    }

        //    if (Interlocked.CompareExchange(ref this._currentSize, sizeLocal, sizeLocal + 1) == sizeLocal + 1)
        //    {
        //        //Here we are the only one accessing index sizeLocal...
        //        //UNLESS we hit ABA problem. E.g.:
        //        // We try to change index from 5 to 4 and access index 5. We are preempted thread B manage to change index to 4 start accessing index 5, 
        //        //  then it's preempted by thread C which pops - changing the index back to 5. When we are woken up, we see the index as 5 and thought nothing changed,
        //        //  we successfuly change it to 4 and start accessing index 5 - but now we are doing it concurrently with thread B! We are in race!!!
        //        // Since this is a stack (top of stack index moves back and forth) - we are in even higher risk to hit this

        //        //Conclusion: Serialized access is needed!

        //        //Options: - either use CompareExchange with bool _changingStack variable
        //        //         - or use SpinLock - SpinLock is more sofisticated version of CompareExchange strategy, and more readable!
        //        //         - during high contention it's actually better to use locking (not to waste CPU by just spinning)!!!
        //    }

        //}

        public T Fetch()
        {
            T item;

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                int sizeLocal = --this._currentSize;
                if (sizeLocal < 0)
                {
                    this._currentSize++;
                    throw new ObjectPoolException(ObjectPoolException.ReasonStatus.PoolIsEmpty);
                }

                item = this._items[sizeLocal];
                //this is for fail fast reasons; however it's more performant not to perform writes after reads
                //this._items[sizeLocal] = default(T);

#if POOLS_INSTRUMENTATION
                MarkItemAsTaken(item);
#endif
            }
            finally
            {
                if (lockTaken)
                    _lock.Exit();
            }
            
            return item;
        }

#if POOLS_INSTRUMENTATION
        private void MarkItemAsTaken(T item)
        {
            if (_takenItems.Any(taken => object.ReferenceEquals(taken, item)))
                throw new Exception("Double taken identical item");

//            item.Stamp();
            _takenItems.Add(item);

            //if (_takenItems2.Any(taken => object.ReferenceEquals(taken.Item1, item)))
            //    throw new Exception("Double taken identical item");

            //item.Stamp();
            //_takenItems2.Add(new Tuple<T, StackTrace>(item, new StackTrace()));
        }

        public void CheckTimestamps()
        {
            DateTime now = DateTime.UtcNow;
            TimeSpan[] stamps = new TimeSpan[_initialSize];
            int fetchedItemsCount = 0;

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                fetchedItemsCount = _initialSize - _currentSize;

                if(fetchedItemsCount != _takenItems.Count)
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Pool[{0}] has {1} fetched items, but {2} taken", this._poolName, fetchedItemsCount, _takenItems.Count);

                for (int i = 0; i < fetchedItemsCount; i++)
                {
//                    stamps[i] = now - _takenItems[i].TimeStampUtc;
                }
            }
            finally
            {
                if (lockTaken)
                    _lock.Exit();
            }

            if (fetchedItemsCount > 0)
            {
                stamps = stamps.Take(fetchedItemsCount).OrderByDescending(i => i).ToArray();
                LogLevel level = LogLevel.Info;
                if (stamps[0] > TimeSpan.FromMinutes(120))
                    level = LogLevel.Fatal;
                else if (stamps[0] > TimeSpan.FromMinutes(50))
                    level = LogLevel.Warn;

                LogFactory.Instance.GetLogger(null).Log(level, "Age of items in pool[{0}] - fetched {1}: {2}", this._poolName, fetchedItemsCount, String.Join(", ", stamps));
            }
            else
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Info, "Pool[{0}] has 0 fetched items", this._poolName);
            }
        }
#endif

        public void Return(T item)
        {
            if (_releaseItemMethod != null)
                _releaseItemMethod(item);

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                int sizeLocal = this._currentSize;
                if (sizeLocal == _initialSize)
                    throw new ObjectPoolException(ObjectPoolException.ReasonStatus.PoolIsFull);


#if POOLS_INSTRUMENTATION
                for (int i = sizeLocal-1; i >= 0; i--)
                {
                    if (object.ReferenceEquals(_items[i], item))
                    {
                        LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Double returning object to pool! - stacktrace: " + new StackTrace().ToString());
                        throw new Exception("Double returning object to pool!");
                    }                  
                }

 //               item.Clear();
                int removed = _takenItems.RemoveAll(taken => object.ReferenceEquals(taken, item));
                if(removed<=0)
                    throw new Exception("Returning unknown object to pool!");
                if(removed > 1)
                    throw new Exception("Returning item that was taken more than once!");
#endif

                this._items[sizeLocal] = item;
                this._currentSize++;
            }
            finally
            {
                if (lockTaken)
                    _lock.Exit();
            }
        }
    }





    /// <summary>
    /// GcFree pool build on ringbuffer - very similar concept to NDisruptor
    ///  However due to this being accessed by multiple writters and readers simultanously
    /// It is ~100x slower than SpinLock implementation
    /// Storing this code for future reference or perf tunning
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RingBufferPool<T> : IObjectPool<T> where T : class
    {
        private readonly Action<T> _releaseItemMethod;

        //LIFO usage is more friendly to caches - therefore stack
        private T[] _items;

        private long _currentClaimedIdx = 0;
        private long _currentRemovedIdx = 0;
        private long _availableIdx = 0;
        private long _nextReturnItemIdx = 0;

        private readonly long _initialSize;
        private readonly long _mask;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private T ItemAtIndex(long index)
        {
            return _items[index & _mask];
        }

        public T Fetch()
        {
            long localClaimedIdx = Interlocked.Increment(ref _currentClaimedIdx);

            if (localClaimedIdx > Interlocked.Read(ref _availableIdx))
            {
                Interlocked.Decrement(ref _currentClaimedIdx);
                throw new ObjectPoolException(ObjectPoolException.ReasonStatus.PoolIsEmpty);
            }

            //exchange not needed
            T val = Interlocked.Exchange(ref _items[localClaimedIdx & _mask], null);

            //Increment once all predecesors incremented
            var spinWait = default(SpinWait);
            while (Interlocked.CompareExchange(ref _currentRemovedIdx, localClaimedIdx, localClaimedIdx - 1) != localClaimedIdx - 1)
            {
                spinWait.SpinOnce();
            }

            return val;
        }

        public void Return(T item)
        {
            if (_releaseItemMethod != null)
                _releaseItemMethod(item);

            long localNextReturnItemIdx = Interlocked.Increment(ref _nextReturnItemIdx);

            if (localNextReturnItemIdx > Interlocked.Read(ref _currentClaimedIdx))
            {
                Interlocked.Decrement(ref _nextReturnItemIdx);
                throw new ObjectPoolException(ObjectPoolException.ReasonStatus.PoolIsFull);
            }

            var spinWait = default(SpinWait);
            while (Interlocked.Read(ref _currentRemovedIdx) + _initialSize < localNextReturnItemIdx)
            {
                spinWait.SpinOnce();
            }

            //exchange not needed
            Interlocked.Exchange(ref _items[localNextReturnItemIdx & _mask], item);


            while (Interlocked.CompareExchange(ref _availableIdx, localNextReturnItemIdx + _initialSize, localNextReturnItemIdx + _mask) != localNextReturnItemIdx + _mask)
            {
                spinWait.SpinOnce();
            }

        }

        private static bool IsPowerOfTwo(long x)
        {
            return (x & (x - 1)) == 0;
        }

        public RingBufferPool(Func<T> factoryMethod, Action<T> releaseItemMethod, int initialSize)
        {
            if (!IsPowerOfTwo(initialSize))
                throw new Exception("length must be power of two");

            _releaseItemMethod = releaseItemMethod;

            _items = new T[initialSize];
            for (int i = 0; i < initialSize; i++)
            {
                _items[i] = factoryMethod();
            }

            _initialSize = initialSize;
            _mask = _initialSize - 1;
            _availableIdx = initialSize;
        }

        public RingBufferPool(IEnumerable<T> items, Action<T> releaseItemMethod)
        {
            _releaseItemMethod = releaseItemMethod;

            _items = items.ToArray();

            _initialSize = _items.Length;
            _mask = _initialSize - 1;
            _availableIdx = _items.Length;

            if (!IsPowerOfTwo(_items.Length))
                throw new Exception("length must be power of two");
        }


        public bool IsEmpty
        {
            get { throw new NotImplementedException(); }
        }

        public int NumberOfAvailableItems
        {
            get { throw new NotImplementedException(); }
        }

        public int TotalPoolSize
        {
            get { throw new NotImplementedException(); }
        }

        public int NumberOfUsedItems
        {
            get { throw new NotImplementedException(); }
        }

        public bool TryFetch(out T item)
        {
            throw new NotImplementedException();
        }




        public int TryFetchBatch(T[] itemsToPopulate, int requestedBatchSize)
        {
            throw new NotImplementedException();
        }
    }
}

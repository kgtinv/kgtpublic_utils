﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public interface IWaitHandle
    {
        void WaitOne();
        bool WaitOne(TimeSpan span);
        bool WaitOne(int millisecondsTimeout);
        WaitHandle UnderlyingHandle { get; }
    }

    public interface IManualResetEvent: IWaitHandle, IDisposable
    {
        void Set();
        void Reset();
    }

    public class CrossAppDomainManualResetEvent: UncollectableMarshalByRefObject, IManualResetEvent
    {
        private readonly ManualResetEventSlim _mrses;
        private readonly ManualResetEvent _mrse;
        private IManualResetEvent _manualResetEventImplementation;

        public CrossAppDomainManualResetEvent(bool slim, bool initialState)
        {
            if(slim)
                _mrses = new ManualResetEventSlim(initialState);
            else
                _mrse = new ManualResetEvent(initialState);
        }

        public void WaitOne()
        {
            _mrses?.Wait();
            _mrse?.WaitOne();
        }

        public bool WaitOne(TimeSpan span)
        {
            return _mrses?.Wait(span) ?? _mrse.WaitOne(span);
        }

        public bool WaitOne(int millisecondsTimeout)
        {
            return _mrses?.Wait(millisecondsTimeout) ?? _mrse.WaitOne(millisecondsTimeout);
        }

        public void Dispose()
        {
            _mrses?.Dispose();
            _mrse?.Dispose();
        }

        public void Set()
        {
            _mrse?.Set();
            _mrses?.Set();
        }

        public void Reset()
        {
            _mrse?.Reset();
            _mrses?.Reset();
        }

        public WaitHandle UnderlyingHandle => _mrses?.WaitHandle ?? _mrse;
    }
}

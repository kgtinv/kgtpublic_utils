﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public static class DecimalUtils
    {
        private static Action<decimal, byte[]> GetDecimalBytes = GetDecimalConvertingMethod();

        private static Action<decimal, byte[]> GetDecimalConvertingMethod()
        {
            MethodInfo method = typeof(decimal).GetMethod("GetBytes", BindingFlags.NonPublic | BindingFlags.Static);

            var parameter1 = method.GetParameters().First();
            var parameter2 = method.GetParameters().Single(p => p != parameter1);

            var argument1 = Expression.Parameter(typeof(Decimal), "decimalArgument");
            var argument2 = Expression.Parameter(typeof(byte[]), "byteArrayArgument");

            var methodCall = Expression.Call(
                method,
                Expression.Convert(argument1, parameter1.ParameterType),
                Expression.Convert(argument2, parameter2.ParameterType)
                );

            Action<decimal, byte[]> GetDecimalBytes = Expression.Lambda<Action<decimal, byte[]>>(
                Expression.Convert(methodCall, typeof(void)),
                argument1, argument2
                ).Compile();

            return GetDecimalBytes;
        }

        public static Action<decimal, int[]> SetDecimalBits = GetDecimalSettingMethod();

        private static Action<decimal, int[]> GetDecimalSettingMethod()
        {
            MethodInfo method = typeof(decimal).GetMethod("SetBits", BindingFlags.NonPublic | BindingFlags.Instance);

            var parameter1 = method.GetParameters().First();
            //var parameter2 = method.GetParameters().Single(p => p != parameter1);
            var instance = Expression.Parameter(typeof(decimal), "instance");

            //var argument1 = Expression.Parameter(typeof(Decimal), "decimalArgument");
            var argument1 = Expression.Parameter(typeof(int[]), "intArrayArgument");

            var methodCall = Expression.Call(
                instance,
                method,
                Expression.Convert(argument1, parameter1.ParameterType)
                );

            Action<decimal, int[]> SetDecimalBits = Expression.Lambda<Action<decimal, int[]>>(
                Expression.Convert(methodCall, typeof(void)),
                instance, argument1
                ).Compile();

            return SetDecimalBits;
        }

        public static Func<int, int, int, int, decimal> CreateDecimalFromBits = GetDecimalCtorMethod();

        private static Func<int, int, int, int, decimal> GetDecimalCtorMethod()
        {
            ConstructorInfo constructor = typeof(decimal).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { typeof(int), typeof(int), typeof(int), typeof(int) }, null);

            var parameter1 = constructor.GetParameters().First();
            var parameter2 = constructor.GetParameters().First(p => p != parameter1);
            var parameter3 = constructor.GetParameters().First(p => p != parameter1 && p != parameter2);
            var parameter4 = constructor.GetParameters().First(p => p != parameter1 && p != parameter2 & p != parameter3);


            var argument1 = Expression.Parameter(typeof(int), "lo");
            var argument2 = Expression.Parameter(typeof(int), "mid");
            var argument3 = Expression.Parameter(typeof(int), "hi");
            var argument4 = Expression.Parameter(typeof(int), "flags");

            var methodCall = Expression.New(
                constructor,
                Expression.Convert(argument1, parameter1.ParameterType),
                Expression.Convert(argument2, parameter2.ParameterType),
                Expression.Convert(argument3, parameter3.ParameterType),
                Expression.Convert(argument4, parameter4.ParameterType)
                );

            Func<int, int, int, int, decimal> createDecimalFromBits = Expression.Lambda<Func<int, int, int, int, decimal>>(
                Expression.Convert(methodCall, typeof(decimal)),
                argument1, argument2, argument3, argument4
                ).Compile();

            return createDecimalFromBits;
        }


        // size up to 1 073 741 823 (inclusive) - it's using just 30 bits
        private const long MAX_ACCEPTED_SIZE = 1073741823;
        private const long MAX_ACCEPTED_SIZE_MASK = ~MAX_ACCEPTED_SIZE;

        // mantisa up to 31 bits
        private const int MAX_ACCEPTED_MANTISA_MASK = ~2147483647;
        private const long MAX_ACCEPTED_MANTISA_LONG = 2147483647;

        //max 3 bits
        private const uint MAX_ACCEPTED_EXPONENT_AND_SIGN_MASK = 0xfff8ffff;

        public static ulong ConvertPriceToAtomicValue(decimal price, decimal size)
        {
            //Composition of result:
            // 30 bits for size (means up to approximately 1MLD)
            // 31 bits for decimal mantisa (means up to approximately 10 digit places for prices > 1)
            //  3 bits for decimal exponent (means ability to expres 7 significant digits - however we are also constrained by mantisa)

            long sizeLong = (long)size;

            if ((sizeLong & MAX_ACCEPTED_SIZE_MASK) != 0)
                throw new TobConverterException("Large size");

            decimal normalizedPrice = price.Normalize();

            int[] priceBits = decimal.GetBits(normalizedPrice);

            if (priceBits[2] != 0 || priceBits[1] != 0 || (priceBits[0] & MAX_ACCEPTED_MANTISA_MASK) != 0)
                throw new TobConverterException("Large mantisa");

            if ((priceBits[3] & MAX_ACCEPTED_EXPONENT_AND_SIGN_MASK) != 0)
                throw new TobConverterException("Large exponent or negative number");

            ulong encodedValue;

            //first stuff the exponent and create space for mantisa (we shift just by 15 as exponent is already in higher byte - already shifted by 16. Together 31)
            encodedValue = (((ulong)priceBits[3]) << 15);

            //stuff the mantisa
            encodedValue += (ulong)priceBits[0];

            //create space for size
            encodedValue <<= 30;

            //stuff the size
            encodedValue += (ulong)sizeLong;

            return encodedValue;
        }

        //higher 3 bits
        private const ulong ENCODED_EXPONENT_MASK = 0xe000000000000000;

        public static void ConvertPriceFromAtomicValue(ulong atomicValue, out decimal price, out decimal size)
        {
            size = atomicValue & MAX_ACCEPTED_SIZE;

            byte scale = (byte)((atomicValue & ENCODED_EXPONENT_MASK) >> 61);
            int lowBits = (int)(((long)(atomicValue >> 30)) & MAX_ACCEPTED_MANTISA_LONG);

            //decimal is strunct so calling new causes stackalloc
            price = new decimal(lowBits, 0, 0, false, scale);
        }

        public static void ConvertPriceFromAtomicValue(ulong atomicValue, out AtomicDecimal price, out decimal size)
        {
            size = atomicValue & MAX_ACCEPTED_SIZE;

            byte scale = (byte)((atomicValue & ENCODED_EXPONENT_MASK) >> 61);
            int lowBits = (int)(((long)(atomicValue >> 30)) & MAX_ACCEPTED_MANTISA_LONG);

            //decimal is strunct so calling new causes stackalloc
            price = new AtomicDecimal(lowBits, scale);
        }

        //max decimal exponent is 28 - co we keep this to maximum with 5 bits
        //Rest of ulong (64 - 5 = 59 bits) are used for mantisa - up to full 17 decimal significant digits
        public static ulong CompressUnsignedDecimal(decimal d, byte[] sharedBuffer)
        {
            GetDecimalBytes(d, sharedBuffer);

            if ((sharedBuffer[7] & ~7) != 0
                ||
                sharedBuffer[8] != 0
                ||
                sharedBuffer[9] != 0
                ||
                sharedBuffer[10] != 0
                ||
                sharedBuffer[11] != 0)
                throw new DecimalUtilsException("Too large decimal to convert");

            if (sharedBuffer[15] != 0)
                throw new DecimalUtilsException("Negative number");

            ulong result;

            result = ((ulong)sharedBuffer[14]) << 59;

            result |= sharedBuffer[0];
            result |= (((ulong)sharedBuffer[1]) << 8);
            result |= (((ulong)sharedBuffer[2]) << 16);
            result |= (((ulong)sharedBuffer[3]) << 24);
            result |= (((ulong)sharedBuffer[4]) << 32);
            result |= (((ulong)sharedBuffer[5]) << 40);
            result |= (((ulong)sharedBuffer[6]) << 48);
            result |= ((((ulong)sharedBuffer[7]) & 7) << 56); //and just 3 lower bits of the seventh byte

            return result;
        }

        //higher 5 bits
        private const ulong ENCODED_EXPONENT_OF_SOLE_DECIMAL_MASK = 0xf800000000000000;
        public static decimal UncompressUnsignedDecimal(ulong l)
        {
            int lo, mid;
            byte scale = (byte)(l >> 59);

            lo = (int)l;
            mid = (int)((l & ~ENCODED_EXPONENT_OF_SOLE_DECIMAL_MASK) >> 32);

            return new decimal(lo, mid, 0, false, scale);
        }
    }

    [Serializable]
    public class DecimalUtilsException : Exception
    {
        public DecimalUtilsException() { }
        public DecimalUtilsException(string message) : base(message) { }
        public DecimalUtilsException(string message, Exception inner) : base(message, inner) { }
        protected DecimalUtilsException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}

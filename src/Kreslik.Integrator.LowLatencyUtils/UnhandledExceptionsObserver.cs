﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public class UnhandledExceptionsObserver: UncollectableMarshalByRefObject
    {
        public static readonly UnhandledExceptionsObserver Instance;

        static UnhandledExceptionsObserver()
        {
            //Careful!!
            // optimizer may optimize away initialization of unused field:
            // https://stackoverflow.com/questions/32885626/why-clr-optimizing-away-unused-static-field-with-initialization
            Instance = new UnhandledExceptionsObserver();
        }

        public static void SetupUnhandledErrorsObserver() { /* This is it - we just need to force static initialization */ }

        public event Action UnhandledException;

        private UnhandledExceptionsObserver()
        {
            SetupUnhandledHandlerForCurrentDomain();
            TaskScheduler.UnobservedTaskException +=
                (sender, exargs) =>
                    OnUnhandledException(exargs.Exception, UnhandledExceptionType.UnhandledTaskException);
        }

        private void SetupUnhandledHandlerForCurrentDomain()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            SetUpUnhandledHandlerForAppDomain(currentDomain);
        }

        public void SetUpUnhandledHandlerForAppDomain(AppDomain appDomain)
        {
            appDomain.UnhandledException += OnUnhandledAppDomainException;
        }

        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void OnUnhandledAppDomainException(object sender,
            UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            OnUnhandledException((Exception) unhandledExceptionEventArgs.ExceptionObject,
                unhandledExceptionEventArgs.IsTerminating
                    ? UnhandledExceptionType.UnhandledAppDomainException_Terminating
                    : UnhandledExceptionType.UnhandledAppDomainException_NonTerminating);
        }

        internal enum UnhandledExceptionType
        {
            UnhandledAppDomainException_Terminating,
            UnhandledAppDomainException_NonTerminating,
            UnhandledTaskException,
        }


        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void OnUnhandledException(Exception unhandledException, UnhandledExceptionType unhandledExceptionType)
        {
            string errorMessage;

            switch (unhandledExceptionType)
            {
                case UnhandledExceptionType.UnhandledTaskException:
                    errorMessage = "UNHANLED TASK EXCEPTION. This will not terminate the process, but state consistency should be verified";
                    break;
                case UnhandledExceptionType.UnhandledAppDomainException_NonTerminating:
                    errorMessage = "UNHANLED EXCEPTION. This might not terminate the process, but state consistency should be verified";
                    break;
                case UnhandledExceptionType.UnhandledAppDomainException_Terminating:
                default:
                    errorMessage = "UNHANDLED EXCEPTION TERMINATING THE PROCESS!!!";
                    break;
            }

            ILogger logger = _logger.Value;
            logger.LogException(LogLevel.Fatal, errorMessage, unhandledException);

            Console.WriteLine(unhandledException);
            UnhandledException.SafeInvoke(logger);

            logger.LogConsoleInfo("Waiting 3 seconds after unhandled error - to give chance to all buffers to flush");
            //LogFactory.ForceFlush();

            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(3));

            //DO NOT REMOVE!
            // otherwise .net can unwind the logger thread and the record might wanish without writting to log file
            logger.Log(LogLevel.Trace,  "All unhandled error info written above this line.");
        }

        private readonly Lazy<ILogger> _logger = new Lazy<ILogger>(() => LogFactory.Instance.GetLogger("UnhandledErrors"));
    }
}

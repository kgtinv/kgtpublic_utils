﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DecimalProxy
    {
        public readonly int flags;
        public readonly int hi;
        public readonly int lo;
        public readonly int mid;

    }

    public static class DecimalEx
    {
        public static DecimalProxy GetBits(this decimal d)
        {
            unsafe
            {
                return *(((DecimalProxy*)(&d)));
            }
        }

        /// <summary>
        /// Gets the number of significant digits after the fraction point (in decimal base)
        ///  if normalizeFirst is false then the function is about 5 times faster, but explicit
        ///  zeros after fraction point are also considered signficant. So
        ///  1.25m.NumberOfSignificantFractionDecimals(true) == 1.2500m.NumberOfSignificantFractionDecimals(true)
        ///  but
        ///  1.25m.NumberOfSignificantFractionDecimals(false) != 1.2500m.NumberOfSignificantFractionDecimals(false)
        /// </summary>
        /// <param name="value">Value to inspect</param>
        /// <param name="normalizeFirst">Indicates whether trialing zeros after fraction point should be removed first. (opertes ~5-times slower if specified)</param>
        /// <returns>Number of significant digits after the fraction point (in decimal base)</returns>
        public static int NumberOfSignificantFractionDecimals(this decimal value, bool normalizeFirst)
        {
            return (normalizeFirst ? value.Normalize() : value).GetBits().flags >> 16 & 0x7fff;
        }

        public static bool IsExactlyWholeNumber(this decimal d)
        {
            return d.NumberOfSignificantFractionDecimals(true) == 0;
        }

        private static decimal _sixteenZerosDecimal = 10000000000000000m; //16 zeros
        private static DecimalProxy _sixteenZerosDecimalBits = _sixteenZerosDecimal.GetBits();

        public static decimal DecimalPowerOf10(int exponent)
        {
            if (exponent == 0)
                return 1m;
            else if (exponent < 0)
                return new decimal(1, 0, 0, false, (byte) -exponent);
            else if (exponent < 8)
                return new decimal(10000000, 0, 0, false, (byte) -(exponent - 7));
            else if (exponent < 17)
                return new decimal(_sixteenZerosDecimalBits.lo, _sixteenZerosDecimalBits.mid,
                    _sixteenZerosDecimalBits.hi, false, (byte) -(exponent - 16));
            else
                throw new ArgumentException(string.Format("Unexpected exponent: {0}", exponent));
        }

        public static decimal CutDecimalPlaces(this decimal value, int maxNumberOfDecimals, bool cutZeroesAsWell = false)
        {
            //do not normalize if not necessary
            if (value.NumberOfSignificantFractionDecimals(!cutZeroesAsWell) > maxNumberOfDecimals)
            {
                decimal decimalsMovingMultiplier = DecimalPowerOf10(maxNumberOfDecimals);
                decimal cutValue = value < 0
                    ? decimal.Ceiling(value * decimalsMovingMultiplier)
                    : decimal.Floor(value * decimalsMovingMultiplier);
                value = cutValue / decimalsMovingMultiplier;

                if (cutZeroesAsWell)
                {
                    value = value.Normalize();
                }
            }

            return value;
        }

        public static readonly decimal Decimal_Epsilon = new decimal(1, 0, 0, false, 28);

        /// <summary>
        /// Checks whether decimal number can be considered a whole number (within a tolerance allowing fraction inefficiencies)
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static bool IsWholeNumber(this decimal d)
        {
            return IsWholeNumber(d, 100 * Decimal_Epsilon);
        }

        /// <summary>
        /// Checks whether decimal number can be considered a whole number (within a tolerance allowing fraction inefficiencies)
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static bool IsWholeNumber(this decimal d, decimal maxPrecision)
        {
            return Math.Abs(d - decimal.Round(d, 0, MidpointRounding.AwayFromZero)) < maxPrecision;
        }

        public static decimal AddMinimumIncrement(this decimal d)
        {
            return d.AddMinimumIncrement(true);
        }

        public static decimal SubtractMinimumIncrement(this decimal d)
        {
            return d.AddMinimumIncrement(false);
        }

        public static decimal AddMinimumIncrement(this decimal d, bool positiveIncrement)
        {
            if (positiveIncrement ? (d == decimal.MaxValue) : (d == decimal.MinValue))
            {
                throw new OverflowException($"Cannot increment decimal.{(positiveIncrement ? "MaxValue" : "MinValue")}");
            }

            d = d.Normalize();

            for (int scale = 28; scale >= 0; scale--)
            {
                decimal newValue = d + new decimal(1, 0, 0, !positiveIncrement, (byte)scale);
                if (newValue != d)
                {
                    return newValue;
                }
            }

            throw new Exception($"Cannot find minimal increment to {d}");
        }
    }

    public struct ShortAtomicDecimal
    {
        //6 decimal places behind decimal point
        private const ulong CONST_DIVISOR = 1000000;

        //higher 5 bits
        private const ulong MOST_SIGNIFICANT_BIT = 0x8000000000000000;

        private readonly ulong _convertedValue;

        public bool IsSignaled
        {
            get { return (_convertedValue & MOST_SIGNIFICANT_BIT) != 0; }
        }

        public ShortAtomicDecimal(decimal value, bool isSignaled)
        {
            _convertedValue = (ulong) (value*CONST_DIVISOR);
            if (isSignaled)
                _convertedValue |= MOST_SIGNIFICANT_BIT;
        }

        public ShortAtomicDecimal(AtomicDecimal value, bool isSignaled)
            :this(value.ToDecimal(), isSignaled)
        { }

        private ShortAtomicDecimal(ulong convertedValue)
        {
            this._convertedValue = convertedValue;
        }

        public decimal ToDecimal()
        {
            return ((decimal) (_convertedValue & ~MOST_SIGNIFICANT_BIT))/CONST_DIVISOR;
        }

        public long ToTransferableLongValue()
        {
            return (long)this._convertedValue;
        }

        public static ShortAtomicDecimal CreateFromTransferableLongValue(long value)
        {
            return new ShortAtomicDecimal((ulong)value);
        }
    }

    public struct AtomicPredictionInfo
    {
        public enum PredictionDirectionEnum
        {
            Unknown = 0,
            None = 1,
            Up = 2,
            Down = 3
        }

        public static PredictionDirectionEnum ToOpposite(PredictionDirectionEnum predictionDirection)
        {
            switch (predictionDirection)
            {
                case PredictionDirectionEnum.Up:
                    return PredictionDirectionEnum.Down;
                case PredictionDirectionEnum.Down:
                    return PredictionDirectionEnum.Up;
                default:
                    return predictionDirection;
            }
        }

        public AtomicPredictionInfo(DateTime predictionTimeUtc, PredictionDirectionEnum predictionDirection)
        {
            _convertedValue = (ulong) predictionTimeUtc.TimeOfDay.Ticks;

            ulong directionConvertedValue = ((ulong) predictionDirection) << _PREDICTION_DIRECTION_SHIFT_COUNT;
            _convertedValue |= directionConvertedValue;
        }

        private AtomicPredictionInfo(ulong convertedValue)
        {
            this._convertedValue = convertedValue;
        }

        //Not used
        private const ulong _MOST_SIGNIFICANT_BIT = 0x8000000000000000;
        private const ulong _SECOND_AND_THIRD_SIGNIFICANT_BIT = 0x6000000000000000;
        private const int _PREDICTION_DIRECTION_SHIFT_COUNT = 61;

        private readonly ulong _convertedValue;

        public DateTime PredictionTimeUtc
        {
            get
            {
                long storedTicksCount =
                    (long) (_convertedValue & ~_MOST_SIGNIFICANT_BIT & ~_SECOND_AND_THIRD_SIGNIFICANT_BIT);
                if (storedTicksCount == 0)
                {
                    //no value stored yet
                    return DateTime.MinValue;
                }
                DateTime now = DateTime.UtcNow;
                DateTime storedTime = DateTime.UtcNow.Date.AddTicks(storedTicksCount);
                //value is likely from another day; however give some space for dis-synchronised clocks
                return storedTime < now.AddMinutes(1) ? storedTime : DateTime.MinValue;
            }
        }

        public PredictionDirectionEnum PredictionDirection
        {
            get
            {
                return
                    (PredictionDirectionEnum)
                    ((_convertedValue & _SECOND_AND_THIRD_SIGNIFICANT_BIT) >> _PREDICTION_DIRECTION_SHIFT_COUNT);
            }
        }

        public long ToTransferableLongValue()
        {
            return (long)this._convertedValue;
        }

        public static AtomicPredictionInfo CreateFromTransferableLongValue(long value)
        {
            return new AtomicPredictionInfo((ulong)value);
        }
    }

    public struct AtomicDecimal
    {

        //1 MLD - 9 decimal places behind decimal point
        public const ulong CONST_DIVISOR = 1000000000;
        //1 MLD - 9 decimal places behind decimal point
        public const int CONST_SCALE = 9;
        //10 MLD - 10 decimal places before deciaml point
        public const ulong MAX_MANTISA = 10000000000;

        private readonly ulong _convertedValue;

        public static AtomicDecimal Zero = new AtomicDecimal(0);
        public static AtomicDecimal MaxValue = new AtomicDecimal(ulong.MaxValue);
        public static decimal MaxValueDecimal = (new AtomicDecimal(ulong.MaxValue)).ToDecimal();

        public decimal ToDecimal()
        {
            //decimal is struct so new results in stackalloca - no GC garbage
            return new decimal((int)_convertedValue, (int)(_convertedValue >> 32), 0, false, CONST_SCALE);
        }

        //public long ToTransferableLongValue()
        //{
        //    return (long) this._convertedValue;
        //}

        //public static AtomicDecimal CreateFromTransferableLongValue(long value)
        //{
        //    return new AtomicDecimal((ulong) value);
        //}


        public AtomicDecimal(decimal d)
            : this(d, false)
        { }

        public AtomicDecimal DivideByPowerOf2(int powerOf2)
        {
            return new AtomicDecimal(this._convertedValue >> powerOf2);
        }

        public AtomicDecimal MultiplyByPowerOf2(int powerOf2)
        {
            return new AtomicDecimal(this._convertedValue << powerOf2);
        }

        public AtomicDecimal(decimal d, bool normalizeFirst)
        {
            if (normalizeFirst)
                d = d.Normalize();

            if (d > MaxValueDecimal)
            {
                if (d == decimal.MaxValue)
                {
                    _convertedValue = ulong.MaxValue;
                    return;
                }
                throw new Exception("Too large number");
            }

            if (d < 0)
            {
                throw new Exception("Negative numbers are unsupported");
            }

            DecimalProxy dp = d.GetBits();

            int scale = dp.flags >> 16;
            
            if (scale > CONST_SCALE)
                throw new Exception("Too large scale");

            //BEWARE: just converting to ulong can create some mess in upper int (e.g. ulong ul = (ulong) -2109254592) - as conversion can silently overflow
            _convertedValue = ((((ulong)(uint)dp.mid) << 32) | ((ulong)(uint)dp.lo)) * (ulong)Math.Pow(10, CONST_SCALE - scale);
        }

        public static AtomicDecimal ConvertToAtomicDecimalCuttingExcessScale(decimal d)
        {
            return new AtomicDecimal(decimal.Round(d, CONST_SCALE, MidpointRounding.AwayFromZero), true);
        }

        public AtomicDecimal(int i)
        {
            if(i < 0)
                throw new Exception("Negative numbers are unsupported");

            _convertedValue = ((ulong) i) * CONST_DIVISOR;
        }

        internal AtomicDecimal(int mantisa, int exponent)
        {
            _convertedValue = (ulong)mantisa * (ulong)Math.Pow(10, CONST_SCALE - exponent);
        }

        internal AtomicDecimal(ulong convertedValue)
        {
            this._convertedValue = convertedValue;
        }


        public static bool operator ==(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue == d2._convertedValue;
        }

        public static bool operator !=(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue != d2._convertedValue;
        }

        public static bool operator <(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue < d2._convertedValue;
        }

        public static bool operator <=(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue <= d2._convertedValue;
        }

        public static bool operator >(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue > d2._convertedValue;
        }

        public static bool operator >=(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue >= d2._convertedValue;
        }

        public static AtomicDecimal operator -(AtomicDecimal d1, AtomicDecimal d2)
        {
            if(d1 < d2)
                throw new ArithmeticException(string.Format("AtomicDecimal cannot subtract larger ({0}) number from smaller ({1})", d2, d1));

            return new AtomicDecimal(d1._convertedValue - d2._convertedValue);
        }

        public static AtomicDecimal Difference(AtomicDecimal d1, AtomicDecimal d2)
        {
            if(d1 > d2)
                return new AtomicDecimal(d1._convertedValue - d2._convertedValue);
            else
                return new AtomicDecimal(d2._convertedValue - d1._convertedValue);
        }

        public static AtomicDecimal operator +(AtomicDecimal s1, AtomicDecimal s2)
        {
            return new AtomicDecimal(s1._convertedValue + s2._convertedValue);
        }

        public static AtomicDecimal operator +(AtomicDecimal s1, decimal d2)
        {
            if(d2 < 0)
                return s1 - AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(d2);
            else
                return s1 + AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(d2);
        }

        public static AtomicDecimal operator -(AtomicDecimal s1, decimal d2)
        {
            if (d2 < 0)
                return s1 + AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(d2);
            else
                return s1 - AtomicDecimal.ConvertToAtomicDecimalCuttingExcessScale(d2);
        }

        public static AtomicDecimal SafeAdd(AtomicDecimal s1, AtomicDecimal s2)
        {
            if (ulong.MaxValue - s1._convertedValue < s2._convertedValue)
                return AtomicDecimal.MaxValue;
            else
                return new AtomicDecimal(s1._convertedValue + s2._convertedValue); 
        }

        public static AtomicDecimal SafeSubtract(AtomicDecimal s1, AtomicDecimal s2, out AtomicDecimal remainder)
        {
            if (s2._convertedValue > s1._convertedValue)
            {
                remainder = new AtomicDecimal(s2._convertedValue - s1._convertedValue);
                return AtomicDecimal.Zero;
            }
            else
            {
                remainder = AtomicDecimal.Zero;
                return new AtomicDecimal(s1._convertedValue - s2._convertedValue);
            }
        }

        public AtomicDecimal SafeAdd(AtomicDecimal s2)
        {
            return AtomicDecimal.SafeAdd(this, s2);
        }

        public AtomicDecimal SafeSubtract(AtomicDecimal s2, out AtomicDecimal remainder)
        {
            return AtomicDecimal.SafeSubtract(this, s2, out remainder);
        }

        public static implicit operator AtomicDecimal(int value)
        {
            if (value == 0)
                return AtomicDecimal.Zero;

            return new AtomicDecimal(value);
        }

        public static int Compare(AtomicDecimal d1, AtomicDecimal d2)
        {
            //return Comparer<ulong>.Default.Compare(d1._convertedValue, d2._convertedValue);

            if (d1._convertedValue < d2._convertedValue)
                return -1;
            else if (d1._convertedValue > d2._convertedValue)
                return 1;
            else
                return 0;
        }

        public override string ToString()
        {
            return string.Format("Atomic decimal: {0} (stored as {1})", this.ToDecimal(), this._convertedValue);
        }

        public static AtomicDecimal Max(AtomicDecimal d1, AtomicDecimal d2, AtomicDecimal d3)
        {
            return d1._convertedValue > d2._convertedValue
                ? (d1._convertedValue > d3._convertedValue ? d1 : d3)
                : (d2._convertedValue > d3._convertedValue ? d2 : d3);
        }

        public static AtomicDecimal Max(AtomicDecimal d1, AtomicDecimal d2)
        {
            return d1._convertedValue > d2._convertedValue ? d1 : d2;
        }
    }

    public static class PriceCalculationUtilsEx
    {
        public static bool IsWorseOrEqualPrice(this AtomicDecimal priceA, AtomicDecimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA >= priceB;
            else
                return priceA <= priceB;
        }

        public static bool IsWorsePrice(this AtomicDecimal priceA, AtomicDecimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA > priceB;
            else
                return priceA < priceB;
        }

        public static bool IsBetterPrice(this AtomicDecimal priceA, AtomicDecimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA < priceB;
            else
                return priceA > priceB;
        }

        public static bool IsBetterOrEqualPrice(this AtomicDecimal priceA, AtomicDecimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA <= priceB;
            else
                return priceA >= priceB;
        }

        public static AtomicDecimal ImprovePrice(this AtomicDecimal priceA, AtomicDecimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA - increment;
            else
                return priceA + increment;
        }

        public static AtomicDecimal ImprovePrice(this AtomicDecimal priceA, decimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA - increment;
            else
                return priceA + increment;
        }

        public static AtomicDecimal DeterioratePrice(this AtomicDecimal priceA, AtomicDecimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA + increment;
            else
                return priceA - increment;
        }

        public static AtomicDecimal DeterioratePrice(this AtomicDecimal priceA, decimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA + increment;
            else
                return priceA - increment;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public static class ByteUtils
    {
        private const string baseCharsCaseSensitive =   "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        private const string baseCharsCaseInsensitive = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static readonly int[] baseCharCodes;

        static ByteUtils()
        {
            baseCharCodes = new int[127];
            for (int idx = 0; idx < 127; idx++)
            {
                baseCharCodes[idx] = -1;
            }
            int val = 0;
            foreach (char c in baseCharsCaseSensitive)
            {
                baseCharCodes[(int) c] = val++;
            }
        }

        public static String ConvertToBase(int num, bool caseSensitive)
        {
            string chars = caseSensitive ? baseCharsCaseSensitive : baseCharsCaseInsensitive;
            int nbase = chars.Length;

            int r;
            String newNumber = "";

            // in r we have the offset of the char that was converted to the new base
            do
            {
                r = num % nbase;
                newNumber = chars[r] + newNumber;
                num = num / nbase;
            } while (num > 0);

            return newNumber;
        }

        public static void ReverseFillBufferWithIntConvertedToBase(byte[] buffer, bool caseSensitive, int num, ref int lowIdxToStartFilling )
        {
            if(lowIdxToStartFilling >= buffer.Length)
                return;

            string chars = caseSensitive ? baseCharsCaseSensitive : baseCharsCaseInsensitive;
            int nbase = chars.Length;

            int r;

            // in r we have the offset of the char that was converted to the new base
            do
            {
                r = num % nbase;
                buffer[lowIdxToStartFilling++] = (byte) chars[r];
                num = num / nbase;
            } while (num > 0 && lowIdxToStartFilling < buffer.Length);
        }

        public static byte ConvertToBaseByte(int num, bool caseSensitive)
        {
            string chars = caseSensitive ? baseCharsCaseSensitive : baseCharsCaseInsensitive;

            if(num >= chars.Length)
                throw new Exception("Too large number to be converted to single base byte");

            return (byte) chars[num];
        }

        public static int ConvertFromBaseByte(byte @byte)
        {
            if(@byte >= 127)
                throw new Exception("Byte out of ranges allowed for base chars");

            int val = baseCharCodes[@byte];

            if(val == -1)
                throw new Exception("Byte is not recongnized base char");

            return val;
        }

        public static int ConvertFromBaseByte(byte higherByte, byte lowerByte, bool caseSensitive)
        {
            int nbase = caseSensitive ? baseCharsCaseSensitive.Length : baseCharsCaseInsensitive.Length;

            return ConvertFromBaseByte(higherByte)*nbase + ConvertFromBaseByte(lowerByte);
        }

        public static string ConvertToAsciiStringTrimingNulls(this byte[] byteArray)
        {
            //return System.Text.Encoding.ASCII.GetString(byteArray, 0, Math.Max(0, Array.IndexOf<byte>(byteArray, 0)));

            int endIdx = Array.IndexOf<byte>(byteArray, 0);
            if (endIdx < 0)
                endIdx = byteArray.Length;

            return System.Text.Encoding.ASCII.GetString(byteArray, 0, endIdx);
        }
    }
}

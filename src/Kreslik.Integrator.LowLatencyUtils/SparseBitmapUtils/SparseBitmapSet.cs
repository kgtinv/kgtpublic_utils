﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils.IndexesProvider;

namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils
{
    //Provider of copied object to assist thread safety
    [DataContract]
    [KnownType(typeof(SparseBitmap))]
    public class SparseBitmapSetProvider
    {
        [DataMember]
        private readonly ISparseBitmap[][] _sparseBitmaps;
        //[DataMember]
        //private readonly int _bitmapLength;
        [DataMember]
        private readonly int _metricsCount;

        public SparseBitmapSetProvider(ISparseBitmap[][] sparseBitmaps)
        {
            _sparseBitmaps = sparseBitmaps;
            int bitmapLength = sparseBitmaps[0][0].Length;
            _metricsCount = sparseBitmaps.Length;

            if (sparseBitmaps.SelectMany(a => a).Any(b => b.Length != bitmapLength))
            {
                throw new Exception($"All bitmaps expected to have same number of flags. However not all bitmaps have {bitmapLength} flags");
            }
        }

        public SparseBitmapSet CreateSparseBitmapSet()
        {
            return
                new SparseBitmapSet(
                    _sparseBitmaps.Select(bitmapArray => bitmapArray.Select(b => b.GetEnumerator()).ToArray()).ToArray(),
                    _metricsCount);
        }
    }

    //NOT THREAD SAFE!!
    public class SparseBitmapSet
    {
        //first index is metric, second is bucket
        //private readonly ISparseBitmap[][] _sparseBitmaps;
        private readonly ISparseBitmapEnumerator[][] _sparseBitmapEnumerators;
        private readonly int _metricsCount;

        public SparseBitmapSet(ISparseBitmap[][] sparseBitmaps)
        {
            //_sparseBitmaps = sparseBitmaps;
            _sparseBitmapEnumerators =
                sparseBitmaps.Select(bitmapArray => bitmapArray.Select(b => b.GetEnumerator()).ToArray()).ToArray();
            var bitmapLength = sparseBitmaps[0][0].Length;
            _metricsCount = sparseBitmaps.Length;

            if (sparseBitmaps.SelectMany(a => a).Any(b => b.Length != bitmapLength))
            {
                throw new Exception($"All bitmaps expected to have same number of flags. However not all bitmaps have {bitmapLength} flags");
            }
        }

        internal SparseBitmapSet(ISparseBitmapEnumerator[][] sparseBitmapEnumerators, int metricsCount)
        {
            _sparseBitmapEnumerators = sparseBitmapEnumerators;
            _metricsCount = metricsCount;
        }

        //
        // Goes through all data metric by metric; for each metric finds next lowest index greater or equal to current candidate
        //   if metric has that one, candidate is reset and searching as well
        //
        public int GetFirstMatchingIndex(IIndexesProvider indexesProvider)
        {
            return GetMatchingIndexes(indexesProvider).DefaultIfEmpty(-1).First();
        }

        //
        // Goes through all data metric by metric; for each metric finds next lowest index greater or equal to current candidate
        //   if metric has that one, candidate is reset and searching as well
        //
        public IEnumerable<int> GetMatchingIndexes(IIndexesProvider indexesProvider)
        {
            if (indexesProvider.RangesCount != _sparseBitmapEnumerators.Length)
            {
                throw new Exception(
                    $"Discrepancy in dimensions. There are {_sparseBitmapEnumerators.Length} bitmap groups; while ranges provider has {indexesProvider.RangesCount} ranges. All those should be equal");
            }

            for (int boundIdx = 0; boundIdx < indexesProvider.RangesCount; boundIdx++)
            {
                CircularInterval interval = indexesProvider.GetIndexes(boundIdx);
                for (byte bitmapIdx = interval.Interval.LowBound; bitmapIdx <= interval.Interval.HighBound; bitmapIdx++)
                {
                    bitmapIdx = interval.FixIndex(bitmapIdx);
                    _sparseBitmapEnumerators[boundIdx][bitmapIdx].Reset();
                }
            }

            int intersectionCandidate = 0;
            int metricsMeetingIntersectionCandidate = 0;
            do
            {
                for (int metricIdx = 0; metricIdx < _metricsCount; metricIdx++)
                {
                    int lowestIndexOfLogicalOr = int.MaxValue;
                    CircularInterval interval = indexesProvider.GetIndexes(metricIdx);
                    for (byte bucketIdx = interval.Interval.LowBound; bucketIdx <= interval.Interval.HighBound; bucketIdx++)
                    {
                        bucketIdx = interval.FixIndex(bucketIdx);
                        if (
                            _sparseBitmapEnumerators[metricIdx][bucketIdx].MoveToNextNonzeroDataIndexGreaterOrEqual(
                                intersectionCandidate))
                        {
                            lowestIndexOfLogicalOr = Math.Min(lowestIndexOfLogicalOr,
                                _sparseBitmapEnumerators[metricIdx][bucketIdx].CurrentIndex);
                        }
                    }
                    //current metric has not any more flags
                    if (lowestIndexOfLogicalOr == int.MaxValue)
                    {
                        yield break;
                    }
                    if (lowestIndexOfLogicalOr == intersectionCandidate)
                    {
                        metricsMeetingIntersectionCandidate++;
                        if (metricsMeetingIntersectionCandidate >= _metricsCount)
                        {
                            ulong intersectionWord = BitsHelper.AllOnes;
                            int metricIdxInner = 0;
                            for (; metricIdxInner < _metricsCount && intersectionWord != 0; metricIdxInner++)
                            {
                                ulong metricIndexesAdded = 0;
                                CircularInterval intervalInner = indexesProvider.GetIndexes(metricIdxInner);
                                for (byte bucketIdxInner = intervalInner.Interval.LowBound; bucketIdxInner <= intervalInner.Interval.HighBound; bucketIdxInner++)
                                {
                                    bucketIdxInner = intervalInner.FixIndex(bucketIdxInner);
                                    if (_sparseBitmapEnumerators[metricIdxInner][bucketIdxInner].CurrentIndex == intersectionCandidate)
                                    {
                                        metricIndexesAdded |=
                                            _sparseBitmapEnumerators[metricIdxInner][bucketIdxInner].CurrentValue;
                                    }
                                }
                                intersectionWord &= metricIndexesAdded;
                            }

                            //no early break for cycle
                            if (metricIdxInner == _metricsCount && intersectionWord != 0)
                            {
                                int setBitIdx = -1;
                                while ((setBitIdx = intersectionWord.GetFirstSetBitIdx(setBitIdx + 1)) != -1)
                                {
                                    yield return intersectionCandidate * BitsHelper.BitsInUlong + setBitIdx;
                                }
                            }

                            //there was no intersection on this index
                            intersectionCandidate++;
                            //no metric has yet this index
                            metricsMeetingIntersectionCandidate = 0;
                        }
                    }
                    else
                    {
                        //one metric already has this index
                        metricsMeetingIntersectionCandidate = 1;
                        intersectionCandidate = lowestIndexOfLogicalOr;
                    }
                }
                //we can do this forewer since once we have single metric with no more dirty words - we return -1
            } while (true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils
{
    public static class BitsHelper
    {
        public enum WordsBlockContentFlag
        {
            //00
            AllZeros = 0,
            //01
            AllOnes = 1,
            //10
            DirtyWords = 2,
            //11
            FlagMask = 3
        }

        public const ulong AllOnes = UInt64.MaxValue;
        public const ulong AllZeros = 0UL;
        public const int BitsInUlong = sizeof(ulong) * 8;

        public static bool IsAllZeros(this ulong value)
        {
            return value == AllZeros;
        }

        public static bool IsAllOnes(this ulong value)
        {
            return value == AllOnes;
        }

        public static bool HasSomeOnes(this ulong value)
        {
            return value != AllZeros;
        }

        public static int GetFirstSetBitIdx(this ulong value)
        {
            return GetFirstSetBitIdx(value, 0);
        }

        public static int GetLastSetBitIdx(this ulong value)
        {
            int currentBitIdx = 0;
            for (; value != 0 && currentBitIdx < BitsInUlong; currentBitIdx++)
            {
                value >>= 1;
            }

            return currentBitIdx - 1;
        }

        public static int GetFirstSetBitIdx(this ulong value, int startIdxInclusive)
        {
            value >>= startIdxInclusive;

            for (int currentBitIdx = startIdxInclusive; value != 0 && currentBitIdx < BitsInUlong; currentBitIdx++)
            {
                if ((value & 1) == 1)
                {
                    return currentBitIdx;
                }

                value >>= 1;
            }

            return -1;
        }

        public static bool[] GetSetBits(this ulong value)
        {
            bool[] flags = new bool[BitsInUlong];

            return GetSetBits(value, flags);
        }

        public static bool[] GetSetBits(this ulong value, bool[] bufferToUse)
        {
            bool[] flags = bufferToUse;
            bufferToUse.ClearWhole();

            for (int currentBitIdx = 0; value != 0; currentBitIdx++)
            {
                if ((value & 1) == 1)
                {
                    flags[currentBitIdx] = true;
                }

                value >>= 1;
            }

            return flags;
        }

        public static ulong GetUlongWithBitSetOnPosition(int position)
        {
            if (position < 0)
            {
                return 0;
            }

            if (position >= BitsInUlong)
            {
                throw new ArgumentException($"Cannot set bit #{position} in ulong, since it has only {BitsInUlong} bits.");
            }

            else return 1UL << position;
        }

        public static ulong ToBitmapFragment(this IReadOnlyList<bool> flags, int startIndex)
        {
            ulong bitmap = AllZeros;

            for (int bitIdx = 0; bitIdx < BitsInUlong; bitIdx++)
            {
                if (flags.Count > startIndex + bitIdx && flags[startIndex + bitIdx])
                {
                    bitmap |= (1UL << bitIdx);
                }
            }

            return bitmap;
        }

        public static IEnumerable<ulong> ToBitmap(this IReadOnlyList<bool> flags)
        {
            for (int startIdx = 0; startIdx < flags.Count; startIdx += BitsInUlong)
            {
                yield return flags.ToBitmapFragment(startIdx);
            }
        }

        public static IEnumerable<ulong> ToBitmap(this IEnumerable<bool> flags)
        {
            int bitsInCurrentWord = 0;
            ulong bitmap = AllZeros;
            foreach (bool flag in flags)
            {
                if (flag)
                {
                    bitmap |= (1UL << bitsInCurrentWord);
                }

                bitsInCurrentWord++;
                if (bitsInCurrentWord == BitsInUlong)
                {
                    yield return bitmap;
                    bitsInCurrentWord = 0;
                    bitmap = AllZeros;
                }
            }

            if (bitsInCurrentWord != 0)
            {
                yield return bitmap;
            }
        }
    }
}
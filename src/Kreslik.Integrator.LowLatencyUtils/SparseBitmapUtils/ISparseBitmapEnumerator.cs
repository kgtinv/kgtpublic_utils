﻿namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils
{
    public interface ISparseBitmapEnumerator
    {
        ulong CurrentValue { get; }
        int CurrentIndex { get; }
        bool MoveToNextNonzeroDataIndex();
        bool MoveToNextNonzeroDataIndexGreaterOrEqual(int idx);
        bool HasNextNonzeroData { get; }
        void Reset();
    }
}
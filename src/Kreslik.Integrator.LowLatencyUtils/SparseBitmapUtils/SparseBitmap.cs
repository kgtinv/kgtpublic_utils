﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils
{
    /// <summary>
    /// Sparse bitmap inspired by EWAH simplementation (see e.g. here: https://arxiv.org/pdf/0901.3751.pdf)
    ///  with the difference that we store dirty words and indexes in two separate arrays 
    ///    - making the random access and skips twice as fast (as in ewah the dirty words and metadata are interleaved)
    /// </summary>
    [DataContract]
    public class SparseBitmap: ISparseBitmap
    {
        // What we store:
        //  - dense packed dirty words
        //  - indication of what real index is the dirty word
        //  - indication of what bits are in between the dirtyWords
        // For fast search of next nonzero word we also store:
        //  - array of inclusive start indexes of block of nonzero data
        //  - same dimension array of exclusive end indexes of blocks of nonzero data
        //  - same dimension of tuples (Intervals) with indexes of the block start/end indexing in real indexes array (this is for fast search of actual value fetched as next nonzero index)

        //dense packing of dirty words
        [DataMember]
        private readonly ulong[] _sparseBuffer;
        //indication of what real index is word in _sparseBuffer 
        //  + last 2 bits indicate what the current block contains (all 1; all 0; or mixture - 'dirty data') - only dirty data are stored in sparseBuffer
        [DataMember]
        private readonly int[] _sparseBlocksRealIndexesWithFlag;
        //same dimension as _sparseBlocksRealIndexesWithFlag, has indexes into _sparseBuffer
        [DataMember]
        private readonly int[] _flagArrayToDirtWordBufferMapping;

        //all following arrays are of a same dimension
        //this one is being searched to get/move to next nonzero word. High bound is exclusive
        [DataMember]
        private readonly Interval[] _realIndexesIntervalsOfNonzeroBlocks;
        //this holds indexes into _sparseBlocksRealIndexesWithFlag
        // this way we can know in which subinterval of _sparseBlocksRealIndexesWithFlag we should search particular index - and we need to search it there to find out what
        // value is on the index (1, 0, or dirty)
        [DataMember]
        private readonly Interval[] _indexesOfBlockStartEndIntoIndirectIndexingArray;

        [DataMember]
        public int Length { get; private set; }

        public SparseBitmap(IEnumerable<bool> flags)
        {
            List<ulong> dirtyWords = new List<ulong>();
            List<int> realIndexesOfDirtyWords = new List<int>();
            List<int> flagArrayToDirtWordBufferMapping = new List<int>();
            List<Interval> realIndexesIntervalsOfNonzeroBlocks = new List<Interval>();
            List<Interval> indexesOfBlockStartEndIntoIndirectIndexing = new List<Interval>();
            int currentRealIndex = 0;

            BitsHelper.WordsBlockContentFlag currentBlockContentFlag = BitsHelper.WordsBlockContentFlag.AllZeros;

            foreach (IEnumerableExtension.EnumeratingItems<ulong> enumeratingItems in flags.ToBitmap().ToOneForwardEnumerable())
            {
                Length++;
                ulong bitmapFragment = enumeratingItems.Current;

                BitsHelper.WordsBlockContentFlag currentWordContentFlag = bitmapFragment.IsAllZeros()
                    ? BitsHelper.WordsBlockContentFlag.AllZeros
                    : (bitmapFragment.IsAllOnes()
                        ? BitsHelper.WordsBlockContentFlag.AllOnes
                        : BitsHelper.WordsBlockContentFlag.DirtyWords);

                bool isChangeInContinuousBlocks = currentWordContentFlag != currentBlockContentFlag;

                bool isLastWord = !enumeratingItems.HasNext;

                //make sure that last nonzero block is ended
                if (!isChangeInContinuousBlocks &&
                    isLastWord &&
                    currentBlockContentFlag != BitsHelper.WordsBlockContentFlag.AllZeros)
                {
                    isChangeInContinuousBlocks = true;
                }

                //store bitmap if it's dirty (no need to store changes if it's not dirty - as full 1 or 0 words are handled by flags in real indexes array)
                if (currentWordContentFlag == BitsHelper.WordsBlockContentFlag.DirtyWords)
                {
                    dirtyWords.Add(bitmapFragment);
                }

                //store index if it's change
                if (isChangeInContinuousBlocks)
                {
                    int currentIdx = (currentRealIndex << 2) | (int)currentWordContentFlag;
                    realIndexesOfDirtyWords.Add(currentIdx);
                    flagArrayToDirtWordBufferMapping.Add(currentWordContentFlag ==
                                                         BitsHelper.WordsBlockContentFlag.DirtyWords
                        ? dirtyWords.Count - 1
                        : -1);

                    //start of new nonzero block
                    if (currentBlockContentFlag == BitsHelper.WordsBlockContentFlag.AllZeros ||
                        (isLastWord && !realIndexesIntervalsOfNonzeroBlocks.Any()))
                    {
                        realIndexesIntervalsOfNonzeroBlocks.Add(new Interval(currentRealIndex, -1));
                        indexesOfBlockStartEndIntoIndirectIndexing.Add(new Interval(realIndexesOfDirtyWords.Count - 1, -1));
                    }

                    //end of previous nonzero block
                    if (currentWordContentFlag == BitsHelper.WordsBlockContentFlag.AllZeros || isLastWord)
                    {
                        realIndexesIntervalsOfNonzeroBlocks[realIndexesIntervalsOfNonzeroBlocks.Count - 1]
                            =
                            new Interval(
                                realIndexesIntervalsOfNonzeroBlocks[
                                    realIndexesIntervalsOfNonzeroBlocks.Count - 1].LowBound,
                                //high bound is exclusive - current index is already pointing to zero
                                isLastWord ? currentRealIndex + 1 : currentRealIndex);

                        indexesOfBlockStartEndIntoIndirectIndexing[indexesOfBlockStartEndIntoIndirectIndexing.Count - 1]
                            =
                            new Interval(
                                indexesOfBlockStartEndIntoIndirectIndexing[
                                    indexesOfBlockStartEndIntoIndirectIndexing.Count - 1].LowBound,
                                //highBound is exclusive
                                realIndexesOfDirtyWords.Count);
                    }
                }

                currentBlockContentFlag = currentWordContentFlag;
                currentRealIndex++;
            }

            _sparseBuffer = dirtyWords.ToArray();
            _sparseBlocksRealIndexesWithFlag = realIndexesOfDirtyWords.ToArray();
            _flagArrayToDirtWordBufferMapping = flagArrayToDirtWordBufferMapping.ToArray();
            _realIndexesIntervalsOfNonzeroBlocks = realIndexesIntervalsOfNonzeroBlocks.ToArray();
            _indexesOfBlockStartEndIntoIndirectIndexingArray = indexesOfBlockStartEndIntoIndirectIndexing.ToArray();
        }


        public ISparseBitmapEnumerator GetEnumerator()
        {
            return new SparseBitmapEnumerator(this);
        }

        private class SparseBitmapEnumerator : ISparseBitmapEnumerator
        {
            private readonly SparseBitmap _sparseBitmap;
            private int _currentNonzeroBlockIdx = -1;
            private Interval _currentBlock = Interval.Empty;
            private ulong? _currentValue = null;

            public int CurrentIndex { get; private set; } = -1;
            public bool HasNextNonzeroData { get; private set; } = true;

            public SparseBitmapEnumerator(SparseBitmap sparseBitmap)
            {
                _sparseBitmap = sparseBitmap;
            }

            public void Reset()
            {
                _currentNonzeroBlockIdx = -1;
                _currentBlock = Interval.Empty;
                _currentValue = null;
                CurrentIndex = -1;
                HasNextNonzeroData = true;
            }

            private void MoveToNextNonzeroBlock()
            {
                HasNextNonzeroData = _sparseBitmap._indexesOfBlockStartEndIntoIndirectIndexingArray.Length <
                              _currentNonzeroBlockIdx + 1;

                if (HasNextNonzeroData)
                {
                    _currentNonzeroBlockIdx++;
                    _currentBlock =
                        _sparseBitmap._realIndexesIntervalsOfNonzeroBlocks[_currentNonzeroBlockIdx];
                    CurrentIndex = _currentBlock.LowBound;
                }
                else
                {
                    CurrentIndex = -1;
                }

                _currentValue = null;
            }

            public ulong CurrentValue
            {
                get
                {
                    if (!_currentValue.HasValue)
                    {
                        _currentValue = FetchCurrentValue();
                    }

                    return _currentValue.Value;
                }
            }

            private ulong FetchCurrentValue()
            {
                if (!HasNextNonzeroData || CurrentIndex < 0)
                {
                    throw new Exception(
                        $"Cannot access CurrentValue if enumerator has not any more data ({!HasNextNonzeroData}) or if the current index is uninitialized ({CurrentIndex < 0})");
                }

                //search _sparseBitmap._sparseBlocksRealIndexesWithFlag using start/end indexes from _sparseBitmap._indexesOfBlockStartEndIntoIndirectIndexingArray
                // and then look on the flag - if its 1 or 0 immediately return, otherwise fetch dirty word

                Interval startEndToSearch = _sparseBitmap._indexesOfBlockStartEndIntoIndirectIndexingArray[_currentNonzeroBlockIdx];
                int blockStartIndexWithFlags =
                    ArraySearchHelpers.GetClosestLowerOrEqualArrayIndex(
                        _sparseBitmap._sparseBlocksRealIndexesWithFlag,
                        //we need to shift the current index to correctly search in array with flagged values
                        //and since 
                        (CurrentIndex << 2),
                        (int)BitsHelper.WordsBlockContentFlag.FlagMask,
                        startEndToSearch.LowBound,
                        startEndToSearch.HighBound);

                if (blockStartIndexWithFlags == -1)
                {
                    throw new Exception($"Cannot find item for index {CurrentIndex}");
                }

                int realIndexWithFlags = _sparseBitmap._sparseBlocksRealIndexesWithFlag[blockStartIndexWithFlags];

                if ((realIndexWithFlags & (int)BitsHelper.WordsBlockContentFlag.DirtyWords) != 0)
                {
                    int dirtyBufferIdxBlockStart =
                        _sparseBitmap._flagArrayToDirtWordBufferMapping[blockStartIndexWithFlags];
                    if (dirtyBufferIdxBlockStart < 0)
                    {
                        throw new Exception(
                            $"Unexpected situation - index {blockStartIndexWithFlags} is allegedly holding dirty word based on flags, but the sparse buffer index is not populated for this index");
                    }

                    int dirtyBufferIdx = dirtyBufferIdxBlockStart + CurrentIndex - (realIndexWithFlags >> 2);

                    return _sparseBitmap._sparseBuffer[dirtyBufferIdx];
                }
                else if ((realIndexWithFlags & (int)BitsHelper.WordsBlockContentFlag.AllOnes) != 0)
                {
                    return BitsHelper.AllOnes;
                }
                else
                {
                    return BitsHelper.AllZeros;
                }
            }

            public bool MoveToNextNonzeroDataIndex()
            {
                if (!HasNextNonzeroData)
                {
                    return false;
                }

                if (CurrentIndex + 1 < _currentBlock.HighBound)
                {
                    CurrentIndex++;
                    _currentValue = null;
                }
                else
                {
                    MoveToNextNonzeroBlock();
                }

                return HasNextNonzeroData;
            }

            public bool MoveToNextNonzeroDataIndexGreaterOrEqual(int idx)
            {
                if (!HasNextNonzeroData)
                {
                    return false;
                }

                if (idx <= CurrentIndex)
                {
                    return true;
                }

                _currentValue = null;

                //optimize for case where we just move in current block
                if (_currentBlock.Compare(idx, true) == 0)
                {
                    CurrentIndex = idx;
                    return true;
                }

                //binary search _sparseBitmap._inclusiveRealStartIndexesOfNonzeroBlocks
                _currentNonzeroBlockIdx = ArraySearchHelpers.GetInclusiveOrNextInterval(
                    _sparseBitmap._realIndexesIntervalsOfNonzeroBlocks, idx, Math.Max(0, _currentNonzeroBlockIdx),
                    _sparseBitmap._realIndexesIntervalsOfNonzeroBlocks.Length);

                if (_currentNonzeroBlockIdx == -1)
                {
                    HasNextNonzeroData = false;
                    CurrentIndex = -1;
                    return false;
                }

                _currentBlock =
                    _sparseBitmap._realIndexesIntervalsOfNonzeroBlocks[_currentNonzeroBlockIdx];
                //is the searched index inside some block or is it in the next block?
                CurrentIndex = _currentBlock.Compare(idx, true) == 0 ? idx : _currentBlock.LowBound;

                return true;
            }
        }

        public static class ArraySearchHelpers
        {
            public static int GetClosestLowerOrEqualArrayIndex(Array array, int inclusiveStartIndex, int length, int item)
            {
                int index = Array.BinarySearch(array, inclusiveStartIndex, length, item);
                if (index < 0)
                {
                    index = ~index - 1;
                }

                return index;
            }

            //inspired by Array.BinarySearch
            public static int GetClosestLowerOrEqualArrayIndex(int[] array, int item, int bitMaskToExclude, int inclusiveLowBound,
                int exclusiveHighBound)
            {
                if (inclusiveLowBound < 0)
                {
                    inclusiveLowBound = 0;
                }

                if (exclusiveHighBound > array.Length)
                {
                    exclusiveHighBound = array.Length;
                }

                int bitMask = ~bitMaskToExclude;

                item &= bitMask;

                //searched item is behind the array
                if (inclusiveLowBound >= exclusiveHighBound || item < (array[inclusiveLowBound] & bitMask))
                {
                    return -1;
                }

                int low = inclusiveLowBound;
                int hi = exclusiveHighBound - 1;

                while (low <= hi)
                {
                    int median = GetMedian(low, hi);
                    //we mask each number separately and then subtract; as otherwise we could mask negative numbers
                    // which leads to unexpected results
                    int num = item - (array[median] & bitMask);

                    if (num == 0)
                        return median;
                    if (num < 0)
                        hi = median - 1;
                    else
                        low = median + 1;
                }

                return hi;
            }

            //inspired by Array.BinarySearch
            public static int GetInclusiveOrNextInterval(Interval[] intervals, int item, int inclusiveLowBound,
                int exclusiveHighBound)
            {
                if (inclusiveLowBound < 0)
                {
                    inclusiveLowBound = 0;
                }

                if (exclusiveHighBound > intervals.Length)
                {
                    exclusiveHighBound = intervals.Length;
                }

                //searched item is behind the array
                if (inclusiveLowBound >= exclusiveHighBound || item >= intervals[exclusiveHighBound - 1].HighBound)
                {
                    return -1;
                }

                //try the first item first
                if (intervals[inclusiveLowBound].Compare(item, true) <= 0)
                {
                    return inclusiveLowBound;
                }

                //inclusiveLowBound was already searched - so +1
                int low = inclusiveLowBound + 1;
                int hi = exclusiveHighBound - 1;

                while (low <= hi)
                {
                    int median = GetMedian(low, hi);
                    int num = intervals[median].Compare(item, true);

                    if (num == 0)
                        return median;
                    if (num < 0)
                        hi = median - 1;
                    else
                        low = median + 1;
                }

                //we want 'next' (see name)
                return hi + 1;
            }

            private static int GetMedian(int low, int hi)
            {
                return low + (hi - low >> 1);
            }
        }
    }
}

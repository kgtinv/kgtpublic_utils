﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils.IndexesProvider
{
    public struct CircularInterval
    {
        public Interval<byte> Interval { get; }
        private readonly byte _exclusiveMax;

        public CircularInterval(byte inclusiveMin, byte inclusiveMax, byte rangeLength)
        {
            _exclusiveMax = rangeLength;
            Interval = new Interval<byte>(inclusiveMin, inclusiveMax);
        }

        public byte FixIndex(byte index)
        {
            if (index >= _exclusiveMax)
            {
                index -= _exclusiveMax;
            }

            return index;
        }
    }

    public interface IIndexesProvider
    {
        //Slowing down
        //IEnumerable<int> GetIndexes(int rangeId);
        CircularInterval GetIndexes(int rangeId);
        int RangesCount { get; }
    }
}

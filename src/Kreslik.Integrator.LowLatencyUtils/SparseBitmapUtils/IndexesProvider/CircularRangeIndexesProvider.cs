using System;
using System.Collections.Generic;
using System.Linq;

namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils.IndexesProvider
{
    public class CircularRangeIndexesProvider : IIndexesProvider
    {
        private readonly byte[] _lowBoundIndexes;
        private readonly byte[] _highBoundIndexes;
        private readonly int[] _valuesCountInRange;

        public CircularRangeIndexesProvider(byte[] lowBoundIndexes, byte[] highBoundIndexes, int[] valuesCountInRange)
        {
            _lowBoundIndexes = lowBoundIndexes;
            _highBoundIndexes = highBoundIndexes;
            _valuesCountInRange = valuesCountInRange;

            if (_lowBoundIndexes.Length != _highBoundIndexes.Length || (_valuesCountInRange != null && _valuesCountInRange.Length != _lowBoundIndexes.Length))
            {
                throw new Exception($"bounds array discrepancy. Low: {_lowBoundIndexes.Length}, High: {_highBoundIndexes.Length}; Counts: {_valuesCountInRange?.Length}");
            }
        }

        public IEnumerable<int> GetIndexes(int rangeId)
        {
            int lowBound = _lowBoundIndexes[rangeId];
            int highBound = _highBoundIndexes[rangeId];

            if (lowBound <= highBound)
            {
                return Enumerable.Range(lowBound, highBound + 1 - lowBound);
            }
            else
            {
                return
                    Enumerable.Range(lowBound, _valuesCountInRange[rangeId] - lowBound)
                        .Concat(Enumerable.Range(0, highBound + 1));
            }
        }

        CircularInterval IIndexesProvider.GetIndexes(int rangeId)
        {
            return new CircularInterval(_lowBoundIndexes[rangeId], _highBoundIndexes[rangeId],
                _valuesCountInRange == null ? byte.MaxValue : (byte) _valuesCountInRange[rangeId]);
        }

        public int RangesCount => _lowBoundIndexes.Length;
    }
}
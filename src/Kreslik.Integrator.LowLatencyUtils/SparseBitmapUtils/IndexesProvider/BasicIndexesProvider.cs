﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils.IndexesProvider
{
    public class BasicIndexesProvider : IIndexesProvider
    {
        private readonly byte[] _lowBoundIndexes;
        private readonly byte[] _highBoundIndexes;

        public BasicIndexesProvider(byte[] lowBoundIndexes, byte[] highBoundIndexes)
        {
            _lowBoundIndexes = lowBoundIndexes;
            _highBoundIndexes = highBoundIndexes;

            if (_lowBoundIndexes.Length != _highBoundIndexes.Length)
            {
                throw new Exception($"bounds array discrepancy. Low: {_lowBoundIndexes.Length}, High: {_highBoundIndexes.Length}");
            }
        }

        public IEnumerable<int> GetIndexes(int rangeId)
        {
            return Enumerable.Range(_lowBoundIndexes[rangeId], _highBoundIndexes[rangeId] + 1 - _lowBoundIndexes[rangeId]);
        }

        CircularInterval IIndexesProvider.GetIndexes(int rangeId)
        {
            return new CircularInterval(_lowBoundIndexes[rangeId], _highBoundIndexes[rangeId], Byte.MaxValue);
        }

        public int RangesCount => _lowBoundIndexes.Length;
    }
}
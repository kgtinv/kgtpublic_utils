﻿namespace Kreslik.Integrator.LowLatencyUtils.SparseBitmapUtils
{
    public interface ISparseBitmap
    {
        ISparseBitmapEnumerator GetEnumerator();
        int Length { get; }
    }
}
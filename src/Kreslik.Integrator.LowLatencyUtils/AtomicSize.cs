﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    //Warning: Only use as a field, never as a property!!
    // if used as property CLR will make on the fly copies and you won't be changing the underlying value
    // if we change this struct to immutable, then we loose some of the goodness of atomicity - e.g.:
    //           _someVariable = _someVariable.InterlockedAdd(x);
    //   would cause memory fence at the time of interlocked, but before assigning result back to the variable
    public struct AtomicSize
    {
        private const decimal _CONST_DIVISOR = 10000m;
        private long _sizeConverted;
        public static AtomicSize ZERO = new AtomicSize(0);
        public static AtomicSize MaxValue = new AtomicSize(long.MaxValue);
        public static AtomicSize MinValue = new AtomicSize(long.MinValue);

        private AtomicSize(long convertedSize)
        {
            this._sizeConverted = convertedSize;
        }

        public static AtomicSize FromDecimal(decimal d)
        {
            return new AtomicSize((long)(d * _CONST_DIVISOR));
        }

        public static AtomicSize Abs(AtomicSize a)
        {
            return new AtomicSize(Math.Abs(a._sizeConverted));
        }

        public static int Sign(AtomicSize a)
        {
            return Math.Sign(a._sizeConverted);
        }

        public static int Sign(AtomicSizeZeroCrossingTracking a)
        {
            return AtomicSizeZeroCrossingTracking.Sign(a);
        }

        internal static AtomicSize FromInternalSizeConverted(long sizeConverted)
        {
            return new AtomicSize(sizeConverted);
        }

        internal long SizeConverted_InternalUseOnly { get { return this._sizeConverted; } }

        public decimal ToDecimal()
        {
            if (_sizeConverted == 0)
                return 0m;
            else
                return ((decimal)_sizeConverted) / _CONST_DIVISOR;
        }
        public decimal InterlockedToDecimal()
        {
            long sizeConverted = Interlocked.Read(ref this._sizeConverted);

            if (sizeConverted == 0)
                return 0m;
            else
                return ((decimal)sizeConverted) / _CONST_DIVISOR;
        }

        public AtomicSize InterlockedAdd(decimal value)
        {
            if (value == 0)
                return this;
            else
            {
                long sizeConverted = (long)(value * _CONST_DIVISOR);
                long result = Interlocked.Add(ref this._sizeConverted, sizeConverted);
                return new AtomicSize(result);
            }
        }

        public AtomicSize InterlockedAdd(AtomicSize value)
        {
            if (value == AtomicSize.ZERO)
                return this;
            else
            {
                long result = Interlocked.Add(ref this._sizeConverted, value._sizeConverted);
                return new AtomicSize(result);
            }
        }

        public bool IsSameSignAndCloserToZeroThan(AtomicSize secondVal)
        {
            return
                this._sizeConverted == 0 && secondVal._sizeConverted != 0
                ||
                (this._sizeConverted > 0
                    ? secondVal._sizeConverted >= 0 && this._sizeConverted < secondVal._sizeConverted
                    : secondVal._sizeConverted <= 0 && this._sizeConverted > secondVal._sizeConverted);
        }

        public void InterlockedZeroOut()
        {
            Interlocked.Exchange(ref this._sizeConverted, 0);
        }

        public decimal InterlockedZeroOutReturningPrevious()
        {
            long sizeConverted = Interlocked.Exchange(ref this._sizeConverted, 0);
            return ((decimal)sizeConverted) / _CONST_DIVISOR;
        }

        public bool InterlockedIsPositive()
        {
            return Interlocked.Read(ref this._sizeConverted) > 0;
        }

        public bool InterlockedIsNegative()
        {
            return Interlocked.Read(ref this._sizeConverted) < 0;
        }

        public AtomicSize InterlockedRead()
        {
            return new AtomicSize(Interlocked.Read(ref this._sizeConverted));
        }

        public bool InterlockedIsZero()
        {
            return Interlocked.Read(ref this._sizeConverted) == 0;
        }

        public override string ToString()
        {
            return this.ToDecimal().ToString() + " [atomic]";
        }

        public static AtomicSize operator +(AtomicSize s1, AtomicSize s2)
        {
            return new AtomicSize(s1._sizeConverted + s2._sizeConverted);
        }

        public static AtomicSize operator -(AtomicSize s1)
        {
            return new AtomicSize(-s1._sizeConverted);
        }

        public static AtomicSize operator -(AtomicSize s1, AtomicSize s2)
        {
            return new AtomicSize(s1._sizeConverted - s2._sizeConverted);
        }

        public static AtomicSize operator *(AtomicSize s1, AtomicSize s2)
        {
            // a/x * b/x = ab/x^2 = (ab/x)/x
            return new AtomicSize((long)(s1._sizeConverted * s2._sizeConverted / _CONST_DIVISOR));
        }

        public static AtomicSize operator *(AtomicSize s1, decimal d2)
        {
            // a/x * b = ab/x
            return new AtomicSize((long)(s1._sizeConverted * d2));
        }

        public static bool operator <(AtomicSize s1, AtomicSize s2)
        {
            return s1._sizeConverted < s2._sizeConverted;
        }

        public static bool operator <=(AtomicSize s1, AtomicSize s2)
        {
            return s1._sizeConverted <= s2._sizeConverted;
        }

        public static bool operator >(AtomicSize s1, AtomicSize s2)
        {
            return s1._sizeConverted > s2._sizeConverted;
        }

        public static bool operator >=(AtomicSize s1, AtomicSize s2)
        {
            return s1._sizeConverted >= s2._sizeConverted;
        }

        public static bool operator ==(AtomicSize s1, AtomicSize s2)
        {
            return s1._sizeConverted == s2._sizeConverted;
        }

        public static bool operator !=(AtomicSize s1, AtomicSize s2)
        {
            return s1._sizeConverted != s2._sizeConverted;
        }

        public static AtomicSize Max(AtomicSize s1, AtomicSize s2)
        {
            return s1 > s2 ? s1 : s2;
        }

        public static AtomicSize Min(AtomicSize s1, AtomicSize s2)
        {
            return s1 > s2 ? s2 : s1;
        }
    }

    //Unfortunately no inheritance between structs - so need to repeat the code
    public struct AtomicSizeZeroCrossingTracking
    {
        private const decimal _CONST_DIVISOR = 10000m;
        private long _sizeConverted;
        private DateTime _zeroLastCrossed;
        public static AtomicSizeZeroCrossingTracking ZERO = new AtomicSizeZeroCrossingTracking(0);

        private AtomicSizeZeroCrossingTracking(long convertedSize)
        {
            this._sizeConverted = convertedSize;
            _zeroLastCrossed = DateTime.UtcNow;
        }

        public static explicit operator AtomicSize(AtomicSizeZeroCrossingTracking a)
        {
            return AtomicSize.FromInternalSizeConverted(a._sizeConverted);
        }

        public static AtomicSizeZeroCrossingTracking FromDecimal(decimal d)
        {
            return new AtomicSizeZeroCrossingTracking((long)(d * _CONST_DIVISOR));
        }

        public static int Sign(AtomicSizeZeroCrossingTracking a)
        {
            return Math.Sign(a._sizeConverted);
        }

        public DateTime ZeroLastCrossed { get { return this._zeroLastCrossed; } }

        public decimal ToDecimal()
        {
            if (_sizeConverted == 0)
                return 0m;
            else
                return ((decimal)_sizeConverted) / _CONST_DIVISOR;
        }
        public decimal InterlockedToDecimal()
        {
            long sizeConverted = Interlocked.Read(ref this._sizeConverted);

            if (sizeConverted == 0)
                return 0m;
            else
                return ((decimal)sizeConverted) / _CONST_DIVISOR;
        }

        public AtomicSize InterlockedAdd(decimal value)
        {
            if (value == 0)
                return AtomicSize.FromInternalSizeConverted(this._sizeConverted);
            else
            {
                long sizeConverted = (long)(value * _CONST_DIVISOR);
                long result = Interlocked.Add(ref this._sizeConverted, sizeConverted);
                if (result == 0 || Math.Sign(result) != Math.Sign(result - sizeConverted))
                {
                    _zeroLastCrossed = DateTime.UtcNow;
                }
                return AtomicSize.FromInternalSizeConverted(result);
            }
        }

        public AtomicSize InterlockedAdd(AtomicSize value)
        {
            return this.InterlockedAdd(value.ToDecimal());
        }

        public void InterlockedZeroOut()
        {
            _zeroLastCrossed = DateTime.UtcNow;
            Interlocked.Exchange(ref this._sizeConverted, 0);
        }

        public decimal InterlockedZeroOutReturningPrevious()
        {
            _zeroLastCrossed = DateTime.UtcNow;
            long sizeConverted = Interlocked.Exchange(ref this._sizeConverted, 0);
            return ((decimal)sizeConverted) / _CONST_DIVISOR;
        }

        public bool InterlockedIsPositive()
        {
            return Interlocked.Read(ref this._sizeConverted) > 0;
        }

        public bool InterlockedIsZero()
        {
            return Interlocked.Read(ref this._sizeConverted) == 0;
        }

        public bool IsZero()
        {
            return this._sizeConverted == 0;
        }

        public override string ToString()
        {
            return this.ToDecimal().ToString() + " [atomic]";
        }

        public static bool operator <(AtomicSizeZeroCrossingTracking s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1._sizeConverted < s2._sizeConverted;
        }

        public static bool operator <=(AtomicSizeZeroCrossingTracking s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1._sizeConverted <= s2._sizeConverted;
        }

        public static bool operator >(AtomicSizeZeroCrossingTracking s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1._sizeConverted > s2._sizeConverted;
        }

        public static bool operator >=(AtomicSizeZeroCrossingTracking s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1._sizeConverted >= s2._sizeConverted;
        }

        public static bool operator ==(AtomicSizeZeroCrossingTracking s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1._sizeConverted == s2._sizeConverted;
        }

        public static bool operator !=(AtomicSizeZeroCrossingTracking s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1._sizeConverted != s2._sizeConverted;
        }

        public static bool operator ==(AtomicSizeZeroCrossingTracking s1, AtomicSize s2)
        {
            return s1._sizeConverted == s2.SizeConverted_InternalUseOnly;
        }

        public static bool operator !=(AtomicSizeZeroCrossingTracking s1, AtomicSize s2)
        {
            return s1._sizeConverted != s2.SizeConverted_InternalUseOnly;
        }

        public static bool operator <(AtomicSizeZeroCrossingTracking s1, AtomicSize s2)
        {
            return s1._sizeConverted < s2.SizeConverted_InternalUseOnly;
        }

        public static bool operator >(AtomicSizeZeroCrossingTracking s1, AtomicSize s2)
        {
            return s1._sizeConverted > s2.SizeConverted_InternalUseOnly;
        }

        public static bool operator <(AtomicSize s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1.SizeConverted_InternalUseOnly < s2._sizeConverted;
        }

        public static bool operator >(AtomicSize s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1.SizeConverted_InternalUseOnly > s2._sizeConverted;
        }

        public static bool operator <=(AtomicSize s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1.SizeConverted_InternalUseOnly <= s2._sizeConverted;
        }

        public static bool operator >=(AtomicSize s1, AtomicSizeZeroCrossingTracking s2)
        {
            return s1.SizeConverted_InternalUseOnly >= s2._sizeConverted;
        }

        public static AtomicSize operator -(AtomicSizeZeroCrossingTracking s1, AtomicSize s2)
        {
            return AtomicSize.FromInternalSizeConverted(s1._sizeConverted - s2.SizeConverted_InternalUseOnly);
        }

        public static AtomicSize operator -(AtomicSize s1, AtomicSizeZeroCrossingTracking s2)
        {
            return AtomicSize.FromInternalSizeConverted(s1.SizeConverted_InternalUseOnly - s2._sizeConverted);
        }

        public static AtomicSize operator +(AtomicSizeZeroCrossingTracking s1, AtomicSize s2)
        {
            return AtomicSize.FromInternalSizeConverted(s1._sizeConverted + s2.SizeConverted_InternalUseOnly);
        }

        public static AtomicSize operator +(AtomicSize s1, AtomicSizeZeroCrossingTracking s2)
        {
            return AtomicSize.FromInternalSizeConverted(s1.SizeConverted_InternalUseOnly + s2._sizeConverted);
        }
    }
}

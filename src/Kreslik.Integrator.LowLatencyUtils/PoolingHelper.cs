﻿//#define POOLS_STACK_CAPTURE

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    [Serializable]
    public class ObjectPoolingException : Exception
    {
        public ObjectPoolingException() { }
        public ObjectPoolingException(string message) : base(message) { }
        public ObjectPoolingException(string message, Exception inner) : base(message, inner) { }
    }

    public interface IPoolingHelper
    {
        void Acquire();
        void SetValid();
        void AcquireInitial();
        void Release();
        bool TryRelease();
        void ClearAcquireCount();

        void MarkReleaseDisallowed();
        void ClearReleaseDisallowed();

        void Leak();
    }

    public class PoolingHelper : IPoolingHelper
    {
        protected int _referenceCount;
        //this is not volatile, so AcquireNeeds to be called before used from different thread 
        // (which is requirement anyway - even without this field)
        private bool _valid = false;
        private bool _releaseDisallowed = false;

        private Action _releaseAction;

        private static int _instancesCount;
        private readonly int _instanceId = Interlocked.Increment(ref _instancesCount);

#if POOLS_STACK_CAPTURE
        private List<StackTrace> _acquireStackTraces = new List<StackTrace>();
        private List<StackTrace> _releaseStackTraces = new List<StackTrace>();


        static PoolingHelper()
        {
            LogFactory.Instance.GetLogger(null)
                .Log(LogLevel.Fatal,
                    "PoolingHelper build with POOLS_STACK_CAPTURE directive turned on. This will have severe impact on performance. IMMEDIATELY STOP TRADING");
        }

        public void FlushStacks(ILogger logger)
        {
            string traces = string.Format("Acquire Traces:{0}{1}{0}{0}Release Traces:{0}{2}",
                Environment.NewLine, string.Join(Environment.NewLine, _acquireStackTraces),
                string.Join(Environment.NewLine, _releaseStackTraces));
            logger.Log(LogLevel.Error, traces);
        }
#endif

        public PoolingHelper(Action releaseAction)
        {
            this._releaseAction = releaseAction;
        }

        private void CheckValid()
        {
            if (!_valid)
            { 
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Pooled item [{0}] used before acquisition or after releasing: {1}", _instanceId, new StackTrace());
#if POOLS_STACK_CAPTURE
                this.FlushStacks(LogFactory.Instance.GetLogger(null));    
#endif
            }
        }

        public void Acquire()
        {
            CheckValid();

            //Acquire initial should have been already called so _refCount before this call is expected to be >= 1
            // And after call > 1

            int refCountAfterAcquire = Interlocked.Increment(ref this._referenceCount);

            if (refCountAfterAcquire <= 1 && (!_releaseDisallowed || refCountAfterAcquire <= 0))
            {
                //_valid = false;
                //intentionally prevent item from being able to be returned to the pool
                Interlocked.Exchange(ref this._referenceCount, 100000);
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Pooled item [{0}] ref count after acquire: {1} unexpected (Expected > 1). Release Disallowed: {3}. {2}", _instanceId, refCountAfterAcquire, new StackTrace(), _releaseDisallowed);
#if POOLS_STACK_CAPTURE
                this.FlushStacks(LogFactory.Instance.GetLogger(null));    
#endif
            }

#if POOLS_STACK_CAPTURE
            _acquireStackTraces.Add(new StackTrace());
#endif
        }

        public void Leak()
        {
            //intentionally prevent item from being able to be returned to the pool
            Interlocked.Exchange(ref this._referenceCount, 100000);
        }

        public void MarkReleaseDisallowed()
        {
            this._releaseDisallowed = true;
        }

        public void ClearReleaseDisallowed()
        {
            this._releaseDisallowed = false;

            if (this._referenceCount == 0)
            {
                _valid = false;
                this._releaseAction();
            }
        }

        public void SetValid()
        {
            this._valid = true;
        }

        public void AcquireInitial()
        {
            int refCountBeforeInitialization = Interlocked.CompareExchange(ref this._referenceCount, 1, 0);

            if (refCountBeforeInitialization != 0)
            {
                _valid = false;
                //intentionally prevent item from being able to be returned to the pool
                Interlocked.Exchange(ref this._referenceCount, 100000);

#if POOLS_STACK_CAPTURE
                this.FlushStacks(LogFactory.Instance.GetLogger(null));    
#endif
                throw new ObjectPoolingException(string.Format("Pooled item [{0}] initialization called multiple times (before intitialization ref count: {1}, expected 0)", _instanceId, refCountBeforeInitialization));
            }

#if POOLS_STACK_CAPTURE

            //since we know that up till now all validations succeeded - we can get rid of history that ended in 'zero' state
            _acquireStackTraces.Clear();
            _releaseStackTraces.Clear();

            _acquireStackTraces.Add(new StackTrace());
#endif
            _valid = true;
        }

        public void ClearAcquireCount()
        {
            _valid = false;
            int refCountBeforeClearing = Interlocked.CompareExchange(ref this._referenceCount, 0, 1);

            if (refCountBeforeClearing != 1)
            {
#if POOLS_STACK_CAPTURE
                this.FlushStacks(LogFactory.Instance.GetLogger(null));    
#endif
                throw new ObjectPoolingException(
                    string.Format("Pooled item [{0}] Expected to be used by exactly one holder (instead ref count was: {1}, while expected 1)", _instanceId, refCountBeforeClearing));
            }
        }

        public void Release()
        {
            this.TryRelease();
        }

        public bool TryRelease()
        {
            CheckValid();
#if POOLS_STACK_CAPTURE
            _releaseStackTraces.Add(new StackTrace());
#endif

            int afterReleasingRefCount;
            if (!_valid)
            {
                //intentionally prevent item from being able to be returned to the pool
                Interlocked.Exchange(ref this._referenceCount, 100000);
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Pooled item [{0}] was not valid during releasing. {1}",
                        _instanceId, new StackTrace());
#if POOLS_STACK_CAPTURE
                this.FlushStacks(LogFactory.Instance.GetLogger(null));    
#endif
                return false;
            }
            else
                afterReleasingRefCount = Interlocked.Decrement(ref this._referenceCount);

            if (afterReleasingRefCount == 0)
            {
                if (!_releaseDisallowed)
                {
                    _valid = false;
                    this._releaseAction();
                }
            }
            else if (afterReleasingRefCount < 0)
            {
                _valid = false;
                //intentionally prevent item from being able to be returned to the pool
                Interlocked.Exchange(ref this._referenceCount, 100000);

                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Pooled item [{0}] ref count after releasing: {1} (expected >= 0). {2}",
                        _instanceId, afterReleasingRefCount, new StackTrace());
#if POOLS_STACK_CAPTURE
                this.FlushStacks(LogFactory.Instance.GetLogger(null));    
#endif
                return false;
            }

            return true;
        }

        ~PoolingHelper()
        {
            if (this._referenceCount != 0)
            {
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Pooled item [{0}] leaked (ref count: {1})", _instanceId, _referenceCount);
#if POOLS_STACK_CAPTURE
                this.FlushStacks(LogFactory.Instance.GetLogger(null));    
#endif
            }
        }
    }

    //public sealed class PoolingHelperPooled : PoolingHelper
    //{
    //    public PoolingHelperPooled(Action releaseAction) 
    //        : base(releaseAction)
    //    { }

    //    ~PoolingHelperPooled()
    //    {
    //        if (this._referenceCount != 0)
    //            LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Pooled item leaked");
    //    }
    //}
}

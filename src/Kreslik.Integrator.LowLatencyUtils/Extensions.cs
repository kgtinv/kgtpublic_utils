﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public static class DirectoryEx
    {
        public static void Move(string srcPath, string dstPath)
        {
            if (
                string.Compare(Path.GetPathRoot(srcPath), Path.GetPathRoot(dstPath), StringComparison.OrdinalIgnoreCase) ==
                0)
            {
                string parentDir = (new DirectoryInfo(dstPath)).Parent.FullName;
                Directory.CreateDirectory(parentDir);
                Directory.Move(srcPath, dstPath);
            }
            else
            {
                string tempDstPath = dstPath + RandomizationUtils.CreateRandomHash(6);
                CopyFolder(srcPath, tempDstPath);
                try
                {
                    Directory.Move(tempDstPath, dstPath);
                }
                finally
                {
                    if (Directory.Exists(tempDstPath))
                    {
                        Directory.Delete(tempDstPath, true);
                    }
                }
                Common.DirectoryEx.DeleteRecursiveIncludingReadOnly(srcPath, true);
            }
        }

        private static void CopyFolder(string srcPath, string dstPath)
        {
            Directory.CreateDirectory(dstPath);
            string[] theDirectories = Directory.GetDirectories(srcPath);
            foreach (string curDir in theDirectories)
            {
                string newDestinationDirName = Path.Combine(dstPath, Path.GetFileName(curDir));
                CopyFolder(curDir, newDestinationDirName);
            }

            string[] theFilesInCurrentDir = Directory.GetFiles(srcPath);
            foreach (string currentFile in theFilesInCurrentDir)
            {
                CopyFile(currentFile, dstPath);
            }
        }

        private static void CopyFile(string source, string destination)
        {
            File.Copy(source, Path.Combine(destination, Path.GetFileName(source)));
        }
    }
}

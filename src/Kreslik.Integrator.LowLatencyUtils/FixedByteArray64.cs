﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    /// <summary>
    /// Fixed size array to be inlined inside the class
    /// 
    /// WARNING: this will work correctly only with blittable types
    ///  as .net marshaler has problems with nonblittable types - see comments here: 
    /// http://msdn.microsoft.com/en-us/library/zycewsya(v=vs.100).aspx
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct FixedByteArray64
    {
        public const int ARRAY_SIZE = 64;
        private fixed byte _buffer[ARRAY_SIZE];


        public void SetBuffer(string asciiString)
        {
            fixed (byte* bytePtr = this._buffer)
            {
                ByteConverter.CopyAsciiStringToByteBuffer(asciiString, bytePtr, 0, ARRAY_SIZE);
            }
        }

        public string ContentAsString
        {
            get
            {
                byte[] bytes = new byte[ARRAY_SIZE];

                unsafe
                {
                    // Pin the buffer to a fixed location in memory. 
                    fixed (byte* bytePtr = this._buffer)
                    {
                        for (int i = 0; i < ARRAY_SIZE; i++)
                        {
                            bytes[i] = *(bytePtr + i);
                        }
                    }
                }

                return Encoding.ASCII.GetString(bytes).TrimEnd('\0');
            }
        }
    }
}

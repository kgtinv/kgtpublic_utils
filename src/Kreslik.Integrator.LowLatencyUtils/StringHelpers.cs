﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public static class StringHelpers
    {
        public static unsafe bool TryOverrideStringContentV2(string s, byte[] newContent, int startIdx, int endIdx)
        {
            if (s.Length < endIdx - startIdx)
                return false;

            fixed (char* strPtr = s)
            {
                //DO NOT change length - otherwise runtime (GC especially) can go crazy 
                //int* strPtrToInt = (int*)strPtr;
                //strPtrToInt[-1] = newContent.Length;

                //instead of that, nullify the rest
                for (int idx = endIdx - startIdx; idx < s.Length; idx++)
                    strPtr[idx] = '\0';

                fixed (byte* bytePtr = newContent)
                {
                    //char* srcCharPtr = strPtr;
                    byte* dstCharPtr = bytePtr + startIdx;
                    char* srcCharPtr = strPtr;
                    for (; startIdx <= endIdx; startIdx++, dstCharPtr++, srcCharPtr++)
                        *srcCharPtr = (char)*dstCharPtr;
                }
            }

            return true;
        }
    }
}

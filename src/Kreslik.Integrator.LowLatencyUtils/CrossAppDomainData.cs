﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    //This is serializable, so that calls are performed within calling appdomain
    //
    // WARNING: This needs a
    // [LoaderOptimization(LoaderOptimization.MultiDomain)]
    // declaration on entrypoint
    // that on the other hand makes marshaling by ref not working in some scenarios
    [Serializable]
    public class CrossAppDomainData<T> where T : class
    {
        private CrossAppDomainDataInner _cache;
        [NonSerialized]
        private T _data;

        public CrossAppDomainData(T data)
        {
            _cache = new CrossAppDomainDataInner(data);
            _data = data;
        }

        public T GetCrossAppDomainData()
        {
            if (_data == null)
            {
                _data = CrossAppDomainDataInner.GetCrossAppDomainData(_cache);
            }

            return _data;
        }

        //This is MarshalByRefObject, so that calls are performed within creator (default) app-domain
        // this way we can get to the actual data and grab it's pointer
        //Inner class is needed since due to MarshalByRefObject we need to call static method. To avoid statics we wrap by outer
        private class CrossAppDomainDataInner : UncollectableMarshalByRefObject
        {
            //Array so that UnsafeAddrOfPinnedArrayElement can be called
            private readonly T[] _data;

            public CrossAppDomainDataInner(T data)
            {
                _data = new T[] { data };
            }

            public Tuple<int, IntPtr> GetAddrOfSharedData()
            {
                int gcCount = GC.CollectionCount(0);
                IntPtr ptr = Marshal.UnsafeAddrOfPinnedArrayElement(_data, 0);
                return new Tuple<int, IntPtr>(gcCount, ptr);
            }

            //Needs to be static to be executed in context of calling app domain (so that we can unwrap the data there)
            public static T GetCrossAppDomainData(CrossAppDomainDataInner cacheInstance)
            {
                const int maxAttmeptsCount = 5;

                for (int attemptId = 1; attemptId <= maxAttmeptsCount; attemptId++)
                {
                    //to prevent collections during the next calls
                    GC.Collect();

                    Tuple<int, IntPtr> addr = cacheInstance.GetAddrOfSharedData();

                    //GCHandle h = GCHandle.FromIntPtr(addr.Item2);
                    //T obj = h.Target as T;

                    T obj = GetInstance(addr.Item2);

                    //No GC has occured between grabbing raw pointer to data and wrapping it into fake object on GC heap
                    // So we can freely return it
                    if (addr.Item1 == GC.CollectionCount(0))
                    {
                        return obj;
                    }
                }

                throw new Exception(
                            $"GC occured during x-app cache passing - this happened multiple times ({maxAttmeptsCount}) despite retry logic in place. Extreme utilization of system resources (memory/CPU) might be the rootcause");
            }

            //Crazy stuff here
            // more context: //https://social.msdn.microsoft.com/Forums/vstudio/en-US/06ac44b0-30d8-44a1-86a4-1716dc431c62/how-to-convert-an-intptr-to-an-object-in-c?forum=clr
            private static unsafe T GetInstance(IntPtr pointer)
            {
                try
                {
                    var fakeInstance = default(T);
                    TypedReference typedReference = __makeref(fakeInstance);
                    *(IntPtr*)(&typedReference) = pointer;
                    T instance = (T)__refvalue(typedReference, T);
                    return instance;
                }
                catch
                {
                    return default(T);
                }
            }
        }
    }
}

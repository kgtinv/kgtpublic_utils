﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    [Serializable]
    public class BufferSegmentException : Exception
    {
        public BufferSegmentException()
        { }

        public BufferSegmentException(string message)
            : base(message)
        { }

        public BufferSegmentException(string message, Exception inner)
            : base(message, inner)
        { }
    }


    internal static class GlobalBuffersHolder
    {
        //TODO: this will be one huge block of memory and subsegments will be cut from it
        internal static byte[] Buffer { get; private set; }
        //byte[][] Buffers

        internal static List<byte[]> _unpooledBuffers = new List<byte[]>(); 

        static GlobalBuffersHolder()
        {
            Buffer = new byte[0];
        }

        //TODO: in future this can be called only during initialization, before anybody acesses the buffer
        public static int GetStartOfNewPooledSegment(int segmentSize)
        {
            int segmentStart = Buffer.Length;
            byte[] newBuffer = Buffer;
            Array.Resize(ref newBuffer, Buffer.Length + segmentSize);
            Buffer = newBuffer;

            return segmentStart;
        }

        public static int GetIndexOfNewUnpooledSegment(int segmentSize)
        {
            lock (_unpooledBuffers)
            {
                for (int idx = 0; idx < _unpooledBuffers.Count; idx++)
                {
                    if (_unpooledBuffers[idx] == null)
                    {
                        _unpooledBuffers[idx] = new byte[segmentSize];
                        return idx;
                    }
                }

                _unpooledBuffers.Add(new byte[segmentSize]);
                return _unpooledBuffers.Count - 1;
            }
        }

        public static byte[] GetBuffer(int globalBufferIndex)
        {
            if (globalBufferIndex == -1)
                return Buffer;
            else 
                return _unpooledBuffers[globalBufferIndex];
        }
    }


    public struct BufferSegmentStruct
    {
        private BufferSegmentStruct(int globalBuffresIdx, int segmentStart, int segmentSize)
            :this()
        {
            if (segmentStart + segmentSize > GlobalBuffersHolder.GetBuffer(globalBuffresIdx).Length)
                throw new ByteConvertingException("Invalid BufferSegmentItem definition during construction");

            this.BufferSegmentStart = segmentStart;
            this._globalBuffresIdx = globalBuffresIdx;
        }

        public static BufferSegmentStruct CreatePooledSegment(int segmentStart, int segmentSize)
        {
            return new BufferSegmentStruct(-1, segmentStart, segmentSize);
        }

        public static BufferSegmentStruct CreateUnpooledSegment(int segmentSize)
        {
            return new BufferSegmentStruct(GlobalBuffersHolder.GetIndexOfNewUnpooledSegment(segmentSize), 0, segmentSize);
        }

        public Byte[] Buffer { get { return GlobalBuffersHolder.GetBuffer(_globalBuffresIdx); } }

        private static BufferSegmentStruct _nullItem = new BufferSegmentStruct(-1, 0, 0);

        public static BufferSegmentStruct NULL_BUFFER_STRUCT
        {
            get { return _nullItem; }
        }

        public int BufferSegmentStart { get; private set; }
        private readonly int _globalBuffresIdx;
    }



    /// <summary>
    /// Helper for work with reusable buffer
    /// NOT A THREAD SAFE (to achive higher performance)
    /// </summary>
    public abstract class BufferSegmentBase
#if POOLS_INSTRUMENTATION
        : TimedObjectBase
#endif
    {
        public BufferSegmentBase(int size)
        {
            this.Buffer = new byte[size];
            this.ContentStart = 0;
            this.ContentSize = 0;
        }

        protected BufferSegmentBase(Byte[] buffer, int contentStart, int contentSize)
        {
            this.Buffer = buffer;
            this.ContentStart = contentStart;
            this.ContentSize = contentSize;
        }

        private BufferSegmentBase() { }

        public Byte[] Buffer { get; protected set; }
        public int ContentStart { get; protected set; }
        public int ContentSize { get; protected set; }
        public abstract int SegmentStart { get; }
        public abstract int SegmentSize { get; }
        public int BytesAvailableTowardsEnd { get { return this.SegmentStart + this.SegmentSize - this.ContentStart - this.ContentSize; } }
        public int FreeSpaceStart { get { return this.ContentStart + this.ContentSize; } }

        public byte this[int i]
        {
            get { return this.Buffer[this.ContentStart + i]; }
        }

        public void IncreaseContentSize(int addedSize)
        {
            if (addedSize > this.BytesAvailableTowardsEnd)
                throw new BufferSegmentException(string.Format("Attempt to add {0} bytes to segment of size {1} and available space {2}", addedSize, ContentSize, this.BytesAvailableTowardsEnd));


            this.ContentSize += addedSize;
        }

        public void CutContentFromBegining(int sizeToCut)
        {
            if (sizeToCut > this.ContentSize)
                throw new BufferSegmentException(string.Format("Attempt to cut {0} bytes from segment of size {1}", sizeToCut, ContentSize));

            this.ContentSize -= sizeToCut;
            this.ContentStart += sizeToCut;

            //Following is possible only for unpooled buffer segments
            ////if there is nothing in the buffer then start again from beginning
            //if (this.ContentSize == 0)
            //{
            //    this.ContentStart = this.SegmentStart;
            //}
            //else
            //{
            //    this.ContentStart += sizeToCut;
            //}
        }

        public void ClearContent()
        {
            this.ContentSize = 0;
            this.ContentStart = this.SegmentStart;
        }

        public void CutContentFromEnd(int sizeToCut)
        {
            if (sizeToCut > this.ContentSize)
                throw new BufferSegmentException(string.Format("Attempt to cut {0} bytes from segment of size {1}", sizeToCut, ContentSize));

            this.ContentSize -= sizeToCut;
        }

        public void MoveContentToChildBufferSegment(int sizeToMove, BufferSegmentBase childSegment)
        {
            childSegment.ContentStart = this.ContentStart;
            childSegment.ContentSize = sizeToMove;

            this.CutContentFromBegining(sizeToMove);
        }

        public void MoveContentToAnotherBufferSegment(BufferSegmentBase anotherSegment)
        {
            this.MoveContentToAnotherBufferSegment(this.ContentSize, anotherSegment);
        }

        public void MoveContentToAnotherBufferSegment(int sizeToMove, BufferSegmentBase anotherSegment)
        {
            if (sizeToMove > anotherSegment.BytesAvailableTowardsEnd)
                throw new BufferSegmentException(
                    string.Format("Attempt to move {0} bytes from segment with free size {1}", sizeToMove,
                        anotherSegment.BytesAvailableTowardsEnd));

            System.Buffer.BlockCopy(this.Buffer, this.ContentStart, anotherSegment.Buffer, anotherSegment.FreeSpaceStart,
                sizeToMove);
            this.CutContentFromBegining(sizeToMove);

            anotherSegment.ContentSize += sizeToMove;
        }

        public bool IsFull
        {
            get { return this.ContentStart == this.SegmentStart && this.ContentSize == this.SegmentSize; }
        }

        public void ShiftContentToBegining()
        {
            //Array.Copy(this._buffer, this.ContentStart, this._buffer, 0, this.ContentSize);
            System.Buffer.BlockCopy(this.Buffer, this.ContentStart, this.Buffer, this.SegmentStart, this.ContentSize);
            this.ContentStart = this.SegmentStart;
        }

        public string GetContentAsAsciiString()
        {
            return System.Text.Encoding.ASCII.GetString(this.Buffer, this.ContentStart, this.ContentSize);
        }

        public string GetContentAsAsciiStringSkippingLeadingWhiteChars()
        {
            int start = this.ContentStart;
            int size = this.ContentSize;
            for (; size > 0 && Char.IsWhiteSpace((char)this.Buffer[start]); start++, size--)
            { }

            return System.Text.Encoding.ASCII.GetString(this.Buffer, start, size);
        }

        public override bool Equals(object obj)
        {
            if (this == null)
                throw new NullReferenceException();
            else if (obj == null)
                return false;
            else if (object.ReferenceEquals(this, obj))
                return true;
            else
                return this.Equals(obj as BufferSegmentBase);
        }

        public bool Equals(BufferSegmentBase value)
        {
            if (this == null)
                throw new NullReferenceException();
            else if (value == null)
                return false;
            else if (object.ReferenceEquals((object)this, (object)value))
                return true;
            else if (this.ContentSize != value.ContentSize)
                return false;
            else
                for (int idx = 0; idx < this.ContentSize; idx++)
                {
                    if (this.Buffer[this.ContentStart + idx] != value.Buffer[value.ContentStart + idx])
                        return false;
                }

            return true;
        }

        public bool Equals(byte[] buffer, bool ignoreZeroBytes)
        {
            if (buffer == null || this.ContentSize > buffer.Length || (!ignoreZeroBytes && buffer.Length != this.ContentSize))
                return false;

            if(buffer.Length > this.ContentSize && buffer[this.ContentSize] != 0)
                return false;

            for (int idx = 0; idx < this.ContentSize; idx++)
            {
                if (this.Buffer[this.ContentStart + idx] != buffer[idx])
                    return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int num1 = 352654597;
            int num2 = num1;
            int idx = this.ContentStart;
            int length = this.ContentSize;
            while (length > 2)
            {
                num1 = (num1 << 5) + num1 + (num1 >> 27) ^ this.Buffer[idx];
                num2 = (num2 << 5) + num2 + (num2 >> 27) ^ this.Buffer[idx + 1];
                idx += 2;
                length -= 4;
            }
            if (length > 0)
                num1 = (num1 << 5) + num1 + (num1 >> 27) ^ this.Buffer[idx];
            return num1 + num2 * 1566083941;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("BufferSegment, ASCII string content: [");
            sb.Append(this.GetContentAsAsciiString());
            sb.Append("] Bytes content: ");
            for (int bufferIdx = this.SegmentStart; bufferIdx < this.SegmentStart + this.SegmentSize; bufferIdx++)
            {
                sb.Append(this.Buffer[bufferIdx].ToString("000"));
                sb.Append(' ');
            }

            return sb.ToString();
        }
    }

    public class ParsedBufferSegment : BufferSegmentBase
    {
        public ParsedBufferSegment(byte[] buffer, int contentStart, int contentSize)
            : base(buffer, contentStart, contentSize)
        { }

        public override int SegmentStart
        {
            get { throw new BufferSegmentException("Property not intented to be used"); }
        }

        public override int SegmentSize
        {
            get { throw new BufferSegmentException("Property not intented to be used"); }
        }

        public void OverrideContent(BufferSegmentBase bufferSegment, int newContentStartFromStartOfParent, int newContentSize)
        {
            this.Buffer = bufferSegment.Buffer;
            this.ContentStart = bufferSegment.ContentStart + newContentStartFromStartOfParent;
            this.ContentSize = newContentSize;
        }

        public static readonly ParsedBufferSegment EMPTY = new ParsedBufferSegment(new byte[0], 0, 0);

        public static ParsedBufferSegment CreateFromAsciiString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return EMPTY;

            byte[] buffer = System.Text.Encoding.ASCII.GetBytes(str);
            return new ParsedBufferSegment(buffer, 0, buffer.Length);
        }
    }

    //public class QuickFixBufferSegment : BufferSegmentBase
    //{
    //    private const int _MAX_MESSAGE_SIZE = 8192;
    //    public const int READ_BATCH_SIZE = 512;
    //    public const int FOOTER_SIZE = 7;

    //    private QuickFixBufferSegment()
    //        : base(_MAX_MESSAGE_SIZE)
    //    { }

    //    private QuickFixBufferSegment(byte[] buffer)
    //        : base(buffer, 0, 0)
    //    { }

    //    public static QuickFixBufferSegment CreateReadingBufferSegment()
    //    {
    //        return new QuickFixBufferSegment();
    //    }

    //    public static QuickFixBufferSegment CreateBufferSegmentView(QuickFixBufferSegment parentSegment)
    //    {
    //        return new QuickFixBufferSegment(parentSegment.Buffer);
    //    }

    //    public int HeaderSize { private get; set; }

    //    public void GetBodyBufferSegment(ref ParsedBufferSegment parsedBufferSegment)
    //    {
    //        parsedBufferSegment.OverrideContent(this, this.HeaderSize, this.ContentSize - this.HeaderSize - FOOTER_SIZE);
    //    }

    //    public override int SegmentStart { get { return 0; } }
    //    public override int SegmentSize { get { return _MAX_MESSAGE_SIZE; } }

    //    public void CompactContentIfNeeded()
    //    {
    //        if (this.BytesAvailableTowardsEnd < READ_BATCH_SIZE)
    //            this.ShiftContentToBegining();
    //    }
    //}


    public class BufferSegmentItem : BufferSegmentBase
    {
        public BufferSegmentItem(Byte[] buffer, int segmentStart, int segmentSize)
            : base(buffer, segmentStart, 0)
        {
            if (segmentStart + segmentSize > buffer.Length)
                throw new ByteConvertingException("Invalid BufferSegmentItem definition during construction");

            this.Buffer = buffer;
            this._segmentStart = segmentStart;
            this._segmentSize = segmentSize;
        }

        private BufferSegmentItem()
            : base(null, 0, 0)
        { }

        private static BufferSegmentItem _nullItem = new BufferSegmentItem();

        public static BufferSegmentItem NULL_BUFFER_ITEM
        {
            get { return _nullItem; }
        }

        private int _segmentStart;
        private int _segmentSize;

        public override int SegmentStart
        {
            get { return _segmentStart; }
        }

        public override int SegmentSize
        {
            get { return _segmentSize; }
        }
    }

    public class QuickFixParentBufferSegmentItem : BufferSegmentItem
    {
        IPoolingHelper _poolingHelper;

        public QuickFixParentBufferSegmentItem(Byte[] buffer, int segmentStart, int segmentSize)
            : base(buffer, segmentStart, segmentSize)
        {
            _poolingHelper = new PoolingHelper(this.ReleaseInternal);
        }

        public void MoveContentFragmentToChild(QuickFixSubsegment childSubsegment, int headerSize, int bodySize)
        {
            this.MoveContentToChildBufferSegment(headerSize + bodySize + QuickFixSubsegment.FOOTER_SIZE, childSubsegment);
            childSubsegment.HeaderSize = headerSize;

            //child segment is holding reference to parent segment
            _poolingHelper.Acquire();
        }

        public QuickFixParentBufferSegmentItem Acquire()
        {
            _poolingHelper.AcquireInitial();
            return this;
        }

        public void Release()
        {
            _poolingHelper.Release();
        }

        public void MarkChildAsReleased(QuickFixSubsegment childSubsegment)
        {
            _poolingHelper.Release();
        }

        private void ReleaseInternal()
        {
            QuickFixSegmentsManager.Instance.Return(this);
        }

        public void Invalidate()
        { }
    }

    public class QuickFixSegmentsManager
    {
        private const int _MAX_STANDART_MESSAGE_SIZE = 8192;
        //1GB
        private const int _MAX_LARGE_MESSAGE_SIZE = 1024*1024*1024;
        private const int _MIN_READ_BATCH_SIZE = 512;
        private IBufferPool<QuickFixParentBufferSegmentItem> _parentSegmentsPool;
        private IExtendedObjectPool<QuickFixSubsegment> _childSubSegmentsPool;
        private SafeTimer _perfCountersTimer;
        private IIntegratorPerformanceCounter _perfCounters = IntegratorPerformanceCounter.Instance;

        //All counteraprties have 2 sessions + 2 STP sessions; 20% of segments are extra
        private static QuickFixSegmentsManager _instance = new QuickFixSegmentsManager((int) ((Counterparty.ValuesCount * 2 + 2) * 1.5));
        public static QuickFixSegmentsManager Instance { get { return _instance; } }

        private QuickFixSegmentsManager(int segmentsCount)
        {
            _parentSegmentsPool = new BufferPool<QuickFixParentBufferSegmentItem>(_MAX_STANDART_MESSAGE_SIZE, segmentsCount,
                PooledQuickFixParentBufferSegmentItem.CreateNewPooled, PooledQuickFixParentBufferSegmentItem.CreateNewUnpooled,
                PooledQuickFixParentBufferSegmentItem.IsPooled, PooledQuickFixParentBufferSegmentItem.ClearBufferSegmentItem, "BufferPoolForQuickFix");
            var basicPool = new GCFreeObjectPool<QuickFixSubsegment>(PooledQuickFixSubsegment.CreateEmptyPooled,
                QuickFixSubsegment.Invalidate,
                segmentsCount*7, "BufferSubsegments");
            _childSubSegmentsPool = new ObjectPoolExtender<QuickFixSubsegment>(basicPool, QuickFixSubsegment.CreateEmpty,
                item => item is PooledQuickFixSubsegment);
            this._perfCountersTimer = new SafeTimer(PerfCountersCallback, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest,
                IsUltraQuickCallback = true
            };
            this.InitializeCountersState();
        }

        private void InitializeCountersState()
        {
            this._perfCounters.SetMessagingBufferSegemntsPoolSize(this._parentSegmentsPool.TotalPoolSize);
            this._perfCounters.SetMessagingSubsegmentsPoolSize(this._childSubSegmentsPool.TotalPoolSize);
        }

        private void PerfCountersCallback()
        {
            this._perfCounters.SetUnpooledMessagingBufferSegemntsInUseCount(this._parentSegmentsPool.NumberOfUnpooledItemsInUse);
            this._perfCounters.SetTotalUnpooledMessagingBufferSegemntsCreated(this._parentSegmentsPool.TotalNumberOfUnpooledItemsCreated);
            this._perfCounters.SetPooledMessagingBufferSegemntsInUseCount(this._parentSegmentsPool.NumberOfPooledItemsInUse);
            this._perfCounters.SetUnpooledMessagingSubsegmentsInUseCount(this._childSubSegmentsPool.NumberOfUnpooledItemsInUse);
            this._perfCounters.SetTotalUnpooledMessagingSubsegmentsCreated(this._childSubSegmentsPool.TotalNumberOfUnpooledItemsCreated);
            this._perfCounters.SetPooledMessagingSubsegmentsInUseCount(this._childSubSegmentsPool.NumberOfPooledItemsInUse);
        }

        public void Return(QuickFixParentBufferSegmentItem parentBufferSegment)
        {
            this._parentSegmentsPool.Return(parentBufferSegment);
        }

        public void Return(QuickFixSubsegment bufferSubsegment)
        {
            bufferSubsegment.ClearAcquireCount();
            this.ReturnReleased(bufferSubsegment);
        }

        internal void ReturnReleased(QuickFixSubsegment bufferSubsegment)
        {
            //prefetched items might not have parent yet populated
            if (bufferSubsegment.ParentSegment != null)
                bufferSubsegment.ParentSegment.MarkChildAsReleased(bufferSubsegment);
            this._childSubSegmentsPool.Return(bufferSubsegment);
        }

        public QuickFixParentBufferSegmentItem FetchNextSegmet()
        {
            return this._parentSegmentsPool.Fetch().Acquire();
        }

        private QuickFixParentBufferSegmentItem FetchNextSegmentReleasingCurrent(QuickFixParentBufferSegmentItem current)
        {
            QuickFixParentBufferSegmentItem next = this.FetchNextSegmet();
            current.MoveContentToAnotherBufferSegment(next);
            current.Release();
            return next;
        }

        public QuickFixParentBufferSegmentItem FetchNextIfNeeded(QuickFixParentBufferSegmentItem current)
        {
            //if current is full, there is no sense in attempting to move it
            // also, if we are currently servising unpooled increased segment with too much occupied size, then we cannot simply move it, as content wouldn't fit
            if (current.BytesAvailableTowardsEnd < _MIN_READ_BATCH_SIZE && !current.IsFull && current.ContentSize <= _MAX_STANDART_MESSAGE_SIZE)
                return this.FetchNextSegmentReleasingCurrent(current);
            else 
                return current;
        }

        /// <summary>
        /// Indicates whether current segment is already too much occupied and cannot be shifted or mooved to another standart segment
        ///  Allocation of increased segment is required
        /// </summary>
        /// <param name="current"></param>
        /// <returns></returns>
        public bool NeedsMoveToLargeUnpooledSegment(QuickFixParentBufferSegmentItem current)
        {
            return current.BytesAvailableTowardsEnd < _MIN_READ_BATCH_SIZE &&
                   current.ContentStart == current.SegmentStart;
        }

        public QuickFixParentBufferSegmentItem MoveToLargeUnpooledSegment(QuickFixParentBufferSegmentItem current)
        {
            if (current.SegmentSize * 2 > _MAX_LARGE_MESSAGE_SIZE)
                throw new BufferSegmentException("Attempt to allocate too large segment (> 1GB) for single message");

            QuickFixParentBufferSegmentItem largeUnpooled = PooledQuickFixParentBufferSegmentItem.CreateNewUnpooled(current.SegmentSize*2);
            largeUnpooled.Acquire();
            current.MoveContentToAnotherBufferSegment(largeUnpooled);
            current.Release();
            return largeUnpooled;
        }

        //public QuickFixSubsegment MoveContentFragmentFromParentToChild(QuickFixParentBufferSegmentItem parentSegment, int headerSize,
        //    int bodySize)
        //{
        //    QuickFixSubsegment childSubsegment = this._childSubSegmentsPool.Fetch();
        //    return this.MoveContentFragmentFromParentToChild(parentSegment, childSubsegment, headerSize, bodySize);
        //}

        public QuickFixSubsegment MoveContentFragmentFromParentToChild(QuickFixParentBufferSegmentItem parentSegment, QuickFixSubsegment childSubsegment, int headerSize,
            int bodySize)
        {
            childSubsegment.OverrideContent(parentSegment, headerSize, bodySize);
            return childSubsegment;
        }

        public int TryFetchBatchOfChildSubsegments(QuickFixSubsegment[] childSegmentsBatch)
        {
            int fetchedCnt = this._childSubSegmentsPool.TryFetchBatch(childSegmentsBatch, childSegmentsBatch.Length);
            for (int idx = 0; idx < fetchedCnt; idx++)
            {
                if(childSegmentsBatch[idx] == null)
                    break;
                
                childSegmentsBatch[idx].AcquireUnchecked();
            }

            return fetchedCnt;
        }

        public QuickFixSubsegment FetchNextChildSubsegment()
        {
            var subsegment = this._childSubSegmentsPool.Fetch();
            subsegment.AcquireUnchecked();
            return subsegment;
        }

        private class PooledQuickFixSubsegment : QuickFixSubsegment
        {
            private PooledQuickFixSubsegment()
                : base()
            { }

            public static PooledQuickFixSubsegment CreateEmptyPooled()
            {
                return new PooledQuickFixSubsegment();
            }

            ~PooledQuickFixSubsegment()
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Pooled BufferSubsegment leaked");
            }
        }

        private class PooledQuickFixParentBufferSegmentItem : QuickFixParentBufferSegmentItem
        {
            internal static void ClearBufferSegmentItem(QuickFixParentBufferSegmentItem item)
            {
                item.ClearContent();
                //this is not needed, but might help to 'fail fast'
                Array.Clear(item.Buffer, item.SegmentStart, item.SegmentSize);
                item.Invalidate();
            }

            internal static bool IsPooled(QuickFixParentBufferSegmentItem item)
            {
                return item is PooledQuickFixParentBufferSegmentItem;
            }

            internal static QuickFixParentBufferSegmentItem CreateNewPooled(Byte[] buffer, int segmentStart, int segmentSize)
            {
                return new PooledQuickFixParentBufferSegmentItem(buffer, segmentStart, segmentSize);
            }

            internal static QuickFixParentBufferSegmentItem CreateNewUnpooled(int segmentSize)
            {
                return new QuickFixParentBufferSegmentItem(new byte[segmentSize], 0, segmentSize);
            }

            private PooledQuickFixParentBufferSegmentItem(Byte[] buffer, int segmentStart, int segmentSize)
                : base(buffer, segmentStart, segmentSize) { }

            ~PooledQuickFixParentBufferSegmentItem()
            {
                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Pooled BufferSegment leaked");
            }
        }
    }
    
    public class QuickFixSubsegment : BufferSegmentBase, IPooledBufferSegment
    {
        public const int FOOTER_SIZE = 7;

        protected QuickFixSubsegment()
            : base(null, 0, 0)
        {
            _poolingHelper = new PoolingHelper(ReleaseInternal);
        }

        public static QuickFixSubsegment CreateEmpty()
        {
            return new QuickFixSubsegment();
        }

        IPoolingHelper _poolingHelper;

        internal void OverrideContent(QuickFixParentBufferSegmentItem parentSegment, int headerSize, int bodySize)
        {
            this.Buffer = parentSegment.Buffer;
            parentSegment.MoveContentFragmentToChild(this, headerSize, bodySize);
            this.ParentSegment = parentSegment;

            _poolingHelper.SetValid();
        }

        internal void OverrideContentAndAcquire(QuickFixParentBufferSegmentItem parentSegment, int headerSize, int bodySize)
        {
            this.Buffer = parentSegment.Buffer;
            parentSegment.MoveContentFragmentToChild(this, headerSize, bodySize);
            this.ParentSegment = parentSegment;

            _poolingHelper.AcquireInitial();
        }

        public static void Invalidate(QuickFixSubsegment item)
        {
            item.ContentSize = int.MinValue;
            item.ContentStart = int.MinValue;
            item.Buffer = null;
            item.ParentSegment = null;
        }

        public void Acquire()
        {
            this._poolingHelper.Acquire();
        }

        internal void AcquireUnchecked()
        {
            this._poolingHelper.AcquireInitial();
        }

        internal void ClearAcquireCount()
        {
            this._poolingHelper.ClearAcquireCount();
        }

        private void ReleaseInternal()
        {
            QuickFixSegmentsManager.Instance.ReturnReleased(this);
        }

        public void Release()
        {
            this._poolingHelper.Release();
        }

        public QuickFixParentBufferSegmentItem ParentSegment { get; private set; }


        public int HeaderSize { private get; set; }

        public void GetBodyBufferSegment(ref ParsedBufferSegment parsedBufferSegment)
        {
            parsedBufferSegment.OverrideContent(this, this.HeaderSize, this.ContentSize - this.HeaderSize - FOOTER_SIZE);
        }


        public override int SegmentStart
        {
            get { throw new NotImplementedException(); }
        }

        public override int SegmentSize
        {
            get { throw new NotImplementedException(); }
        }
    }
}

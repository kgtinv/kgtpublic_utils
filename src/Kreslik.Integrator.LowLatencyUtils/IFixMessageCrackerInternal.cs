﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.Contracts.Internal
{
    public interface IFixMessageCracker
    {
        byte RecognizedMessageType { get; }
        //this should be a switch statemenet - as framework optimizes switches
        // to either linear search or dictionary lookup (to what would be faster in each specific case)
        void ConsumeFixTag(int tagId, BufferSegmentBase tagValueSegment);
        bool TryRecoverFromParsingErrors { get; }
        void Reset(DateTime currentMessageReceivedTimeUtc, QuickFixSubsegment currentMessage);
        void MessageParsingDone();
    }
}

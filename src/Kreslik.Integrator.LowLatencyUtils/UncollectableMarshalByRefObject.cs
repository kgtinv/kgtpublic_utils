﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public abstract class UncollectableMarshalByRefObject: MarshalByRefObject
    {
        //This is to forcefull solution - but for now ok
        // it prevents the object in remote app domain to be collected
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.Infrastructure)]
        public override object InitializeLifetimeService()
        {
            return null;
        }


        //Below is the sample code how to properly solve the leases


        //public class Sponsor : MarshalByRefObject, ISponsor
        //{
        //    private DateTime lastRenewal = DateTime.Now;
        //    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.Infrastructure)]
        //    public TimeSpan Renewal(ILease lease)
        //    {
        //        Console.WriteLine("Request to renew the lease time.");
        //        Console.WriteLine("Time since last renewal: " +
        //           (DateTime.Now - lastRenewal).ToString());

        //        lastRenewal = DateTime.Now;
        //        return TimeSpan.FromSeconds(10);
        //    }
        //}

        //class RemoteClass : MarshalByRefObject, ISponsor
        //{
        //    public string Echo(string s)
        //    {
        //        return s;
        //    }


        //    private DateTime lastRenewal = DateTime.Now;
        //    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.Infrastructure)]
        //    public TimeSpan Renewal(ILease lease)
        //    {
        //        Console.WriteLine("Request to renew the lease time.");
        //        Console.WriteLine("Time since last renewal: " +
        //           (DateTime.Now - lastRenewal).ToString());

        //        lastRenewal = DateTime.Now;
        //        return TimeSpan.FromSeconds(10);
        //    }

        //    [SecurityPermissionAttribute(SecurityAction.Demand,
        //                           Flags = SecurityPermissionFlag.Infrastructure)]
        //    public override Object InitializeLifetimeService()
        //    {
        //        ILease lease = (ILease)base.InitializeLifetimeService();
        //        if (lease.CurrentState == LeaseState.Initial)
        //        {
        //            lease.InitialLeaseTime = TimeSpan.FromSeconds(3);
        //            lease.SponsorshipTimeout = TimeSpan.FromSeconds(6);
        //            lease.RenewOnCallTime = TimeSpan.FromSeconds(3);
        //        }
        //        return lease;
        //    }
        //}
        //class Program
        //{
        //    static RemoteClass rc;
        //    static TimeSpan time;
        //    static Timer timer;
        //    static void Main(string[] args)
        //    {
        //        AppDomain ad = AppDomain.CreateDomain("OtherDomain");
        //        rc = (RemoteClass)ad.CreateInstanceAndUnwrap(
        //        Assembly.GetExecutingAssembly().FullName,
        //        typeof(RemoteClass).FullName);
        //        Sponsor s = new Sponsor();
        //        ((ILease)rc.GetLifetimeService()).Register(s);
        //        //GC.Collect();
        //        time = TimeSpan.FromSeconds(15);
        //        timer = new Timer((o) =>
        //        {
        //            time = time.Subtract(TimeSpan.FromSeconds(1));
        //            //GC.Collect();
        //            Console.WriteLine(time);
        //            if (time.TotalSeconds <= 1) { Console.WriteLine(rc.Echo("Hello")); timer.Dispose(); }
        //        }); timer.Change(0, 1000); Console.ReadLine();
        //    }
        //}
    }
}

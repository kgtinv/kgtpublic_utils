﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public interface ILocalTobArray
    {
        DateTime LastWriteTimeUtc { get; }
        void TryRefresh();
        void ReceiveBestPrice(Symbol symbol, PriceSide side, out decimal price, out decimal size,
            out Counterparty counterparty);
    }

    public class ToBReceiver
    {
        public class ToBReceiverIndex
        {
            public Symbol Symbol { get; private set; }
            public PriceSide PriceSide { get; private set; }
            public HotspotCounterparty HotspotCounterparty { get; private set; }

            internal ToBReceiverIndex(Symbol symbol, PriceSide priceSide, HotspotCounterparty hotspotCounterparty)
            {
                //dummy call to find out if there are any exceptions (e.g. unsupported Counterparty)
                ToBconverter.GetOffset(symbol, priceSide, hotspotCounterparty);

                this.Symbol = symbol;
                this.PriceSide = priceSide;
                this.HotspotCounterparty = hotspotCounterparty;
            }
        }

        public ToBReceiverIndex CreateToBReceiverIndex(Symbol symbol, PriceSide priceSide, HotspotCounterparty hotspotCounterparty)
        {
            return new ToBReceiverIndex(symbol, priceSide, hotspotCounterparty);
        }


        private MemoryMappedViewAccessor _memoryAccessor;
        //need to GC root this as view accessor doesn't take full ownership
        private MemoryMappedFile _mmfReceiver;
        private ILogger _logger;
        private int _timestampIndex;
        private long _fataleErrorTimestampDelayTicks = TimeSpan.FromSeconds(1).Ticks;
        private long _disconnectTimestampDelayTicks = TimeSpan.FromSeconds(2).Ticks;
        private EventWaitHandle _sharedSignalEvent;
        private EncodedVersionedIndexHelperSingleThreaded _receiverIndexHelper = new EncodedVersionedIndexHelperSingleThreaded();
        //without the timestamps
        private ulong[] _localArrayOfPrices = new ulong[ToBconverter.IndexOfUpdateTimestamp / sizeof(long)];
        private ulong[] _localTempArrayOfPrices = new ulong[ToBconverter.IndexOfUpdateTimestamp / sizeof(long)];

        private static TimeSpan[] _retryIntervals = new TimeSpan[] { TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10) };

        public ToBReceiver(ILogger logger)
        {
            this.Ready = false;
            this._logger = logger;
            _timestampIndex = ToBconverter.IndexOfUpdateTimestamp;
            Task.Factory.StartNew(PriceReceivingFunc, TaskCreationOptions.LongRunning).ObserveException();
        }

        private void InitializeMMFView()
        {
            int attempts = 0;

            do
            {
                System.Threading.Thread.Sleep(_retryIntervals[Math.Min(attempts, _retryIntervals.Length - 1)]);
                attempts++;

                try
                {
                    _mmfReceiver = MemoryMappedFile.OpenExisting(ToBconverter.SHARED_SEGMENT_NAME);
                    _memoryAccessor = _mmfReceiver.CreateViewAccessor(0, ToBconverter.MMFSize);
                    Volatile.Write(ref _ready, true);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Exception while attempting (attempt #{0}) to open shared hotspot prices location. Will retry until available.", attempts);
                }
            } while (!Ready);

            attempts = 0;
            while (!EventWaitHandle.TryOpenExisting(ToBconverter.SHARED_EVENT_NAME, out _sharedSignalEvent))
            {
                attempts++;
                this._logger.Log(LogLevel.Fatal, "Failed to open share ToB sharing signalization event (attempt #{0})", attempts);
                System.Threading.Thread.Sleep(_retryIntervals[Math.Min(attempts - 1, _retryIntervals.Length - 1)]);
            }
        }

        private object _disconnectingLocker = new object();
        internal long LastTimestampWritten { get; private set; }
        private void CheckFreshness()
        {
            LastTimestampWritten = _memoryAccessor.ReadInt64(_timestampIndex);
            this.CheckFreshness(LastTimestampWritten);
        }

        private void CheckFreshness(long lastTimestampWritten)
        {
            if (HighResolutionDateTime.UtcNow.Ticks - lastTimestampWritten > _fataleErrorTimestampDelayTicks)
            {
                lock (_disconnectingLocker)
                {
                    if (!Ready)
                        return;

                    if (HighResolutionDateTime.UtcNow.Ticks - lastTimestampWritten > _disconnectTimestampDelayTicks)
                    {
                        Ready = false;
                        throw new Exception(
                            "Shared file timestamp is too old, publisher is probably disconnected, reinitializing");
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Info, "ToBReceiver is detecting suspiciously delayed timestamp");
                    }
                }
            }
        }

        public event Action Stopped;

        private void PriceReceivingFunc()
        {
            while (true)
            {
                if (!Ready)
                {
                    if (Stopped != null)
                        Stopped();
                    if (_memoryAccessor != null)
                        this._memoryAccessor.Dispose();
                    InitializeMMFView();
                }

                try
                {
                    if (_sharedSignalEvent.WaitOne(TimeSpan.FromSeconds(2)))
                    {
                        DateTime now = HighResolutionDateTime.UtcNow;
                        int changedIndex;
                        DateTime sentTime;
                        if (_receiverIndexHelper.TryGetDecodedIndex(
                                _memoryAccessor.ReadUInt64(ToBconverter.IndexOfChangedIndexValue), now, out changedIndex, out sentTime))
                        {
                            //read one value
                            ulong updatedAtomicValue = _memoryAccessor.ReadUInt64(changedIndex);
                            int priceIdxLocal = changedIndex/sizeof (long);
                            _localArrayOfPrices[priceIdxLocal] = updatedAtomicValue;
                            this.HandleNewFreshPrice(priceIdxLocal, updatedAtomicValue, now, sentTime);
                        }
                        else
                        {
                            //read the whole array and compare
                            int read = _memoryAccessor.ReadArray(0, _localTempArrayOfPrices, 0, _localTempArrayOfPrices.Length);
                            if (read != _localTempArrayOfPrices.Length)
                                this._logger.Log(LogLevel.Fatal, "ToB communicater read from shared memory {0} items instead of expected {1}",
                                    read, _localTempArrayOfPrices.Length);

                            for (int priceIdx = 0; priceIdx < _localTempArrayOfPrices.Length; priceIdx++)
                            {
                                if (_localTempArrayOfPrices[priceIdx] != _localArrayOfPrices[priceIdx])
                                {
                                    _localArrayOfPrices[priceIdx] = _localTempArrayOfPrices[priceIdx];
                                    this.HandleNewFreshPrice(priceIdx, _localTempArrayOfPrices[priceIdx], now, sentTime);
                                }
                            }
                        }
                    }
                    else
                    {
                        this.CheckFreshness();
                    }
                }
                catch (TobConverterException tobConverterException)
                {
                    this._logger.LogException(LogLevel.Fatal, tobConverterException, "ToBReceiver receiving atomic price value that cannot be decoded.");
                }
                catch (DecimalUtilsException decimalUtilsException)
                {
                    this._logger.LogException(LogLevel.Fatal, decimalUtilsException, "ToBReceiver receiving atomic price value that cannot be decoded.");
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal,
                        "Exception during reading hotspot prices from remote process. Will try to reinitialize", e);
                    Ready = false;
                }
                
            }
        }

        private bool _ready;

        public bool Ready
        {
            get { return this._ready; }
            set
            {
                if (this._ready != value)
                {
                    this._ready = value;
                    if (this.ReadyStateChanging != null)
                        this.ReadyStateChanging(value);
                }
            }
        }

        public event Action<bool> ReadyStateChanging; 

        public delegate void NewPriceDelegate(
            AtomicDecimal price, decimal size, Symbol symbol, PriceSide side, Counterparty counterparty,
            DateTime counterpartySent, DateTime integratorReceived);

        public event NewPriceDelegate NewPriceAvailable;
        public event Action<Symbol, PriceSide, Counterparty> PriceCanceled; 

        private void HandleNewFreshPrice(int priceIdxLocal, ulong priceConverted, DateTime priceRead, DateTime priceSent)
        {
            Symbol symbol;
            PriceSide priceSide;
            Counterparty cpt;
            AtomicDecimal price;
            decimal size;

            //BEWARE! priceIdxLocal is already divided by sizeof(ulong)
            ToBconverter.GetOffsetDimensions(priceIdxLocal, out symbol, out priceSide, out cpt);

            if (priceConverted == 0)
            {
                try
                {
                    if (PriceCanceled != null)
                        PriceCanceled(symbol, priceSide, cpt);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, "Exception during pushing hotspot price", e);
                }
            }
            else
            {
                //get size and price from converted value
                DecimalUtils.ConvertPriceFromAtomicValue(priceConverted, out price, out size);

                try
                {
                    if (NewPriceAvailable != null)
                        NewPriceAvailable(price, size, symbol, priceSide, cpt, priceSent, priceRead);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, "Exception during pushing hotspot price", e);
                }
            }
        }

        public bool ReceivePrice(bool forceReadWithoutCache, ToBReceiverIndex toBReceiverIndex, out decimal price, out decimal size)
        {
            price = 0m;
            size = 0m;

            if (!this.Ready)
                return false;

            ulong atomicValue = 0;
            try
            {
                if (forceReadWithoutCache)
                    atomicValue =
                        _memoryAccessor.ReadUInt64(ToBconverter.GetOffset(toBReceiverIndex.Symbol,
                            toBReceiverIndex.PriceSide, toBReceiverIndex.HotspotCounterparty));
                else
                    atomicValue = this._localArrayOfPrices[ToBconverter.GetOffset(toBReceiverIndex.Symbol,
                        toBReceiverIndex.PriceSide, toBReceiverIndex.HotspotCounterparty)/sizeof (long)];
                DecimalUtils.ConvertPriceFromAtomicValue(atomicValue, out price, out size);
                return true;
            }
            catch (TobConverterException tobConverterException)
            {
                this._logger.LogException(LogLevel.Fatal, tobConverterException,
                    "ToBReceiver receiving atomic price value that cannot be decoded. Value: {0}", atomicValue);
            }
            catch (DecimalUtilsException decimalUtilsException)
            {
                this._logger.LogException(LogLevel.Fatal, decimalUtilsException,
                    "ToBReceiver receiving atomic price value that cannot be decoded. Value: {0}", atomicValue);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal,
                    "ToBReceiver experiencing exception during receiving a price. Trying to reinitialize price share receiving",
                    e);
            }

            return false;
        }
    }


    public class ToBSender
    {
        //private ToBSenderUnit[] _senderUnits = new ToBSenderUnit[Symbol.ValuesCount];
        private readonly MemoryMappedViewAccessor _memoryAccessor;
        //need to GC root this as view accessor doesn't take full ownership
        private readonly MemoryMappedFile _mmf;
        private readonly EventWaitHandle _sharedSignalEvent;
        private bool _unusable;
        public ILogger Logger { get; set; }
        private int _timestampIndex;
        private SafeTimer<object> _stampingTimer;
        private EncodedVersionedIndexHelperSingleThreaded _senderIndexHelper = new EncodedVersionedIndexHelperSingleThreaded();
        private HotspotCounterparty _nonDisruptingCounterparty;

        public ToBSender(ILogger logger, HotspotCounterparty nonDisruptingCounterparty)
        {
            // long (8 bytes) per each symbol and side 
            _mmf = MemoryMappedFile.CreateOrOpen(ToBconverter.SHARED_SEGMENT_NAME, ToBconverter.MMFSize);
            _memoryAccessor = _mmf.CreateViewAccessor(0, ToBconverter.MMFSize);
            bool createdNew;
            _sharedSignalEvent = new EventWaitHandle(false, EventResetMode.AutoReset, ToBconverter.SHARED_EVENT_NAME, out createdNew);
            if (!createdNew)
                throw new Exception(string.Format("Cannot initialize Hotspot ToB shared signal (Event {0} already exists)", ToBconverter.SHARED_EVENT_NAME));
            _timestampIndex = ToBconverter.IndexOfUpdateTimestamp;
            this.Logger = logger;
            _stampingTimer = new SafeTimer<object>(StampFile, TimeSpan.FromMilliseconds(500), true)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            _stampingTimer.ExecuteSynchronouslyWithReschedulingIfRequested();
            this._nonDisruptingCounterparty = nonDisruptingCounterparty;
        }

        private void StampFile(object unused)
        {
            _memoryAccessor.Write(_timestampIndex, HighResolutionDateTime.UtcNow.Ticks);
            Thread.MemoryBarrier();
        }

        public void SendConvertedPrice(long index, ulong convertedPrice, DateTime priceSent, Counterparty priceCounterparty)
        {
            if (_unusable)
                return;

            try
            {
                _memoryAccessor.Write(index, convertedPrice);
                bool disrupt = priceCounterparty != this._nonDisruptingCounterparty;
                _memoryAccessor.Write(ToBconverter.IndexOfChangedIndexValue, _senderIndexHelper.GetEncodedIndex((int) index, priceSent, disrupt));
                //this signals to the reader process, and also serves as caches flushing
                if(disrupt)
                    _sharedSignalEvent.Set();


                //This barier might be needed to ensure caches flush
                //  however it appears that MMF actually writtes through
                //Thread.MemoryBarrier();

                // Uncler if this call guarantees flush of local caches.
                //  Memory fencing is likely safer bet
                //_memoryAccessor.Flush();
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e,
                    "ToBSender experiencing exception during sending a price. Disabling price sharing till next restart. ConvertedPrice: {0}, Index: {1}",
                    convertedPrice, index);
                this._unusable = true;
                this._memoryAccessor.Dispose();
            }
        }

        public void SendCancelAllQuotes(Counterparty counterparty)
        {
            if (_unusable)
                return;

            try
            {
                DateTime now = HighResolutionDateTime.UtcNow;
                long lowInclusiveBound, highExclusiveBound;
                ToBconverter.GetBoundsForCounterparty(counterparty, out lowInclusiveBound, out highExclusiveBound);
                for (long idx = lowInclusiveBound; idx < highExclusiveBound; idx += sizeof(ulong))
                {
                    //CAREFULL: need to convert to ulong
                    _memoryAccessor.Write(idx, (ulong) 0);
                }
                bool disrupt = counterparty != this._nonDisruptingCounterparty;
                //intentionally more _senderIndexHelper calls - so that reader is off-sync and must read whole array
                _senderIndexHelper.GetEncodedIndex(0, now, disrupt);
                _senderIndexHelper.GetEncodedIndex(0, now, disrupt);
                _memoryAccessor.Write(ToBconverter.IndexOfChangedIndexValue, _senderIndexHelper.GetEncodedIndex(0, now, disrupt));
                if (disrupt)
                    _sharedSignalEvent.Set();
            }
            catch (Exception e)
            {
                this.Logger.LogException(LogLevel.Fatal, e,
                    "ToBSender experiencing exception during sending all quotes cancel. Disabling price sharing till next restart. Counterparty: {0}",
                    counterparty);
                this._unusable = true;
                this._memoryAccessor.Dispose();
            }
        }
    }


    public class EncodedVersionedIndexHelperSingleThreaded
    {
        private ulong _version = MAX_VERSION_MASK-1;
        private const uint MILLISECONDS_TODAY_MASK = 134217727;
        private const uint MAX_VERSION_MASK = 131071;
        private const int MILLISECONDS_BITS = 27;
        private const uint MAX_INDEX = 1048575;

        public ulong GetEncodedIndex(int index, DateTime sentTime, bool increaseVersion)
        {
            if (index > MAX_INDEX)
                throw new Exception("Index is too high for conversion");

            if (increaseVersion)
                //single threaded
                _version = (_version + 1) & MAX_VERSION_MASK;

            return (((ulong) index) << 44) + (_version << MILLISECONDS_BITS) + (uint) sentTime.TimeOfDay.TotalMilliseconds;
        }

        public bool TryGetDecodedIndex(ulong encodedIndex, DateTime now, out int index, out DateTime sentTime)
        {
            //single threaded
            _version = (_version + 1) & MAX_VERSION_MASK;
            index = (int)(encodedIndex >> 44);

            sentTime = now.Date + TimeSpan.FromMilliseconds(encodedIndex & MILLISECONDS_TODAY_MASK);
            if (sentTime > now && sentTime > now.AddHours(1))
                sentTime = sentTime.AddDays(-1);

            ulong versionDecoded = (encodedIndex >> MILLISECONDS_BITS) & MAX_VERSION_MASK;
            if (versionDecoded == _version)
            {
                return true;
            }
            else
            {
                _version = versionDecoded;
                return false;
            }
        }
    }

    public static class ToBconverter
    {
        //@"Global\HotspotTobPrices";  <--- this is for global visibility across sessions, however needs UAC
        public const string SHARED_SEGMENT_NAME = @"HotspotTobPrices";
        public const string SHARED_EVENT_NAME = @"HotspotToBUpdatedEvent";

        public static int GetOffset(Symbol symbol, PriceSide side, Counterparty counterparty)
        {
            return GetOffsetInternal(symbol, side, GetOrderRankOfCounterparty(counterparty));
        }

        internal static int GetOffsetInternal(Symbol symbol, PriceSide side, int counterpartyIdx)
        {
            //each symbol and side is stored on one long
            return (2 * ((int)symbol) + ((int)side)) * sizeof(long) +
                   counterpartyIdx * _sizeOfMMFForOneCounterparty;
        }

        internal static void GetOffsetDimensions(int offsetIndexLocal, out Symbol symbol, out PriceSide priceSide,
            out Counterparty counterparty)
        {
            //offsetIndexLocal already divided by sizeof(long)

            int cptIdxLocal = 0;
            if (offsetIndexLocal >= _sizeOfMMFForOneCounterpartyLocal)
            {
                cptIdxLocal = offsetIndexLocal/_sizeOfMMFForOneCounterpartyLocal;
                offsetIndexLocal -= cptIdxLocal*_sizeOfMMFForOneCounterpartyLocal;
            }
            counterparty = GetCounterpartyForOrderRank(cptIdxLocal);

            //Bid 0, Ask 1
            if ((offsetIndexLocal & 1) == 1)
                priceSide = PriceSide.Ask;
            else
                priceSide = PriceSide.Bid;

            symbol = (Symbol) (offsetIndexLocal >> 1);
        }

        //num of supported cpts
        internal const int NumberOfSupportedCounterparties = 2;

        public static readonly IEnumerable<HotspotCounterparty> SupportedCounterparties = new List<HotspotCounterparty>()
        {
            //Hotspot prices sharing not supported any more

            //HotspotCounterparty.H4T,
            //HotspotCounterparty.HTF
        };

        public static readonly IEnumerable<HotspotCounterparty> CounterpartiesForBook = new List<HotspotCounterparty>()
        {
            HotspotCounterparty.H4T
        };

        internal static void GetBoundsForCounterparty(Counterparty counterparty, out long lowerInclusiveBound, out long highExclusiveBound)
        {
            int cptIdx = GetOrderRankOfCounterparty(counterparty);

            lowerInclusiveBound = cptIdx*_sizeOfMMFForOneCounterparty;
            highExclusiveBound = lowerInclusiveBound + _sizeOfMMFForOneCounterparty;
        }

        private static int GetOrderRankOfCounterparty(Counterparty counterparty)
        {
            if (counterparty == HotspotCounterparty.H4T)
                return 0;
            if (counterparty == HotspotCounterparty.HTF && NumberOfSupportedCounterparties > 1)
                return 1;

            throw new TobConverterException("Fast memory sharing of ToBs not coded for counterparty " +
                                            counterparty);
        }

        internal static Counterparty GetCounterpartyForOrderRank(int counterpartyOrderRank)
        {
            if (counterpartyOrderRank == 0)
                return HotspotCounterparty.H4T;
            if (counterpartyOrderRank == 1 && NumberOfSupportedCounterparties > 1)
                return HotspotCounterparty.HTF;

            throw new TobConverterException("Fast memory sharing of ToBs not coded for counterparty rank " +
                                            counterpartyOrderRank);
        }

        // For each symbol 1 value for ask and one for bit
        private static int _sizeOfMMFForOneCounterparty = Symbol.ValuesCount * 2 * sizeof(long);
        private static int _sizeOfMMFForOneCounterpartyLocal = Symbol.ValuesCount * 2;

        public static int MMFSize
        {
            // For each symbol 1 value for ask and one for bit (=> Symbol.ValuesCount*2)
            //  those for HTF and HT3 (=> *2)
            //  plus at the end there is one timestamp value (=> + 2) + the changed index value
            // And each value is of an atomic size of long (=> *sizeof (long))
            get { return _sizeOfMMFForOneCounterparty * NumberOfSupportedCounterparties + 2 * sizeof(long); }
        }

        public static int IndexOfUpdateTimestamp
        {
            //Basicaly MMFSize - 1 * sizof(long)
            get { return _sizeOfMMFForOneCounterparty * NumberOfSupportedCounterparties; }
        }

        private static int _indexOfChangedIndexValue = _sizeOfMMFForOneCounterparty * NumberOfSupportedCounterparties + 1 * sizeof(long);
        public static int IndexOfChangedIndexValue
        {
            get { return _indexOfChangedIndexValue; }
        }
    }

    [Serializable]
    public class TobConverterException : Exception
    {
        public TobConverterException() { }
        public TobConverterException(string message) : base(message) { }
        public TobConverterException(string message, Exception inner) : base(message, inner) { }
        protected TobConverterException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public class BlockingBuffersComposite<TItemBase, TCoallescableItem> : IBlockingBuffer<TItemBase>
        where TCoallescableItem: class, TItemBase, ICoallescingItem
    {
        private readonly ICoallescingBlockingBuffer<TCoallescableItem> _coallescingBlockingBuffer;
        private readonly OrderedBlockingBuffer<TItemBase> _orderedBlockingBuffer;

        public BlockingBuffersComposite(
            int maxIndex, Action<TItemBase> itemConsumingAction, bool usePriceObjectsPooling, string identity, ILogger logger)
        {
            this._coallescingBlockingBuffer = new CoallescingBlockingBuffer<TCoallescableItem>(maxIndex,
                itemConsumingAction, usePriceObjectsPooling, identity, logger);
            this._orderedBlockingBuffer = new OrderedBlockingBuffer<TItemBase>(itemConsumingAction,
                usePriceObjectsPooling, identity, logger);
        }

        public int Count
        {
            get { return _coallescingBlockingBuffer.Count + _orderedBlockingBuffer.Count; }
        }

        public int DroppedItems
        {
            get { return _coallescingBlockingBuffer.DroppedItems + _orderedBlockingBuffer.DroppedItems; }
        }

        public event Action<Exception> ExceptionDuringDispatchingOccured
        {
            add
            {
                this._coallescingBlockingBuffer.ExceptionDuringDispatchingOccured += value;
                this._orderedBlockingBuffer.ExceptionDuringDispatchingOccured += value;
            }

            remove
            {
                this._coallescingBlockingBuffer.ExceptionDuringDispatchingOccured -= value;
                this._orderedBlockingBuffer.ExceptionDuringDispatchingOccured -= value;
            }
        }

        public void Add(TItemBase item)
        {
            TCoallescableItem coallescableItem = item as TCoallescableItem;
            if (coallescableItem != null)
            {
                this._coallescingBlockingBuffer.Add(coallescableItem);
            }
            else
            {
                this._orderedBlockingBuffer.Add(item);
            }
        }

        public void CompleteAdding()
        {
            this._coallescingBlockingBuffer.CompleteAdding();
            this._orderedBlockingBuffer.CompleteAdding();
        }
    }
}

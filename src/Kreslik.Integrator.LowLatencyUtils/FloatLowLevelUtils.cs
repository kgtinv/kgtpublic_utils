﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.LowLatencyUtils
{
    public static class FloatLowLevelUtils
    {
        public static unsafe float CutLSB(this float f)
        {
            int fAsInt = *(int*)&f;

            int resultAsInt = fAsInt & ~0x7;

            float result = *(float*)&resultAsInt;

            return result;
        }

        public static unsafe int CountCommonMSB(float f1, float f2)
        {
            int f1AsInt = *(int*)&f1;
            int f2AsInt = *(int*)&f2;

            int exclusiveOr = f1AsInt ^ f2AsInt;

            int leadingZeros = 0;
            while ((exclusiveOr >>= 1) != 0)
            {
                leadingZeros++;
            }
            return 32 - leadingZeros;
        }
    }
}

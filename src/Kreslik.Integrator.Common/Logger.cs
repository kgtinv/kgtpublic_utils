﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public enum LogLevel
    {
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }

    public interface ILogger
    {
        void Log(LogLevel level, string message);

        void Log(LogLevel level, IPooledBufferSegment pooledBuffer);

        void Log(LogLevel level, string message, IPooledBufferSegment pooledBuffer);

        //void LogWithDt(LogLevel level, DateTime correctedTime, string message);
        void Log(LogLevel level, string messageFmt, params object[] args);
        //void Log(LogLevel level, IFormatProvider formatProvider, string messageFmt, params object[] args);
        void LogException(LogLevel level, string message, Exception e);
        void LogException(LogLevel level, Exception e, string messageFmt, params object[] args);
        //void LogException(Exception e);
        bool IsEnabled(LogLevel level);

        bool IsLowDiskSpaceConstraintLoggingOn { get; }
        event Action<bool> IsLowDiskSpaceChanged;
        IReadonlyChangeTrackingType<bool> IsUltraLowDiskSpace { get; }
    }

    public interface ISimpleLogFactory
    {
        ILogger GetLogger(string logName);
    }

    public interface ILogFactory: ISimpleLogFactory
    {
        /// <summary>
        /// Creates logger device while specifying the name of log device and importance of log messages
        /// </summary>
        /// <param name="logName">Name of the device (can translte to a filename)</param>
        /// <param name="isLessImportant">false if logger logs very few >=Warn messages</param>
        /// <returns></returns>
        ILogger GetLogger(string logName, bool isLessImportant);

        ILogger GetLogger(string logName, bool isLessImportant, bool doNotLogDuringLowDiskSpace);
    }

    public interface ILogLocationsInfo
    {
        string CurrentLogsDirectory { get; }
        event Action<string> CurrentLogsDirectoryChanged;
        string CurrentExecutionDirectory { get; }
    }

    public static class LogLocationBehavior
    {
        public static bool AttachLogsToParentProcess = false;
    }

    public class LogFactory : ILogFactory
    {
        private static ILogFactory _instance = new LogFactory();
        private static ParentLogger _parentLogger = new ParentLogger();

        //Assign this now, fur future usage - before the Console.Out get possibly redirected by .SetOut
        public static readonly TextWriter OriginalConsoleWriter = Console.Out;

        internal static void InitializeTimers()
        {
            _parentLogger.InitializeTimers();
        }

        public static void ForceFlush()
        {
            _parentLogger.Flush();
        }

        public static ILogFactory Instance { get { return _instance; } }

        public ILogger GetLogger(string logName)
        {
            return this.GetLogger(logName, false, false);
        }

        public ILogger GetLogger(string logName, bool isLessImportant)
        {
            return this.GetLogger(logName, isLessImportant, false);
        }

        public ILogger GetLogger(string logName, bool isLessImportant, bool doNotLogDuringLowDiskSpace)
        {
            return _parentLogger.GetLogger(logName, isLessImportant);
        }

        public static event Action<string> NewFatalLogEntry
        {
            add { _parentLogger.NewFatalRecord += value; }
            remove { _parentLogger.NewFatalRecord -= value; }
        }

        public static IImportantInfoProvider ImportantInfoProvider
        {
            get { return _parentLogger.BufferedEmailsSender; }
        }

        public static ILogLocationsInfo LogLocationsInfo => _parentLogger.LogLocationsInfo;

        private class LogEvent
        {
            private DateTime _timeStamp;
            private int _threadId;
            public LogLevel LogLevel { get; private set; }
            public int LoggerId { get; private set; }
            public bool IsLessImportant { get; private set; }

            private string _message;
            private object[] _args;
            private Exception _exception;
            public IPooledBufferSegment PooledBufferSegment { get; private set; }

            private static bool _isAccurateHighResolutionTimeAvailable =
                HighResolutionDateTime.IsAccurateDateTimeAvailable;

            private LogEvent(int loggerId, LogLevel logLevel)
            {
                this._timeStamp = _isAccurateHighResolutionTimeAvailable ? HighResolutionDateTime.UtcNow : DateTime.UtcNow;
                this._threadId = Thread.CurrentThread.ManagedThreadId;
                this.LoggerId = loggerId;
                this.LogLevel = logLevel;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, IPooledBufferSegment pooledBufferSegment, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                pooledBufferSegment.Acquire();
                this.PooledBufferSegment = pooledBufferSegment;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, IPooledBufferSegment pooledBufferSegment, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                pooledBufferSegment.Acquire();
                this.PooledBufferSegment = pooledBufferSegment;
                this._message = message;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, bool isLessImportant, DateTime correctedTimestamp)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this.IsLessImportant = isLessImportant;
                this._timeStamp = correctedTimestamp;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, bool isLessImportant, object[] args)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this._args = args;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, string message, Exception exception, bool isLessImportant)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this._exception = exception;
                this.IsLessImportant = isLessImportant;
            }

            public LogEvent(int loggerId, LogLevel logLevel, Exception exception, string message, bool isLessImportant, object[] args)
                : this(loggerId, logLevel)
            {
                this._message = message;
                this._args = args;
                this._exception = exception;
                this.IsLessImportant = isLessImportant;
            }

            public string GetMessagePrefix()
            {
                return string.Format("{0} {1} {2} ", _timeStamp.ToString("yyyy-MM-dd HH:mm:ss.fffffff UTC"), _threadId,
                                  LogLevel.ToString().ToUpper());
            }

            public string GetMessageSuffix()
            {
                string message = _message;
                if (_args != null)
                {
                    try
                    {
                        message = string.Format(message, _args);
                    }
                    catch (Exception e)
                    {
                        message =
                            string.Format(
                                "#MALFORMED PARAMETERS ARRAY FOR LOGGER# Original Level: {0}, Message: {1} Arguments: {2}{3}Exception thrown during formatting: {4}",
                                LogLevel.Fatal,
                                _message,
                                string.Join(";", _args.Select(
                                    o =>
                                    {
                                        try
                                        {
                                            return o == null ? "<NULL>" : o.ToString();
                                        }
                                        catch (Exception innerEx)
                                        {
                                            return string.Format("<ToString() throws: {0}>", innerEx);
                                        }
                                    })), Environment.NewLine, e);
                        this.LogLevel = LogLevel.Error;
                    }
                }
                if (_exception != null)
                {
                    message = string.Format("{0} {1}", message, _exception);
                }

                return message;
            }

            public override string ToString()
            {
                if (_args == null)
                {
                    return string.Format("{0} {1} {2} {3} {4}", _timeStamp.ToString("yyyy-MM-dd HH:mm:ss.fffffff UTC"), _threadId,
                                  LogLevel.ToString().ToUpper(), _message, _exception);
                }
                else
                {
                    return string.Format("{0} {1} {2} {3}", _timeStamp.ToString("yyyy-MM-dd HH:mm:ss.fffffff UTC"), _threadId,
                                  LogLevel.ToString().ToUpper(), string.Format(_message, _args));
                }
            }
        }

        public static class LogArchivingHelper
        {
            [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
            public static void ArchiveDirectory(string target_dir, bool ignoreErrors)
            {
                try
                {
                    ArchiveDirectoryInternal(target_dir, ignoreErrors);
                }
                catch (AccessViolationException e)
                {
                    if (!ignoreErrors)
                        throw new Exception(string.Format("Archivation failed (AV): {0}", e.ToString()));
                }
                catch (Exception e)
                {
                    if (!ignoreErrors)
                        throw;
                }
            }

            private static void ArchiveDirectoryInternal(string target_dir, bool ignoreErrors)
            {
                string renamedDir = target_dir + "_toBeDeleted";
                string zippedDir = target_dir + ".zip";

                if (Directory.Exists(target_dir) && !Directory.Exists(renamedDir) &&
                    !File.Exists(zippedDir))
                {
                    try
                    {
                        //this will inform us abould opened handle and also will claim the folder for us
                        Directory.Move(target_dir, renamedDir);
                    }
                    catch (Exception e)
                    {
                        if (ignoreErrors)
                            return;
                        else if (!InteropSHFileOperationWrapper.RenameDirNativeSilent(target_dir, renamedDir))
                            //Native operation fails if some other process (not system) still holds handle
                            throw new Exception("Cannot rename directory " + target_dir, e);
                    }


                    try
                    {
                        //this should already succeed, as we zip freshly renamed dir
                        System.IO.Compression.ZipFile.CreateFromDirectory(renamedDir, zippedDir);
                    }
                    catch (Exception)
                    {
                        if (!ignoreErrors)
                            throw;
                    }

                    try
                    {
                        //this can fail - as explorer can still keep opened the renamed dir
                        DeleteDirectory(renamedDir);
                    }
                    catch (Exception e)
                    {
                        //this fails if somebody still keeps opened handle 
                        // (this should have been filtered out by rename already, but something could happen in the meantime)
                        if (!InteropSHFileOperationWrapper.DeleteDirNativeSilent(renamedDir) && !ignoreErrors)
                            throw new Exception("Cannot delete directory " + renamedDir, e);

                        //just for sure verify that directory was really removed
                        if (!ignoreErrors && Directory.Exists(renamedDir))
                            throw new Exception("Directory still exists after deletion " + renamedDir, e);
                    }
                }
            }

            private static void DeleteDirectory(string target_dir)
            {
                string[] files = Directory.GetFiles(target_dir);
                string[] dirs = Directory.GetDirectories(target_dir);

                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }

                foreach (string dir in dirs)
                {
                    DeleteDirectory(dir);
                }

                Directory.Delete(target_dir, false);
            }
        }

        private class ParentLogger//: CriticalFinalizerObject
        {
            private BlockingCollection<LogEvent> _logEvents = new BlockingCollection<LogEvent>(new ConcurrentQueue<LogEvent>());
            private List<StreamWrapper> _streams = new List<StreamWrapper>();
            private List<byte[]> _loggerNames = new List<byte[]>();
            private object _streamsLocker = new object();
            private StreamWrapper _allEventsStream;
            private StreamWrapper _lessImportantEventsStream;
            private StreamWrapper _veryImportantEventsStream;
            private LogLevel _levelThreshold = LogLevel.Warn;
            private SafeTimer _timer;
            private readonly DirWrapper _dirWrapper;
            internal Action<string> NewFatalRecord;
            private SafeTimer _logArchivationTimer;
            internal BufferedEmailsSender BufferedEmailsSender { get; private set; }
            private Dictionary<string, ILogger> _existingLoggers;
            DiskFreeSpaceWatcher _diskSpaceWatcher;

            public ILogLocationsInfo LogLocationsInfo => _dirWrapper;

            public ParentLogger()
            {
                _dirWrapper = new DirWrapper();

                _allEventsStream = new StreamWrapper(_dirWrapper, "EverythingTogether");
                _lessImportantEventsStream = new StreamWrapper(_dirWrapper, "Important");
                _veryImportantEventsStream = new StreamWrapper(_dirWrapper, "VeryImportant");

                BufferedEmailsSender =
                new BufferedEmailsSender(
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailTo == null ? null :
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailTo.Split(new char[] { ';' }),
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailCc == null ? null :
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailCc.Split(new char[] { ';' }),
                    "fatal log item",
                    CommonSettings.CommonBehavior.Logging.FatalErrorsEmailMinDelaysBetweenEmails);

                //Multicast delegate keeps reference to an instace owning the method, so if logger references the
                // delegate then the BufferedEmailsSender will not be subject to GC - so no need to keep it in a locla variable
                this.NewFatalRecord += BufferedEmailsSender.AddEntry;

                _existingLoggers = new Dictionary<string, ILogger>()
                {
                    {string.Empty, new Logger(-1, this, false)}
                };

                ThreadPool.QueueUserWorkItem(ProcessLogEvents);

                _diskSpaceWatcher = _dirWrapper.CreateDiskFreeSpaceWatcher();

                BypassLoggingEntirely = new ChangeTrackingType<bool>(false, bypass =>
                {
                    if (bypass)
                    {
                        bool unconditional = File.Exists("BypassLoggingEntirely_Legacy");

                        string msg;
                        if (unconditional)
                        {
                            msg =
                                "Complete, unconditional bypass of logging requested - no logs will appear in console or files after this point. Delete 'BypassLoggingEntirely_Legacy' file in current folder to reverse this behavior (picked automatically).";
                        }
                        else
                        {
                            msg =
                                $"Complete bypass of logging on low disk space requested and current disk space ({this._diskSpaceWatcher.FreeGigaBytes:N2} GBs on {_dirWrapper.BaseDriveName}) dropped below threshold ({CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB} GBs). No logs will appear in console or files after this point. Delete 'BypassLoggingOnLowDiskSpace' file in current folder or free up enough space on {_dirWrapper.BaseDriveName} to reverse this behavior (picked automatically).";
                        }

                        AnnounceFlagMessageToConsole(msg);
                    }
                });

                BypassLoggingOnLowDiskSpace = new ChangeTrackingType<bool>(false, bypass =>
                {
                    if (bypass)
                    {
                        string msg = $"Conditional bypass of logging requested. Delete 'BypassLoggingOnLowDiskSpace' file in current folder to reverse this behavior (picked automatically). Otherwise if free space on {_dirWrapper.BaseDriveName} drops below {CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB} GB (currently: {this._diskSpaceWatcher.FreeGigaBytes:N2} GB), the complete bypass of logging will kick in.";
                        AnnounceFlagMessageToConsole(msg);
                    }
                });

                CheckConstrainedLoggingFiles();

                _diskSpaceWatcher.RegisterHandler(
                    CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB, () =>
                    {
                        IsLowDiskSpace.Value = true;
                        this.LogAsync(new LogEvent(-1, LogLevel.Fatal,
                            "Available disk space is {0}GB, so stopping some logging. (if BypassLoggingOnLowDiskSpace is set ({1}) than all logging will by bypassed)", false,
                            new object[] { _diskSpaceWatcher.FreeGigaBytes, BypassLoggingOnLowDiskSpace.Value }));
                    },
                    CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB + 1, () =>
                    {
                        IsLowDiskSpace.Value = false;
                        this.LogAsync(new LogEvent(-1, LogLevel.Warn,
                            "Available disk space is {0}GB, so resuming logging", false,
                            new object[] { _diskSpaceWatcher.FreeGigaBytes }));
                    });

                _diskSpaceWatcher.RegisterHandler(
                    CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB * 1.5, () =>
                    {
                        this.LogAsync(new LogEvent(-1, LogLevel.Fatal,
                                                   "Available disk space is {0}GB, low disk space risk is approaching. Delete some files otherwise [Integrator process: {1}] will soon constraint some logging and (if this is Integrator server) then it will stop data collection",
                                                   false,
                                                   new object[] { _diskSpaceWatcher.FreeGigaBytes, this.GetIntegratorProcessType() }));
                    },
                    CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB * 1.5 + 1, () =>
                    {
                        this.LogAsync(new LogEvent(-1, LogLevel.Warn,
                                                   "Available disk space is {0}GB, low disk space risk is resolved", false,
                                                   new object[] { _diskSpaceWatcher.FreeGigaBytes }));
                    });

                _diskSpaceWatcher.RegisterHandler(
                    CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB / 2.0, () =>
                    {
                        IsUltraLowDiskSpace.Value = true;
                        this.LogAsync(new LogEvent(-1, LogLevel.Fatal,
                                                   "Available disk space is {0}GB, this is deemed critical level. All existing logs will be deleted if BypassLoggingOnLowDiskSpace turned on ({1})",
                                                   false,
                                                   new object[] { _diskSpaceWatcher.FreeGigaBytes, BypassLoggingOnLowDiskSpace.Value }));

                        if (BypassLoggingOnLowDiskSpace)
                        {
                            DeleteAllLogs();
                        }
                    },
                    CommonSettings.CommonBehavior.Logging.AvailableDiskSpaceThresholdToStopMDLogging_GB / 2.0 + 1, () =>
                    {
                        IsUltraLowDiskSpace.Value = false;
                        this.LogAsync(new LogEvent(-1, LogLevel.Trace,
                                                   "Available disk space is {0}GB, critical low disk space risk is resolved", false,
                                                   new object[] { _diskSpaceWatcher.FreeGigaBytes }));
                    });
            }

            private void CheckConstrainedLoggingFiles()
            {
                BypassLoggingOnLowDiskSpace.Value = File.Exists("BypassLoggingOnLowDiskSpace");
                BypassLoggingEntirely.Value = File.Exists("BypassLoggingEntirely_Legacy") || (BypassLoggingOnLowDiskSpace && IsLowDiskSpace);

                if (File.Exists("BypassLoggingEntirely"))
                {
                    AnnounceFlagMessageToConsole("!!!WARNING!!!");
                    AnnounceFlagMessageToConsole("Attempt to use deprecated file 'BypassLoggingEntirely' - renaming to 'BypassLoggingOnLowDiskSpace'. Legacy (unrecommended!) behavior is available with file 'BypassLoggingEntirely_Legacy'");
                    AnnounceFlagMessageToConsole("!!!WARNING!!!");
                    File.Move("BypassLoggingEntirely", "BypassLoggingOnLowDiskSpace");
                }
            }

            //this cannot be performed as part of initialization, since that would cause reference loop
            internal void InitializeTimers()
            {
                _timer = new SafeTimer(FlushHandler, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2), true)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Normal
                };

                //let's perform first archivation next day (this day folders might be still in use by other processes)
                _logArchivationTimer = new SafeTimer(ArchiveOldLogDirs,
                    //lets start at random time after midnight - this way processes will not step on each others foot
                    TillNextUtcMidnight().Add(TimeSpan.FromMinutes(30 + new Random(Environment.TickCount).NextDouble() * 30)),
                    TimeSpan.FromDays(1), true)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1min,
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
            }

            private static void AnnounceFlagMessageToConsole(string message)
            {
                try
                {
                    using (ConsoleColoringHelper consoleColoringHelper = new ConsoleColoringHelper())
                    {
                        ConsoleColoringHelper.SetColorsToFailure();
                        TextWriter consoleWriter = LogFactory.OriginalConsoleWriter ?? Console.Out;
                        consoleWriter.WriteLine(message);
                    }
                }
                catch (Exception)
                {
                    //nothing we can do here - logging is bypassed
                }
            }

            //initialized in ctor, as access to instance fields is required
            internal readonly ChangeTrackingType<bool> BypassLoggingEntirely;
            private readonly ChangeTrackingType<bool> BypassLoggingOnLowDiskSpace;

            private TimeSpan TillNextUtcMidnight()
            {
                DateTime utcNow = DateTime.UtcNow;
                return utcNow.Date.AddDays(1) - utcNow;
            }

            private void ArchiveOldLogDirs()
            {
                try
                {
                    foreach (string oldLogDirsInSiblingFolder in _dirWrapper.OldLogDirsInSiblingFolders)
                    {
                        try
                        {
                            //here we might try to force archive by second 'false' parameter
                            // that can however cause bugging log entries and pop-up windows if handles opened somewhere
                            LogArchivingHelper.ArchiveDirectory(oldLogDirsInSiblingFolder, true);
                        }
                        catch (Exception e)
                        {
                            this.LogAsync(new LogEvent(-1, LogLevel.Fatal, e,
                                                   "Cannot perform archivation of log folder [{0}] - it's likely opened somewhere, please close opened handles and integrator will retry after UTC midnight",
                                                   false, new[] { oldLogDirsInSiblingFolder }));
                        }
                    }
                }
                catch (Exception e)
                {
                    this.LogAsync(new LogEvent(-1, LogLevel.Fatal, e,
                                                   "Cannot perform archivation of log folders - probably attempt to access inaccessible paths. will retry after UTC midnight",
                                                   false, new object[0]));
                }
                
            }

            private void DeleteAllLogs()
            {
                _dirWrapper.MarkAsInNeedForClose(true);
                this.Flush();

                try
                {
                    foreach (string oldLogDirsInSiblingFolder in _dirWrapper.ParentLogDirsInSiblingFolders.ToArray())
                    {
                        DirectoryEx.DeleteRecursiveIncludingReadOnly(oldLogDirsInSiblingFolder, true);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed during deleting logs");
                    Console.WriteLine(e);
                }
            }

            public ILogger GetLogger(string loggerIdentity, bool isLessImportant)
            {
                lock (_streamsLocker)
                {
                    ILogger logger;
                    if (loggerIdentity == null)
                    {
                        loggerIdentity = string.Empty;
                    }

                    if (!_existingLoggers.TryGetValue(loggerIdentity, out logger))
                    {
                        _streams.Add(new StreamWrapper(_dirWrapper, loggerIdentity));
                        _loggerNames.Add(Encoding.Default.GetBytes(loggerIdentity + " "));
                        int loggerId = _streams.Count - 1;
                        logger = new Logger(loggerId, this, isLessImportant);
                        _existingLoggers[loggerIdentity] = logger;
                    }

                    return logger;
                }
            }

            public void LogAsync(LogEvent logEvent)
            {
                this._logEvents.Add(logEvent);
            }

            public ChangeTrackingType<bool> IsLowDiskSpace = new ChangeTrackingType<bool>(false);
            public ChangeTrackingType<bool> IsUltraLowDiskSpace = new ChangeTrackingType<bool>(false);


            private byte[] GetLoggerName(int loggerId)
            {
                if (loggerId >= 0)
                {
                    return _loggerNames[loggerId];
                }
                else
                {
                    return new byte[0];
                }
            }

            //memory pool for byte arrays
            private readonly BytesContainer _bytesContainer = new BytesContainer();

            private void FlushHandler()
            {
                this.Flush();

                _diskSpaceWatcher.CheckEvents();

                CheckConstrainedLoggingFiles();
            }

            private string GetIntegratorProcessType()
            {
                return IntegratorPerformanceCounter.CurrentIntegratorProcessType.HasValue
                    ? IntegratorPerformanceCounter.CurrentIntegratorProcessType.ToString()
                    : "Local Process (Splitter or Client)";
            }

            internal void Flush()
            {
                for (int i = 0; i < _streams.Count; i++)
                {
                    _streams[i]?.Flush();
                }
                _allEventsStream?.Flush();
                _lessImportantEventsStream?.Flush();
                _veryImportantEventsStream?.Flush();
            }

            //~ParentLogger()
            //{
            //    this.Flush();
            //}

            private void ProcessLogEvents(object unused)
            {
                foreach (LogEvent logEvent in _logEvents.GetConsumingEnumerable())
                {
                    this.TryProcessLogEntry(logEvent);
                }
            }

            private void TryProcessLogEntry(LogEvent logEvent)
            {
                try
                {
                    _bytesContainer.FillWithLogEvent(logEvent);

                    if (logEvent.LoggerId >= 0)
                    {
                        _streams[logEvent.LoggerId].WriteLine(_bytesContainer);
                    }

                    _allEventsStream.WriteLine(_bytesContainer, GetLoggerName(logEvent.LoggerId));

                    if (logEvent.LogLevel >= _levelThreshold)
                    {
                        if (logEvent.IsLessImportant)
                        {
                            _lessImportantEventsStream.WriteLine(_bytesContainer, GetLoggerName(logEvent.LoggerId));
                        }
                        else
                        {
                            _veryImportantEventsStream.WriteLine(_bytesContainer, GetLoggerName(logEvent.LoggerId));
                        }

                        if (logEvent.LogLevel >= LogLevel.Fatal && this.NewFatalRecord != null)
                        {
                            string logEntry =
                                _bytesContainer.GetEventAsString(
                                    Encoding.Default.GetString(GetLoggerName(logEvent.LoggerId)));
                            try
                            {
                                LogFactory.OriginalConsoleWriter.WriteLine(logEntry);
                            }
                            catch (Exception e)
                            {
                                //bugfix of https://kgtintegrator.atlassian.net/browse/KGTNNCS-661
                                //we cannot log fatal - as that would again go to console (causing infinite cycle)
                                this.LogAsync(new LogEvent(-1, LogLevel.Error, "Unexpected error during console logging", e, false));
                            }
                            this.NewFatalRecord(logEntry);
                        }
                    }

                    if (logEvent.PooledBufferSegment != null)
                        logEvent.PooledBufferSegment.Release();
                }
                catch (Exception e)
                {
                    this.LogAsync(new LogEvent(-1, LogLevel.Fatal, "Unexpected error during logging", e, false));
                }
            }


            //private BlockingCollection<LogEvent> _logEvents = new BlockingCollection<LogEvent>(new ConcurrentQueue<LogEvent>());
            //private List<Stream> _streams = new List<Stream>();
            //private List<byte[]> _loggerNames = new List<byte[]>();
            //private object _streamsLocker = new object();
            //private string _targetDir;
            //private Stream _allEventsStream;
            //private Stream _importantEventsStream;
            //private LogLevel _levelThreshold = LogLevel.Warn;
            //private volatile bool _flush = false;
            //private Timer _timer;

            //public ParentLogger()
            //{
            //    string basedir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            //    _targetDir = System.IO.Path.Combine(basedir, "logs", DateTime.UtcNow.ToString("yyyy-MM-dd"),
            //                                              Process.GetCurrentProcess().Id.ToString());
            //    if (!Directory.Exists(_targetDir))
            //    {
            //        Directory.CreateDirectory(_targetDir);
            //    }

            //    _allEventsStream = File.Open(
            //        System.IO.Path.Combine(_targetDir, "EverythingTogether.log"),
            //        FileMode.Append,
            //        FileAccess.Write,
            //        FileShare.Read);

            //    _importantEventsStream = File.Open(
            //        System.IO.Path.Combine(_targetDir, "Important.log"),
            //        FileMode.Append,
            //        FileAccess.Write,
            //        FileShare.Read);

            //    ThreadPool.QueueUserWorkItem(ProcessLogEvents);
            //    _timer = new Timer((o) => { _flush = true; }, null, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));
            //}

            //public int AddLogger(string loggerIdentity)
            //{
            //    lock (_streamsLocker)
            //    {
            //        _streams.Add(
            //            File.Open(
            //                System.IO.Path.Combine(_targetDir, loggerIdentity + ".log"),
            //                FileMode.Append,
            //                FileAccess.Write,
            //                FileShare.Read)
            //            );

            //        _loggerNames.Add(Encoding.Default.GetBytes(loggerIdentity + " "));

            //        return _streams.Count - 1;
            //    }
            //}

            //public void LogAsync(LogEvent logEvent)
            //{
            //    this._logEvents.Add(logEvent);
            //}

            //private void ProcessLogEvents(object unused)
            //{
            //    foreach (LogEvent logEvent in _logEvents.GetConsumingEnumerable())
            //    {
            //        string prefix = logEvent.GetMessagePrefix();
            //        string suffix = logEvent.GetMessageSuffix();

            //        byte[] prefixBytes = Encoding.Default.GetBytes(prefix);
            //        byte[] suffixBytes = Encoding.Default.GetBytes(suffix);

            //        _streams[logEvent.LoggerId].Write(prefixBytes, 0, prefixBytes.Length);
            //        _streams[logEvent.LoggerId].Write(suffixBytes, 0, suffixBytes.Length);

            //        _allEventsStream.Write(prefixBytes, 0, prefixBytes.Length);
            //        _allEventsStream.Write(_loggerNames[logEvent.LoggerId], 0, _loggerNames[logEvent.LoggerId].Length);
            //        _allEventsStream.Write(suffixBytes, 0, suffixBytes.Length);

            //        if (logEvent.LogLevel >= _levelThreshold)
            //        {
            //            _importantEventsStream.Write(prefixBytes, 0, prefixBytes.Length);
            //            _importantEventsStream.Write(_loggerNames[logEvent.LoggerId], 0, _loggerNames[logEvent.LoggerId].Length);
            //            _importantEventsStream.Write(suffixBytes, 0, suffixBytes.Length);

            //            System.Console.Out.Write(prefix + Encoding.Default.GetString(_loggerNames[logEvent.LoggerId]) + suffix);
            //        }

            //        if (_flush)
            //        {
            //            _flush = false;

            //            for (int i = 0; i < _streams.Count; i++)
            //            {
            //                _streams[i].Flush();
            //            }
            //            _allEventsStream.Flush();
            //            _importantEventsStream.Flush();
            //        }
            //    }
            //}

            private class BytesContainer
            {
                public BytesContainer()
                {
                    _bytes = new byte[2048];
                }

                public void FillWithLogEvent(LogEvent logEvent)
                {
                    string firstString = logEvent.GetMessagePrefix();

                    try
                    {
                        _firstStringLength = Encoding.Default.GetBytes(firstString, 0, firstString.Length, _bytes, 0);
                    }
                    catch (ArgumentException ex)
                    {
                        int maxCount = Encoding.Default.GetMaxByteCount(firstString.Length);
                        if (_bytes.Length < maxCount)
                        {
                            _bytes = new byte[maxCount + 2048];
                        }
                        _firstStringLength = Encoding.Default.GetBytes(firstString, 0, firstString.Length, _bytes, 0);
                    }


                    string secondString = logEvent.GetMessageSuffix();
                    _secondStringLength = 0;
                    if (secondString != null)
                    {
                        try
                        {
                            _secondStringLength = Encoding.Default.GetBytes(secondString, 0, secondString.Length, _bytes, _firstStringLength);
                        }
                        catch (ArgumentException ex)
                        {
                            int maxCount = Encoding.Default.GetMaxByteCount(secondString.Length);
                            if (_bytes.Length < maxCount + _firstStringLength)
                            {
                                byte[] tmpArr = new byte[maxCount + _firstStringLength];
                                Buffer.BlockCopy(_bytes, 0, tmpArr, 0, _firstStringLength);
                                _bytes = tmpArr;
                            }
                            _secondStringLength = Encoding.Default.GetBytes(secondString, 0, secondString.Length, _bytes, _firstStringLength);
                        }
                    }

                    this._pooledBufferSegment = logEvent.PooledBufferSegment;
                }

                public string GetEventAsString(string middlePart)
                {
                    return Encoding.Default.GetString(_bytes, 0, _firstStringLength) + middlePart +
                    (this._pooledBufferSegment != null
                        ? this._pooledBufferSegment.GetContentAsAsciiString()
                        : Encoding.Default.GetString(_bytes, _firstStringLength, _secondStringLength));
                }

                public void StuffFirstString(Action<byte[], int, int> bytesConsumingAction)
                {
                    bytesConsumingAction(this._bytes, 0, _firstStringLength);
                }

                public void StuffSecondString(Action<byte[], int, int> bytesConsumingAction)
                {
                    if (_secondStringLength != 0)
                        bytesConsumingAction(this._bytes, _firstStringLength, _secondStringLength);

                    if (_pooledBufferSegment != null)
                        bytesConsumingAction(this._pooledBufferSegment.Buffer, this._pooledBufferSegment.ContentStart,
                            this._pooledBufferSegment.ContentSize);
                }

                private byte[] _bytes;
                private int _firstStringLength;
                private int _secondStringLength;
                private IPooledBufferSegment _pooledBufferSegment;
            }

            private class StreamWrapper: CriticalFinalizerObject
            {
                private const int MAX_LINES = 500000;
                private readonly byte[] NEWLINE = Encoding.Default.GetBytes(Environment.NewLine);
                private Stream _stream;
                private string _filename;
                private int _currentWriteCount;
                private Action<byte[], int, int> _streamWriteAction;

                private DirWrapper _dirWrapper;

                public StreamWrapper(DirWrapper dirWrapper, string fileName)
                {
                    this._dirWrapper = dirWrapper;
                    this._filename = fileName;

                    this.FilesCount = 0;
                    this._currentWriteCount = 0;

                    this.CreateStream();
                }

                private void CreateStream()
                {
                    //we really want to perform just one execution in parallel
                    if (Monitor.TryEnter(this))
                    {
                        try
                        {

                            if (_stream != null)
                            {
                                Stream streamToClose = _stream;
                                //Delaying this as it might have been still in use
                                TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(1),
                                    () => streamToClose.Close());
                            }

                            var stream = File.Open(
                                System.IO.Path.Combine(_dirWrapper.GetPath(this),
                                    string.Format("{0}_{1}.log", _filename, FilesCount)),
                                FileMode.Append,
                                FileAccess.Write,
                                FileShare.Read);
                            //swap only once created
                            _stream = stream;
                            _streamWriteAction = _stream.Write;
                        }
                        finally 
                        {
                            Monitor.Exit(this);
                        }                      
                    }
                }

                public void WriteLine(BytesContainer bytesContainer)
                {
                    bytesContainer.StuffFirstString(_streamWriteAction);
                    bytesContainer.StuffSecondString(_streamWriteAction);
                    _stream.Write(NEWLINE, 0, NEWLINE.Length);
                    _currentWriteCount++;
                }


                public void WriteLine(BytesContainer bytesContainer, byte[] middleBytes)
                {
                    bytesContainer.StuffFirstString(_streamWriteAction);
                    _stream.Write(middleBytes, 0, middleBytes.Length);
                    bytesContainer.StuffSecondString(_streamWriteAction);
                    _stream.Write(NEWLINE, 0, NEWLINE.Length);
                    _currentWriteCount++;
                }

                public int FilesCount { get; set; }

                public void Flush()
                {
                    _stream.Flush();

                    if (!this._dirWrapper.IsActual(this, this.FilesCount))
                    {
                        this.FilesCount = 0;
                        this._currentWriteCount = 0;

                        this.CreateStream();
                    }

                    if (_currentWriteCount > MAX_LINES)
                    {
                        this.FilesCount++;
                        this._currentWriteCount = 0;

                        this.CreateStream();
                    }
                }

                ~StreamWrapper()
                {
                    _stream?.Close();
                }
            }

            private class DirWrapper: ILogLocationsInfo
            {
                private string _targetDir;
                private readonly string _basedir;
                private string _oldTagetDir;
                private HashSet<StreamWrapper> _currentStreams = new HashSet<StreamWrapper>();
                private HashSet<StreamWrapper> _oldStreams;
                private int _targetDirCreationDay = -1;

                private const int MAX_FILES_IN_FOLDER = 60;
                private const string LOGS_DIR = "logs";

                public string CurrentLogsDirectory => _targetDir;
                public event Action<string> CurrentLogsDirectoryChanged;
                public string CurrentExecutionDirectory => _basedir;

                private static bool IsWithinWeb
                {
                    get
                    {
                        return string.Equals("web.config",
                            Path.GetFileName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile),
                            StringComparison.InvariantCultureIgnoreCase);
                    }
                }

                public DirWrapper()
                {
                    //_basedir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    _basedir = System.IO.Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
                    //any changes in web bin folder leads to whole app domain teardown
                    _basedir = IsWithinWeb ? Directory.GetParent(_basedir).FullName : _basedir;
                    _basedir = _basedir ?? string.Empty;

                    CreateDir(false);
                }

                public DiskFreeSpaceWatcher CreateDiskFreeSpaceWatcher()
                {
                    return new DiskFreeSpaceWatcher(BaseDriveName);
                }

                public string BaseDriveName => Directory.GetDirectoryRoot(_basedir);

                private static IEnumerable<string> AllAccessibleDirsOnSameLevel(string dirToSearch, string pattern)
                {
                    IEnumerable<string> allDirsInPreviousLevel = new List<string>() { dirToSearch };
                    List<string> allDirsInCurrentLevel = new List<string>();

                    List<string> foundDirs = new List<string>();

                    int maxDepth = 100;

                    do
                    {
                        foreach (string parentDir in allDirsInPreviousLevel)
                        {
                            try
                            {
                                allDirsInCurrentLevel.AddRange(Directory.GetDirectories(parentDir));
                            }
                            catch (UnauthorizedAccessException)
                            {
                                // this is it
                                // there is no other way how to instruct Directory.GetDirectories to skip directories to which we don't have access
                            }
                        }

                        if (allDirsInCurrentLevel.Count == 0)
                            break;

                        foundDirs.AddRange(
                            allDirsInCurrentLevel.Where(
                                path =>
                                    string.Equals(Path.GetFileName(path), pattern,
                                        StringComparison.InvariantCultureIgnoreCase)));

                        if (foundDirs.Count > 0)
                        {
                            maxDepth = Math.Max(4, maxDepth);
                        }

                        allDirsInPreviousLevel = allDirsInCurrentLevel.Except(foundDirs);
                        allDirsInCurrentLevel = new List<string>();
                    } while (--maxDepth > 0);

                    return foundDirs;
                }

                public IEnumerable<string> ParentLogDirsInSiblingFolders
                {
                    get
                    {
                        string dirToSearch = _basedir;
                        var parent = Directory.GetParent(_basedir);
                        if (parent != null)
                        {
                            var parentParent = Directory.GetParent(parent.FullName);
                            if (parentParent != null)
                                dirToSearch = parentParent.FullName;
                            else
                                dirToSearch = parent.FullName;
                        }

                        return
                            //prevent UAT and release web to step on each other foot
                            (
                                IsWithinWeb
                                    ? new List<string>() {Path.Combine(_basedir, LOGS_DIR)}
                                    : (AllAccessibleDirsOnSameLevel(dirToSearch, LOGS_DIR))
                            );
                    }
                }

                private static string[] GetDirectoriesSafe(string path)
                {
                    try
                    {
                        return Directory.GetDirectories(path);
                    }
                    catch (Exception)
                    {
                        return new string[0];
                    }
                }

                public IEnumerable<string> OldLogDirsInSiblingFolders
                {
                    get
                    {
                        return
                            ParentLogDirsInSiblingFolders
                            .Select(GetDirectoriesSafe)
                            .SelectMany(dir => dir)
                            //Do not pack todays dirs as they might still be in use. They will be picked up later
                            // or folders that are alredy in process of archivation
                            .Where(dir => Directory.GetCreationTimeUtc(dir) < DateTime.UtcNow.Date && !dir.EndsWith("_toBeDeleted"))
                            .ToArray();
                    }
                }

                private void CreateDir(bool forceNew)
                {
                    Process parentProcess = null;
                    if (LogLocationBehavior.AttachLogsToParentProcess)
                    {
                        parentProcess = ProcessUtils.GetParentProcess();
                    }

                    string finalDirName = Process.GetCurrentProcess().Id.ToString();
                    if (AppDomain.CurrentDomain.Id != 1)
                    {
                        string appDomainFriendlyNameescaped =
                            System.Text.RegularExpressions.Regex.Replace(AppDomain.CurrentDomain.FriendlyName,
                                "[" +
                                System.Text.RegularExpressions.Regex.Escape(new string(Path.GetInvalidFileNameChars())) +
                                "]", "_");

                        finalDirName += "_" + appDomainFriendlyNameescaped;
                    }

                    if (parentProcess != null)
                    {
                        finalDirName =
                            Path.Combine(string.Format("{0}_{1}", parentProcess.Id, Process.GetCurrentProcess().ProcessName),
                                finalDirName);
                    }

                    //This is trick to ensure common folder for app domain subfolders
                    // As the domains can be created later on - we use process start time
                    DateTime folderTime = forceNew ? DateTime.UtcNow : (parentProcess ?? Process.GetCurrentProcess()).StartTime;
                    if (folderTime.Date < DateTime.UtcNow.Date)
                    {
                        folderTime = DateTime.UtcNow.Date;
                    }

                    _targetDir = System.IO.Path.Combine(_basedir, LOGS_DIR,
                                                        string.Format("{0:yyyy-MM-dd-HHmm-UTC}_{1}", folderTime, Environment.MachineName),
                                                        finalDirName);

                    _targetDirCreationDay = DateTime.UtcNow.Day;

                    if (!Directory.Exists(_targetDir))
                    {
                        DirectorySecurity dirSec = new DirectorySecurity();
                        dirSec.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl,
                                                                      InheritanceFlags.ContainerInherit |
                                                                      InheritanceFlags.ObjectInherit,
                                                                      PropagationFlags.InheritOnly,
                                                                      AccessControlType.Allow));
                        dirSec.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl,
                                                                      InheritanceFlags.ContainerInherit |
                                                                      InheritanceFlags.ObjectInherit,
                                                                      PropagationFlags.None, AccessControlType.Allow));
                        Directory.CreateDirectory(_targetDir, dirSec);
                    }

                    CurrentLogsDirectoryChanged?.Invoke(_targetDir);
                }

                private Random _random = new Random();
                public string GetPath(StreamWrapper streamWrapper)
                {
                    if (_oldStreams != null && _oldStreams.Contains(streamWrapper))
                    {
                        _oldStreams.Remove(streamWrapper);
                        if (_currentStreams == null) _currentStreams = new HashSet<StreamWrapper>();
                        _currentStreams.Add(streamWrapper);

                        if (_oldStreams.Count == 0)
                        {
                            _oldStreams = null;
                            string dirToArchive = _oldTagetDir;
                            //atempt to archive it now, if not possible it will be archived after midnight
                            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(120 + _random.NextDouble() * 200),
                                () => LogArchivingHelper.ArchiveDirectory(Directory.GetParent(dirToArchive).FullName, true));
                        }
                    }

                    if (_currentStreams != null && !_currentStreams.Contains(streamWrapper))
                    {
                        _currentStreams.Add(streamWrapper);
                    }

                    return _targetDir;
                }

                public void MarkAsInNeedForClose(bool forceNewDir)
                {
                    _oldStreams = _currentStreams;
                    _currentStreams = new HashSet<StreamWrapper>();
                    _oldTagetDir = _targetDir;

                    foreach (StreamWrapper sw in _oldStreams)
                    {
                        sw.FilesCount = 0;
                    }

                    CreateDir(forceNewDir);
                }
                    

                public bool IsActual(StreamWrapper streamWrapper, int fileCnt)
                {
                    if (DateTime.UtcNow.Day != _targetDirCreationDay || fileCnt > MAX_FILES_IN_FOLDER)
                    {
                        MarkAsInNeedForClose(fileCnt > MAX_FILES_IN_FOLDER);

                        return false;
                    }
                    else if (_oldStreams != null && _oldStreams.Contains(streamWrapper))
                    {
                        return false;
                    }

                    return true;
                }
            }
        }

        private class Logger: ILogger
        {
            private readonly ParentLogger _parentLogger;
            private readonly int _loggerId;
            private readonly bool _isLessImportant;
            private readonly bool _doNotLogDuringLowDiskSpace;

            internal Logger(int id, ParentLogger parentLogger)
                : this(id, parentLogger, false, true)
            { }

            internal Logger(int id, ParentLogger parentLogger, bool isLessImportant)
                : this(id, parentLogger, isLessImportant, true)
            { }

            internal Logger(int id, ParentLogger parentLogger, bool isLessImportant, bool doNotLogDuringLowDiskSpace)
            {
                this._parentLogger = parentLogger;
                this._loggerId = id;
                this._isLessImportant = isLessImportant;
                this._doNotLogDuringLowDiskSpace = doNotLogDuringLowDiskSpace;
            }

            //
            // Following methods might maybe rather use blocking qeue and an async wroker
            //  that would THEN log to NLOG, as that will likely be far more efficent than using
            //  NLog built in asynchronous wrapper (as there needs to be 1 wrapper per device)
            //

            public void Log(LogLevel level, string message)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, this._isLessImportant));
            }

            public void Log(LogLevel level, IPooledBufferSegment pooledBuffer)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, pooledBuffer, this._isLessImportant));
            }

            public void Log(LogLevel level, string message, IPooledBufferSegment pooledBuffer)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, pooledBuffer, this._isLessImportant));
            }

            //public void LogWithDt(LogLevel level, DateTime correctedTimestamp, string message)
            //{
            //    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, this._isLessImportant, correctedTimestamp));
            //}


            // Following is the way how to include caller information in our logging
            //

            //public void Log(
            //    LogLevel level, 
            //    string message,
            //    [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            //    [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            //    [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
            //{
            //    //this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), message);
            //      LogEventInfo logEvent = new LogEventInfo(level, loggerName, null, messageFmt, args);
            //      logEvent.Properties.Add("callerpath", sourceFilePath);
            //      logEvent.Properties.Add("callermember", memberName);
            //      logEvent.Properties.Add("callerline", sourceLineNumber);
            //      //in config layout you can then specify ${event-context:item=callerpath}:${event-context:item=callermember}(${event-context:item=callerline})
            //      //DOWNSIDE - it is NOT possible to combine optional arguments and params argument (withou awkward syntax explicitly creating the object array)
            //      //  so all overloads would need to be specified explicitly
            //}


            public void Log(LogLevel level, string messageFmt, params object[] args)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, messageFmt, this._isLessImportant, args));
            }

            //public void Log(LogLevel level, IFormatProvider formatProvider, string messageFmt, params object[] args)
            //{
            //    this._logger.Log(NLog.LogLevel.FromOrdinal((int)level), formatProvider, messageFmt, args);
            //}

            public void LogException(LogLevel level, string message, Exception e)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, message, e, this._isLessImportant));
            }

            public void LogException(LogLevel level, Exception e, string messageFmt, params object[] args)
            {
                if (!this._parentLogger.BypassLoggingEntirely)
                    _parentLogger.LogAsync(new LogEvent(_loggerId, level, e, messageFmt, this._isLessImportant, args));
            }

            //public void LogException(Exception e)
            //{
            //    this._logger.LogException(NLog.LogLevel.FromOrdinal((int)LogLevel.Error),
            //                              "Experienced unexpected exception", e);
            //}

            public bool IsEnabled(LogLevel level)
            {
                return true;
            }

            public bool IsLowDiskSpaceConstraintLoggingOn
            {
                get { return this._doNotLogDuringLowDiskSpace && this._parentLogger.IsLowDiskSpace; }
            }

            public event Action<bool> IsLowDiskSpaceChanged
            {
                add { this._parentLogger.IsLowDiskSpace.ValueChanged += value; }

                remove { this._parentLogger.IsLowDiskSpace.ValueChanged -= value; }
            }

            public IReadonlyChangeTrackingType<bool> IsUltraLowDiskSpace
            {
                get { return this._parentLogger.IsUltraLowDiskSpace; }
            }
        }
    }
}
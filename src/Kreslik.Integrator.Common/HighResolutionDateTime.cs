﻿//#define HIGHRESTIMERS_TRACING

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.NMbgDevIO;

namespace Kreslik.Integrator.Common
{
    public static class HighResolutionDateTime
    {
        private static TimeSpan _resolutionThreshold = TimeSpan.FromMilliseconds(NMbgTimer._TIMERS_RESOLUTION_THRESHOLD_MS);
        public static TimeSpan ResolutionThreshold { get { return _resolutionThreshold; } }
        //Initialize the clock on the first use
        private static NMbgTimer _appdomainGlobalSharedClock; //= GetAppdomainGlobalSharedClock();
        public static IHighResolutionTimerManager HighResolutionTimerManager { get; private set; }

        static HighResolutionDateTime()
        {
            _appdomainGlobalSharedClock = GetAppdomainGlobalSharedClock();
        }

        private static NMbgTimer GetAppdomainGlobalSharedClock()
        {
            //First assing just the basic clocks - so that we can use it for logging issues during
            // creation of advanced clock, without the risk to end up with NullReferenceException or in an infinite recursion
            _appdomainGlobalSharedClock = NMbgTimer.GetBasicTimerWithoutMeibergClock();

            ILogger timerLogger = LogFactory.Instance.GetLogger("Timer");

            if (CommonSettings.CommonBehavior.AttemptToUseMeinbergClocks)
            {
                //Not sure WHY THE HECK but if it's here (not two lines above)
                // it causes NullRef exception even if everything is nonnull
                //ILogger timerLogger = LogFactory.Instance.GetLogger(null);

                try
                {
                    //possible to configure deviceId and poll interval here
                    NMbgTimer tempTimer = NMbgTimer.GetAsynchronousTimer();
                    tempTimer.UnhandledAsynchronousException +=
                        (exception, isFatal) =>
                        timerLogger.LogException(isFatal ? LogLevel.Fatal : LogLevel.Error, "Got error from Meinberg timer ", exception);
                    tempTimer.NewInformationAvailable += (message, isFatal) => timerLogger.Log(isFatal ? LogLevel.Fatal : LogLevel.Info, message);
                    _appdomainGlobalSharedClock = tempTimer;
                }
                catch (Exception e)
                {
                    timerLogger.LogException(LogLevel.Error, "Got error during creating advanced timer", e);
                    timerLogger.Log(LogLevel.Fatal,
                                    "Due to issues during initialization of advanced logger, just a basic HighResolution timer will be used (AttemptToUseMeinbergClocks in Common settings xml in backend was set to true - therefore logging fatal in this condition)");
                }
            }

            HighResolutionTimerManager =
                new HighResolutionTimerManager(_appdomainGlobalSharedClock.QueryAccurateTimeStampFunc,
                    _appdomainGlobalSharedClock.GetAccurateTimeStampFunc, ThreadPoolEx.Instance);

#if HIGHRESTIMERS_TRACING
            ILogger log = LogFactory.Instance.GetLogger("TimersTrace");
            log.Log(LogLevel.Fatal, "Compiled with HIGHRESTIMERS_TRACING - this will have severe affect on performance. Not to be used in production");
            _appdomainGlobalSharedClock.HighResolutionTimerManager.OnLogEntry += s => log.Log(LogLevel.Trace, s);
#endif

            try
            {
                TimerTaskFlagUtils.TestSelf();
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "TimerTaskFlagUtils found corruption. Execution is dangerous under those circumstances", e);
                Thread.Sleep(500);
                throw;
            }

            //now we already initialized our timers queue and ThreadPoolEx (both needing logger)
            // so we can finally initialize timers inside logger
            LogFactory.InitializeTimers();
            return _appdomainGlobalSharedClock;
        }

        public static bool IsAccurateDateTimeAvailable
        {
            get { return _appdomainGlobalSharedClock.IsAccurateDateTimeAvailable; }
        }

        public static event Action<bool> AccurateDateTimeAvailabilityChanged
        {
            add { _appdomainGlobalSharedClock.AccurateDateTimeAvailabilityChanged += value; }

            remove { _appdomainGlobalSharedClock.AccurateDateTimeAvailabilityChanged -= value; }
        }

        public static bool RegisterHandlerForGapsInPolling(TimeSpan minimumGapToInformAbout,
                                                    Action<long, long> detectedGapHandler)
        {
            return _appdomainGlobalSharedClock.RegisterHandlerForGapsInPolling(minimumGapToInformAbout,
                                                                               detectedGapHandler);
        }

        public static TimeSpan TimestampsPollingInterval
        {
            get { return _appdomainGlobalSharedClock.TimestampsPollingInterval; }
        }

        public static DateTime UtcNow
        {
            get
            {
                return _appdomainGlobalSharedClock.UtcNow;
            }
        }
    }
}

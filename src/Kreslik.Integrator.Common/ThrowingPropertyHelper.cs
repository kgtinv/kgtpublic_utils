﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kreslik.Integrator.Common
{
    public static class ThrowingPropertyHelper<T>
    {
        public static T ThrowOnAccess
        {
            get
            {
                throw new Exception("Not expected to be accessed");
            }
        }

        public static T ThrowOnAccessWithMessage(string errorMessage)
        {
            
                throw new Exception(errorMessage);
        }
    }
}

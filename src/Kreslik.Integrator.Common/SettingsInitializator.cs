﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class SettingsInitializator : ISettingsInitializator
    {
        private string _connectionString;
        private string _environmentName;
        public string InstanceDescription { get; private set; }

        public string EnvironmentName
        {
            get { return this._environmentName; }
        }

        public string InstanceName { get; private set; }

        public bool IsUat { get; private set; }

        public bool IsTradingInstance { get; private set; }

        public bool IsIntegratorProcess { get; }

        private bool _processTypeSet;
        public void SetProcessProcessType(IntegratorProcessType processType)
        {
            if(_processTypeSet)
                return;

            this.IntegratorProcessType = processType;
            this.InstanceDescription = string.Format("({0}-{1} on {2})", this.InstanceName, processType, Environment.MachineName);

            _processTypeSet = true;
        }

        public IntegratorProcessType IntegratorProcessType { get; private set; }

        static ISettingsInitializator _instance = new SettingsInitializator();

        public static ISettingsInitializator Instance
        {
            get { return _instance; }
        }

        private SettingsInitializator()
            : this(
                //Environment.GetCommandLineArgs() to get args and then
                //ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                //fileMap.ExeConfigFilename = configFilePath;
                //Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                ConfigurationManager.AppSettings["IntegratorEnvironmentName"],
                ConfigurationManager.ConnectionStrings["IntegratorDB"] == null
                    ? null
                    : ConfigurationManager.ConnectionStrings["IntegratorDB"].ConnectionString,
                ConfigurationManager.AppSettings["IntegratorInstanceName"])
        { }

        private SettingsInitializator(string integratorEnvironmentName, string connectionString, string instanceName)
        {
            this._connectionString = connectionString;
            this._environmentName = integratorEnvironmentName;
            this.InstanceName = instanceName;
            this.IsIntegratorProcess = !string.IsNullOrEmpty(instanceName);
            this.IsUat = !string.IsNullOrEmpty(instanceName) && !instanceName.StartsWith("PRO_", StringComparison.InvariantCultureIgnoreCase);
            this.IsTradingInstance = !string.IsNullOrEmpty(instanceName) && !instanceName.EndsWith("nontrading", StringComparison.OrdinalIgnoreCase);
            this.InstanceDescription = string.Format("({0} on {1})", instanceName, Environment.MachineName);
        }

        private static bool _isEnvironmentAvailable = false;

        public string UpdateCredentialsInConnectionString(string originalConnectionString)
        {
            System.Data.SqlClient.SqlConnectionStringBuilder originalConnStrBldr = new SqlConnectionStringBuilder(originalConnectionString);
            System.Data.SqlClient.SqlConnectionStringBuilder configuredConnStrBldr = new SqlConnectionStringBuilder(_connectionString);

            originalConnStrBldr.UserID = configuredConnStrBldr.UserID;
            originalConnStrBldr.Password = configuredConnStrBldr.Password;

            return originalConnectionString.ToString();
        }

        private void CheckIsEnvironmentAvailableIfNeeded()
        {
            if (_isEnvironmentAvailable)
                return;

            if (string.IsNullOrEmpty(_environmentName) || string.IsNullOrEmpty(_connectionString))
            {
                this.ReportError("integratorEnvironmentName and connectionString must be populated");
            }

            lock (_settingsXmls)
            {
                if (_isEnvironmentAvailable)
                    return;

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(this._connectionString))
                    {
                        sqlConnection.Open();
                        using (
                            SqlCommand getEnvironmentExistsCommand = new SqlCommand("[dbo].[GetEnvironmentExists_SP]",
                                                                                    sqlConnection))
                        {
                            getEnvironmentExistsCommand.CommandType = CommandType.StoredProcedure;
                            getEnvironmentExistsCommand.Parameters.AddWithValue("@EnvironmentName",
                                                                                _environmentName);

                            using (var reader = getEnvironmentExistsCommand.ExecuteReader())
                            {
                                if (!reader.Read())
                                {
                                    this.ReportError(string.Format("Environment [{0}] unknown in backend",
                                                                   _environmentName));
                                }
                                else
                                {
                                    PrecacheEnvironmentIfNeeded();
                                    _isEnvironmentAvailable = true;
                                }
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.ReportError(string.Format("Unable to load environemnt due to Sql Error:{0}{1}",
                                                   Environment.NewLine, e.ToString()));
                }
                catch (Exception e)
                {
                    this.ReportError(string.Format("Unable to load environemnt due to Error:{0}{1}", Environment.NewLine,
                                                   e.ToString()));
                }
            }
        }

        private static bool _isEnvironmentCached = false;

        private static Dictionary<string, string> _settingsXmls =
            new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase); 
        private void PrecacheEnvironmentIfNeeded()
        {
            if (_isEnvironmentCached)
                return;

            if (string.IsNullOrEmpty(_environmentName) || string.IsNullOrEmpty(_connectionString))
            {
                this.ReportError("integratorEnvironmentName and connectionString must be populated");
            }

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this._connectionString))
                {
                    sqlConnection.Open();
                    List<string> settingsNames = new List<string>();
                    using (
                        SqlCommand getEnvironmentSettingsCommand = new SqlCommand(
                            "[dbo].[GetEnvironmentSettingsSet_SP]",
                            sqlConnection))
                    {
                        getEnvironmentSettingsCommand.CommandType = CommandType.StoredProcedure;
                        getEnvironmentSettingsCommand.Parameters.AddWithValue("@EnvironmentName",
                                                                              _environmentName);

                        using (var reader = getEnvironmentSettingsCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string settingsName = reader.GetString(reader.GetOrdinal("SettingsName"));
                                int instancesOfSetting = reader.GetInt32(reader.GetOrdinal("DistinctValuesCount"));

                                if (instancesOfSetting < 1)
                                {
                                    this.ReportError(
                                        string.Format(
                                            "Setting {0} is set as mandatory for environment {1}, but it was not found in DB",
                                            settingsName, _environmentName));
                                }

                                if (instancesOfSetting > 1)
                                {
                                    this.ReportError(string.Format("Setting {0} configured {1} times for environment {2}, unique setting is need",
                                            settingsName, instancesOfSetting, _environmentName));
                                }

                                settingsNames.Add(settingsName);
                            }
                        }
                    }

                    foreach (string settingsName in settingsNames)
                    {
                        string settingsXml = this.GetSettingsXmlString(settingsName);
                        if (string.IsNullOrEmpty(settingsXml))
                        {
                            this.ReportError(
                                    string.Format(
                                        "There is no DB records for settings [{0}] in environment [{1}]",
                                        settingsName, _environmentName));
                        }
                        else
                        {
                            _settingsXmls.Add(settingsName, settingsXml);
                        }
                    }

                    _isEnvironmentCached = true;
                }
            }
            catch (SqlException e)
            {
                this.ReportError(string.Format("Unable to load environemnt due to Sql Error:{0}{1}", Environment.NewLine,
                                               e.ToString()));
            }
            catch (SettingsInitializationException e)
            {
                throw;
            }
            catch (Exception e)
            {
                this.ReportError(string.Format("Unable to load environemnt due to Error:{0}{1}", Environment.NewLine, e.ToString()));
            }
        }

        private string GetSettingsXmlString(string settingsName)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this._connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand getSettingsCommand = new SqlCommand("[dbo].[GetSettingsXml_SP]", sqlConnection))
                    {
                        getSettingsCommand.CommandType = CommandType.StoredProcedure;
                        getSettingsCommand.Parameters.AddWithValue("@EnvironmentName", this._environmentName);
                        getSettingsCommand.Parameters.AddWithValue("@SettingName", settingsName);
                        getSettingsCommand.Parameters.AddWithValue("@UpdatedAfter", System.Data.SqlTypes.SqlDateTime.MinValue.Value);

                        using (var reader = getSettingsCommand.ExecuteXmlReader())
                        {
                            if (reader.Read())
                            {
                                return reader.ReadOuterXml();
                            }
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                this.ReportError(string.Format("Got SqlException when obtaining Settings for {0}{1}{2}", settingsName,
                                               Environment.NewLine, sqlEx.ToString()));
            }
            catch (Exception e)
            {
                this.ReportError(string.Format("Got Exception when obtaining Settings for {0}{1}{2}", settingsName,
                                               Environment.NewLine, e.ToString()));
            }

            return null;
        }

        public bool HasSettings(string settingName)
        {
            CheckIsEnvironmentAvailableIfNeeded();

            return _settingsXmls.ContainsKey(settingName);
        }

        public T ReadSettings<T>(string settingName, string xsdConfigurationUri) where T : class
        {
            CheckIsEnvironmentAvailableIfNeeded();

            T settingObj = null;

            if (!_settingsXmls.ContainsKey(settingName))
            {
                this.ReportError(string.Format("Environment {0} doesn't contain requested setting {1}", _environmentName,
                                               settingName));
            }

            try
            {
                settingObj = SettingsReader.DeserializeConfigurationFromString<T>(_settingsXmls[settingName],
                                                                                            xsdConfigurationUri);
            }
            catch (Exception e)
            {
                this.ReportError(string.Format("Got Exception when obtaining Settings for {0}{1}{2}", settingName,
                                               Environment.NewLine, e.ToString()));
            }

            return settingObj;
        }

        private void ReportError(string error)
        {
            Console.WriteLine(error);

            string dir = System.IO.Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);

            System.IO.StreamWriter file = new System.IO.StreamWriter(Path.Combine(dir,"__PreinitializationError.log"));
            file.WriteLine(error);
            file.Close();

            throw new SettingsInitializationException(error);
        }
    }

    [Serializable]
    public class SettingsInitializationException : Exception
    {
        public SettingsInitializationException()
        {
        }

        public SettingsInitializationException(string message) : base(message)
        {
        }

        public SettingsInitializationException(string message, Exception inner) : base(message, inner)
        {
        }

        protected SettingsInitializationException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}

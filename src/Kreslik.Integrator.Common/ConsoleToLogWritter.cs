﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class ConsoleToLogRedirector
    {
        public static void RedirectConsoleToLog(ILogger log)
        {
            Console.SetOut(new ConsoleToLogWriter(log, false));
        }

        public static void DuplicateConsoleToLog(ILogger log)
        {
            Console.SetOut(new ConsoleToLogWriter(log, true));
        }

        public static void StopRedirection()
        {
            Console.SetOut(LogFactory.OriginalConsoleWriter);
        }

        private class ConsoleToLogWriter : TextWriter
        {
            private readonly ILogger _logger;
            private readonly bool _duplicate;
            private readonly StringBuilder _sb = new StringBuilder();

            public ConsoleToLogWriter(ILogger logger, bool duplicate)
            {
                this._logger = logger;
                this._duplicate = duplicate;
            }

            public override Encoding Encoding { get { return Encoding.UTF8; } }

            public override void Write(string value)
            {
                if (this._duplicate)
                {
                    LogFactory.OriginalConsoleWriter.Write(value);
                }

                lock (_sb)
                {
                    _sb.Append(value);

                    if (value.Contains(Environment.NewLine))
                    {
                        _logger.Log(LogLevel.Info, "Console: " + _sb.ToString());
                        _sb.Clear();
                    }
                    TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(1), AutoflushIfNeeded);
                }

            }

            private void AutoflushIfNeeded()
            {
                lock (_sb)
                {
                    if(_sb.Length > 0)
                    {
                        _logger.Log(LogLevel.Info, "Console (delayed autoflush - no newline sent within second after writing string(s)): " + _sb.ToString());
                        _sb.Clear();
                    }
                }
            }

            public override void WriteLine(string value)
            {
                if (this._duplicate)
                {
                    LogFactory.OriginalConsoleWriter.WriteLine(value);
                }
                lock (_sb)
                {
                    if (_sb.Length > 0)
                    {
                        _logger.Log(LogLevel.Info, "Console: " + _sb.ToString());
                        _sb.Clear();
                    }
                }
                _logger.Log(LogLevel.Info, "Console: " + value);
            }
        }
    }

    
}

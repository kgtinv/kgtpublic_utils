﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface IThreadPoolExPerformanceCounters
    {
        void Initialize(int initialThreadsNum);
        long IncrementQueuedTasks();
        void DecrementQueuedTasks(TimeSpan spanTheTaskWasQueued);
        long AvailableThreadsCount { get; }
        void IncrementAvailableThreads();
        void DecrementAvailableThreads();
        long IncrementTotalThreads();
        void DecrementTotalThreads();
    }

    public interface IIntegratorPerformanceCounter : IThreadPoolExPerformanceCounters
    {
        void FixMessageReceived();
        void FixMessageSend();
        void ExecutableQuoteReceived();
        void BytesRead(int bytesCount);
        void ExternalOrderSent();
        void AddInternalOrder();
        void RemoveInternalOrder();
        void AddDCFileFallbackCase();
        void SetUnpooledMessagingBufferSegemntsInUseCount(int count);
        void SetTotalUnpooledMessagingBufferSegemntsCreated(int count);
        void SetPooledMessagingBufferSegemntsInUseCount(int count);
        void SetMessagingBufferSegemntsPoolSize(int count);
        void SetUnpooledMessagingSubsegmentsInUseCount(int count);
        void SetTotalUnpooledMessagingSubsegmentsCreated(int count);
        void SetPooledMessagingSubsegmentsInUseCount(int count);
        void SetMessagingSubsegmentsPoolSize(int count);
        void SetUnpooledPriceObjectsInUseCount(int count);
        void SetTotalUnpooledPriceObjectsCreated(int count);
        void SetPooledPriceObjectsInUseCount(int count);
        void SetPriceObjectsPoolSize(int count);

        void SetLastTimestampsGapInMs(int miliseconds);
    }

    public class FakeIntegratorPerformanceCounter : DummyThreadPoolExPerformanceCounters, IIntegratorPerformanceCounter
    {
        public void FixMessageReceived() { }
        public void FixMessageSend() { }
        public void ExecutableQuoteReceived() { }
        public void BytesRead(int bytesCount) { }
        public void ExternalOrderSent() { }
        public void AddInternalOrder() { }
        public void RemoveInternalOrder() { }
        public void AddDCFileFallbackCase() { }
        public void SetUnpooledMessagingBufferSegemntsInUseCount(int count) { }
        public void SetTotalUnpooledMessagingBufferSegemntsCreated(int count) { }
        public void SetPooledMessagingBufferSegemntsInUseCount(int count) { }
        public void SetMessagingBufferSegemntsPoolSize(int count) { }
        public void SetUnpooledMessagingSubsegmentsInUseCount(int count) { }
        public void SetTotalUnpooledMessagingSubsegmentsCreated(int count) { }
        public void SetPooledMessagingSubsegmentsInUseCount(int count) { }
        public void SetMessagingSubsegmentsPoolSize(int count) { }
        public void SetUnpooledPriceObjectsInUseCount(int count) { }
        public void SetTotalUnpooledPriceObjectsCreated(int count) { }
        public void SetPooledPriceObjectsInUseCount(int count) { }
        public void SetPriceObjectsPoolSize(int count) { }

        public void SetLastTimestampsGapInMs(int miliseconds) { }
    }

    public class IntegratorPerformanceCounter : IIntegratorPerformanceCounter
    {
        private const string COUNTER_CATEGORY_NAME = "KGT Integrator Counters";
        private const string COUNTER_INSTANCE_MAINPROCESS = "Business Logic Process";
        private const string COUNTER_INSTANCE_HOTSPOTPROCESS = "Hotspot Process";
        private const string COUNTER_INSTANCE_DATACOLLECTIONPROCESS = "DC Process";
        private const string COUNTER_INSTANCE_METRICSPROCESS = "DC Process";
        private const string COUNTER_CATEGORY_DESCRIPTION = "Counters monitoring the KGT integrator service";
        private const string MESSAGES_COUNTER_NAME = "Rx Messages per sec";
        private const string MESSAGES_COUNTER_DESCRIPTION = "Total number of received FIX messages per second";
        private const string TXMESSAGES_COUNTER_NAME = "Tx Messages per sec";
        private const string TXMESSAGES_COUNTER_DESCRIPTION = "Total number of sent FIX messages per second";
        private const string EXECQUOTES_COUNTER_NAME = "Exec Quotes per sec";
        private const string EXECQUOTES_COUNTER_DESCRIPTION = "Total number of received executable quotes per second";
        private const string BYTESREAD_COUNTER_NAME = "Rx Bytes per sec";
        private const string BYTESREAD_COUNTER_DESCRIPTION = "Total number of received bytes per second";
        private const string BYTESREADTOTAL_COUNTER_NAME = "Rx Bytes total";
        private const string BYTESREADTOTAL_COUNTER_DESCRIPTION = "Total number of received bytes";
        private const string ORDERSTOTAL_COUNTER_NAME = "Orders sent total";
        private const string ORDERSTOTAL_COUNTER_DESCRIPTION = "Total number of sent external orders";
        private const string INTERNALORDERSTOTAL_COUNTER_NAME = "Internal Orders sent total";
        private const string INTERNALORDERSTOTAL_COUNTER_DESCRIPTION = "Total number of sent internal orders";
        private const string ACTIVEINTERNALORDERSTOTAL_COUNTER_NAME = "Active Internal Orders total";
        private const string ACTIVEINTERNALORDERSTOTAL_COUNTER_DESCRIPTION = "Total number of active internal orders";

        private const string DCFALLBACK_COUNTER_NAME = "DC Fallback Cases";
        private const string DCFALLBACK_COUNTER_DESCRIPTION = "Total number of via-file fallback sent cases from start of process";

        private const string MSGINGPOOLEDBUFFERS_COUNTER_NAME = "Fix Messaging Pooled Buffers Usage";
        private const string MSGINGPOOLEDBUFFERS_COUNTER_DESCRIPTION = "Current # of used buffer segments from pool for Fix Messaging";
        private const string MSGINGUNPOOLEDBUFFERS_COUNTER_NAME = "Fix Messaging Unpooled Buffers Usage";
        private const string MSGINGUNPOOLEDBUFFERS_COUNTER_DESCRIPTION = "Current # of used unpooled buffer segments for Fix Messaging";
        private const string MSGINGTOTALUNPOOLEDBUFFERS_COUNTER_NAME = "Fix Messaging Total Unpooled Buffers Created";
        private const string MSGINGTOTALUNPOOLEDBUFFERS_COUNTER_DESCRIPTION = "Total # of created unpooled buffer segments for Fix Messaging";
        private const string MSGINGPOOLEDBUFFERSSIZE_COUNTER_NAME = "Fix Messaging Buffers Pool Size";
        private const string MSGINGPOOLEDBUFFERSSIZE_COUNTER_DESCRIPTION = "Total size of byte segments buffers for Fix Messaging";
        private const string MSGINGPOOLEDSUBSEGMENTS_COUNTER_NAME = "Fix Messaging Pooled subsegments Usage";
        private const string MSGINGPOOLEDSUBSEGMENTS_COUNTER_DESCRIPTION = "Current # of used subsegments from pool for Fix Messaging";
        private const string MSGINGUNPOOLEDSUBSEGMENTS_COUNTER_NAME = "Fix Messaging Unpooled Subsegments Usage";
        private const string MSGINGUNPOOLEDSUBSEGMENTS_COUNTER_DESCRIPTION = "Current # of used unpooled subsegments for Fix Messaging";
        private const string MSGINGTOTALUNPOOLEDSUBSEGMENTS_COUNTER_NAME = "Fix Messaging Total Unpooled Subsegments Created";
        private const string MSGINGTOTALUNPOOLEDSUBSEGMENTS_COUNTER_DESCRIPTION = "Total # of created unpooled sbsegments for Fix Messaging";
        private const string MSGINGPOOLEDSUBSEGMENTSSIZE_COUNTER_NAME = "Fix Messaging Subsegments Pool Size";
        private const string MSGINGPOOLEDSUBSEGMENTSSIZE_COUNTER_DESCRIPTION = "Total size of subsegments pool for Fix Messaging";

        private const string PRICEOBJECTSPOOLED_COUNTER_NAME = "PriceObjects: Pool Usage";
        private const string PRICEOBJECTSPOOLED_COUNTER_DESCRIPTION = "Current # of used PriceObjects from pool";
        private const string PRICEOBJECTSUNPOOLED_COUNTER_NAME = "PriceObjects: Unpooled Usage";
        private const string PRICEOBJECTSUNPOOLED_COUNTER_DESCRIPTION = "Current # of used unpooled PricrObjects";
        private const string PRICEOBJECTSUNPOOLEDTOTAL_COUNTER_NAME = "PriceObjects: Unpooled Total";
        private const string PRICEOBJECTSUNPOOLEDTOTAL_COUNTER_DESCRIPTION = "Total # of created unpooled PriceObjects";
        private const string PRICEOBJECTSPOOLSIZE_COUNTER_NAME = "PriceObjects: Pool Size";
        private const string PRICEOBJECTSPOOLSIZE_COUNTER_DESCRIPTION = "Total size of PriceObjects pool";

        private const string LASTTIMESTAMPSGAP_COUNTER_NAME = "Last Timestamps Gap [ms]";
        private const string LASTTIMESTAMPSGAP_COUNTER_DESCRIPTION = "Duration in milliseconds of last detected gap in timestamps";

        private const string THREADPOOLEXAVAILTH_COUNTER_NAME = "ThreadPoolEx - Available Threads";
        private const string THREADPOOLEXAVAILTH_COUNTER_DESCRIPTION = "ThreadPoolEx - total number of currently available thread for immediate execution";
        private const string THREADPOOLEXTOTALTH_COUNTER_NAME = "ThreadPoolEx - Total Threads";
        private const string THREADPOOLEXTOTALTH_COUNTER_DESCRIPTION = "ThreadPoolEx - total number of currently working and available threads";
        private const string THREADPOOLEXQUEUEDWK_COUNTER_NAME = "ThreadPoolEx - Queued work items";
        private const string THREADPOOLEXQUEUEDWK_COUNTER_DESCRIPTION = "ThreadPoolEx - total number of currently waiting work items for execution";

        private const string THREADPOOLEXLASTQUEUEDUR_COUNTER_NAME = "ThreadPoolEx - Last queue wait [ms]";
        private const string THREADPOOLEXLASTQUEUEDUR_COUNTER_DESCRIPTION = "ThreadPoolEx - Last wait time [ms] of lastly executed workitem";
        private const string THREADPOOLEXAVGQUEUEDUR_COUNTER_NAME = "ThreadPoolEx - Avg queue wait [ms]";
        private const string THREADPOOLEXAVGQUEUEDUR_COUNTER_DESCRIPTION = "ThreadPoolEx - Average wait time in queue [ms] of all items executed by ThreadPoolEx from startup.";


        private bool _initialized = false;
        private PerformanceCounter _messagesPerSec = null;
        private PerformanceCounter _txmessagesPerSec = null;
        private PerformanceCounter _execQuotesPerSec = null;
        private PerformanceCounter _bytesReadPerSec = null;
        private PerformanceCounter _bytesReadTotal = null;
        private PerformanceCounter _ordersTotal = null;
        private PerformanceCounter _internalOrdersTotal = null;
        private PerformanceCounter _activeInternalOrders = null;

        private PerformanceCounter _dcFileFallbackCases = null;

        private PerformanceCounter _msgingPooledBuffers = null;
        private PerformanceCounter _msgingPooledBuffersSize = null;
        private PerformanceCounter _msgingUnpooledBuffers = null;
        private PerformanceCounter _msgingUnpooledBuffersTotalCreated = null;
        private PerformanceCounter _msgingPooledSubsegments = null;
        private PerformanceCounter _msgingPooledSubsegmentsSize = null;
        private PerformanceCounter _msgingUnpooledSubsegments = null;
        private PerformanceCounter _msgingUnpooledSubsegmentsTotalCreated = null;
        private PerformanceCounter _priceObjectPooledElements = null;
        private PerformanceCounter _priceObjectPoolSize = null;
        private PerformanceCounter _priceObjectUnpooledElements = null;
        private PerformanceCounter _priceObjectUnpooledElementsTotalCreated = null;

        private PerformanceCounter _lastGapMs = null;

        private PerformanceCounter _threadPoolExAvailTh = null;
        private PerformanceCounter _threadPoolExTotalTh = null;
        private PerformanceCounter _threadPoolExQueuedWk = null;
        private PerformanceCounter _threadPoolExLastWait = null;
        private PerformanceCounter _threadPoolExAvgWait = null;

        public enum InstanceType
        {
            MainProcess,
            HotspotProcess,
            DcProcess
        }

        public static void InitializeInstance(IntegratorProcessType instanceType)
        {
            if (instanceType == IntegratorProcessType.Web || instanceType == IntegratorProcessType.NotAnyIntegratorService)
                return;

            _instance = new IntegratorPerformanceCounter((LogFactory.Instance).GetLogger(null), instanceType);
        }

        public static IIntegratorPerformanceCounter _instance = new FakeIntegratorPerformanceCounter();
        public static IIntegratorPerformanceCounter Instance
        {
            get { return _instance; }
        }

        private static IntegratorProcessType? _integratorProcessType;
        public static IntegratorProcessType? CurrentIntegratorProcessType { get { return _integratorProcessType; } }

        private IntegratorPerformanceCounter(ILogger logger, IntegratorProcessType instanceType)
        {
            string instanceName;
            switch (instanceType)
            {
                case IntegratorProcessType.BusinessLogic:
                    instanceName = COUNTER_INSTANCE_MAINPROCESS;
                    break;
                case IntegratorProcessType.HotspotMarketData:
                    instanceName = COUNTER_INSTANCE_HOTSPOTPROCESS;
                    break;
                case IntegratorProcessType.DataCollection:
                    instanceName = COUNTER_INSTANCE_DATACOLLECTIONPROCESS;
                    break;
                case IntegratorProcessType.MetricsInMemoryDb:
                    instanceName = COUNTER_INSTANCE_METRICSPROCESS;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("instanceType");
            }

            this._initialized = this.Initialize(logger, false, instanceName);
            _integratorProcessType = instanceType;
        }

        public void FixMessageReceived()
        {
            if (_initialized)
            {
                _messagesPerSec.Increment();
            }
        }

        public void FixMessageSend()
        {
            if (_initialized)
            {
                _txmessagesPerSec.Increment();
            }
        }

        public void ExecutableQuoteReceived()
        {
            if (_initialized)
            {
                _execQuotesPerSec.Increment();
            }
        }

        public void BytesRead(int bytesCount)
        {
            if (_initialized)
            {
                _bytesReadTotal.IncrementBy(bytesCount);
                _bytesReadPerSec.IncrementBy(bytesCount);
            }
        }

        public void ExternalOrderSent()
        {
            if (_initialized)
            {
                _ordersTotal.Increment();
            }
        }

        public void AddInternalOrder()
        {
            if (_initialized)
            {
                _internalOrdersTotal.Increment();
                _activeInternalOrders.Increment();
            }
        }

        public void RemoveInternalOrder()
        {
            if (_initialized)
            {
                _activeInternalOrders.Decrement();
            }
        }

        public void AddDCFileFallbackCase()
        {
            if (_initialized)
            {
                _dcFileFallbackCases.Increment();
            }
        }

        public void SetUnpooledMessagingBufferSegemntsInUseCount(int count)
        {
            if (_initialized)
            {
                _msgingUnpooledBuffers.RawValue = count;
            }
        }

        public void SetTotalUnpooledMessagingBufferSegemntsCreated(int count)
        {
            if (_initialized)
            {
                _msgingUnpooledBuffersTotalCreated.RawValue = count;
            }
        }

        public void SetPooledMessagingBufferSegemntsInUseCount(int count)
        {
            if (_initialized)
            {
                _msgingPooledBuffers.RawValue = count;
            }
        }

        public void SetMessagingBufferSegemntsPoolSize(int count)
        {
            if (_initialized)
            {
                _msgingPooledBuffersSize.RawValue = count;
            }
        }

        public void SetUnpooledMessagingSubsegmentsInUseCount(int count)
        {
            if (_initialized)
            {
                _msgingUnpooledSubsegments.RawValue = count;
            }
        }

        public void SetTotalUnpooledMessagingSubsegmentsCreated(int count)
        {
            if (_initialized)
            {
                _msgingUnpooledSubsegmentsTotalCreated.RawValue = count;
            }
        }

        public void SetPooledMessagingSubsegmentsInUseCount(int count)
        {
            if (_initialized)
            {
                _msgingPooledSubsegments.RawValue = count;
            }
        }

        public void SetMessagingSubsegmentsPoolSize(int count)
        {
            if (_initialized)
            {
                _msgingPooledSubsegmentsSize.RawValue = count;
            }
        }

        public void SetUnpooledPriceObjectsInUseCount(int count)
        {
            if (_initialized)
            {
                _priceObjectUnpooledElements.RawValue = count;
            }
        }

        public void SetTotalUnpooledPriceObjectsCreated(int count)
        {
            if (_initialized)
            {
                _priceObjectUnpooledElementsTotalCreated.RawValue = count;
            }
        }

        public void SetPooledPriceObjectsInUseCount(int count)
        {
            if (_initialized)
            {
                _priceObjectPooledElements.RawValue = count;
            }
        }

        public void SetPriceObjectsPoolSize(int count)
        {
            if (_initialized)
            {
                _priceObjectPoolSize.RawValue = count;
            }
        }

        public void SetLastTimestampsGapInMs(int miliseconds)
        {
            if (_initialized)
            {
                _lastGapMs.RawValue = miliseconds;
            }
        }

        public static bool UninstallCounters(ILogger logger)
        {
            if (PerformanceCounterCategory.Exists(COUNTER_CATEGORY_NAME))
            {
                try
                {
                    PerformanceCounterCategory.Delete(COUNTER_CATEGORY_NAME);
                }
                catch (UnauthorizedAccessException e)
                {
                    logger.LogException(LogLevel.Error, e,
                                    "Couldn't delete the performance counter category '{0}'. Aplication needs to be start elevated in order to create the counters. Once they are created, it can start without elevation",
                                    COUNTER_CATEGORY_NAME);
                    return false;
                }

            }

            return true;
        }

        public static bool InstallCountersIfNeeded(ILogger logger)
        {
            if (!PerformanceCounterCategory.Exists(COUNTER_CATEGORY_NAME))
            {
                try
                {
                    CreateCounter();
                }
                catch (SecurityException e)
                {
                    logger.LogException(LogLevel.Error, e,
                                        "Couldn't create the performance counter category '{0}'. Aplication needs to be start elevated in order to create the counters. Once they are created, it can start without elevation",
                                        COUNTER_CATEGORY_NAME);
                    return false;
                }
            }

            return true;
        }

        private bool Initialize(ILogger logger, bool deleteCounterCategoryIfExists, string instanceName)
        {
            if (deleteCounterCategoryIfExists)
            {
                if (!UninstallCounters(logger)) return false;
            }

            if (!InstallCountersIfNeeded(logger)) return false;

            _messagesPerSec = new PerformanceCounter(COUNTER_CATEGORY_NAME, MESSAGES_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _txmessagesPerSec = new PerformanceCounter(COUNTER_CATEGORY_NAME, TXMESSAGES_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _execQuotesPerSec = new PerformanceCounter(COUNTER_CATEGORY_NAME, EXECQUOTES_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _bytesReadPerSec = new PerformanceCounter(COUNTER_CATEGORY_NAME, BYTESREAD_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _bytesReadTotal = new PerformanceCounter(COUNTER_CATEGORY_NAME, BYTESREADTOTAL_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _ordersTotal = new PerformanceCounter(COUNTER_CATEGORY_NAME, ORDERSTOTAL_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _internalOrdersTotal = new PerformanceCounter(COUNTER_CATEGORY_NAME, INTERNALORDERSTOTAL_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _activeInternalOrders = new PerformanceCounter(COUNTER_CATEGORY_NAME, ACTIVEINTERNALORDERSTOTAL_COUNTER_NAME, instanceName, false) { RawValue = 0 };

            _dcFileFallbackCases = new PerformanceCounter(COUNTER_CATEGORY_NAME, DCFALLBACK_COUNTER_NAME, instanceName, false) { RawValue = 0 };

            _msgingPooledBuffers = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGPOOLEDBUFFERS_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _msgingPooledBuffersSize = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGPOOLEDBUFFERSSIZE_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _msgingUnpooledBuffers = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGUNPOOLEDBUFFERS_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _msgingUnpooledBuffersTotalCreated = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGTOTALUNPOOLEDBUFFERS_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _msgingPooledSubsegments = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGPOOLEDSUBSEGMENTS_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _msgingPooledSubsegmentsSize = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGPOOLEDSUBSEGMENTSSIZE_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _msgingUnpooledSubsegments = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGUNPOOLEDSUBSEGMENTS_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _msgingUnpooledSubsegmentsTotalCreated = new PerformanceCounter(COUNTER_CATEGORY_NAME, MSGINGTOTALUNPOOLEDSUBSEGMENTS_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _priceObjectPooledElements = new PerformanceCounter(COUNTER_CATEGORY_NAME, PRICEOBJECTSPOOLED_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _priceObjectPoolSize = new PerformanceCounter(COUNTER_CATEGORY_NAME, PRICEOBJECTSPOOLSIZE_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _priceObjectUnpooledElements = new PerformanceCounter(COUNTER_CATEGORY_NAME, PRICEOBJECTSUNPOOLED_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _priceObjectUnpooledElementsTotalCreated = new PerformanceCounter(COUNTER_CATEGORY_NAME, PRICEOBJECTSUNPOOLEDTOTAL_COUNTER_NAME, instanceName, false) { RawValue = 0 };

            _lastGapMs = new PerformanceCounter(COUNTER_CATEGORY_NAME, LASTTIMESTAMPSGAP_COUNTER_NAME, instanceName, false) { RawValue = 0 };

            _threadPoolExAvailTh = new PerformanceCounter(COUNTER_CATEGORY_NAME, THREADPOOLEXAVAILTH_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _threadPoolExTotalTh = new PerformanceCounter(COUNTER_CATEGORY_NAME, THREADPOOLEXTOTALTH_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _threadPoolExQueuedWk = new PerformanceCounter(COUNTER_CATEGORY_NAME, THREADPOOLEXQUEUEDWK_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _threadPoolExLastWait = new PerformanceCounter(COUNTER_CATEGORY_NAME, THREADPOOLEXLASTQUEUEDUR_COUNTER_NAME, instanceName, false) { RawValue = 0 };
            _threadPoolExAvgWait = new PerformanceCounter(COUNTER_CATEGORY_NAME, THREADPOOLEXAVGQUEUEDUR_COUNTER_NAME, instanceName, false) { RawValue = 0 };

            return true;
        }

        private static void CreateCounter()
        {
            CounterCreationDataCollection counterData = new CounterCreationDataCollection();

            CounterCreationData integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.RateOfCountsPerSecond32;
            integratorCounter.CounterName = MESSAGES_COUNTER_NAME;
            integratorCounter.CounterHelp = MESSAGES_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.RateOfCountsPerSecond32;
            integratorCounter.CounterName = TXMESSAGES_COUNTER_NAME;
            integratorCounter.CounterHelp = TXMESSAGES_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.RateOfCountsPerSecond32;
            integratorCounter.CounterName = EXECQUOTES_COUNTER_NAME;
            integratorCounter.CounterHelp = EXECQUOTES_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.RateOfCountsPerSecond32;
            integratorCounter.CounterName = BYTESREAD_COUNTER_NAME;
            integratorCounter.CounterHelp = BYTESREAD_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems64;
            integratorCounter.CounterName = BYTESREADTOTAL_COUNTER_NAME;
            integratorCounter.CounterHelp = BYTESREADTOTAL_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = ORDERSTOTAL_COUNTER_NAME;
            integratorCounter.CounterHelp = ORDERSTOTAL_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems64;
            integratorCounter.CounterName = INTERNALORDERSTOTAL_COUNTER_NAME;
            integratorCounter.CounterHelp = INTERNALORDERSTOTAL_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = ACTIVEINTERNALORDERSTOTAL_COUNTER_NAME;
            integratorCounter.CounterHelp = ACTIVEINTERNALORDERSTOTAL_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = DCFALLBACK_COUNTER_NAME;
            integratorCounter.CounterHelp = DCFALLBACK_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGPOOLEDBUFFERS_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGPOOLEDBUFFERS_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGPOOLEDBUFFERSSIZE_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGPOOLEDBUFFERSSIZE_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGUNPOOLEDBUFFERS_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGUNPOOLEDBUFFERS_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGTOTALUNPOOLEDBUFFERS_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGTOTALUNPOOLEDBUFFERS_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGPOOLEDSUBSEGMENTS_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGPOOLEDSUBSEGMENTS_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGPOOLEDSUBSEGMENTSSIZE_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGPOOLEDSUBSEGMENTSSIZE_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGUNPOOLEDSUBSEGMENTS_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGUNPOOLEDSUBSEGMENTS_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = MSGINGTOTALUNPOOLEDSUBSEGMENTS_COUNTER_NAME;
            integratorCounter.CounterHelp = MSGINGTOTALUNPOOLEDSUBSEGMENTS_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = PRICEOBJECTSPOOLED_COUNTER_NAME;
            integratorCounter.CounterHelp = PRICEOBJECTSPOOLED_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = PRICEOBJECTSPOOLSIZE_COUNTER_NAME;
            integratorCounter.CounterHelp = PRICEOBJECTSPOOLSIZE_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = PRICEOBJECTSUNPOOLED_COUNTER_NAME;
            integratorCounter.CounterHelp = PRICEOBJECTSUNPOOLED_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = PRICEOBJECTSUNPOOLEDTOTAL_COUNTER_NAME;
            integratorCounter.CounterHelp = PRICEOBJECTSUNPOOLEDTOTAL_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = LASTTIMESTAMPSGAP_COUNTER_NAME;
            integratorCounter.CounterHelp = LASTTIMESTAMPSGAP_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = THREADPOOLEXAVAILTH_COUNTER_NAME;
            integratorCounter.CounterHelp = THREADPOOLEXAVAILTH_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = THREADPOOLEXTOTALTH_COUNTER_NAME;
            integratorCounter.CounterHelp = THREADPOOLEXTOTALTH_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = THREADPOOLEXQUEUEDWK_COUNTER_NAME;
            integratorCounter.CounterHelp = THREADPOOLEXQUEUEDWK_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = THREADPOOLEXLASTQUEUEDUR_COUNTER_NAME;
            integratorCounter.CounterHelp = THREADPOOLEXLASTQUEUEDUR_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            integratorCounter = new CounterCreationData();

            integratorCounter.CounterType = PerformanceCounterType.NumberOfItems32;
            integratorCounter.CounterName = THREADPOOLEXAVGQUEUEDUR_COUNTER_NAME;
            integratorCounter.CounterHelp = THREADPOOLEXAVGQUEUEDUR_COUNTER_DESCRIPTION;

            counterData.Add(integratorCounter);

            PerformanceCounterCategory.Create(COUNTER_CATEGORY_NAME, COUNTER_CATEGORY_DESCRIPTION,
                                              PerformanceCounterCategoryType.MultiInstance, counterData);
        }

        public void Initialize(int initialThreadsNum)
        {
            this._threadPoolExAvailTh.RawValue = initialThreadsNum;
            this._threadPoolExTotalTh.RawValue = initialThreadsNum;
            this._threadPoolExQueuedWk.RawValue = 0;
        }

        public long IncrementQueuedTasks()
        {
            return this._threadPoolExQueuedWk.Increment();
        }

        private int _totalTasks = 0;
        private long _totalTicks = 0;
        private const uint TICKS_PER_MILLISECOND = 10000;
        public void DecrementQueuedTasks(TimeSpan spanTheTaskWasQueued)
        {
            this._threadPoolExQueuedWk.Decrement();
            _threadPoolExLastWait.RawValue = (int) spanTheTaskWasQueued.TotalMilliseconds;
            _threadPoolExAvgWait.RawValue = Interlocked.Add(ref _totalTicks, spanTheTaskWasQueued.Ticks)/TICKS_PER_MILLISECOND/
                                            Interlocked.Increment(ref _totalTasks);

        }

        public long AvailableThreadsCount { get { return this._threadPoolExAvailTh.RawValue; } }
        public void IncrementAvailableThreads()
        {
            this._threadPoolExAvailTh.Increment();
        }

        public void DecrementAvailableThreads()
        {
            this._threadPoolExAvailTh.Decrement();
        }

        public long IncrementTotalThreads()
        {
            return this._threadPoolExTotalTh.Increment();
        }

        public void DecrementTotalThreads()
        {
            this._threadPoolExTotalTh.Decrement();
        }
    }
}

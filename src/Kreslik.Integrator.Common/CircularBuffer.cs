﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface ICircularBuffer<T>
    {
        //int Count { get; }
        //int Capacity { get; }
        void Enqueue(T item);
        //T Dequeue();
        void Clear();
        /// <summary>
        /// Indexing from newest item (the last inserted is on 0)
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        T this[int index] { get; }
        T Newest { get; }
        T Oldest { get; }
        //int IndexOf(T item);
        //void Insert(int index, T item);
        //void RemoveAt(int index);
        T[] ToArray();
        bool IsFull { get; }
        bool IsEmpty { get; }
        void ClearOldestN(int n);
        void ClearNewestItem();
        int ItemsCount { get; }
    }

    public interface ICircularBufferWithAggregation<T> : ICircularBuffer<T>
    {
        T Aggregate(Func<T[], T> aggregateFunc);
        T EnqueueNewAndReturnReplacedItem(T item);
    }

    public interface IOveridableTopCircularBuffer<T>
    {
        T Newest { set; }
    }

    public interface IOveridableTopCircularBufferWithAggregation<T> : IOveridableTopCircularBuffer<T>,
        ICircularBufferWithAggregation<T>
    {
        new T Newest { get; set; }
    }

    public class ZeroBuffer<T> : ICircularBuffer<T>
    {
        public void Enqueue(T item)
        {
            //this is it
        }

        public void Clear()
        {
            //this is it
        }

        public T[] ToArray()
        {
            return null;
        }

        public bool IsFull
        {
            get { return true; }
        }

        public void ClearOldestN(int n)
        {
            //this is it
        }


        public T this[int index]
        {
            get { return default(T); }
        }

        public T Newest
        {
            get { return default(T); }
        }

        public T Oldest
        {
            get { return default(T); }
        }

        public int ItemsCount { get { return 0; } }
        public bool IsEmpty { get { return false; } }
        public void ClearNewestItem()
        {
            //this is it
        }
    }

    public class CircularBuffer<T> : IOveridableTopCircularBufferWithAggregation<T>
    {
        private T[] _items;
        private int _nextHeadIdx;
        private int _itemsCount;
        private int _capacity;

        public CircularBuffer(int capacity)
        {
            this._items = new T[capacity];
            this._capacity = capacity;
        }

        public void Enqueue(T item)
        {
            this._items[this._nextHeadIdx] = item;
            
            if(this._itemsCount < this._capacity)
                this._itemsCount++;

            this._nextHeadIdx ++;
            if (this._nextHeadIdx >= this._capacity)
                this._nextHeadIdx = 0;
        }

        public T EnqueueNewAndReturnReplacedItem(T item)
        {
            T replaced = this._items[this._nextHeadIdx];
            this.Enqueue(item);
            return replaced;
        }

        public void Clear()
        {
            Array.Clear(this._items, 0, this._capacity);
            this._nextHeadIdx = 0;
            this._itemsCount = 0;
        }

        public T this[int index]
        {
            get
            {
                return this._items[this.TransformZeroBasedIndexToCircularIndex(index)];
            }

            private set { this._items[this.TransformZeroBasedIndexToCircularIndex(index)] = value; }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int TransformZeroBasedIndexToCircularIndex(int zeroBasedIndex)
        {
            if (zeroBasedIndex >= _itemsCount || zeroBasedIndex < 0)
                throw new IndexOutOfRangeException(string.Format("Requested index: {0}, current items count: {1}",
                    zeroBasedIndex, this._itemsCount));

            int circularIndex = this._nextHeadIdx - 1 - zeroBasedIndex;
            if (circularIndex < 0)
                circularIndex += this._capacity;
            else if (circularIndex >= this._capacity)
                circularIndex -= this._capacity;

            return circularIndex;
        }

        public T Newest
        {
            get { return this[0]; }
            set { this[0] = value; }
        }

        T ICircularBuffer<T>.Newest => Newest;

        T IOveridableTopCircularBuffer<T>.Newest {set { Newest = value; } }

        public T[] ToArray()
        {
            T[] zeroBasedArray = new T[_itemsCount];
            for (int zeroBasedIdx = 0; zeroBasedIdx < _itemsCount; zeroBasedIdx++)
            {
                //we insert from back - since we need to have items in chronological order (but zero based indexing is from newest)
                zeroBasedArray[_itemsCount - 1 - zeroBasedIdx] = this[zeroBasedIdx];
            }

            return zeroBasedArray;
        }

        public bool IsFull
        {
            get { return this._itemsCount == this._capacity; }
        }

        public void ClearOldestN(int n)
        {
            if (n < 0)
            {
                throw new IndexOutOfRangeException(string.Format("Requested items to clear: {0}, current items count: {1}",
                    n, this._itemsCount));
            }
            this._itemsCount = Math.Max(0, this._itemsCount - n);
        }

        public void ClearNewestItem()
        {
            if (this._itemsCount <= 0)
            {
                return;
            }

            this._nextHeadIdx--;
            if (this._nextHeadIdx < 0)
                this._nextHeadIdx = this._capacity - 1;
            this._itemsCount--;
        }

        public T Aggregate(Func<T[], T> aggregateFunc)
        {
            return aggregateFunc(this._items);
        }

        public T Oldest
        {
            get { return this[this._itemsCount - 1]; }
        }

        public int ItemsCount { get {return _itemsCount;} }
        public bool IsEmpty { get { return _itemsCount <= 0; } }
    }
}

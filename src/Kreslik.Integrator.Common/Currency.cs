﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public struct Currency
    {
        //WARNING: account for default initialization!!!
        //   the _value is +1 offseted - so that the dafault unitialized 0 would result to -1 - which is NULL
        private readonly byte _value;
        private static readonly Dictionary<string, byte> _instanceNamesToIds = new Dictionary<string, byte>();
        private static readonly List<string> _instanceIdsToNames = new List<string>();

        public static readonly Currency NULL = new Currency("NULLCurrency");

        public static readonly Currency AED = new Currency(@"AED");
        public static readonly Currency AFN = new Currency(@"AFN");
        public static readonly Currency ALL = new Currency(@"ALL");
        public static readonly Currency AMD = new Currency(@"AMD");
        public static readonly Currency ANG = new Currency(@"ANG");
        public static readonly Currency AOA = new Currency(@"AOA");
        public static readonly Currency ARS = new Currency(@"ARS");
        public static readonly Currency AUD = new Currency(@"AUD");
        public static readonly Currency AWG = new Currency(@"AWG");
        public static readonly Currency AZN = new Currency(@"AZN");
        public static readonly Currency BAM = new Currency(@"BAM");
        public static readonly Currency BBD = new Currency(@"BBD");
        public static readonly Currency BDT = new Currency(@"BDT");
        public static readonly Currency BGN = new Currency(@"BGN");
        public static readonly Currency BHD = new Currency(@"BHD");
        public static readonly Currency BIF = new Currency(@"BIF");
        public static readonly Currency BMD = new Currency(@"BMD");
        public static readonly Currency BND = new Currency(@"BND");
        public static readonly Currency BOB = new Currency(@"BOB");
        public static readonly Currency BOV = new Currency(@"BOV");
        public static readonly Currency BRL = new Currency(@"BRL");
        public static readonly Currency BSD = new Currency(@"BSD");
        public static readonly Currency BTN = new Currency(@"BTN");
        public static readonly Currency BWP = new Currency(@"BWP");
        public static readonly Currency BYR = new Currency(@"BYR");
        public static readonly Currency BZD = new Currency(@"BZD");
        public static readonly Currency CAD = new Currency(@"CAD");
        public static readonly Currency CDF = new Currency(@"CDF");
        public static readonly Currency CHE = new Currency(@"CHE");
        public static readonly Currency CHF = new Currency(@"CHF");
        public static readonly Currency CHW = new Currency(@"CHW");
        public static readonly Currency CLF = new Currency(@"CLF");
        public static readonly Currency CLP = new Currency(@"CLP");
        public static readonly Currency CNH = new Currency(@"CNH");
        public static readonly Currency CNY = new Currency(@"CNY");
        public static readonly Currency COP = new Currency(@"COP");
        public static readonly Currency COU = new Currency(@"COU");
        public static readonly Currency CRC = new Currency(@"CRC");
        public static readonly Currency CSD = new Currency(@"CSD");
        public static readonly Currency CUP = new Currency(@"CUP");
        public static readonly Currency CVE = new Currency(@"CVE");
        public static readonly Currency CYP = new Currency(@"CYP");
        public static readonly Currency CZK = new Currency(@"CZK");
        public static readonly Currency DJF = new Currency(@"DJF");
        public static readonly Currency DKK = new Currency(@"DKK");
        public static readonly Currency DOP = new Currency(@"DOP");
        public static readonly Currency DZD = new Currency(@"DZD");
        public static readonly Currency EEK = new Currency(@"EEK");
        public static readonly Currency EGP = new Currency(@"EGP");
        public static readonly Currency ERN = new Currency(@"ERN");
        public static readonly Currency ETB = new Currency(@"ETB");
        public static readonly Currency EUR = new Currency(@"EUR");
        public static readonly Currency FJD = new Currency(@"FJD");
        public static readonly Currency FKP = new Currency(@"FKP");
        public static readonly Currency GBP = new Currency(@"GBP");
        public static readonly Currency GEL = new Currency(@"GEL");
        public static readonly Currency GHC = new Currency(@"GHC");
        public static readonly Currency GHS = new Currency(@"GHS");
        public static readonly Currency GIP = new Currency(@"GIP");
        public static readonly Currency GMD = new Currency(@"GMD");
        public static readonly Currency GNF = new Currency(@"GNF");
        public static readonly Currency GTQ = new Currency(@"GTQ");
        public static readonly Currency GWP = new Currency(@"GWP");
        public static readonly Currency GYD = new Currency(@"GYD");
        public static readonly Currency HKD = new Currency(@"HKD");
        public static readonly Currency HNL = new Currency(@"HNL");
        public static readonly Currency HRK = new Currency(@"HRK");
        public static readonly Currency HTG = new Currency(@"HTG");
        public static readonly Currency HUF = new Currency(@"HUF");
        public static readonly Currency IDR = new Currency(@"IDR");
        public static readonly Currency ILS = new Currency(@"ILS");
        public static readonly Currency INR = new Currency(@"INR");
        public static readonly Currency IQD = new Currency(@"IQD");
        public static readonly Currency IRR = new Currency(@"IRR");
        public static readonly Currency ISK = new Currency(@"ISK");
        public static readonly Currency JMD = new Currency(@"JMD");
        public static readonly Currency JOD = new Currency(@"JOD");
        public static readonly Currency JPY = new Currency(@"JPY");
        public static readonly Currency KES = new Currency(@"KES");
        public static readonly Currency KGS = new Currency(@"KGS");
        public static readonly Currency KHR = new Currency(@"KHR");
        public static readonly Currency KMF = new Currency(@"KMF");
        public static readonly Currency KPW = new Currency(@"KPW");
        public static readonly Currency KRW = new Currency(@"KRW");
        public static readonly Currency KWD = new Currency(@"KWD");
        public static readonly Currency KYD = new Currency(@"KYD");
        public static readonly Currency KZT = new Currency(@"KZT");
        public static readonly Currency LAK = new Currency(@"LAK");
        public static readonly Currency LBP = new Currency(@"LBP");
        public static readonly Currency LKR = new Currency(@"LKR");
        public static readonly Currency LRD = new Currency(@"LRD");
        public static readonly Currency LSL = new Currency(@"LSL");
        public static readonly Currency LTL = new Currency(@"LTL");
        public static readonly Currency LVL = new Currency(@"LVL");
        public static readonly Currency LYD = new Currency(@"LYD");
        public static readonly Currency MAD = new Currency(@"MAD");
        public static readonly Currency MDL = new Currency(@"MDL");
        public static readonly Currency MGA = new Currency(@"MGA");
        public static readonly Currency MKD = new Currency(@"MKD");
        public static readonly Currency MMK = new Currency(@"MMK");
        public static readonly Currency MNT = new Currency(@"MNT");
        public static readonly Currency MOP = new Currency(@"MOP");
        public static readonly Currency MRO = new Currency(@"MRO");
        public static readonly Currency MTL = new Currency(@"MTL");
        public static readonly Currency MUR = new Currency(@"MUR");
        public static readonly Currency MVR = new Currency(@"MVR");
        public static readonly Currency MWK = new Currency(@"MWK");
        public static readonly Currency MXN = new Currency(@"MXN");
        public static readonly Currency MXV = new Currency(@"MXV");
        public static readonly Currency MYR = new Currency(@"MYR");
        public static readonly Currency MZN = new Currency(@"MZN");
        public static readonly Currency NAD = new Currency(@"NAD");
        public static readonly Currency NGN = new Currency(@"NGN");
        public static readonly Currency NIO = new Currency(@"NIO");
        public static readonly Currency NOK = new Currency(@"NOK");
        public static readonly Currency NPR = new Currency(@"NPR");
        public static readonly Currency NZD = new Currency(@"NZD");
        public static readonly Currency OMR = new Currency(@"OMR");
        public static readonly Currency PAB = new Currency(@"PAB");
        public static readonly Currency PEN = new Currency(@"PEN");
        public static readonly Currency PGK = new Currency(@"PGK");
        public static readonly Currency PHP = new Currency(@"PHP");
        public static readonly Currency PKR = new Currency(@"PKR");
        public static readonly Currency PLN = new Currency(@"PLN");
        public static readonly Currency PYG = new Currency(@"PYG");
        public static readonly Currency QAR = new Currency(@"QAR");
        public static readonly Currency ROL = new Currency(@"ROL");
        public static readonly Currency RON = new Currency(@"RON");
        public static readonly Currency RSD = new Currency(@"RSD");
        public static readonly Currency RUB = new Currency(@"RUB");
        public static readonly Currency RWF = new Currency(@"RWF");
        public static readonly Currency SAR = new Currency(@"SAR");
        public static readonly Currency SBD = new Currency(@"SBD");
        public static readonly Currency SCR = new Currency(@"SCR");
        public static readonly Currency SDD = new Currency(@"SDD");
        public static readonly Currency SDG = new Currency(@"SDG");
        public static readonly Currency SEK = new Currency(@"SEK");
        public static readonly Currency SGD = new Currency(@"SGD");
        public static readonly Currency SHP = new Currency(@"SHP");
        public static readonly Currency SIT = new Currency(@"SIT");
        public static readonly Currency SKK = new Currency(@"SKK");
        public static readonly Currency SLL = new Currency(@"SLL");
        public static readonly Currency SOS = new Currency(@"SOS");
        public static readonly Currency SRD = new Currency(@"SRD");
        public static readonly Currency STD = new Currency(@"STD");
        public static readonly Currency SVC = new Currency(@"SVC");
        public static readonly Currency SYP = new Currency(@"SYP");
        public static readonly Currency SZL = new Currency(@"SZL");
        public static readonly Currency THB = new Currency(@"THB");
        public static readonly Currency TJS = new Currency(@"TJS");
        public static readonly Currency TMM = new Currency(@"TMM");
        public static readonly Currency TND = new Currency(@"TND");
        public static readonly Currency TOP = new Currency(@"TOP");
        public static readonly Currency TRY = new Currency(@"TRY");
        public static readonly Currency TTD = new Currency(@"TTD");
        public static readonly Currency TWD = new Currency(@"TWD");
        public static readonly Currency TZS = new Currency(@"TZS");
        public static readonly Currency UAH = new Currency(@"UAH");
        public static readonly Currency UGX = new Currency(@"UGX");
        public static readonly Currency USD = new Currency(@"USD");
        public static readonly Currency USN = new Currency(@"USN");
        public static readonly Currency USS = new Currency(@"USS");
        public static readonly Currency UYI = new Currency(@"UYI");
        public static readonly Currency UYU = new Currency(@"UYU");
        public static readonly Currency UZS = new Currency(@"UZS");
        public static readonly Currency VEB = new Currency(@"VEB");
        public static readonly Currency VEF = new Currency(@"VEF");
        public static readonly Currency VND = new Currency(@"VND");
        public static readonly Currency VUV = new Currency(@"VUV");
        public static readonly Currency WST = new Currency(@"WST");
        public static readonly Currency XAF = new Currency(@"XAF");
        public static readonly Currency XCD = new Currency(@"XCD");
        public static readonly Currency XDR = new Currency(@"XDR");
        public static readonly Currency XOF = new Currency(@"XOF");
        public static readonly Currency XPF = new Currency(@"XPF");
        public static readonly Currency YER = new Currency(@"YER");
        public static readonly Currency ZAR = new Currency(@"ZAR");
        public static readonly Currency ZMK = new Currency(@"ZMK");
        public static readonly Currency ZWD = new Currency(@"ZWD");
        public static readonly Currency XVN = new Currency(@"XVN");
        public static readonly Currency FTS = new Currency(@"FTS");
        public static readonly Currency DJI = new Currency(@"DJI");
        public static readonly Currency SPX = new Currency(@"SPX");
        public static readonly Currency SPY = new Currency(@"SPY");
        public static readonly Currency NDX = new Currency(@"NDX");
        public static readonly Currency DAX = new Currency(@"DAX");
        public static readonly Currency DXY = new Currency(@"DXY");
        public static readonly Currency CAC = new Currency(@"CAC");
        public static readonly Currency STX = new Currency(@"STX");
        public static readonly Currency NIK = new Currency(@"NIK");
        public static readonly Currency AUS = new Currency(@"AUS");
        public static readonly Currency XAU = new Currency(@"XAU");
        public static readonly Currency XAG = new Currency(@"XAG");
        public static readonly Currency WTI = new Currency(@"WTI");
        public static readonly Currency BRE = new Currency(@"BRE");
        public static readonly Currency XPT = new Currency(@"XPT");
        public static readonly Currency XPD = new Currency(@"XPD");
        public static readonly Currency GAS = new Currency(@"GAS");
        public static readonly Currency YAU = new Currency(@"YAU");
        public static readonly Currency YAG = new Currency(@"YAG");
        public static readonly Currency GLD = new Currency(@"GLD");
        public static readonly Currency SLV = new Currency(@"SLV");
        public static readonly Currency NDY = new Currency(@"NDY");
        public static readonly Currency DJY = new Currency(@"DJY");
        public static readonly Currency HSI = new Currency(@"HSI");
        public static readonly Currency IBX = new Currency(@"IBX");
        public static readonly Currency XBT = new Currency(@"XBT");
        public static readonly Currency XET = new Currency(@"XET");




        private Currency(string name)
        {
            if (_instanceIdsToNames.Count > byte.MaxValue)
                throw new ArgumentOutOfRangeException(
                    string.Format(
                        "Attempt to create currency [{0}] that is already over the range of underlying byte storage", name));


            this._value =  (byte)_instanceIdsToNames.Count;
            if(_value > 0)
                _instanceNamesToIds.Add(name, this._value);
            _instanceIdsToNames.Add(name);
        }

        private Currency(byte value)
        {
            this._value = value;
        }

        public static explicit operator Currency(string name)
        {
            byte code;
            if (_instanceNamesToIds.TryGetValue(name, out code))
            {
                return new Currency(code);
            }
            else
            {
                throw new InvalidCastException("Unknown Currency value: " + name);
            }
        }

        public static explicit operator Currency(int value)
        {
            if (_instanceIdsToNames.Count-1 > value)
            {
                return new Currency((byte)(value + 1));
            }
            else
            {
                throw new InvalidCastException("Unknown Currency integer value: " + value);
            }
        }

        public static explicit operator String(Currency currency)
        {
            return _instanceIdsToNames[currency._value];
        }

        public static explicit operator int(Currency counterpart)
        {
            return counterpart._value - 1;
        }

        public static IEnumerable<string> Values
        {
            get { return _instanceNamesToIds.Keys; }
        }

        public static int ValuesCount
        {
            get { return _instanceIdsToNames.Count-1; }
        }

        public override string ToString()
        {
            return _instanceIdsToNames[this._value];
        }

        public static bool operator ==(Currency a, Currency b)
        {
            return a._value == b._value;
        }

        public static bool operator !=(Currency a, Currency b)
        {
            return a._value != b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Currency.NULL_Currency instead", true)]
        public static bool operator ==(Currency a, Currency? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Currency.NULL_Currency instead", true)]
        public static bool operator !=(Currency a, Currency? b)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Currency))
                return false;

            Currency curr = (Currency) obj;
            return curr._value == this._value;
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public abstract class TimeInterval
    {
        protected TimeInterval(TimeSpan start, TimeSpan end, TimeZoneCode timeZone)
        {
            Start = start;
            End = end;
            TimeZone = timeZone;

            CheckBounds();
        }

        public bool IsWithinBounds(DateTime utcDate)
        {
            //Be carefull - if end < start - we need to add one full interval to end - or easier - inverse the inclusiveness condition

            //we first need to localize - as if we'd first take base datetime and then localize it can be shifted
            DateTime localizedTestedDateTime = TradingHoursHelper.Instance.ConvertFromUtc(utcDate, TimeZone);
            DateTime localizedBaseDateTime = this.GetBaseDate(localizedTestedDateTime);

            DateTime startDateTime = localizedBaseDateTime + Start;
            DateTime endDateTime = localizedBaseDateTime + End;

            return AreBoundsSwitched
                ? !localizedTestedDateTime.Between(endDateTime, startDateTime, false)
                : localizedTestedDateTime.Between(startDateTime, endDateTime, true);
        }

        public DateTime AddSpan(DateTime startTimeLocal, TimeZoneCode timeZoneCode, TimeSpan span)
        {
            DateTime startTimeUtc = TradingHoursHelper.Instance.ConvertToUtc(startTimeLocal, timeZoneCode);
            DateTime endTimeUtc = this.AddSpan(startTimeUtc, span);
            return TradingHoursHelper.Instance.ConvertFromUtc(endTimeUtc, timeZoneCode);
        }

        public DateTime AddSpan(DateTime startTimeUtc, TimeSpan span)
        {
            DateTime endUtc = startTimeUtc + span;
            TimeSpan boundsCompensation = TimeSpan.Zero;

            TimeSpan singlePeriodNetDuration = FullIntervalPeriod - (End - Start).Abs();

            if (span > singlePeriodNetDuration)
            {
                endUtc += (End - Start).Abs().Times(span.WholeDivideBy(singlePeriodNetDuration));
                startTimeUtc += FullIntervalPeriod.Times(span.WholeDivideBy(singlePeriodNetDuration));
            }

            //Todo: IsWithinBounds
            DateTime firstStartOfExclusionUtc =
                TradingHoursHelper.Instance.ConvertToUtc(this.GetBaseDate(startTimeUtc).Add(TimeSpanEx.Min(Start, End)), TimeZone);
            DateTime firstEndOfExclusionUtc =
                TradingHoursHelper.Instance.ConvertToUtc(this.GetBaseDate(startTimeUtc).Add(TimeSpanEx.Max(Start, End)), TimeZone);

            bool boundsAdded = false;
            if (startTimeUtc.Between(firstStartOfExclusionUtc, firstEndOfExclusionUtc, false))
            {
                boundsCompensation += firstEndOfExclusionUtc - startTimeUtc;
                boundsAdded = true;
            }

            DateTime lastStartOfExclusionUtc = 
                TradingHoursHelper.Instance.ConvertToUtc(this.GetBaseDate(endUtc).Add(TimeSpanEx.Min(Start, End)), TimeZone);
            DateTime lastEndOfExclusionUtc =
                TradingHoursHelper.Instance.ConvertToUtc(this.GetBaseDate(endUtc).Add(TimeSpanEx.Max(Start, End)), TimeZone);


            if ((lastStartOfExclusionUtc != firstStartOfExclusionUtc || !boundsAdded) 
                && endUtc.Between(lastStartOfExclusionUtc, lastEndOfExclusionUtc, false))
            {
                boundsCompensation += (End - Start).Abs();
            }

            if (
                (startTimeUtc < firstStartOfExclusionUtc && endUtc > firstEndOfExclusionUtc)
                ||
                (startTimeUtc < lastStartOfExclusionUtc && endUtc > lastEndOfExclusionUtc)
            )
            {
                boundsCompensation += (End - Start).Abs();
            }

            return endUtc + boundsCompensation;
        }

        public TimeSpan DateTimesDistanceExcludingInterval(DateTime utcDate1, DateTime utcDate2)
        {
            TimeSpan pureDistance = (utcDate1 - utcDate2).Abs();
            if (pureDistance > FullIntervalPeriod)
            {
                DateTime start = DateTimeEx.Min(utcDate1, utcDate2);
                DateTime end = DateTimeEx.Max(utcDate1, utcDate2);

                //we calculate distance between rounded bounds - this will always cross same number of exclusion spans
                TimeSpan distanceOfRoundedBounds = (this.GetBaseDate(end) -
                                                    this.GetBaseDate(start).Add(FullIntervalPeriod));

                //we will add the reminders before and after rounded bounds
                return DateTimesDistanceExcludingInterval(start, this.GetBaseDate(start).Add(FullIntervalPeriod)) +
                       DateTimesDistanceExcludingInterval(this.GetBaseDate(end), end) +
                       //add the rounded bounds distance
                       distanceOfRoundedBounds -
                       //and subtract the number of exclusion spans crossed
                       (End - Start).Abs().Times(distanceOfRoundedBounds.WholeDivideBy(FullIntervalPeriod));
            }

            //we first need to localize - as if we'd first take base datetime and then localize it can be shifted
            DateTime localizedLowerDate = TradingHoursHelper.Instance.ConvertFromUtc(DateTimeEx.Min(utcDate1, utcDate2), TimeZone);
            DateTime localizedHigherDate = TradingHoursHelper.Instance.ConvertFromUtc(DateTimeEx.Max(utcDate1, utcDate2), TimeZone);

            return LocalizedDateTimesDistanceExcludingInterval(localizedLowerDate, localizedHigherDate);
        }

        private TimeSpan LocalizedDateTimesDistanceExcludingInterval(DateTime localizedLowerDate, DateTime localizedHigherDate)
        {
            //We need to base each other separately. As e.g. for over the DST changing weekend the Friday and Monday are in different offsets
            //On the other hand by that we can extend bounds unwontedly (e.g. 1am-2am hours day interval, can became 25hours interval if end bound is in second day)
            //So to solve this we count intervals separately if they cross border
            DateTime lowerDateBase = this.GetBaseDate(localizedLowerDate);
            DateTime higherDateBase = this.GetBaseDate(localizedHigherDate);

            if (lowerDateBase != higherDateBase && higherDateBase != localizedHigherDate)
            {
                return LocalizedDateTimesDistanceExcludingInterval(localizedLowerDate, higherDateBase)
                       +
                       LocalizedDateTimesDistanceExcludingInterval(higherDateBase, localizedHigherDate);
            }

            //those are the bounds of the exclusion interval
            DateTime localizedStart = lowerDateBase + TimeSpanEx.Min(Start, End);
            DateTime localizedEnd = lowerDateBase + TimeSpanEx.Max(Start, End);

            if (AreBoundsSwitched)
            {
                DateTime excludedIntervalStart = DateTimeEx.Max(localizedLowerDate, localizedStart);
                DateTime excludedIntervalEnd = DateTimeEx.Min(localizedHigherDate, localizedEnd);

                return TimeSpanEx.Max(TimeSpan.Zero, excludedIntervalEnd - excludedIntervalStart);
            }

            if (!DateTimeEx.HaveOverlap(localizedLowerDate, localizedHigherDate, localizedStart, localizedEnd))
            {
                //there is no overlap with exclusion period - whole interval is counted
                return localizedHigherDate - localizedLowerDate;
            }

            //The interval starts in exclusion interval - so we count only what starts at the end of exclusion interval
            if (localizedLowerDate.Between(localizedStart, localizedEnd))
            {
                return TimeSpanEx.Max(TimeSpan.Zero, localizedHigherDate - localizedEnd);
            }

            //The interval ends in exclusion interval - so we count only what ends at the start of exclusion interval
            if (localizedHigherDate.Between(localizedStart, localizedEnd))
            {
                return TimeSpanEx.Max(TimeSpan.Zero, localizedStart - localizedLowerDate);
            }

            //there must  be a full intersection over exclusion interval
            return localizedHigherDate - localizedLowerDate - (End - Start).Abs();
        }

        protected abstract void CheckBounds();
        protected abstract DateTime GetBaseDate(DateTime localizedDate);
        protected abstract TimeSpan FullIntervalPeriod { get; }

        public TimeSpan Start { get; }
        public TimeSpan End { get; }
        public TimeZoneCode TimeZone { get; }
        public bool AreBoundsSwitched => Start > End;
    }

    public class SingleDayTimeInterval : TimeInterval
    {
        public SingleDayTimeInterval(TimeSpan start, TimeSpan end, TimeZoneCode timeZone)
            : base(start, end, timeZone)
        { }

        protected override void CheckBounds()
        {
            if (!Start.IsTimeOfDay() || !End.IsTimeOfDay())
            {
                throw new Exception(string.Format(
                    "Start [{0}] or end [{1}] interval are not time of day (0 - 24 hours)", Start, End));
            }
        }

        protected override DateTime GetBaseDate(DateTime localizedDate)
        {
            return localizedDate.Date;
        }

        protected override TimeSpan FullIntervalPeriod => TimeSpan.FromDays(1);
    }

    /// <summary>
    /// The interval within week starting from Monday with 0.
    /// Monday 1PM is 00:13:00
    /// Sunday 7AM is 06:07:00
    /// </summary>
    public class SingleWeekTimeInterval : TimeInterval
    {
        public SingleWeekTimeInterval(TimeSpan start, TimeSpan end, TimeZoneCode timeZone)
            : base(start, end, timeZone)
        { }

        protected override void CheckBounds()
        {
            if (!Start.IsTimeOfWeek() || !End.IsTimeOfWeek())
            {
                throw new Exception(string.Format(
                    "Start [{0}] or end [{1}] interval are not time of week (0 - 7 days)", Start, End));
            }
        }

        protected override DateTime GetBaseDate(DateTime localizedDate)
        {
            int dayNumberFromMonday = ((int)localizedDate.DayOfWeek + 6) % 7;
            return new DateTime(localizedDate.Year, localizedDate.Month, localizedDate.Day).AddDays(-dayNumberFromMonday);
        }

        protected override TimeSpan FullIntervalPeriod => TimeSpan.FromDays(7);
    }

    public class NoGapInterval : TimeInterval
    {
        public NoGapInterval() : base(TimeSpan.Zero, TimeSpan.Zero, TimeZoneCode.UTC)
        { }

        protected override void CheckBounds()
        { }

        protected override DateTime GetBaseDate(DateTime localizedDate)
        {
            return DateTime.MinValue;
        }

        protected override TimeSpan FullIntervalPeriod { get {return TimeSpan.MaxValue;} }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Windows.Forms.VisualStyles;

namespace Kreslik.Integrator.Common
{
    public interface IGmailTriggerSettings
    {
        string UserName { get; }
        string Password { get; }
        string Url { get; }
        int Port { get; }
        string[] TriggerPhrases { get; }
        string[] TriggerSuppressPhrases { get; }
        string[] TriggerSuppressSenders { get; }
    }

    public class GmailTriggerSettings : IGmailTriggerSettings
    {
        public GmailTriggerSettings(string userName, string password, string url, int port, string[] triggerPhrases,
            string[] triggerSuppressPhrases, string[] triggerSuppressSenders)
        {
            UserName = userName;
            Password = password;
            Url = url;
            Port = port;
            TriggerPhrases = triggerPhrases;
            TriggerSuppressPhrases = triggerSuppressPhrases;
            TriggerSuppressSenders = triggerSuppressSenders;
        }

        public GmailTriggerSettings(CommonSettings.AutomailerSettings automailerSettings, string[] triggerPhrases,
            string[] triggerSuppressPhrases, string[] triggerSuppressSenders)
            : this(
                automailerSettings.SmtpUsername, automailerSettings.SmtpPassword, automailerSettings.ImapServer,
                automailerSettings.ImapPort, triggerPhrases, triggerSuppressPhrases, triggerSuppressSenders)
        { }

        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string Url { get; private set; }
        public int Port { get; private set; }
        public string[] TriggerPhrases { get; private set; }
        public string[] TriggerSuppressPhrases { get; private set; }
        public string[] TriggerSuppressSenders { get; private set; }
    }

    public interface IEmail
    {
        string Subject { get; }
        string Body { get; }
    }

    public class Email: IEmail
    {
        public Email(string subject, string body)
        {
            Subject = subject;
            Body = body;
        }

        public string Subject { get; private set; }
        public string Body { get; private set; }

        public override string ToString()
        {
            return string.Format("Subject: {0}{1}Body:{1}{2}", this.Subject, Environment.NewLine, this.Body);
        }
    }

    public class GmailTrigger : IDisposable
    {
        private readonly IGmailTriggerSettings _settings;
        private ImapEx _imap;

        private  long _nexMessageId = -1;

        private readonly TimeSpan _queryInterval;
        private readonly ILogger _logger;

        private const int BufferSize = 2048;
        private const int DefaultQueryIntervalInSeconds = 30;

        private static readonly TimeSpan[] _onErrorDelays = new TimeSpan[]
        {
            TimeSpan.FromSeconds(DefaultQueryIntervalInSeconds), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(30),
            TimeSpan.FromHours(1), TimeSpan.FromHours(4)
        };

        private int _onErrorDelayIdx = 0;
        private TimeSpan _currentWaitInterval;

        private readonly CancellationTokenSource _shutdownCancellationTokenSource = new CancellationTokenSource();
        private ManualResetEvent _processingDoneEvent = new ManualResetEvent(false);

        public GmailTrigger(IGmailTriggerSettings settings, ILogger logger)
            : this(settings, logger, TimeSpan.FromSeconds(DefaultQueryIntervalInSeconds)) { }

        public GmailTrigger(IGmailTriggerSettings settings, ILogger logger, TimeSpan queryInterval)
        {
            _settings = settings;
            _logger = logger;
            _queryInterval = queryInterval;
        }

        public void InitializeAsync()
        {
            TaskEx.QueueLongRunningWithErrorHandling(InitializeInternal);
        }

        private void InitializeInternal()
        {
            //if this is re-try - give a failing instance chance to finish error handling
            Thread.Sleep(TimeSpan.FromSeconds(1));
            int failedAttempts = 0;
            do
            {
                try
                {
                    _imap = new ImapEx(_logger);
                    _imap.Login(_settings.Url, Convert.ToUInt16(_settings.Port), _settings.UserName, _settings.Password);
                    _imap.SelectFolder("INBOX");

                    _nexMessageId = _imap.NextMessageId;

                    _imap.Disconnected += InitializeAsync;
                    TaskEx.QueueLongRunningWithErrorHandling(UpdateData);

                    _logger.Log(LogLevel.Info, "GmailTrigger successfully initialized");
                    return;
                }
                catch (Exception e)
                {
                    TimeSpan delay = _onErrorDelays[failedAttempts % _onErrorDelays.Length];
                    failedAttempts++;
                    _logger.LogException(LogLevel.Fatal, e,
                        "Unable to initialize communication with gmail. Errors in row: {0}, Will retry after: {1}",
                        failedAttempts, delay);
                    _shutdownCancellationTokenSource.Token.WaitHandle.WaitOne(_currentWaitInterval);
                }
            } while (!_shutdownCancellationTokenSource.IsCancellationRequested);

            _logger.Log(LogLevel.Info, "GmailTrigger finished attempts to logon after shutdown request");
            this._processingDoneEvent.Set();
        }
       
        private enum GetDataResult
        {
            NoData,
            SomeData,
            Error
        }

        private GetDataResult TryGetData()
        {
            string subject, sender, body;

            try
            {
                _currentWaitInterval = _queryInterval;
                if (!_imap.GetHeaderParts(_nexMessageId.ToString(), out subject, out sender))
                {
                    _onErrorDelayIdx = 0;
                    return GetDataResult.NoData;
                }

                // if there is no exact match in trigger phases continue with next email
                if (!_settings.TriggerPhrases.Any(phrase => subject.ContainsCaseInsensitive(phrase)) ||
                    _settings.TriggerSuppressSenders.Any(supressSender => sender.ContainsCaseInsensitive(supressSender)) ||
                    _settings.TriggerSuppressPhrases.Any(phrase => subject.ContainsCaseInsensitive(phrase)))
                {
                    _nexMessageId++;
                    return GetDataResult.SomeData;
                }
                body = _imap.GetBody(_nexMessageId);
            }
            catch (ImapDisconnectedException)
            {
                throw;
            }
            catch (Exception e)
            {
                _currentWaitInterval = _onErrorDelays[_onErrorDelayIdx % _onErrorDelays.Length];
                _logger.LogException(_onErrorDelayIdx == 0 ? LogLevel.Error : LogLevel.Fatal, e, "Unexpected exception during fetching mails from Gmail. Errors in row: {0}, Will retry after: {1}",
                    _onErrorDelayIdx, _currentWaitInterval);
                _onErrorDelayIdx++;
                return GetDataResult.Error;
            }

            IEmail email = new Email(subject, body);
            try
            {
                OnNewEmail(email);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Encountered exception when calling OnNewEmail event for email: {0}", email);
            }
            _nexMessageId++;
            _onErrorDelayIdx = 0;
            return GetDataResult.SomeData;
        }

        private void UpdateData()
        {
            try
            {
                while (!_shutdownCancellationTokenSource.IsCancellationRequested)
                {
                    GetDataResult getDataResult = TryUpdateData();
                    if (getDataResult != GetDataResult.SomeData)
                    {
                        _shutdownCancellationTokenSource.Token.WaitHandle.WaitOne(_currentWaitInterval);
                    }
                }
            }
            catch (ImapDisconnectedException)
            {
                this._logger.Log(LogLevel.Warn, "GmailTrigger finishing single processing loop for underlying connection as it disconnected - other connection will be spawned in parallel");
                return;
            }


            _logger.Log(LogLevel.Info, "GmailTrigger finished it's processing after shutdown request");
            this._processingDoneEvent.Set();
        }

        private GetDataResult TryUpdateData() { return IsSubscribed ? TryGetData() : GetDataResult.NoData; }

        public bool IsSubscribed { get { return this.NewEmail != null; } }


        public WaitHandle ProcessingDoneHandle { get { return this._processingDoneEvent; } }

        public void StopProcessingSchedules()
        {
            _logger.Log(LogLevel.Info, "GmailTrigger received request to shutdown");
            _shutdownCancellationTokenSource.Cancel();
        }

        private void OnNewEmail(IEmail email)
        {
            if (NewEmail != null) { NewEmail(email); }
        }

        public event Action<IEmail> NewEmail;

        public void Dispose()
        {
            this.NewEmail = null;
            if (_imap != null)
                _imap.Dispose();
        }
    }
}

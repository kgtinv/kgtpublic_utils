﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface IOrderBookInfo<TNode>
    {
        bool IsEmpty { get; }   
        TNode[] GetSortedClone();
    }

    public interface IOrderBookInputs<TNode>
    {
        int AddNew(TNode node);
        bool RemoveIdentical(TNode node);
        int ResortIdentical(TNode node);
        void Clear();
    }

    public delegate void BookPriceUpdate<TNode>(IBookTop<TNode> sender, TNode node, DateTime timeStamp);
    public delegate void BookMultiplePricesUpdate<TNode, in TIdentity>(IBookTop<TNode> sender, TIdentity pricesIdentity, DateTime timeStamp);

    public interface IBookTop<TNode>
    {
        event BookPriceUpdate<TNode> BookTopImproved;
        event BookPriceUpdate<TNode> BookTopDeterioratedInternal;
        event BookPriceUpdate<TNode> BookTopReplaced;
        TNode MaxNodeInternal { get; }
        TNode MaxNode { get; }
        DateTime MaxNodeUpdatedUtc { get; }
        TradingTargetType TradingTargetType { get; }
    }

    public interface IBookTopExtended<TNode, out TIdentity> : IBookTop<TNode>
    {
        event BookPriceUpdate<TNode> NewNodeArrived;
        event BookPriceUpdate<TNode> ExistingNodeInvalidated;
        event BookMultiplePricesUpdate<TNode, TIdentity> NodesRemoved;
    }

    public interface IOrderBookOutputs<TNode> : IBookTopExtended<TNode, Counterparty>
    {
        event Action<IEnumerable<TNode>> NodesPurgedAway;
        //Action<TNode> TopChanging { set; }
    }

    public interface IBookTopProvider<TNode> : IBookTopExtended<TNode, Counterparty>
    {
        IBookTopExtended<TNode, Counterparty> GetBidPriceBook(Symbol symbol);
        IBookTopExtended<TNode, Counterparty> GetAskPriceBook(Symbol symbol);
    }

    public interface IOrderBook<TNode> : IOrderBookInfo<TNode>, IOrderBookInputs<TNode>, IOrderBookOutputs<TNode>
    { }


    public class OrderBook<TNode> : IOrderBook<TNode> where TNode : IPooledObject
    {
        private TNode[] _heapArray;
        private TNode _currentTop;
        private int _maxSize;
        private int _currentSize;
        private IComparer<TNode> _rankComparer;
        private Func<TNode, TNode, int> _rankCompareFunc;
        private Func<TNode, TNode, int> _topCompareFunc;
        private Func<TNode, TNode, bool> _itemsEqualsFunc;
        private TNode _nullNode;
        private ILogger _logger;
        private SpinLock _spinLock = new SpinLock(false);

        public OrderBook(int maxHeapSize, IComparer<TNode> rankComparer, IComparer<TNode> topComparer,
                         IEqualityComparer<TNode> equalityComparer, TNode nullItem, TradingTargetType tradingTargetType, ILogger logger)
        {
            this._maxSize = maxHeapSize;
            this.ItemsToRemoveDuringOverflowPurging = maxHeapSize / 3;
            this._currentSize = 0;
            this._heapArray = new TNode[_maxSize];
            this._rankComparer = rankComparer;
            this._rankCompareFunc = rankComparer.Compare;
            this._topCompareFunc = topComparer.Compare;
            this._itemsEqualsFunc = equalityComparer.Equals;
            this._nullNode = nullItem;
            this._currentTop = nullItem;
            this.TradingTargetType = tradingTargetType;
            this._logger = logger;
        }

        public TradingTargetType TradingTargetType { get; private set; }

        public event BookPriceUpdate<TNode> BookTopImproved;
        public event BookPriceUpdate<TNode> BookTopReplaced;
        public event BookPriceUpdate<TNode> BookTopDeterioratedInternal;
        public event Action<IEnumerable<TNode>> NodesPurgedAway;

        public event BookPriceUpdate<TNode> NewNodeArrived
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        public event BookPriceUpdate<TNode> ExistingNodeInvalidated
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        public event BookMultiplePricesUpdate<TNode,  Counterparty> NodesRemoved
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        public int ItemsToRemoveDuringOverflowPurging { get; set; }

        public bool IsEmpty
        {
            get { return _currentSize == 0; }
        }

        public TNode MaxNodeInternal
        {
            get
            {
                bool lockTaken = false;
                _spinLock.Enter(ref lockTaken);

                var value = _currentTop;
                //Callers of MaxNodeInternal are releasing, so acquire here
                value.Acquire();

                if (lockTaken)
                {
                    _spinLock.Exit();
                }

                return value;
            }
        }

        public TNode MaxNode
        {
            get
            {
                var maxNode = _currentTop;
                if (Equals(maxNode, _nullNode)) return default(TNode);
                else return maxNode;
            }
        }

        public DateTime MaxNodeUpdatedUtc
        {
            get; private set; 
        }

        public int AddNew(TNode node)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);


            int newIdx = Insert(node);

            bool topImproved = false;
            if (newIdx == 0)
            {
                topImproved = Equals(_currentTop, _nullNode) || _topCompareFunc(_heapArray[0], _currentTop) > 0;
                _currentTop = node;
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
            }

            TNode[] purgedItems = null;
            //purge
            if (_currentSize == _maxSize)
            {
                _currentSize = _maxSize - ItemsToRemoveDuringOverflowPurging;
                purgedItems = new TNode[ItemsToRemoveDuringOverflowPurging];
                Array.Copy(_heapArray, _currentSize, purgedItems, 0, ItemsToRemoveDuringOverflowPurging);
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topImproved)
            {
                if (this.BookTopImproved != null)
                {
                    this.TryInvokeNodeAction(this.BookTopImproved, _currentTop, this.MaxNodeUpdatedUtc);
                }
            }

            if (purgedItems != null && this.NodesPurgedAway != null)
            {
                this.NodesPurgedAway(purgedItems);
            }

            return newIdx;
        }

        public bool RemoveIdentical(TNode node)
        {
            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to remove
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsEqualsFunc(_heapArray[idx], node))
                {
                    break;
                }
            }

            bool topDeclined = false;
            bool topReplaced = false;

            bool removed;
            removed = RemoveAt(idx);

            //We were removing the top and the new top (if present) is different
            if (removed && idx == 0)
            {
                if ((_currentSize == 0 || _topCompareFunc(_currentTop, _heapArray[0]) != 0))
                {
                    topDeclined = true;
                }
                else
                {
                    topReplaced = true;
                }

                _currentTop = _currentSize == 0 ? this._nullNode : _heapArray[0];
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topDeclined && this.BookTopDeterioratedInternal != null)
            {
                this.TryInvokeNodeAction(this.BookTopDeterioratedInternal, _currentTop, this.MaxNodeUpdatedUtc);
            }
            else if (topReplaced && this.BookTopReplaced != null)
            {
                this.TryInvokeNodeAction(this.BookTopReplaced, _currentTop, this.MaxNodeUpdatedUtc);
            }

            return removed;
        }

        public int ResortIdentical(TNode node)
        {
            int newIndex = -1;

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            //Find item to resort
            int idx = 0;
            for (; idx < _currentSize; idx++)
            {
                if (_itemsEqualsFunc(_heapArray[idx], node))
                {
                    break;
                }
            }

            //item not found
            if (idx >= _currentSize)
            {
                return idx;
            }

            newIndex = CascadeDown(idx);

            //did we 'downsort' the top, or just worsen it?
            bool topDeclined = false;
            bool topReplaced = false;
            bool topImproved = false;
            if (idx == 0)
            {
                int rankChange = _topCompareFunc(_currentTop, _heapArray[0]);
                if (rankChange > 0)
                {
                    topImproved = true;
                }
                else if(rankChange < 0)
                {
                    topDeclined = true;
                }
                else
                {
                    topReplaced = true;
                }

                _currentTop = _heapArray[0];
                this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
            }
            //did we 'upsort' something to top?
            else if (newIndex == 0 && idx != newIndex)
            {
                topImproved = _currentSize != 0 && _topCompareFunc(_heapArray[0], _currentTop) > 0;
            }

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            if (topDeclined && this.BookTopDeterioratedInternal != null)
            {
                this.TryInvokeNodeAction(this.BookTopDeterioratedInternal, _currentTop, this.MaxNodeUpdatedUtc);
            }
            else if (topReplaced && this.BookTopReplaced != null)
            {
                this.TryInvokeNodeAction(this.BookTopReplaced, _currentTop, this.MaxNodeUpdatedUtc);
            }
            else if (topImproved && this.BookTopImproved != null)
            {
                this.TryInvokeNodeAction(this.BookTopImproved, _currentTop, this.MaxNodeUpdatedUtc);
            }

            return newIndex;
        }

        public void Clear()
        {
            this._currentSize = 0;
            this._currentTop = _nullNode;
            this.MaxNodeUpdatedUtc = HighResolutionDateTime.UtcNow;
            this.TryInvokeNodeAction(this.BookTopDeterioratedInternal, _currentTop, this.MaxNodeUpdatedUtc);
        }

        public TNode[] GetSortedClone()
        {
            TNode[] heapArrayCopy = new TNode[_currentSize];

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);

            Array.Copy(_heapArray, heapArrayCopy, _currentSize);

            if (lockTaken)
            {
                _spinLock.Exit();
            }

            Array.Sort(heapArrayCopy, _rankComparer);
            //We want the result in descending order
            Array.Reverse(heapArrayCopy);

            return heapArrayCopy;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            bool lockTaken = false;
            _spinLock.Enter(ref lockTaken);
            TNode[] heapArrayCopy = new TNode[_currentSize];
            Array.Copy(_heapArray, heapArrayCopy, _currentSize);
            if (lockTaken)
            {
                _spinLock.Exit();
            }

            sb.AppendLine();
            sb.Append("Elements of the Heap Array are : ");
            for (int m = 0; m < _currentSize; m++)
                if (heapArrayCopy[m] != null)
                    sb.Append(heapArrayCopy[m].ToString() + " ");
                else
                    sb.Append("-- ");
            sb.AppendLine();
            int emptyLeaf = 32;
            int itemsPerRow = 1;
            int column = 0;
            int j = 0;
            sb.AppendLine("..............................................................");
            while (_currentSize > 0)
            {
                if (column == 0)
                    for (int k = 0; k < emptyLeaf; k++)
                        sb.Append(' ');
                sb.Append(heapArrayCopy[j].ToString());

                if (++j == _currentSize)
                    break;
                if (++column == itemsPerRow)
                {
                    emptyLeaf /= 2;
                    itemsPerRow *= 2;
                    column = 0;
                    sb.AppendLine();
                }
                else
                    for (int k = 0; k < emptyLeaf * 2 - 2; k++)
                        sb.Append(' ');
            }
            sb.AppendLine();
            sb.AppendLine("..............................................................");

            return sb.ToString();
        }

        private void TryInvokeNodeAction(BookPriceUpdate<TNode> nodeAction, TNode nodeValue, DateTime timeStamp)
        {
            if (nodeAction != null)
            {
                try
                {
                    nodeAction(this, nodeValue, timeStamp);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e,
                                              "Experienced unhandled exception during invocation of {0} (order book) with value {1}", nodeAction.Method.Name, nodeValue);
                }
            }
        }

        private bool RemoveAt(int index)
        {
            bool success = false;

            if (index >= 0 && index < _currentSize)
            {
                _heapArray[index] = _heapArray[--_currentSize];
                int parent = (index - 1) / 2;
                if (_rankCompareFunc(_heapArray[index], _heapArray[parent]) > 0)
                    CascadeUp(index);
                else
                    CascadeDown(index);

                success = true;
            }

            return success;
        }

        private int Insert(TNode value)
        {
            int resultIndex = -1;

            if (_currentSize < _maxSize)
            {
                _heapArray[_currentSize] = value;
                resultIndex = CascadeUp(_currentSize++);
            }

            return resultIndex;
        }

        private int UpdateAt(int index, TNode newValue)
        {
            int resultIndex = -1;

            if (index >= 0 && index < _currentSize)
            {
                TNode oldValue = _heapArray[index];
                _heapArray[index] = newValue;

                if (_rankCompareFunc(oldValue, newValue) < 0)
                    resultIndex = CascadeUp(index);
                else
                    resultIndex = CascadeDown(index);
            }

            return resultIndex;
        }

        private int CascadeUp(int index)
        {
            int parent = (index - 1) / 2;
            TNode bottom = _heapArray[index];
            while (index > 0 && _rankCompareFunc(_heapArray[parent], bottom) < 0)
            {
                _heapArray[index] = _heapArray[parent];
                index = parent;
                parent = (parent - 1) / 2;
            }
            _heapArray[index] = bottom;
            return index;
        }

        private int CascadeDown(int index)
        {
            int largerChild;
            TNode top = _heapArray[index];
            while (index < _currentSize / 2)
            {
                int leftChild = 2 * index + 1;
                int rightChild = leftChild + 1;
                if (rightChild < _currentSize && _rankCompareFunc(_heapArray[leftChild], _heapArray[rightChild]) < 0)
                    largerChild = rightChild;
                else
                    largerChild = leftChild;
                if (_rankCompareFunc(top, _heapArray[largerChild]) >= 0)
                    break;
                _heapArray[index] = _heapArray[largerChild];
                index = largerChild;
            }
            _heapArray[index] = top;
            return index;
        }
    }
}

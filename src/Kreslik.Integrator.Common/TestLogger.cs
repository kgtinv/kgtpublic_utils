﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class TestLogger : ILogger
    {
        private StringBuilder _logContent = new StringBuilder();
        private Action<string> _assertionFailAction;
        private LogLevel _logLevelToFail;

        public static TestLogger CreateTestLoggerThrowingOnFail()
        {
            return new TestLogger(errorLogEntry => { throw new Exception(errorLogEntry); });
        }

        public TestLogger(Action<string> assertionFailAction, LogLevel logLevelToFail)
        {
            this._assertionFailAction = assertionFailAction;
            this._logLevelToFail = logLevelToFail;
        }

        public TestLogger(Action<string> assertionFailAction)
            :this(assertionFailAction, LogLevel.Warn)
        { }

        public bool FailTestOnUnexpectedLogs { get; set; }

        private readonly List<string> _allowedFailureLogMessagePatterns = new List<string>();

        public void AddAllowedFailureLogPortion(string logMessagePattern)
        {
            if (!string.IsNullOrEmpty(logMessagePattern))
            {
                _allowedFailureLogMessagePatterns.Add(logMessagePattern);
            }
        }

        public string Content
        {
            get { return _logContent.ToString(); }
        }

        private void CheckLevel(LogLevel level, string message)
        {
            if (FailTestOnUnexpectedLogs && level >= _logLevelToFail)
            {
                if (!_allowedFailureLogMessagePatterns.Any(message.Contains))
                {
                    _assertionFailAction("Warning or higher level message logged. Log content: " + this.Content);
                }
            }
        }

        public void Log(LogLevel level, string message)
        {
            _logContent.Append(level + " " + message);
            _logContent.AppendLine();

            CheckLevel(level, message);
        }

        public void Log(LogLevel level, string messageFmt, params object[] args)
        {
            _logContent.Append(level + " ");
            _logContent.AppendFormat(messageFmt, args);
            _logContent.AppendLine();

            CheckLevel(level, string.Format(messageFmt, args));
        }

        public void Log(LogLevel level, IFormatProvider formatProvider, string messageFmt, params object[] args)
        {
            _logContent.Append(level + " ");
            _logContent.AppendFormat(messageFmt, args);
            _logContent.AppendLine();

            CheckLevel(level, string.Format(messageFmt, args));
        }

        public void LogException(LogLevel level, string message, Exception e)
        {
            _logContent.Append(level + " " + message + " " + e);
            _logContent.AppendLine();

            CheckLevel(level, message);
        }

        public void LogException(Exception e)
        {
            _logContent.Append(LogLevel.Error + " " + e);
            _logContent.AppendLine();

            CheckLevel(LogLevel.Error, string.Empty);
        }

        public bool IsEnabled(LogLevel level)
        {
            return true;
        }


        public void LogException(LogLevel level, Exception e, string messageFmt, params object[] args)
        {
            _logContent.Append(level + " ");
            _logContent.AppendFormat(messageFmt, args);
            _logContent.Append(" ");
            _logContent.Append(e);
            _logContent.AppendLine();

            CheckLevel(level, string.Format(messageFmt, args));
        }

        public bool IsLowDiskSpaceConstraintLoggingOn
        {
            get { return false; }
        }


        public event Action<bool> IsLowDiskSpaceChanged;

        public void LogWithDt(LogLevel level, DateTime correctedTime, string message) { throw new NotImplementedException(); }

        public void Log(LogLevel level, IPooledBufferSegment pooledBuffer) { throw new NotImplementedException(); }

        public void Log(LogLevel level, string message, IPooledBufferSegment pooledBuffer) { throw new NotImplementedException(); }
        public IReadonlyChangeTrackingType<bool> IsUltraLowDiskSpace { get; } = new ChangeTrackingType<bool>(false);
    }
}

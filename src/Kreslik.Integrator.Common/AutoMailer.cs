﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public delegate void NewImportantInfoHandler(string subject, string body, int entriesCount);

    public interface IImportantInfoProvider
    {
        event NewImportantInfoHandler NewImportantInfo;
    }

    public class BufferedEmailsSender : IImportantInfoProvider
    {
        private readonly IEnumerable<string> _toAddresses;
        private readonly IEnumerable<string> _ccAddresses;
        private readonly string _subjectSuffix;
        private List<TimeSpan> _minDelaysBetweenEmails;
        private int _currentDelayIdx = 0;
        private TimeSpan _maxMinDelayBetweenEmails;
        private List<string> _entries = new List<string>();
        private DateTime _lastEmailSent = DateTime.MinValue;
        public event NewImportantInfoHandler NewImportantInfo;

        public BufferedEmailsSender(IEnumerable<string> toAddresses, IEnumerable<string> ccAddresses,
                                    string subjectSuffix, List<TimeSpan> minDelaysBetweenEmails)
        {
            this._toAddresses = toAddresses;
            this._ccAddresses = ccAddresses;
            this._subjectSuffix = subjectSuffix;
            if (minDelaysBetweenEmails == null || minDelaysBetweenEmails.Count == 0)
            {
                minDelaysBetweenEmails = new List<TimeSpan>(){TimeSpan.FromSeconds(2), TimeSpan.FromMinutes(5)};
            }
            this._minDelaysBetweenEmails = minDelaysBetweenEmails;
            _maxMinDelayBetweenEmails = _minDelaysBetweenEmails[_minDelaysBetweenEmails.Count - 1];
        }

        public void AddEntry(string entry)
        {
            lock (_entries)
            {
                _entries.Add(entry);
                if (_entries.Count == 1)
                {
                    //remaining time to the end of current waiting slot
                    TimeSpan delay = _lastEmailSent + _minDelaysBetweenEmails[_currentDelayIdx] - DateTime.UtcNow;

                    //if we already waited longer 
                    if (delay < TimeSpan.Zero)
                    {
                        //than check if we wainted longer than longest waiting interval and reset waiting if yes
                        if ((_lastEmailSent + _maxMinDelayBetweenEmails - DateTime.UtcNow) <
                            TimeSpan.Zero)
                        {
                            _currentDelayIdx = 0;
                        }

                        //make sure to allways reset delay to be greater than zero!!
                        delay = _minDelaysBetweenEmails[0];
                    }
                    //otherwise increase waitingbuffering period
                    else
                    {
                        _currentDelayIdx = _currentDelayIdx == _minDelaysBetweenEmails.Count - 1
                                               ? _currentDelayIdx
                                               : _currentDelayIdx + 1;
                    }

                    TaskEx.ExecAfterDelayWithErrorHandling(delay, () =>
                        {
                            string subject;
                            string body;
                            int entriesCount;

                            lock (_entries)
                            {
                                entriesCount = _entries.Count;
                                subject = string.Format("{0} new ", entriesCount) +
                                          _subjectSuffix +
                                          (entriesCount == 1 ? string.Empty : "s") + " " +
                                          SettingsInitializator.Instance.InstanceDescription;
                                body = string.Join(Environment.NewLine, _entries);

                                _entries.Clear();
                                _lastEmailSent = DateTime.UtcNow;
                            }

                            AutoMailer.TrySendMailAsync(subject, body, false, _toAddresses, _ccAddresses);

                            if (this.NewImportantInfo != null)
                                this.NewImportantInfo(subject, body, entriesCount);
                        });
                }
            }
        }
    }

    public class AutoMailer
    {
        public static AutoMailer _instance = new AutoMailer(
            CommonSettings.CommonBehavior.Automailer.SmtpServer,
            CommonSettings.CommonBehavior.Automailer.SmtpPort,
            CommonSettings.CommonBehavior.Automailer.BypassCertificationValidation,
            CommonSettings.CommonBehavior.Automailer.SmtpUsername,
            CommonSettings.CommonBehavior.Automailer.SmtpPassword,
            CommonSettings.CommonBehavior.Automailer.DirectoryForLocalEmails,
            CommonSettings.CommonBehavior.Automailer.SendEmailsOnlyToLocalFolder,
            LogFactory.Instance.GetLogger(null));

        public static void TrySendMailAsync(string subject, string body, bool isBodyHtml, IEnumerable<string> toAddresses,
                                         IEnumerable<string> ccAddresses)
        {
            _instance.TrySendEmailAsync(subject, body, isBodyHtml, toAddresses, ccAddresses, false);
        }

        public static void TrySendMailAsync(string subject, string body, bool isBodyHtml, IEnumerable<string> toAddresses,
                                         IEnumerable<string> ccAddresses, bool forceSendingByEmail)
        {
            _instance.TrySendEmailAsync(subject, body, isBodyHtml, toAddresses, ccAddresses, forceSendingByEmail);
        }

        public static void AbortAllInstanceEmailsRetires()
        {
            _instance.AbortAllEmailsRetires();
        }

        private SmtpClient _remoteSmtpClient;
        private SmtpClient _localFolderSmtpClient;
        private bool _bypassCertValidation;
        private string _fromAddress;
        private ILogger _logger;
        private const int RETRY_COUNT = 4;
        private readonly TimeSpan _retryInterval = TimeSpan.FromMinutes(1);
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private bool _sendEmailsOnlyToLocalFolder;
        
        private AutoMailer(string smtpServer, int smtpPort, bool bypassCertValidation, string smtpUsername,
                          string password, string directoryForLocalEmails, bool sendEmailsOnlyToLocalFolder,
                          ILogger logger)
        {
            _fromAddress = smtpUsername;
            _sendEmailsOnlyToLocalFolder = sendEmailsOnlyToLocalFolder;


            _remoteSmtpClient = new SmtpClient(smtpServer, smtpPort);
            _remoteSmtpClient.UseDefaultCredentials = false;
            System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(smtpUsername,
                                                                                                    password);
            _remoteSmtpClient.Credentials = basicAuthenticationInfo;
            _remoteSmtpClient.EnableSsl = true;

            _bypassCertValidation = bypassCertValidation;


            _localFolderSmtpClient = new SmtpClient(smtpServer, smtpPort);
            _localFolderSmtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;

            if (!Path.IsPathRooted(directoryForLocalEmails))
            {
                string localDir =
                    System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                directoryForLocalEmails = Path.Combine(localDir, directoryForLocalEmails);
            }

            if (!Directory.Exists(directoryForLocalEmails))
            {
                try
                {
                    Directory.CreateDirectory(directoryForLocalEmails);
                }
                catch (Exception e)
                {
                    logger.LogException(LogLevel.Fatal, e, "Couldn't create directory [{0}] for local emails",
                                        directoryForLocalEmails);
                }
            }

            _localFolderSmtpClient.PickupDirectoryLocation = directoryForLocalEmails;

            _logger = logger;
        }

        public void AbortAllEmailsRetires()
        {
            this._cancellationTokenSource.Cancel();
        }

        public void TrySendEmailAsync(string subject, string body, bool isBodyHtml, IEnumerable<string> toAddresses,
                                 IEnumerable<string> ccAddresses, bool forceSendingByEmail)
        {
            //nowhere to send
            if ((toAddresses == null || !toAddresses.Any()) && (ccAddresses == null || !ccAddresses.Any()))
            {
                return;
            }

            Task.Factory.StartNew(() =>
                {
                    int attempt;
                    for (attempt = 0; attempt < RETRY_COUNT && !_cancellationTokenSource.IsCancellationRequested; attempt++)
                    {
                        if (TrySendEmail(subject, body, isBodyHtml, toAddresses, ccAddresses, _sendEmailsOnlyToLocalFolder && !forceSendingByEmail))
                            break;
                        _cancellationTokenSource.Token.WaitHandle.WaitOne(_retryInterval);
                    }

                    if (attempt == RETRY_COUNT || (_cancellationTokenSource.IsCancellationRequested && attempt > 0))
                    {
                        this._logger.Log(LogLevel.Error,
                                         "Couldn't sent email for {0} attempts, so sending it to folder {1}. Email:{2}{3}",
                                         attempt, _localFolderSmtpClient.PickupDirectoryLocation, Environment.NewLine,
                                         body);
                        TrySendEmail(subject, body, isBodyHtml, toAddresses, ccAddresses, true);
                    }
                });

            //return attempt >= RETRY_COUNT;
        }

        private bool TrySendEmail(string subject, string body, bool isBodyHtml, IEnumerable<string> toAddresses,
                                         IEnumerable<string> ccAddresses, bool localOnly)
        {
            try
            {
                SmtpClient smtpClient = localOnly ? _localFolderSmtpClient : _remoteSmtpClient;



                MailAddress from = new MailAddress(_fromAddress);
                MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = from;
                if (toAddresses != null)
                {
                    foreach (string address in toAddresses.Where(address => !string.IsNullOrEmpty(address)))
                    {
                        mail.To.Add(address);
                    }
                }
                if (ccAddresses != null)
                {
                    foreach (string ccAddress in ccAddresses.Where(address => !string.IsNullOrEmpty(address)))
                    {
                        mail.CC.Add(ccAddress);
                    }
                }

                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.ASCII;
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                // text or html
                mail.IsBodyHtml = isBodyHtml;

                if (_bypassCertValidation && !localOnly)
                {
                    var serverCertificateValidationCallback = ServicePointManager.ServerCertificateValidationCallback;
                    ServicePointManager.ServerCertificateValidationCallback =
                        (s, certificate, chain, sslPolicyErrors) => true;
                    smtpClient.Send(mail);
                    ServicePointManager.ServerCertificateValidationCallback = serverCertificateValidationCallback;
                }
                else
                {
                    smtpClient.Send(mail);
                }
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Warn, "Exception during sending an email", e);
                return false;
            }

            return true;
        }
    }
}

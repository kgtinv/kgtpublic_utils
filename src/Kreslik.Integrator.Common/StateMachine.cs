﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    [Serializable]
    public class StateMachineException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public StateMachineException()
        {
        }

        public StateMachineException(string message)
            : base(message)
        {
        }

        public StateMachineException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected StateMachineException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }

    public interface IDestinationStateSyntax<TState, TEvent>
    {
        ICurrentStateSyntax<TState, TEvent> Goto(TState destinationState);
    }

    public interface ICurrentStateSyntax<TState, TEvent>
    {
        ICurrentStateSyntax<TState, TEvent> ExecuteOnEntry(Action action);
        ICurrentStateSyntax<TState, TEvent> ExecuteOnExit(Action action);
        IDestinationStateSyntax<TState, TEvent> On(TEvent triggerEvent);
        ICurrentStateSyntax<TState, TEvent> In(TState state);
    }

    public class StateMachine<TState, TEvent>
        where TState : IComparable
        where TEvent : IComparable
    {

        public ICurrentStateSyntax<TState, TEvent> In(TState state)
        {
            return new StateMachineBuilder(this._stateMachineConfigurationBag, state);
        }

        private class StateMachineBuilder : ICurrentStateSyntax<TState, TEvent>, IDestinationStateSyntax<TState, TEvent>
        {
            public StateMachineBuilder(StateMachineConfigurationBag stateMachineConfigurationBag, TState currentState)
            {
                this._stateMachineConfigurationBag = stateMachineConfigurationBag;
                this._currentState = currentState;
            }

            public ICurrentStateSyntax<TState, TEvent> ExecuteOnEntry(Action action)
            {
                if (this._stateMachineConfigurationBag.EntryActions.ContainsKey(this._currentState))
                {
                    this._stateMachineConfigurationBag.EntryActions[this._currentState] += action;
                }
                else
                {
                    this._stateMachineConfigurationBag.EntryActions[this._currentState] = action;
                }

                return this;
            }

            public ICurrentStateSyntax<TState, TEvent> ExecuteOnExit(Action action)
            {
                if (this._stateMachineConfigurationBag.ExitActions.ContainsKey(this._currentState))
                {
                    this._stateMachineConfigurationBag.ExitActions[this._currentState] += action;
                }
                else
                {
                    this._stateMachineConfigurationBag.ExitActions[this._currentState] = action;
                }

                return this;
            }

            public IDestinationStateSyntax<TState, TEvent> On(TEvent triggerEvent)
            {
                this._triggerEvent = triggerEvent;
                return this;
            }

            public ICurrentStateSyntax<TState, TEvent> Goto(TState destinationState)
            {
                this._stateMachineConfigurationBag.TransitionsMap.Add(
                    new Transition() { StartState = this._currentState, TransitionEvent = this._triggerEvent },
                    destinationState);
                this._triggerEvent = default(TEvent);

                return this;
            }

            public ICurrentStateSyntax<TState, TEvent> In(TState state)
            {
                this._currentState = state;
                return this;
            }

            private readonly StateMachineConfigurationBag _stateMachineConfigurationBag;
            private TState _currentState;
            private TEvent _triggerEvent;
        }

        private class Transition
        {
            private bool Equals(Transition other)
            {
                return StartState.Equals(other.StartState) && TransitionEvent.Equals(other.TransitionEvent);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (StartState.GetHashCode() * 397) ^ (int)TransitionEvent.GetHashCode();
                }
            }

            public static bool operator ==(Transition left, Transition right)
            {
                return Equals(left, right);
            }

            public static bool operator !=(Transition left, Transition right)
            {
                return !Equals(left, right);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Transition)obj);
            }

            public TState StartState { get; set; }
            public TEvent TransitionEvent { get; set; }
        }

        private class StateMachineConfigurationBag
        {
            public StateMachineConfigurationBag()
            {
                this.TransitionsMap = new Dictionary<Transition, TState>();
                this.ExitActions = new Dictionary<TState, Action>();
                this.EntryActions = new Dictionary<TState, Action>();
            }

            public Dictionary<Transition, TState> TransitionsMap { get; private set; }
            public Dictionary<TState, Action> ExitActions { get; private set; }
            public Dictionary<TState, Action> EntryActions { get; private set; }
        }

        private TState _currentState;

        public TState CurrentState
        {
            get { return this._currentState; }
            private set
            {
                if (!this._currentState.Equals(value))
                {
                    this._currentState = value;
                    if (this.StateChanging != null)
                        this.StateChanging(value);
                }
            }
        }

        private bool _throwOnError;
        private StateMachineConfigurationBag _stateMachineConfigurationBag = new StateMachineConfigurationBag();

        public StateMachine(ILogger logger) :
            this(true, logger)
        { }

        public StateMachine(bool throwOnError, ILogger logger)
        {
            this._throwOnError = throwOnError;
            this._logger = logger;
        }

        public event Action<TState> StateChanging;

        private ILogger _logger;

        //public event Action<string> OnError;
        //public event Action<string> OnInformation;

        public void Initialize(TState startState)
        {
            this.CurrentState = startState;
        }

        public bool FireEvent(TEvent triggerEvent)
        {
            _logger.Log(LogLevel.Info, "Attempting to fire event: [{0}]", triggerEvent);

            Transition transitionToken = new Transition() { StartState = CurrentState, TransitionEvent = triggerEvent };
            TState destinationState;
            if (_stateMachineConfigurationBag.TransitionsMap.TryGetValue(transitionToken, out destinationState))
            {
                _logger.Log(LogLevel.Info, "Moving to state: [{0}]", destinationState);

                Action transitionAction;
                if (_stateMachineConfigurationBag.ExitActions.TryGetValue(CurrentState, out transitionAction) &&
                    transitionAction != null)
                {
                    transitionAction();
                }

                CurrentState = destinationState;

                if (_stateMachineConfigurationBag.EntryActions.TryGetValue(CurrentState, out transitionAction) &&
                    transitionAction != null)
                {
                    try
                    {
                        transitionAction();
                    }
                    catch (Exception e)
                    {
                        this._logger.LogException(LogLevel.Fatal, "Unexpected exception during invoking state machine transition action", e);
                        throw;
                    }
                    
                }

                return true;
            }
            else
            {
                _logger.Log(LogLevel.Fatal, "Attempted to perform nonexisting transition from [{0}] with event [{1}].", CurrentState, triggerEvent);
                if (this._throwOnError)
                {
                    throw new StateMachineException(string.Format("Attempted to perform nonexisting transition from [{0}] with event [{1}].", CurrentState, triggerEvent));
                }

                return false;
            }
        }

        public void LogForbiddenAction(string errorMessage)
        {
            _logger.Log(LogLevel.Fatal, "Action in current state [{0}] is forbidden. {1}{2}", CurrentState, Environment.NewLine, errorMessage);

            if (_throwOnError)
            {
                throw new StateMachineException(string.Format("Action in current state [{0}] is forbidden. {1}{2}", CurrentState, Environment.NewLine, errorMessage));
            }
        }
    }
}

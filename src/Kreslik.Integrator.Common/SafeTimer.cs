﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    //public abstract class SafeTimerBase : IDisposable
    //{
    //    //this is due due to ref parameter to Cancel method - it cannot get overloads (due to ref),
    //    // so we need to store exact type
    //    private IAccurateTimer _timerInternal;
    //    private IAccurateTimerInternal _timer { get { return _timerInternal as IAccurateTimerInternal; }}
    //    private bool _rescheduleAfterCallbackDone = false;
    //    private TimeSpan _reschedulingPeriod;

    //    public TimerTaskFlagUtils.WorkItemPriority TaskExecutionPriority
    //    {
    //        get { return TimerTaskFlagUtils.GetWorkItemPriority(this._timer.MultiFlag); }
    //        set { this._timer.MultiFlag = this._timer.MultiFlag.SetWorkItemPriority(value); }
    //    }

    //    public TimerTaskFlagUtils.TimerPrecision RequiredTimerPrecision
    //    {
    //        get { return TimerTaskFlagUtils.GetTimerPrecision(this._timer.MultiFlag); }
    //        set { this._timer.MultiFlag = this._timer.MultiFlag.SetTimerPrecision(value); }
    //    }
    //    public bool IsUltraQuickCallback
    //    {
    //        get { return TimerTaskFlagUtils.IsUltraFastTask(this._timer.MultiFlag); }
    //        set { this._timer.MultiFlag = this._timer.MultiFlag.SetIsUltraFastTask(value); }
    //    }

    //    protected SafeTimerBase(TimeSpan dueTime, TimeSpan period, bool rescheduleAfterCallbackDone)
    //    {
    //        this._rescheduleAfterCallbackDone = rescheduleAfterCallbackDone;
    //        this._reschedulingPeriod = period;
    //        this._timerInternal = HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(TimerCallbackInternal, dueTime,
    //            TimerTaskFlagUtils.WorkItemPriority.Normal, TimerTaskFlagUtils.TimerPrecision.Highest, true, false);
    //    }

    //    protected SafeTimerBase(TimeSpan period, bool rescheduleAfterCallbackDone)
    //    {
    //        this._rescheduleAfterCallbackDone = rescheduleAfterCallbackDone;
    //        this._reschedulingPeriod = period;
    //        this._timerInternal = HighResolutionDateTime.HighResolutionTimerManager.CreateUnscheduledTimer(
    //            TimerCallbackInternal, TimerTaskFlagUtils.WorkItemPriority.Normal,
    //            TimerTaskFlagUtils.TimerPrecision.Highest, false);
    //    }

    //    public void ExecuteSynchronouslyWithReschedulingIfRequested()
    //    {
    //        HighResolutionDateTime.HighResolutionTimerManager.RescheduleTimer(this._timer, _reschedulingPeriod, false);

    //        try
    //        {
    //            ExecuteCallback();
    //        }
    //        catch (Exception e)
    //        {
    //            //this is situation that should happen very rarely - so we can afford creation of new logger for that
    //            // (however both calls actually returns a singleton instance)
    //            LogFactory.Instance.GetLogger(null)
    //                            .LogException(LogLevel.Fatal, e, "Unexpected exception when invoking timer callback");
    //        }
    //    }

    //    private void TimerCallbackInternal()
    //    {
    //        if (InterlockedEx.Read(ref _disposed) != 0)
    //            return;

    //        if (!_rescheduleAfterCallbackDone)
    //        {
    //            HighResolutionDateTime.HighResolutionTimerManager.RescheduleTimer(this._timer, _reschedulingPeriod, true);
    //        }

    //        try
    //        {
    //            ExecuteCallback();
    //        }
    //        catch (Exception e)
    //        {
    //            //this is situation that should happen very rarely - so we can afford creation of new logger for that
    //            // (however both calls actually returns a singleton instance)
    //            LogFactory.Instance.GetLogger(null)
    //                            .LogException(LogLevel.Fatal, e, "Unexpected exception when invoking timer callback");
    //        }

    //        if (_rescheduleAfterCallbackDone)
    //        {
    //            if (InterlockedEx.Read(ref _disposed) != 0)
    //                return;
    //            HighResolutionDateTime.HighResolutionTimerManager.RescheduleTimer(this._timer, _reschedulingPeriod, true);
    //        }
    //    }

    //    protected abstract void ExecuteCallback();
    //    protected abstract void OnDisposing();

    //    public void Change(TimeSpan dueTime, TimeSpan period)
    //    {
    //        _reschedulingPeriod = period;
    //        HighResolutionDateTime.HighResolutionTimerManager.RescheduleTimer(this._timer, dueTime, false);
    //    }

    //    private int _disposed = 0;
    //    public void Dispose()
    //    {
    //        Interlocked.Increment(ref _disposed);

    //        HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref _timerInternal);
    //        this._timerInternal = null;
    //        this.OnDisposing();
    //    }
    //}

    public abstract class SafeTimerBase : IDisposable, ISafeTimer
    {
        private readonly System.Threading.Timer _timer;
        private readonly bool _rescheduleAfterCallbackDone = false;
        private TimeSpan _reschedulingPeriod;

        public TimerTaskFlagUtils.WorkItemPriority TaskExecutionPriority
        {
            set {  }
        }

        public TimerTaskFlagUtils.TimerPrecision RequiredTimerPrecision
        {
            set { }
        }
        public bool IsUltraQuickCallback
        {
            set { }
        }

        protected SafeTimerBase(TimeSpan dueTime, TimeSpan period, bool rescheduleAfterCallbackDone)
        {
            this._rescheduleAfterCallbackDone = rescheduleAfterCallbackDone;
            this._reschedulingPeriod = period;
            TimeSpan periodAdjusted = rescheduleAfterCallbackDone ? Timeout.InfiniteTimeSpan : period;
            this._timer = new Timer(TimerCallbackInternal, null, dueTime, periodAdjusted);
        }

        protected SafeTimerBase(TimeSpan period, bool rescheduleAfterCallbackDone)
        {
            this._rescheduleAfterCallbackDone = rescheduleAfterCallbackDone;
            this._reschedulingPeriod = period;
            this._timer = new Timer(TimerCallbackInternal);
        }

        public void ExecuteSynchronouslyWithReschedulingIfRequested()
        {
            this.TimerCallbackInternal(null);
        }

        private void TimerCallbackInternal(object unused)
        {
            try
            {
                ExecuteCallback();
            }
            catch (Exception e)
            {
                //this is situation that should happen very rarely - so we can afford creation of new logger for that
                // (however both calls actually returns a singleton instance)
                LogFactory.Instance.GetLogger(null)
                                .LogException(LogLevel.Fatal, e, "Unexpected exception when invoking timer callback");
            }

            if (_rescheduleAfterCallbackDone)
            {
                //calling callback can take some time so veryfying if dispose didn't happen in the meantime
                lock (_timer)
                {
                    if (!_disposed)
                        this._timer.Change(_reschedulingPeriod, Timeout.InfiniteTimeSpan);
                }
            }
        }

        protected abstract void ExecuteCallback();
        protected abstract void OnDisposing();

        public void Change(TimeSpan dueTime, TimeSpan period)
        {
            _reschedulingPeriod = period;
            this._timer.Change(dueTime, period);
        }

        public void ChangeDue(TimeSpan dueTime)
        {
            this._timer.Change(dueTime, _reschedulingPeriod);
        }

        private bool _disposed = false;
        public void Dispose()
        {
            lock (_timer)
            {
                if (!_disposed)
                {
                    this._timer.Dispose();
                    this.OnDisposing();
                    _disposed = true;
                }
            }
        }
    }

    public class SafeTimer<T> : SafeTimerBase
    {
        private T _state;
        private Action<T> _callback;

        protected override void ExecuteCallback()
        {
            this._callback(_state);
        }

        protected override void OnDisposing()
        {
            this._callback = null;
            this._state = default(T);
        }

        public SafeTimer(Action<T> callback, T state, TimeSpan dueTime, TimeSpan period)
            : this(callback, state, dueTime, period, false)
        { }

        public SafeTimer(Action<T> callback, T state, TimeSpan dueTime, TimeSpan period, bool rescheduleAfterCallbackDone)
            :base(dueTime, period, rescheduleAfterCallbackDone)
        {
            this._state = state;
            this._callback = callback;
        }

        public SafeTimer(Action<T> callback)
            :this(callback, TimeSpan.MaxValue, false)
        { }

        public T State { get { return _state; } set { _state = value; } }

        public SafeTimer(Action<T> callback, TimeSpan period, bool rescheduleAfterCallbackDone)
            :base(period, rescheduleAfterCallbackDone)
        {
            this._callback = callback;
        }
    }

    public class SafeTimer : SafeTimerBase
    {
        private Action _callback;

        protected override void ExecuteCallback()
        {
            this._callback.Invoke();
        }

        protected override void OnDisposing()
        {
            this._callback = null;
        }

        public void AddCallback(Action callback)
        {
            _callback += callback;
        }

        public SafeTimer(Action callback, TimeSpan dueTime, TimeSpan period)
            : this(callback, dueTime, period, false)
        { }

        public SafeTimer(Action callback, TimeSpan dueTime, TimeSpan period, bool rescheduleAfterCallbackDone)
            :base(dueTime, period, rescheduleAfterCallbackDone)
        {
            this._callback = callback;
        }

        public SafeTimer(Action callback)
            : this(callback, Timeout.InfiniteTimeSpan, false)
        { }

        public SafeTimer(Action callback, bool rescheduleAfterCallbackDone)
            : this(callback, Timeout.InfiniteTimeSpan, rescheduleAfterCallbackDone)
        { }

        public SafeTimer(Action callback, TimeSpan period, bool rescheduleAfterCallbackDone)
            :base(period, rescheduleAfterCallbackDone)
        {
            this._callback = callback;
        }
    }

    public interface ISafeTimer
    {
        void Change(TimeSpan dueTime, TimeSpan period);
        void ExecuteSynchronouslyWithReschedulingIfRequested();
    }

    //usefull for unittesting classes dependend on timers
    public interface ISafeTimerFactory
    {
        ISafeTimer CreateSafeTimer(Action callback, TimeSpan dueTime, TimeSpan period);
        ISafeTimer CreateSafeTimer(Action callback, TimeSpan dueTime, TimeSpan period, bool rescheduleAfterCallbackDone);
        ISafeTimer CreateSafeTimer(Action callback);
        ISafeTimer CreateSafeTimer(Action callback, TimeSpan period, bool rescheduleAfterCallbackDone);
    }

    public class ProductionSafeTimerFactory : ISafeTimerFactory

    {
        private ProductionSafeTimerFactory()
        {
        }

        private static ISafeTimerFactory _instance = new ProductionSafeTimerFactory();

        public static ISafeTimerFactory Instance
        {
            get { return _instance; }
        }

        ISafeTimer ISafeTimerFactory.CreateSafeTimer(Action callback, TimeSpan dueTime, TimeSpan period)
        {
            return new SafeTimer(callback, dueTime, period);
        }

        ISafeTimer ISafeTimerFactory.CreateSafeTimer(Action callback, TimeSpan dueTime, TimeSpan period, bool rescheduleAfterCallbackDone)
        {
            return new SafeTimer(callback, dueTime, period, rescheduleAfterCallbackDone);
        }

        ISafeTimer ISafeTimerFactory.CreateSafeTimer(Action callback)
        {
            return new SafeTimer(callback);
        }

        ISafeTimer ISafeTimerFactory.CreateSafeTimer(Action callback, TimeSpan period, bool rescheduleAfterCallbackDone)
        {
            return new SafeTimer(callback, period, rescheduleAfterCallbackDone);
        }
    }
}

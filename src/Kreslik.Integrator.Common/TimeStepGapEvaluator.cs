﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class TimeStepGapEvaluator
    {
        public TimeStepGapEvaluator(TimeSpan maxGapAllowed, TimeInterval[] allowedGapIntervals)
        {
            _maxGapAllowed = maxGapAllowed;
            _allowedGapIntervals = allowedGapIntervals ?? new TimeInterval[0];
        }

        public DateOfYear[] ExclusionDates { get; set; }

        private readonly TimeSpan _maxGapAllowed;
        private readonly TimeInterval[] _allowedGapIntervals;
        private DateTime _lastStepUtc;

        public bool IsPreceededByGap(DateTime nextStepUtc)
        {
            bool noGapDetected = true;

            if ((nextStepUtc - _lastStepUtc).Abs() > _maxGapAllowed)
            {
                noGapDetected =
                    IsInExclusionDate(nextStepUtc) 
                    ||
                    _allowedGapIntervals.Any(
                        i => i.DateTimesDistanceExcludingInterval(nextStepUtc, _lastStepUtc) <= _maxGapAllowed);
            }

            _lastStepUtc = nextStepUtc;

            return !noGapDetected;
        }

        private bool IsInExclusionDate(DateTime utc)
        {
            return ExclusionDates != null && ExclusionDates.Any(d => d.EqualsTo(utc));
        }

        public void SetCurrentStamp(DateTime utc)
        {
            _lastStepUtc = utc;
        }
    }

    public class DateOfYear
    {
        public DateOfYear(int month, int day)
            :this(null, month, day)
        { }

        public DateOfYear(int year, int month, int day)
            : this((int?)year, month, day)
        { }

        private DateOfYear(int? year, int month, int day)
        {
            Month = month;
            Day = day;
            Year = year;

            if (Month > 12 || Month < 1)
            {
                throw new Exception($"Invalid Month ({Month}). Expected 1 - 12");
            }

            if (Day > 31 || Day < 1)
            {
                throw new Exception($"Invalid Day ({Day}). Expected 1 - 31");
            }
        }

        public int Month { get; }
        public int Day { get; }
        public int? Year { get; }

        public bool EqualsTo(DateTime utc)
        {
            return utc.Month == Month && utc.Day == Day && (!this.Year.HasValue || this.Year.Value == utc.Year);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mscoree;

namespace Kreslik.Integrator.Common
{
    public static class AppDomainEnumerator
    {
        public static IEnumerable<AppDomain> AllLoadedAppDomains
        {
            get
            {
                List<AppDomain> appDomains = new List<AppDomain>();

                IntPtr handle = IntPtr.Zero;
                CorRuntimeHost host = new CorRuntimeHost();
                try
                {
                    host.EnumDomains(out handle);
                    while (true)
                    {
                        object domain;
                        host.NextDomain(handle, out domain);
                        if (domain == null)
                            break;
                        appDomains.Add((AppDomain)domain);
                    }
                }
                finally
                {
                    host.CloseEnum(handle);
                }

                return appDomains;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class ConsoleRunnerHelper
    {
        public static void WaitTillExitRequested(WaitHandle externalExitRequestedEvent, ChangeTrackingType<bool> isFrozenTracker)
        {
            WaitTillExitRequested(externalExitRequestedEvent, isFrozenTracker, "ML Runner", null);
        }

        public static void WaitTillExitRequested(WaitHandle externalExitRequestedEvent, ChangeTrackingType<bool> isFrozenTracker, string namer, Func<bool> allowFreezeFunc)
        {
            ManualResetEvent consoleExitRequested = new ManualResetEvent(false);
            //Default console title contains executable name
            bool isDefaultTitle =
                Console.Title.Contains(
                    Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName));
            namer = isDefaultTitle
                ? namer
                : Console.Title;
            LogFactory.Instance.GetLogger(null)
                .Log(LogLevel.Trace,
                    $"Initial console title: [{Console.Title}], is default: {isDefaultTitle}; new: [{namer}]");
            Console.Title = namer + ". (h)elp<enter> for command help";

            Task.Factory.StartNew(() =>
            {

                while (true)
                {
                    string input = Console.ReadLine();

                    //null means ctrl-Z, and no more input from keyboard will be sent 
                    //   (https://docs.microsoft.com/en-us/dotnet/api/system.console.readline)
                    if (input == null ||
                        input.Equals("end", StringComparison.OrdinalIgnoreCase) ||
                        input.Equals("kill", StringComparison.OrdinalIgnoreCase) ||
                        input.Equals("k", StringComparison.OrdinalIgnoreCase))
                    {
                        consoleExitRequested.Set();
                        return;
                    }

                    if (input.Equals("freeze", StringComparison.OrdinalIgnoreCase) ||
                        input.Equals("f", StringComparison.OrdinalIgnoreCase))
                    {
                        if (allowFreezeFunc == null || allowFreezeFunc())
                        {
                            isFrozenTracker.Value = true;
                        }
                    }

                    if (input.Equals("thaw", StringComparison.OrdinalIgnoreCase) ||
                        input.Equals("t", StringComparison.OrdinalIgnoreCase))
                    {
                        isFrozenTracker.Value = false;
                    }

                    if (input.Equals("help", StringComparison.OrdinalIgnoreCase) ||
                        input.Equals("h", StringComparison.OrdinalIgnoreCase))
                    {
                        OutputHelp();
                    }

                }
            }, TaskCreationOptions.LongRunning);

            WaitHandle.WaitAny(new[] { consoleExitRequested, externalExitRequestedEvent });
        }

        public static void OutputHelp()
        {
            Console.WriteLine();
            Console.WriteLine("-------------===========================================================-------------------");
            Console.WriteLine("          Supported console commands (each command need to be confirmed by <enter>):");
            Console.WriteLine("___________________________________________________________________________________________");
            Console.WriteLine();
            Console.WriteLine("(k)ill    Kills the current app (with some short grace period for storing results)");
            Console.WriteLine("(f)reeze  Freezes the current app - but leaves it in memory for unsuspension");
            Console.WriteLine("(t)thaw   Resumes the run of current app frozen by freeze command.");
            Console.WriteLine("(h)elp    Displays this help");
            Console.WriteLine("-------------===========================================================-------------------");
            Console.WriteLine();
        }
    }
}

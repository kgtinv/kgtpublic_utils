﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class UnitTestDetector
    {
        static UnitTestDetector()
        {
            //Microsoft.VisualStudio.QualityTools.UnitTestFramework
            //Nunit.Framework
            string[] testAssemblyNames = new [] {"UnitTestFramework", "Nunit", "UnitTestRunner", "Kreslik.Integrator.NeuralNetwork.Tests" } ;
            UnitTestDetector.IsInUnitTest =
                //AppDomain.CurrentDomain
                //.GetAssemblies()
                AppDomainEnumerator.AllLoadedAppDomains.Select(TryGetAssemblies)
                .SelectMany(a => a)
                .Any(
                    loadedAssembly =>
                        testAssemblyNames.Any(namePart => loadedAssembly.FullName.ContainsCaseInsensitive(namePart)));

            //List<string> assemblies = null;

            //if (!UnitTestDetector.IsInUnitTest)
            //{
            //    assemblies = AppDomainEnumerator.AllLoadedAppDomains.Select(TryGetAssemblies)
            //        .SelectMany(a => a).Select(a => a.FullName).ToList();
            //    Console.WriteLine(assemblies.ToCsvString());
            //}
        }

        private static Assembly[] TryGetAssemblies(AppDomain appDomain)
        {
            try
            {
                return appDomain.GetAssemblies();
            }
            catch (Exception)
            {
                return new Assembly[0];
            }
        }

        public static bool IsInUnitTest { get; private set; }
    }
}

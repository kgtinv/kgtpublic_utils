﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface IReadonlyChangeTrackingType<out T> where T : IEquatable<T>
    {
        T Value { get; }
        event Action<T> ValueChanged;
    }

    public class ChangeTrackingType<T> : MarshalByRefObject, IReadonlyChangeTrackingType<T> where T: IEquatable<T>
    {
        public ChangeTrackingType(T initialValue, Action<T> initialAction)
        {
            _value = initialValue;
            if (initialAction != null)
            {
                ValueChanged += initialAction;
            }
        }
        public ChangeTrackingType(T initialValue)
        {
            _value = initialValue;
        }

        // it prevents the object in remote app domain to be collected
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.Infrastructure)]
        public override object InitializeLifetimeService()
        {
            return null;
        }

        private T _value;
        public T Value
        {
            get { return this._value; }

            set
            {
                if (!this._value.Equals(value))
                {
                    this._value = value;
                    ValueChanged?.Invoke(value);
                }
            }
        }
        public event Action<T> ValueChanged;

        public static implicit operator T(ChangeTrackingType<T> value)
        {
            return value.Value;
        }
    }

    public interface IWaitable
    {
        void WaitIfNeeded();
    }

    public class WaitableChangeTrackingType : ChangeTrackingType<bool>, IWaitable
    {
        private readonly bool _valueCausingWait;
        private readonly ManualResetEvent _event;
        private readonly ILogger _logger;

        public WaitableChangeTrackingType(
            bool initialValue, 
            Action<bool> initialAction,
            bool valueCausingWait,
            ILogger logger)
            : base(initialValue, initialAction)
        {
            _valueCausingWait = valueCausingWait;
            _event = new ManualResetEvent(initialValue != valueCausingWait);
            _logger = logger;
            this.ValueChanged += OnValueChanged;
        }

        public WaitableChangeTrackingType(bool initialValue, bool valueCausingWait, ILogger logger)
            : this(initialValue, null, valueCausingWait, logger)
        { }

        private void OnValueChanged(bool state)
        {
            using (ConsoleColoringHelper consoleColoringHelper = new ConsoleColoringHelper())
            {
                if (state == _valueCausingWait)
                {
                    Console.Title = "FROZEN " + Console.Title;
                    ConsoleColoringHelper.SetColorsToFailure();
                    _logger.LogConsoleInfo(LogLevel.Info, "Detected condition requesting to suspend observers");

                    _event.Reset();
                }
                else
                {
                    Console.Title = Console.Title.RemoveFromStart("FROZEN ");
                    ConsoleColoringHelper.SetColorsToSuccess();
                    _logger.LogConsoleInfo(LogLevel.Info, "Detected condition requesting to resume observers");

                    _event.Set();
                }
            }
        }

        public void WaitIfNeeded()
        {
            if (this.Value == _valueCausingWait)
            {
                _logger.Log(LogLevel.Info, "Freezeing thread as observed condition is set");
                _event.WaitOne();
                _logger.Log(LogLevel.Info, "Thawing thread as observed condition is unset");
            }
        }
    }
}

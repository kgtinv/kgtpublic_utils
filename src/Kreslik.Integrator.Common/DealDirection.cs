﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public enum DealDirection
    {
        Buy = 0,
        Sell = 1
    }

    public enum TradeDirection
    {
        Long = 0,
        Short = 1,
    }

    public static class DealDirectionExtensions
    {
        public static TradeDirection ToTradeDirection(this DealDirection dealDirection)
        {
            return (TradeDirection) dealDirection;
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kreslik.Integrator.Common
{
    public class TextboxLogFactory : ILogFactory
    {
        public static int MaxLines = 500;
        public static bool AutoScroll = true;

        private class RichTextBoxLoggerComposite : ILogger
        {
            private ILogger _underlyingLogger;
            private RichTextBox _textBox;
            private int _lineCount;

            private class RichTextBoxRowColoringRule
            {
                public RichTextBoxRowColoringRule(LogLevel logLevel, Color fontColor, Color backgroundColor, FontStyle style)
                {
                    this.LogLevel = logLevel;
                    this.FontColor = fontColor;
                    this.BackgroundColor = backgroundColor;
                    this.Style = style;
                }

                public LogLevel LogLevel { get; private set; }
                public Color FontColor { get; private set; }
                public Color BackgroundColor { get; private set; }
                public FontStyle Style { get; private set; }
            }

            private static List<RichTextBoxRowColoringRule> DefaultRowColoringRules;

            static RichTextBoxLoggerComposite()
            {
                DefaultRowColoringRules = new List<RichTextBoxRowColoringRule>()
                    {
                        new RichTextBoxRowColoringRule(LogLevel.Fatal, Color.White, Color.Red, FontStyle.Bold),
                        new RichTextBoxRowColoringRule(LogLevel.Error, Color.Red, Color.Empty,
                            FontStyle.Bold | FontStyle.Italic),
                        new RichTextBoxRowColoringRule(LogLevel.Warn, Color.Orange, Color.Empty, FontStyle.Underline),
                        new RichTextBoxRowColoringRule(LogLevel.Info, Color.Black, Color.Empty, FontStyle.Regular),
                        new RichTextBoxRowColoringRule(LogLevel.Debug, Color.Gray, Color.Empty, FontStyle.Regular),
                        new RichTextBoxRowColoringRule(LogLevel.Trace, Color.DarkGray, Color.Empty, FontStyle.Italic),
                    };
            }

            private void SendTheMessageToRichTextBox(string logMessage, LogLevel level)
            {
                RichTextBox rtbx = this._textBox;
                RichTextBoxRowColoringRule rule = DefaultRowColoringRules.First(r => r.LogLevel == level);

                int startIndex = rtbx.Text.Length;
                rtbx.SelectionStart = startIndex;
                rtbx.SelectionBackColor = rule.BackgroundColor == Color.Empty
                    ? rtbx.BackColor : rule.BackgroundColor;
                rtbx.SelectionColor = rule.FontColor == Color.Empty
                    ? rtbx.ForeColor : rule.FontColor;
                rtbx.SelectionFont = new Font(rtbx.SelectionFont, rtbx.SelectionFont.Style ^ rule.Style);
                rtbx.AppendText(logMessage + "\n");
                rtbx.SelectionLength = rtbx.Text.Length - rtbx.SelectionStart;

                if (TextboxLogFactory.MaxLines > 0)
                {
                    this._lineCount++;
                    if (this._lineCount > TextboxLogFactory.MaxLines)
                    {
                        int pos = rtbx.GetFirstCharIndexFromLine(1);
                        rtbx.Select(0, pos);
                        rtbx.SelectedText = string.Empty;
                        this._lineCount--;
                    }
                }

                if (TextboxLogFactory.AutoScroll)
                {
                    rtbx.Select(rtbx.TextLength, 0);
                    rtbx.ScrollToCaret();
                }
            }

            private BlockingCollection<object[]> _logEvents = new BlockingCollection<object[]>(new ConcurrentQueue<object[]>());
            public RichTextBoxLoggerComposite(ILogger underlyingLogger, RichTextBox textBox)
            {
                this._underlyingLogger = underlyingLogger;
                this._textBox = textBox;
                ThreadPool.QueueUserWorkItem(ProcessLogEvents);
            }

            private delegate void DelSendTheMessageToRichTextBox(string logMessage, LogLevel level);

            public void Log(LogLevel level, string message)
            {
                _underlyingLogger.Log(level, message);

                //prevent logging if window not yet created or allready closed
                if(this._textBox.Parent != null)
                    this._logEvents.Add(new object[] { message, level });
            }

            private void ProcessLogEvents(object unused)
            {
                Delegate writeLogDataDelegate = new DelSendTheMessageToRichTextBox(this.SendTheMessageToRichTextBox);
                foreach (object[] args in _logEvents.GetConsumingEnumerable())
                {
                    if (this._textBox.Parent != null)
                        this._textBox.Invoke(writeLogDataDelegate,args);
                }
            }

            public void Log(LogLevel level, IPooledBufferSegment pooledBuffer)
            {
                this.Log(level, System.Text.Encoding.ASCII.GetString(pooledBuffer.Buffer, pooledBuffer.ContentStart,
                    pooledBuffer.ContentSize));
            }

            public void Log(LogLevel level, string message, IPooledBufferSegment pooledBuffer)
            {
                this.Log(level, message + System.Text.Encoding.ASCII.GetString(pooledBuffer.Buffer, pooledBuffer.ContentStart,
                    pooledBuffer.ContentSize));
            }

            public void Log(LogLevel level, string messageFmt, params object[] args)
            {
                this.Log(level, string.Format(messageFmt, args));
            }

            public void LogException(LogLevel level, string message, Exception e)
            {
                this.Log(level, message + " " + e.ToString());
            }

            public void LogException(LogLevel level, Exception e, string messageFmt, params object[] args)
            {
                this.Log(level, string.Format(messageFmt, args) + " " + e.ToString());
            }

            public bool IsEnabled(LogLevel level)
            {
                return _underlyingLogger.IsEnabled(level);
            }

            public bool IsLowDiskSpaceConstraintLoggingOn
            {
                get { return _underlyingLogger.IsLowDiskSpaceConstraintLoggingOn; }
            }

            public event Action<bool> IsLowDiskSpaceChanged;
            public IReadonlyChangeTrackingType<bool> IsUltraLowDiskSpace { get; } = new ChangeTrackingType<bool>(false);
        }


        private Dictionary<string, RichTextBox> _textBoxLoggers = new Dictionary<string, RichTextBox>();

        public TextboxLogFactory(params Tuple<string, RichTextBox>[] textboxesWithPatterns)
        {
            foreach (Tuple<string, RichTextBox> textboxesWithPattern in textboxesWithPatterns)
            {
                _textBoxLoggers[textboxesWithPattern.Item1] = textboxesWithPattern.Item2;
            }
        }

        private RichTextBox GetTextBox(string logName)
        {
            foreach (string pattern in _textBoxLoggers.Keys)
            {
                if (logName.Contains(pattern))
                    return _textBoxLoggers[pattern];
            }

            return null;
        }

        private ILogger CreateLogger(ILogger underlyingLogger, string logName)
        {
            RichTextBox rtb = this.GetTextBox(logName);
            if (rtb != null)
                return new RichTextBoxLoggerComposite(underlyingLogger, rtb);
            else
                return underlyingLogger;
        }

        public ILogger GetLogger(string logName)
        {
            ILogger underlyingLogger = LogFactory.Instance.GetLogger(logName);
            return this.CreateLogger(underlyingLogger, logName);
        }

        public ILogger GetLogger(string logName, bool isLessImportant)
        {
            ILogger underlyingLogger = LogFactory.Instance.GetLogger(logName, isLessImportant);
            return this.CreateLogger(underlyingLogger, logName);
        }

        public ILogger GetLogger(string logName, bool isLessImportant, bool doNotLogDuringLowDiskSpace)
        {
            ILogger underlyingLogger = LogFactory.Instance.GetLogger(logName, isLessImportant, doNotLogDuringLowDiskSpace);
            return this.CreateLogger(underlyingLogger, logName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public struct Interval<T>
    {
        public Interval(T lowBound, T highBound)
        {
            LowBound = lowBound;
            HighBound = highBound;
        }

        public T LowBound { get; }
        public T HighBound { get; }
    }

    [DataContract]
    public struct Interval
    {
        public static readonly Interval Empty = new Interval(-1, -1);

        public Interval(int lowBound, int highBound)
        {
            LowBound = lowBound;
            HighBound = highBound;
        }

        [DataMember]
        public int LowBound { get; private set; }
        [DataMember]
        public int HighBound { get; private set; }
        public int IntervalLength => HighBound - LowBound;

        public int Compare(int number, bool isHighBoundExclusive)
        {
            if (number < LowBound)
            {
                return -1;
            }
            else if (number > HighBound || (isHighBoundExclusive && number == HighBound))
            {
                return 1;
            }

            return 0;
        }

        public static bool operator ==(Interval i1, Interval i2)
        {
            return i1.LowBound == i2.LowBound && i1.HighBound == i2.HighBound;
        }

        public static bool operator !=(Interval i1, Interval i2)
        {
            return !(i1 == i2);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class DiskFreeSpaceWatcher
    {
        private readonly DriveInfo _driveInfo;
        //We need a LONG constant so that multiplying results in long result (and not an overflown int)
        private const long BYTES_IN_GB = 1073741824;
        private SortedList<long, DiskSpaceEvent> _fallBelowTriggers = new SortedList<long, DiskSpaceEvent>(DiskSpaceEvent.FallBelowComparer);
        private long _fallBelowMaxTrigger = long.MinValue;
        private SortedList<long, DiskSpaceEvent> _raiseAboveTriggers = new SortedList<long, DiskSpaceEvent>(DiskSpaceEvent.RaiseAboveComparer);
        private long _raiseAboveMinTrigger = long.MaxValue;

        public DiskFreeSpaceWatcher(string driveName)
        {
            _driveInfo = new DriveInfo(driveName);
        }

        public void CheckEvents()
        {
            long freeBytes = _driveInfo.AvailableFreeSpace;
            bool setTriggers = false;

            if (freeBytes <= _fallBelowMaxTrigger)
            {
                //go through fall below
                _fallBelowTriggers = GoThroughList(_fallBelowTriggers, _raiseAboveTriggers, freeBytes);
                setTriggers = true;
            }

            if (freeBytes >= _raiseAboveMinTrigger)
            {
                //go through raise above
                _raiseAboveTriggers = GoThroughList(_raiseAboveTriggers, _fallBelowTriggers, freeBytes);
                setTriggers = true;
            }

            //we alter both list even during traversing just single
            if (setTriggers)
            {
                _fallBelowMaxTrigger = _fallBelowTriggers.Count > 0 ? _fallBelowTriggers.Values[0].BytesThreshold : long.MinValue;
                _raiseAboveMinTrigger = _raiseAboveTriggers.Count > 0 ? _raiseAboveTriggers.Values[0].BytesThreshold : long.MaxValue;
            }
        }


        public double FreeGigaBytes { get { return _driveInfo.AvailableFreeSpace / (double) BYTES_IN_GB; } }

        //By creating new list we avoid possibility of event recursion
        private SortedList<long, DiskSpaceEvent> GoThroughList(
            SortedList<long, DiskSpaceEvent> triggers,
            SortedList<long, DiskSpaceEvent> contraTriggers,
            long currentValue)
        {
            var traversedList = new SortedList<long, DiskSpaceEvent>(triggers.Comparer);
            foreach (DiskSpaceEvent evt in triggers.Values)
            {
                if (triggers.Comparer.Compare(evt.BytesThreshold, _driveInfo.AvailableFreeSpace) <= 0)
                {
                    evt.Handler.TryPerformAction();
                    if (evt.OptionalEventToRegisterOnFirstEventTriggered != null)
                    {
                        contraTriggers.Add(evt.OptionalEventToRegisterOnFirstEventTriggered.BytesThreshold,
                            evt.OptionalEventToRegisterOnFirstEventTriggered);
                    }
                }
                else
                {
                    traversedList.Add(evt.BytesThreshold, evt);
                }
            }

            return traversedList;
        }

        public enum SpaceThresholdType
        {
            FallBelow,
            RaiseAbove
        }

        public void RegisterHandler(double gigabytesThreshold, SpaceThresholdType spaceThresholdType, Action handler)
        {
            DiskSpaceEvent ev = new DiskSpaceEvent((long) (gigabytesThreshold * BYTES_IN_GB), handler,
                spaceThresholdType);
            RegisterHandler(ev);
        }

        public void RegisterHandler(
            double maxGigabytesFreeSpaceToInvokeActionA, Action actionA,
            double minGigabytesFreeSpaceToInvokeActionB, Action actionB)
        {
            if (maxGigabytesFreeSpaceToInvokeActionA >= minGigabytesFreeSpaceToInvokeActionB)
            {
                throw new Exception(
                    $"A pair of disk watching handlers must not have switched trigger limits (decreasing limit: {maxGigabytesFreeSpaceToInvokeActionA}; increasing limit: {minGigabytesFreeSpaceToInvokeActionB})");
            }

            DiskSpaceEvent ev1 = new DiskSpaceEvent((long)(maxGigabytesFreeSpaceToInvokeActionA * BYTES_IN_GB), actionA,
                SpaceThresholdType.FallBelow);

            DiskSpaceEvent ev2 = new DiskSpaceEvent((long)(minGigabytesFreeSpaceToInvokeActionB * BYTES_IN_GB), actionB,
                SpaceThresholdType.RaiseAbove);

            ev1.OptionalEventToRegisterOnFirstEventTriggered = ev2;
            ev2.OptionalEventToRegisterOnFirstEventTriggered = ev1;

            RegisterHandler(ev1);
        }

        private void RegisterHandler(DiskSpaceEvent ev)
        {
            if (ev.SpaceThresholdType == SpaceThresholdType.FallBelow)
            {
                _fallBelowTriggers.Add(ev.BytesThreshold, ev);
                _fallBelowMaxTrigger = _fallBelowTriggers.Values[0].BytesThreshold;
            }
            else
            {
                _raiseAboveTriggers.Add(ev.BytesThreshold, ev);
                _raiseAboveMinTrigger = _raiseAboveTriggers.Values[0].BytesThreshold;
            }
        }

        private class DiskSpaceEvent
        {
            public DiskSpaceEvent(long bytesThreshold, Action handler, SpaceThresholdType spaceThresholdType)
            {
                BytesThreshold = bytesThreshold;
                Handler = handler;
                SpaceThresholdType = spaceThresholdType;
            }

            public DiskSpaceEvent OptionalEventToRegisterOnFirstEventTriggered { get; set; }

            public long BytesThreshold { get; private set; }
            public Action Handler { get; private set; }
            public SpaceThresholdType SpaceThresholdType { get; private set; }

            public static IComparer<long> FallBelowComparer
            {
                get { return Comparer<long>.Create((l1, l2) => l2.CompareTo(l1)); }
            }

            public static IComparer<long> RaiseAboveComparer
            {
                get { return Comparer<long>.Create((l1, l2) => l1.CompareTo(l2)); }
            }
        }
    }
}

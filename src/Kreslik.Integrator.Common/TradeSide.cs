﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public enum TradeSide : byte
    {
        SellGiven,
        BuyPaid
    }

    public static class TradeSideExtensions
    {
        public static byte? ToNullableByte(this TradeSide? tradeSide)
        {
            //return tradeSide.HasValue ? (byte?) tradeSide.Value : null;
            return (byte?) tradeSide;
        }

        public static TradeSide? ToNullableTradeSide(this byte? b)
        {
            //return b.HasValue ? (TradeSide?) b : null;
            return (TradeSide?) b;
        }

        public static byte? ToNullableByte(this PriceSide? priceSide)
        {
            //return priceSide.HasValue ? (byte?)priceSide.Value : null;
            return (byte?)priceSide;
        }

        public static PriceSide? ToNullablePriceSide(this byte? b)
        {
            //return b.HasValue ? (PriceSide?)b : null;
            return (PriceSide?)b;
        }
    }
}

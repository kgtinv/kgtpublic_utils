﻿using System.Collections.Generic;
using System.Linq;

namespace Kreslik.Integrator.Common
{
    public class SequenceEqualityComparer<T> : CustomEqualityComparer<IEnumerable<T>>
    {
        public SequenceEqualityComparer() :
            base((a, b) => a.SequenceEqual(b), GetSequenceHashCode)
        {
        }

        private static int GetSequenceHashCode(IEnumerable<T> seq)
        {
            int hash = 0;
            if (seq != null)
            {
                foreach (T t in seq)
                {
                    hash *= 17;
                    if (t != null) hash = hash + t.GetHashCode();
                }
            }

            return hash;
        }
    }
}
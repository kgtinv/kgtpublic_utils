﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class StateRefresher: IDisposable
    {
        private readonly TimeSpan _longestRefreshInterval;
        private readonly TimeSpan _shortestRefreshInterval;
        private readonly SafeTimer _refreshTimer;
        private readonly Action _refreshAction;
        private int _fastRefreshWaiting = 0;


        public StateRefresher(TimeSpan longestRefreshInterval, TimeSpan shortestRefreshInterval, Action refreshAction)
        {
            this._longestRefreshInterval = longestRefreshInterval;
            this._shortestRefreshInterval = shortestRefreshInterval;
            this._refreshAction = refreshAction;
            this._refreshTimer = new SafeTimer(RefreshCallback, longestRefreshInterval, false);
            this._refreshTimer.ExecuteSynchronouslyWithReschedulingIfRequested();
        }

        private void RefreshCallback()
        {
            Interlocked.Exchange(ref this._fastRefreshWaiting, 0);
            this._refreshAction();
            if (InterlockedEx.Read(ref this._fastRefreshWaiting) == 0)
            {
                _refreshTimer.ChangeDue(_longestRefreshInterval);
            }
        }

        public void AnnounceStateChanging()
        {
            if (InterlockedEx.CheckAndFlipState(ref this._fastRefreshWaiting, 1, 0))
            {
                _refreshTimer.ChangeDue(_shortestRefreshInterval);
            }
        }

        public void Dispose()
        {
            //make sure that announce will not attempt to change disposed timer
            Interlocked.Exchange(ref this._fastRefreshWaiting, 1);
            this._refreshTimer.Dispose();
            Interlocked.Exchange(ref this._fastRefreshWaiting, 1);
        }
    }
}

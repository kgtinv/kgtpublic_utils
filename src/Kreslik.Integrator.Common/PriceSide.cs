﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public enum PriceSide: byte
    {
        Bid = 0,
        Ask = 1
    }

    public static class PriceSideExtensions
    {
        public static PriceSide ToOppositeSide(this PriceSide side)
        {
            return side == PriceSide.Bid ? PriceSide.Ask : PriceSide.Bid;
        }

        public static DealDirection ToOutgoingDealDirection(this PriceSide side)
        {
            return side == PriceSide.Bid ? DealDirection.Sell : DealDirection.Buy;
        }

        public static DealDirection ToOpositeDirection(this DealDirection direction)
        {
            return direction == DealDirection.Buy ? DealDirection.Sell : DealDirection.Buy;
        }

        public static PriceSide ToInitiatingPriceDirection(this DealDirection direction)
        {
            return direction == DealDirection.Buy ? PriceSide.Ask : PriceSide.Bid;
        }

        public static PriceSide? ToIntegratorOutgoingPriceSide(this DealDirection? direction)
        {
            return direction.HasValue
                ? direction == DealDirection.Buy ? PriceSide.Bid : PriceSide.Ask
                : (PriceSide?) null;
        }
    }
}

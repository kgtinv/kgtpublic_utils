﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class Constants
    {
        public const decimal OneBillionDecimal = 1000000000m;
        public const decimal OneMillionDecimal = 1000000m;
        public const decimal TenThousandDecimal = 10000m;
        public const decimal OneThousandDecimal = 1000m;
        public const decimal OneHundredDecimal = 100m;

        public const double OneBillionDouble = 1000000000d;
        public const double OneMillionDouble = 1000000d;
        public const double TenThousandDouble = 10000d;
        public const double OneThousandDouble = 1000d;
        public const double OneHundredDouble = 100d;
    }
}
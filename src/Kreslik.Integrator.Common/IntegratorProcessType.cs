﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public enum IntegratorProcessType
    {
        NotAnyIntegratorService,
        BusinessLogic,
        HotspotMarketData,
        DataCollection,
        MetricsInMemoryDb,
        Web
    }
}

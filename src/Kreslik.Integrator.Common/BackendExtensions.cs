﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class SqlCommandExtensions
    {
        private static string ParameterValueToString(object value)
        {
            if (value is DateTime)
            {
                return ((DateTime)value).ToLongStringWithFractions();
            }

            if (value is DataTable)
            {
                return ((DataTable)value).ConvertToString();
            }

            string str = value.ToString();

            if (str.Length > 8000)
            {
                return
                    $"(TRIMMED from {str.Length} to 8000): {str.Substring(0, 6000)}{Environment.NewLine}(...){Environment.NewLine}{str.Substring(str.Length - 2000, 2000)}";
            }

            return str;
        }

        public static string ConvertToString(this DataTable dataTable)
        {
            var output = new StringBuilder();

            var columnsWidths = new int[dataTable.Columns.Count];

            // Get column widths
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var length = row[i].ToString().Length;
                    if (columnsWidths[i] < length)
                        columnsWidths[i] = length;
                }
            }

            // Get Column Titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var length = dataTable.Columns[i].ColumnName.Length;
                if (columnsWidths[i] < length)
                    columnsWidths[i] = length;
            }

            // Write Column titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var text = dataTable.Columns[i].ColumnName;
                output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
            }
            output.Append("|\n" + new string('=', output.Length) + "\n");

            // Write Rows
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var text = row[i].ToString();
                    output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                }
                output.Append("|\n");
            }
            return output.ToString();
        }

        private static string PadCenter(string text, int maxLength)
        {
            int diff = maxLength - text.Length;
            return new string(' ', diff / 2) + text + new string(' ', (int)(diff / 2.0 + 0.5));

        }

        public static string GetSpExecuteString(this SqlCommand sqlCommand)
        {
            string result = null;

            try
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("exec {0} ", sqlCommand.CommandText);

                foreach (SqlParameter parameter in sqlCommand.Parameters)
                {
                    if (parameter.Value == DBNull.Value)
                    {
                        sb.AppendFormat(" {0} = DB NULL,", parameter.ParameterName);
                    }
                    else if (parameter.Value == null)
                    {
                        sb.AppendFormat(" {0} = NULL,", parameter.ParameterName);
                    }
                    else
                    {
                        sb.AppendFormat(" {0} = '{1}',", parameter.ParameterName, ParameterValueToString(parameter.Value));
                    }
                }

                result = sb.ToString(0, sb.Length - 1);
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            return result;
        }

        private static T ExecuteReaderOperationWithExceptionLog<T>(Func<T> operation, SqlCommand sqlCommand,
            ILogger logger)
        {
            try
            {
                return operation();
            }
            catch (Exception e)
            {
                logger.LogException(AdoNetHelpers.IsRecoverableSqlException(e) ? LogLevel.Error : LogLevel.Fatal, e,
                    "Exception during executing following command: [{0}]",
                    sqlCommand.GetSpExecuteString());
                throw;
            }
        }

        public static object ExecuteScalarWithExceptionLog(this SqlCommand sqlCommand, ILogger logger)
        {
            return ExecuteReaderOperationWithExceptionLog<object>(sqlCommand.ExecuteScalar, sqlCommand, logger);
        }

        public static int ExecuteNonQueryWithExceptionLog(this SqlCommand sqlCommand, ILogger logger)
        {
            return ExecuteReaderOperationWithExceptionLog<int>(sqlCommand.ExecuteNonQuery, sqlCommand, logger);
        }

        public static SqlDataReader ExecuteReaderWithExceptionLog(this SqlCommand sqlCommand, ILogger logger)
        {
            return ExecuteReaderOperationWithExceptionLog<SqlDataReader>(sqlCommand.ExecuteReader, sqlCommand, logger);
        }
    }

    public static class SqlParameterCollectionExtensions
    {
        public static SqlParameter AddDateTime2(this SqlParameterCollection sqlParameterCollection, string parameterName, DateTime value)
        {
            return AddDateTime2Nullable(sqlParameterCollection, parameterName, value);
        }

        public static SqlParameter AddDateTime2Nullable(this SqlParameterCollection sqlParameterCollection, string parameterName, object value)
        {
            var param = sqlParameterCollection.Add(parameterName, SqlDbType.DateTime2);
            param.Value = value ?? DBNull.Value;
            return param;
        }

        public static SqlParameter AddWithNullValue<T>(this SqlParameterCollection sqlParameterCollection, string parameterName, T value) where T : class
        {
            return sqlParameterCollection.AddWithValue(parameterName, value ?? (object)DBNull.Value);
        }

        public static SqlParameter AddWithNullValue<T>(this SqlParameterCollection sqlParameterCollection, string parameterName, Nullable<T> value) where T : struct
        {
            return sqlParameterCollection.AddWithValue(parameterName, value ?? (object)DBNull.Value);
        }


        public static int? ToNullableInt32(this SqlInt32 value)
        {
            return value.IsNull ? (int?)null : value.Value;
        }

        public static int? GetNullableInt32(this SqlDataReader reader, int ordinalId)
        {
            return reader.GetSqlInt32(ordinalId).ToNullableInt32();
        }

        public static string GetNullableString(this SqlDataReader reader, int ordinalId)
        {
            return reader.IsDBNull(ordinalId) ? null : reader.GetString(ordinalId);
        }

        public static string GetNullableString(this SqlDataReader reader, string columnName)
        {
            return GetNullableString(reader, reader.GetOrdinal(columnName));
        }

        public static decimal? GetNullableDecimal(this SqlDataReader reader, int ordinalId)
        {
            return reader.IsDBNull(ordinalId) ? (decimal?)null : reader.GetDecimal(ordinalId);
        }

        public static T? GetNullable<T>(this SqlDataReader reader, int ordinalId) where T : struct
        {
            return reader.IsDBNull(ordinalId) ? new T?() : reader.GetFieldValue<T>(ordinalId);
        }

        public static T? GetNullable<T>(this SqlDataReader reader, string columnName) where T : struct
        {
            return GetNullable<T>(reader, reader.GetOrdinal(columnName));
        }

        public static bool IsNull(this SqlDataReader reader, string columnName)
        {
            return reader.IsDBNull(reader.GetOrdinal(columnName));
        }

        public static T GetEnum<T>(this SqlDataReader reader, int ordinalId)
        {
            return (T)Enum.Parse(typeof(T), reader.GetString(ordinalId).Trim());
        }

        public static T GetEnum<T>(this SqlDataReader reader, string columnName)
        {
            return GetEnum<T>(reader, reader.GetOrdinal(columnName));
        }

        public static T GetValue<T>(this SqlDataReader reader, string columnName)
        {
            return reader.GetFieldValue<T>(reader.GetOrdinal(columnName));
        }

        private static readonly Type[] _toDecimalConvertibleTypes = new[]
        {
            typeof(sbyte),
            typeof(byte),
            typeof(char),
            typeof(short),
            typeof(ushort),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(float),
            typeof(double),
        };

        public static bool IsConvertibleToDecimal(this Type t)
        {
            return
                //speedup of the identity
                t == typeof(decimal)
                ||
                _toDecimalConvertibleTypes.Contains(t);
        }

        public static decimal GetLargeScaleDecimalSafe(this SqlDataReader reader, int ordinalId)
        {
            try
            {
                //support also other types convertibel to decimal
                return Convert.ToDecimal(reader[ordinalId]);
                //return reader.GetDecimal(ordinalId);
            }
            catch (OverflowException)
            {
                SqlDecimal sqlDecimal = reader.GetSqlDecimal(ordinalId);
                try
                {
                    return SqlDecimal.ConvertToPrecScale(sqlDecimal, 28, 9).Value;
                }
                catch (Exception e) when (e is SqlTruncateException || e is OverflowException)
                {
                    return sqlDecimal.IsPositive ? decimal.MaxValue : decimal.MinValue;
                }
                catch (Exception e)
                {
                    throw new Exception(reader.GetCurrentColumnDescription(ordinalId), e);
                }
            }
            catch (Exception e)
            {
                throw new Exception(reader.GetCurrentColumnDescription(ordinalId), e);
            }
        }

        public static string GetCurrentColumnDescription(this SqlDataReader reader, int ordinalId)
        {
            string columnName = TryGetDescription(ordinalId, reader.GetName);
            object value = TryGetDescription(ordinalId, reader.GetValue);
            string typeName = TryGetDescription(ordinalId, reader.GetDataTypeName);

            return
                $"GetLargeScaleDecimalSafe failed. ColumnName: [{columnName}], ColumnType: [{typeName}], value: [{value}]";
        }

        private static string TryGetDescription<T>(int ordinalId, Func<int, T> getValueFunc)
        {
            try
            {
                return getValueFunc(ordinalId).ToString();
            }
            catch (Exception e)
            {
                return e.GetType().ToString();
            }
        }

        public static decimal GetLargeScaleDecimalSafe(this SqlDataReader reader, string columnName)
        {
            return reader.GetLargeScaleDecimalSafe(reader.GetOrdinal(columnName));
        }
    }
}

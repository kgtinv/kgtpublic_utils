﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Kreslik.Integrator.Common
{
    [Serializable]
    public class ImapDisconnectedException : Exception
    {
        public ImapDisconnectedException()
        { }
    }

    public class ImapEx : IDisposable
    {
        public ImapEx(ILogger logger)
        {
            _logger = logger;
        }

        public void Dispose()
        {
            Disconnect();
        }

        /// <summary>
        ///     Connect to specified host and port
        /// </summary>
        private ImapResponseEnum Connect(string serverUrl, ushort port)
        {
            _imapCommandVal = 0;
            var imapResponse = ImapResponseEnum.ImapFailureResponse;

            try
            {
                _tcpCli = new TcpClient(serverUrl, port);
                _sslStream = new SslStream(_tcpCli.GetStream(), false, ValidateServerCertificate, null)
                {
                    // TODO: consider better value
                    ReadTimeout = 30000
                };

                _sslStream.AuthenticateAsClient(serverUrl);
                _streamReader = new StreamReader(_sslStream);

                var sResult = _streamReader.ReadLine();

                if (sResult.StartsWith(ImapOkServerResponse) || sResult.StartsWith(ImapCapabilityServerResponse))
                {
                    imapResponse = ImapResponseEnum.ImapSuccessResponse;
                }
            }
            catch (Exception ex)
            {
                _logger.LogException(LogLevel.Fatal, "Imap connection problem", ex);
                throw;
            }

            _serverUrl = serverUrl;
            _port = port;
            return imapResponse;
        }


        /// <summary>
        ///     Disconnect connection with Imap server
        /// </summary>
        private void Disconnect()
        {
            try
            {
                _imapCommandVal = 0;
                if (_isConnected)
                {
                    if (_sslStream != null)
                    {
                        _sslStream.Close();
                    }
                    if (_streamReader != null)
                    {
                        _streamReader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, "Imap disconnection problem", e);
            } 
        }

        /// <summary>
        ///     Send command to server and retrieve response
        /// </summary>
        private ImapResponseEnum SendAndReceive(string command, ref ArrayList resultArrayList)
        {
            return SendAndReceiveByNumLines(command, ref resultArrayList, -1);
        }


        /// <summary>
        ///     Send command to server and retrieve response
        /// </summary>
        private ImapResponseEnum SendAndReceiveByNumLines(string command, ref ArrayList resultArrayList, int numlines)
        {
            ImapResponseEnum result;
            _imapCommandVal++;
            var sCommand = ImapCommandIdentifier + command;
            var data = Encoding.ASCII.GetBytes(sCommand.ToCharArray());

            try
            {
                _sslStream.Write(data, 0, data.Length);
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Error, "Imap error in sending message", e);
                throw;
            }

            return ReceiveByNumLines(ref resultArrayList, numlines);
        }

        public event Action Disconnected;

        /// <summary>
        ///     retrieve response
        /// </summary>
        /// <param name="resultArrayList">Imap Server response</param>
        /// <returns>ImapResponseEnum type</returns>
        private ImapResponseEnum ReceiveByNumLines(ref ArrayList resultArrayList, int numlines = -1)
        {
            var imapResponse = ImapResponseEnum.ImapSuccessResponse;
            try
            {
                var read = true;
                var lineCount = 0;
                while (read)
                {
                    var result = _streamReader.ReadLine();
                    if (result == null)
                    {
                        this._logger.Log(LogLevel.Error, "The other end of ssl stream disconnected");
                        Action onDisconnected = Disconnected;
                        if (onDisconnected != null)
                        {
                            Disconnected = null;
                            onDisconnected();
                        }
                        throw new ImapDisconnectedException();
                    }
                    resultArrayList.Add(result);
                    lineCount++;

                    if (result.StartsWith(ImapServerResponseOk) || lineCount == numlines)
                    {
                        read = false;
                        imapResponse = ImapResponseEnum.ImapSuccessResponse;
                    }
                    else if (result.StartsWith(ImapServerResponseNo) || result.StartsWith(ImapServerResponseBad))
                    {
                        read = false;
                        imapResponse = ImapResponseEnum.ImapFailureResponse;
                    }
                }
            }
            catch (ImapDisconnectedException)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Error, "Imap error in receiving messages", e);
                throw;
            }
            return imapResponse;
        }

        

        /// <summary>
        ///     Read the Server Response by specified size
        /// </summary>
        private int ReceiveBuffer(ref string buffer, int size)
        {
            var readTmp = -1;
            var sizeTmp = 0;
            var cBuff = new char[size + 1];
            while (sizeTmp < size)
            {
                readTmp = _streamReader.Read(cBuff, 0, size - sizeTmp);
                buffer += (new string(cBuff)).Substring(0, readTmp);
                sizeTmp += readTmp;
            }
            return readTmp;
        }

        /// <summary>
        ///     Validate Certificate
        /// </summary>
        private bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            if(sslPolicyErrors == SslPolicyErrors.None)
                return true;

            if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateNameMismatch &&
                    certificate.Subject.Contains("gmail.com"))
            {
                _logger.Log(LogLevel.Trace,
                    "ImapEx (Gmail trigger) experienced and ignored SSL certificate name mismatch - this is expected since we are passing resolved IP. The certificate subject: " +
                    certificate.Subject);
            }
            else
            {
                _logger.Log(LogLevel.Fatal,
                    "ImapEx (Gmail trigger) experienced and ignored SSL certificate error - but error should be reviewed (as it's not expected). Error: {0}, Certificate: {1}",
                    sslPolicyErrors, certificate);
            }

            return true;
        }

        /// <summary>
        ///     Joining multiple commands and adding EOL
        /// </summary>
        private string ImapCommandBuilder(params string[] arg)
        {
            var result = string.Join(" ", arg);
            if (arg.LastOrDefault() != ImapCommandEol)
            {
                result += ImapCommandEol;
            }

            return result;
        }

        /// <summary>
        ///     Login to specified Imap host and port
        /// </summary>
        public void Login(string serverUrl, ushort port, string username, string password)
        {
            if (string.IsNullOrEmpty(serverUrl) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("null or emtpy input");
            }

            if (_isConnected && _isLoggedIn)
            {
                if (_serverUrl == serverUrl && _port == port)
                {
                    if (_username == username && _password == password)
                    {
                        // already connected and logged in
                        return;
                    }
                    LogOut();
                }
                else
                {
                    Disconnect();
                }
            }

            _isConnected = false;
            _isLoggedIn = false;


            if (Connect(serverUrl, port) == ImapResponseEnum.ImapSuccessResponse)
            {
                _isConnected = true;
            }
            else
            {
                _logger.Log(LogLevel.Fatal, "Unable to establish connection to imap server");
                return;
            }
            var resultArrayList = new ArrayList();

            var command = ImapCommandBuilder(ImapLoginCommand, username, password);
            try
            {
                if (SendAndReceive(command, ref resultArrayList) == ImapResponseEnum.ImapSuccessResponse)
                {
                    _isLoggedIn = true;
                    _username = username;
                    _password = password;
                }
                else
                {
                    throw new Exception("logging error");
                }
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, "Imap problem during login", e);
                throw;
            }
        }

        /// <summary>
        ///     Logout the user: It logout the user and disconnect the connetion from IMAP server.
        /// </summary>
        public void LogOut()
        {
            if (!_isLoggedIn) return;

            var asResultArray = new ArrayList();
            try
            {
                SendAndReceive(ImapCommandBuilder(ImapLogoutCommand), ref asResultArray);
            }
            catch (Exception e)
            {
                Disconnect();
                _isLoggedIn = false;
                _logger.LogException(LogLevel.Error, "Imap logout error", e);
                throw;
            }
            Disconnect();
            _isLoggedIn = false;
        }


        /// <summary>
        ///     Restore the connection using available old data
        ///     Select the sFolder if previously selected
        /// </summary>
        private void Restore(bool selectFolder)
        {
            if (string.IsNullOrEmpty(_serverUrl) || string.IsNullOrEmpty(_username) || string.IsNullOrEmpty(_password))
            {
                throw new Exception("invalid input");
            }

            _isLoggedIn = false;
            Login(_serverUrl, _port, _username, _password);
            if (selectFolder && !string.IsNullOrEmpty(_mailboxName))
            {
                if (_isFolderSelected)
                {
                    _isFolderSelected = false;
                    SelectFolder(_mailboxName);
                }
                else SelectFolder(_mailboxName);
            }
        }


        /// <summary>
        ///     Get the header for specific partNumber and Message UID
        /// </summary>
        /// <param name="sMessageUid">Unique Identifier of message</param>
        /// <param name="sPartNumber"> Message part number</param>
        /// <param name="asMessageHeader">Return array </param>
        private void GetHeader(string sMessageUid, string sPartNumber, ArrayList asMessageHeader)
        {
            asMessageHeader.Clear();
            var sCommandSuffix = "";
            if (sPartNumber == "0")
            {
                sCommandSuffix = sMessageUid + " " + "BODY[HEADER]";
            }
            else
            {
                sCommandSuffix = sMessageUid + " " + "BODY[" + sPartNumber + ".MIME]";
            }
            var sCommandString = ImapUidfetchCommand + " " + sCommandSuffix + ImapCommandEol;

            try
            {
                //-----------------------
                // SEND SEARCH REQUEST
                var asResultArray = new ArrayList();
                if (SendAndReceive(sCommandString, ref asResultArray) == ImapResponseEnum.ImapSuccessResponse)
                {
                    //-------------------------
                    // PARSE RESPONSE
                    var sKey = "";
                    var sValue = "";
                    var sLastLine = ImapServerResponseOk;
                    foreach (string sLine in asResultArray)
                    {
                        if (sLine.Length <= 0 ||
                            sLine.StartsWith(ImapUntaggedResponsePrefix) ||
                            sLine == ")")
                        {
                            continue;
                        }
                        if (sLine.StartsWith(sLastLine))
                        {
                            break;
                        }
                        var nPos = sLine.IndexOf(" ");
                        if (nPos != -1)
                        {
                            var sTmpLine = sLine.Substring(0, nPos);
                            nPos = sTmpLine.IndexOf(":");
                        }
                        if (nPos != -1)
                        {
                            if (sKey.Length > 0)
                            {
                                asMessageHeader.Add(sKey);
                                asMessageHeader.Add(sValue);
                            }
                            sKey = sLine.Substring(0, nPos).Trim();
                            sValue = sLine.Substring(nPos + 1).Trim();
                        }
                        else
                        {
                            sValue += sLine.Trim();
                        }
                    }
                    if (sKey.Length > 0)
                    {
                        asMessageHeader.Add(sKey);
                        asMessageHeader.Add(sValue);
                    }
                }
                else
                {
                    throw new Exception("Imap Fetch Error");
                }
                // throw new ImapException(ImapException.ImapErrorEnum.IMAP_ERR_FETCHMSG, sCommandSuffix);
            }
            catch (ImapDisconnectedException)
            {
                throw;
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, e, "Exception during receiving header from message (uuid: {0})", sMessageUid);
                LogOut();
                // logg
                throw;
            }
        }


        /// <summary>
        ///     Get the Size of the fetch command response
        ///     response will look like "{<size>}"
        /// </summary>
        /// <param name="sResponse"></param>
        /// <returns></returns>
        private long GetResponseSize(string sResponse)
        {
            // check if there is a size element
            // if not, then the message part number is wrong

            if (sResponse.IndexOf(ImapMessageNil) != -1)
            {
                //Size 0. No Message after this
                return 0L;
            }


            var nStart = sResponse.IndexOf('{');
            if (nStart == -1)
            {
                throw new Exception("Invalid size in Response " + sResponse + ".");
            }
            var nEnd = sResponse.IndexOf('}');
            if (nEnd == -1 || nEnd < nStart)
            {
                throw new Exception("Invalid size in Response " + sResponse + ".");
            }

            var sSize = sResponse.Substring(nStart + 1, nEnd - nStart - 1);
            var nSize = Convert.ToInt64(sSize, 10);

            if (nSize <= 0L)
            {
                throw new Exception("Invalid size in Response " + sResponse + ".");
            }

            return nSize;
        }

        /// <summary>
        ///     Imap server response result
        /// </summary>
        private enum ImapResponseEnum
        {
            /// <summary>
            ///     Imap Server responded "OK"
            /// </summary>
            ImapSuccessResponse,

            /// <summary>
            ///     Imap Server responded "NO" or "BAD"
            /// </summary>
            ImapFailureResponse,

            /// <summary>
            ///     Imap Server responded "*"
            /// </summary>
            ImapIgnoreResponse
        }

        #region constants

        /// <summary>
        ///     Imap Command Identifier value:Initial 0
        /// </summary>
        private static ushort _imapCommandVal;

        /// <summary>
        ///     Imap command Identified prefix: IMAP00
        /// </summary>
        private const string ImapCommandPrefix = "IMAP00";

        /// <summary>
        ///     Imap command identified which is combination of
        ///     Imap identifier prefix and val
        ///     eg. Prefix:IMAP00, Val: 1
        ///     Imap command Identified= IMAP001
        /// </summary>
        private string ImapCommandIdentifier
        {
            get { return ImapCommandPrefix + _imapCommandVal + " "; }
        }

        /// <summary>
        ///     Imap Server OK response which is combination of
        ///     Imap Identifier and Imap OK response.
        ///     eg. IMAP001 OK
        /// </summary>
        private string ImapServerResponseOk
        {
            get { return ImapCommandIdentifier + ImapOkResponse; }
        }

        /// <summary>
        ///     Imap Server NO response which is combination of
        ///     Imap Identifier and Imap NO response.
        ///     eg. IMAP001 NO
        /// </summary>
        private string ImapServerResponseNo
        {
            get { return ImapCommandIdentifier + ImapNoResponse; }
        }

        /// <summary>
        ///     Imap Server BAD response which is combination of
        ///     Imap Identifier and Imap BAD response.
        ///     eg. IMAP001 BAD
        /// </summary>
        private string ImapServerResponseBad
        {
            get { return ImapCommandIdentifier + ImapBadResponse; }
        }

        /// <summary>
        ///     Imap Untagged response prefix: *
        /// </summary>
        private const string ImapUntaggedResponsePrefix = "*";

        /// <summary>
        ///     Impa ok response : OK
        /// </summary>
        private const string ImapOkResponse = "OK";

        /// <summary>
        ///     Imap no response: NO
        /// </summary>
        private const string ImapNoResponse = "NO";

        /// <summary>
        ///     Imap bad response :BAD
        /// </summary>
        private const string ImapBadResponse = "BAD";

        /// <summary>
        ///     Imap bad server response : "* BAD"
        /// </summary>
        private const string ImapBadServerResponse = "* BAD";

        /// <summary>
        ///     Imap ok server response: "* OK"
        /// </summary>
        private const string ImapOkServerResponse = "* OK";

        /// <summary>
        ///     Imap Server response "* CAPABILITY"
        /// </summary>
        private const string ImapCapabilityServerResponse = "* CAPABILITY";

        /// <summary>
        ///     Imap capability command : CAPABILITY
        /// </summary>
        private const string ImapCapabilityCommand = "CAPABILITY";

        /// <summary>
        ///     Imap connect command :CONNECT
        /// </summary>
        private const string ImapConnectCommand = "CONNECT";

        /// <summary>
        ///     Imap login command : LOGIN userid  password
        /// </summary>
        private const string ImapLoginCommand = "LOGIN";

        /// <summary>
        ///     Imap logout command : LOGOUT
        /// </summary>
        private const string ImapLogoutCommand = "LOGOUT";

        /// <summary>
        ///     Imap select command : SELECT INBOX
        /// </summary>
        private const string ImapSelectCommand = "SELECT";


        /// <summary>
        ///     Imap uid fetch command : UID FETCH
        /// </summary>
        private const string ImapUidfetchCommand = "UID FETCH";


        /// <summary>
        ///     Imap command terminator: \r\n
        /// </summary>
        private const string ImapCommandEol = "\r\n";

        /// <summary>
        ///     Imap message nil size : NIL
        /// </summary>
        private const string ImapMessageNil = "NIL";

        /// <summary>
        ///     Imap header subject tag
        /// </summary>
        private const string ImapHeaderSubjectTag = "subject";

        #endregion

        #region properties

        private readonly ILogger _logger;

        /// <summary>
        ///     Imap host
        /// </summary>
        private string _serverUrl;

        /// <summary>
        ///     Imap port ( gmail 993)
        /// </summary>
        private ushort _port;

        /// <summary>
        ///     User id
        /// </summary>
        private string _username;

        /// <summary>
        ///     User Password
        /// </summary>
        private string _password;


        /// <summary>
        ///     Is Imap server connected
        /// </summary>
        private bool _isConnected;

        /// <summary>
        ///     Tcpclient object
        /// </summary>
        private TcpClient _tcpCli;

        /// <summary>
        ///     Network stream object
        /// </summary>
        private SslStream _sslStream;

        /// <summary>
        ///     StreamReader object
        /// </summary>
        private StreamReader _streamReader;

        /// <summary>
        ///     If user has logged in to his mailbox.
        /// </summary>
        private bool _isLoggedIn;

        /// <summary>
        ///     Mailbox (Folder) name. Default INBOX.
        /// </summary>
        private string _mailboxName = "INBOX";

        /// <summary>
        ///     If folder is selected.
        /// </summary>
        private bool _isFolderSelected;

        /// <summary>
        ///     Next UID (this should be gmail specific)
        /// </summary>
        public long NextMessageId { get { return _nextMessageId; } set { _nextMessageId = value; }}
        private long _nextMessageId = -1;

        #endregion

        #region public_interface

        /// <summary>
        ///     Select the folder/mailbox after login
        /// </summary>
        public void SelectFolder(string folder)
        {
            if (!_isLoggedIn)
            {
                Restore(false);
            }

            if (string.IsNullOrEmpty(folder))
            {
                throw new Exception("Invalid argument");
            }

            if (_isFolderSelected)
            {
                if (_mailboxName == folder)
                {
                    //Folder is already selected
                    return;
                }
                _isFolderSelected = false;
            }

            var asResultArray = new ArrayList();
            var sCommand = ImapSelectCommand + " " + folder + ImapCommandEol;

            if (SendAndReceive(sCommand, ref asResultArray) == ImapResponseEnum.ImapSuccessResponse)
            {
                _mailboxName = folder;
                _isFolderSelected = true;
            }
            else
            {
                throw new Exception();
            }


            //-------------------------
            // PARSE RESPONSE

            var result = false;
            foreach (string sLine in asResultArray)
            {
                // If this is an unsolicited response starting with '*'
                if (sLine.IndexOf(ImapUntaggedResponsePrefix) != -1)
                {
                    // parse the line by space
                    string[] asTokens;
                    asTokens = sLine.Split(' ');
                    if (asTokens[2] == "[UIDNEXT")
                    {
                        NextMessageId = Convert.ToInt64(asTokens[3].Substring(0, asTokens[3].Length - 1));
                    }
                }
                // If this line looks like "<command-tag> OK ..."
                else if (sLine.IndexOf(ImapServerResponseOk) != -1)
                {
                    result = true;
                    break;
                }
            }

            if (!result)
            {
                throw new Exception("Unable to select imap folder.");
            }
        }

        public bool GetHeaderParts(string messageUID, out string subject, out string sender)
        {
            subject = null;
            sender = null;

            var asMessageHeader = new ArrayList();

            if (!_isLoggedIn)
            {
                Restore(true);
            }
            if (!_isFolderSelected)
            {
                throw new Exception();
            }

            GetHeader(messageUID, "0", asMessageHeader);

            for (var i = 1; i < asMessageHeader.Count; i++)
            {
                if ((string) asMessageHeader[i - 1] == "Subject")
                {
                    subject = (string)asMessageHeader[i];
                    if(sender != null)
                        return true;
                }
                else if ((string)asMessageHeader[i - 1] == "From")
                {
                    sender = (string)asMessageHeader[i];
                    if(subject != null)
                        return true;
                }
            }

            return subject != null;
        }

        public string GetBody(long messageUID)
        {
            var bodyString = "";

            var sCommandSuffix = "";
            //TZ comment from code review (https://bitbucket.org/kgtinv/kgtpublic_utils_tomaszahradnik/commits/9bd9e2831f35ba38c9d230ba18ceab78e251483e#comment-3847988):
            // "this line is hack and I don't know why it works. 1 in body specially. I didn't completely understand structure of the body but part 1 seems to be text from body."
            sCommandSuffix = messageUID + " BODY[1]";
            var sCommandString = ImapUidfetchCommand + " " + sCommandSuffix + ImapCommandEol;


            //-----------------------
            // SEND SEARCH REQUEST
            var asResultArray = new ArrayList();

            if (SendAndReceiveByNumLines(sCommandString, ref asResultArray, 1) == ImapResponseEnum.ImapSuccessResponse)
            {
                //-------------------------
                // PARSE RESPONSE
                var sLastLine = ImapCommandIdentifier + " " + ImapOkResponse;
                var sLine = asResultArray[0].ToString();
                if (!sLine.StartsWith(ImapUntaggedResponsePrefix))
                {
                    throw new Exception("InValid Message Response " + sLine + ".");
                }
                var lMessageSize = GetResponseSize(sLine);
                if (lMessageSize == 0L)
                {
                    throw new Exception("InValid Message Response " + sLine + ".");
                }
                bodyString = "";
                ReceiveBuffer(ref bodyString, Convert.ToInt32(lMessageSize));
                if (bodyString.Length == 0)
                {
                    throw new Exception("Imap error fetch message");
                }
                //Convert.FromBase64String(sData).ToString();
                if (ReceiveByNumLines(ref asResultArray) != ImapResponseEnum.ImapSuccessResponse)
                {
                    throw new Exception("Imap error fetch message");
                }
            }
            else
            {
                throw new Exception("Imap error fetch message");
            }


            return bodyString;
        }

        #endregion
    }
}
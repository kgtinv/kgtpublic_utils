﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{

    public struct Symbol
    {
        //WARNING: account for default initialization!!!
        //   the _value is +1 offseted - so that the dafault unitialized 0 would result to -1 - which is NULL
        private readonly byte _value;
        private static readonly Dictionary<string, byte> _instanceNamesToIds = new Dictionary<string, byte>();
        private static readonly List<string> _instanceIdsToNames = new List<string>();

        private static readonly Dictionary<string, byte> _instanceShortNamesToIds = new Dictionary<string, byte>();
        private static readonly List<string> _instanceIdsToShortNames = new List<string>();

        private static readonly List<Currency> _baseCurrencies = new List<Currency>();
        private static readonly List<Currency> _termCurrencies = new List<Currency>();
        private static readonly List<Symbol> _allSymbols = new List<Symbol>();

        public static readonly Symbol NULL = new Symbol("NULLSymbol");

        public static readonly Symbol AUD_CAD = new Symbol(@"AUD/CAD");
        public static readonly Symbol AUD_CHF = new Symbol(@"AUD/CHF");
        public static readonly Symbol AUD_DKK = new Symbol(@"AUD/DKK");
        public static readonly Symbol AUD_HKD = new Symbol(@"AUD/HKD");
        public static readonly Symbol AUD_JPY = new Symbol(@"AUD/JPY");
        public static readonly Symbol AUD_NOK = new Symbol(@"AUD/NOK");
        public static readonly Symbol AUD_NZD = new Symbol(@"AUD/NZD");
        public static readonly Symbol AUD_SEK = new Symbol(@"AUD/SEK");
        public static readonly Symbol AUD_SGD = new Symbol(@"AUD/SGD");
        public static readonly Symbol AUD_USD = new Symbol(@"AUD/USD");
        public static readonly Symbol CAD_CHF = new Symbol(@"CAD/CHF");
        public static readonly Symbol CAD_DKK = new Symbol(@"CAD/DKK");
        public static readonly Symbol CAD_JPY = new Symbol(@"CAD/JPY");
        public static readonly Symbol CAD_NOK = new Symbol(@"CAD/NOK");
        public static readonly Symbol CAD_NZD = new Symbol(@"CAD/NZD");
        public static readonly Symbol CAD_SEK = new Symbol(@"CAD/SEK");
        public static readonly Symbol CHF_DKK = new Symbol(@"CHF/DKK");
        public static readonly Symbol CHF_JPY = new Symbol(@"CHF/JPY");
        public static readonly Symbol CHF_NOK = new Symbol(@"CHF/NOK");
        public static readonly Symbol CHF_SEK = new Symbol(@"CHF/SEK");
        public static readonly Symbol DKK_JPY = new Symbol(@"DKK/JPY");
        public static readonly Symbol DKK_NOK = new Symbol(@"DKK/NOK");
        public static readonly Symbol DKK_SEK = new Symbol(@"DKK/SEK");
        public static readonly Symbol EUR_AUD = new Symbol(@"EUR/AUD");
        public static readonly Symbol EUR_CAD = new Symbol(@"EUR/CAD");
        public static readonly Symbol EUR_CHF = new Symbol(@"EUR/CHF");
        public static readonly Symbol EUR_CNH = new Symbol(@"EUR/CNH");
        public static readonly Symbol EUR_CZK = new Symbol(@"EUR/CZK");
        public static readonly Symbol EUR_DKK = new Symbol(@"EUR/DKK");
        public static readonly Symbol EUR_GBP = new Symbol(@"EUR/GBP");
        public static readonly Symbol EUR_HKD = new Symbol(@"EUR/HKD");
        public static readonly Symbol EUR_HUF = new Symbol(@"EUR/HUF");
        public static readonly Symbol EUR_JPY = new Symbol(@"EUR/JPY");
        public static readonly Symbol EUR_MXN = new Symbol(@"EUR/MXN");
        public static readonly Symbol EUR_NOK = new Symbol(@"EUR/NOK");
        public static readonly Symbol EUR_NZD = new Symbol(@"EUR/NZD");
        public static readonly Symbol EUR_PLN = new Symbol(@"EUR/PLN");
        public static readonly Symbol EUR_RUB = new Symbol(@"EUR/RUB");
        public static readonly Symbol EUR_SEK = new Symbol(@"EUR/SEK");
        public static readonly Symbol EUR_TRY = new Symbol(@"EUR/TRY");
        public static readonly Symbol EUR_USD = new Symbol(@"EUR/USD");
        public static readonly Symbol EUR_ZAR = new Symbol(@"EUR/ZAR");
        public static readonly Symbol GBP_AUD = new Symbol(@"GBP/AUD");
        public static readonly Symbol GBP_CAD = new Symbol(@"GBP/CAD");
        public static readonly Symbol GBP_CHF = new Symbol(@"GBP/CHF");
        public static readonly Symbol GBP_CZK = new Symbol(@"GBP/CZK");
        public static readonly Symbol GBP_DKK = new Symbol(@"GBP/DKK");
        public static readonly Symbol GBP_HUF = new Symbol(@"GBP/HUF");
        public static readonly Symbol GBP_JPY = new Symbol(@"GBP/JPY");
        public static readonly Symbol GBP_NOK = new Symbol(@"GBP/NOK");
        public static readonly Symbol GBP_NZD = new Symbol(@"GBP/NZD");
        public static readonly Symbol GBP_PLN = new Symbol(@"GBP/PLN");
        public static readonly Symbol GBP_SEK = new Symbol(@"GBP/SEK");
        public static readonly Symbol GBP_USD = new Symbol(@"GBP/USD");
        public static readonly Symbol HKD_JPY = new Symbol(@"HKD/JPY");
        public static readonly Symbol MXN_JPY = new Symbol(@"MXN/JPY");
        public static readonly Symbol NOK_JPY = new Symbol(@"NOK/JPY");
        public static readonly Symbol NOK_SEK = new Symbol(@"NOK/SEK");
        public static readonly Symbol NZD_CAD = new Symbol(@"NZD/CAD");
        public static readonly Symbol NZD_CHF = new Symbol(@"NZD/CHF");
        public static readonly Symbol NZD_DKK = new Symbol(@"NZD/DKK");
        public static readonly Symbol NZD_JPY = new Symbol(@"NZD/JPY");
        public static readonly Symbol NZD_NOK = new Symbol(@"NZD/NOK");
        public static readonly Symbol NZD_SEK = new Symbol(@"NZD/SEK");
        public static readonly Symbol NZD_SGD = new Symbol(@"NZD/SGD");
        public static readonly Symbol NZD_USD = new Symbol(@"NZD/USD");
        public static readonly Symbol SGD_JPY = new Symbol(@"SGD/JPY");
        public static readonly Symbol USD_CAD = new Symbol(@"USD/CAD");
        public static readonly Symbol USD_CHF = new Symbol(@"USD/CHF");
        public static readonly Symbol USD_CNH = new Symbol(@"USD/CNH");
        public static readonly Symbol USD_CZK = new Symbol(@"USD/CZK");
        public static readonly Symbol USD_DKK = new Symbol(@"USD/DKK");
        public static readonly Symbol USD_HKD = new Symbol(@"USD/HKD");
        public static readonly Symbol USD_HUF = new Symbol(@"USD/HUF");
        public static readonly Symbol USD_ILS = new Symbol(@"USD/ILS");
        public static readonly Symbol USD_JPY = new Symbol(@"USD/JPY");
        public static readonly Symbol USD_MXN = new Symbol(@"USD/MXN");
        public static readonly Symbol USD_NOK = new Symbol(@"USD/NOK");
        public static readonly Symbol USD_PLN = new Symbol(@"USD/PLN");
        public static readonly Symbol USD_RUB = new Symbol(@"USD/RUB");
        public static readonly Symbol USD_SEK = new Symbol(@"USD/SEK");
        public static readonly Symbol USD_SGD = new Symbol(@"USD/SGD");
        public static readonly Symbol USD_TRY = new Symbol(@"USD/TRY");
        public static readonly Symbol USD_ZAR = new Symbol(@"USD/ZAR");
        public static readonly Symbol ZAR_JPY = new Symbol(@"ZAR/JPY");
        public static readonly Symbol EUR_SGD = new Symbol(@"EUR/SGD");
        public static readonly Symbol GBP_HKD = new Symbol(@"GBP/HKD");
        public static readonly Symbol GBP_MXN = new Symbol(@"GBP/MXN");
        public static readonly Symbol GBP_SGD = new Symbol(@"GBP/SGD");
        public static readonly Symbol GBP_TRY = new Symbol(@"GBP/TRY");
        public static readonly Symbol GBP_ZAR = new Symbol(@"GBP/ZAR");
        public static readonly Symbol CHF_MXN = new Symbol(@"CHF/MXN");
        public static readonly Symbol USD_INR = new Symbol(@"USD/INR");
        public static readonly Symbol AUD_ZAR = new Symbol(@"AUD/ZAR");
        public static readonly Symbol CAD_HKD = new Symbol(@"CAD/HKD");
        public static readonly Symbol CAD_MXN = new Symbol(@"CAD/MXN");
        public static readonly Symbol CAD_PLN = new Symbol(@"CAD/PLN");
        public static readonly Symbol CAD_SGD = new Symbol(@"CAD/SGD");
        public static readonly Symbol CAD_ZAR = new Symbol(@"CAD/ZAR");
        public static readonly Symbol CZK_JPY = new Symbol(@"CZK/JPY");
        public static readonly Symbol DKK_HKD = new Symbol(@"DKK/HKD");
        public static readonly Symbol EUR_ILS = new Symbol(@"EUR/ILS");
        public static readonly Symbol GBP_ILS = new Symbol(@"GBP/ILS");
        public static readonly Symbol HUF_JPY = new Symbol(@"HUF/JPY");
        public static readonly Symbol CHF_CZK = new Symbol(@"CHF/CZK");
        public static readonly Symbol CHF_HKD = new Symbol(@"CHF/HKD");
        public static readonly Symbol CHF_HUF = new Symbol(@"CHF/HUF");
        public static readonly Symbol CHF_ILS = new Symbol(@"CHF/ILS");
        public static readonly Symbol CHF_PLN = new Symbol(@"CHF/PLN");
        public static readonly Symbol CHF_SGD = new Symbol(@"CHF/SGD");
        public static readonly Symbol CHF_TRY = new Symbol(@"CHF/TRY");
        public static readonly Symbol CHF_ZAR = new Symbol(@"CHF/ZAR");
        public static readonly Symbol NOK_HKD = new Symbol(@"NOK/HKD");
        public static readonly Symbol NZD_HKD = new Symbol(@"NZD/HKD");
        public static readonly Symbol NZD_PLN = new Symbol(@"NZD/PLN");
        public static readonly Symbol NZD_ZAR = new Symbol(@"NZD/ZAR");
        public static readonly Symbol PLN_HUF = new Symbol(@"PLN/HUF");
        public static readonly Symbol PLN_JPY = new Symbol(@"PLN/JPY");
        public static readonly Symbol SEK_HKD = new Symbol(@"SEK/HKD");
        public static readonly Symbol SEK_JPY = new Symbol(@"SEK/JPY");
        public static readonly Symbol SGD_DKK = new Symbol(@"SGD/DKK");
        public static readonly Symbol SGD_HKD = new Symbol(@"SGD/HKD");
        public static readonly Symbol SGD_MXN = new Symbol(@"SGD/MXN");
        public static readonly Symbol SGD_NOK = new Symbol(@"SGD/NOK");
        public static readonly Symbol SGD_SEK = new Symbol(@"SGD/SEK");
        public static readonly Symbol TRY_JPY = new Symbol(@"TRY/JPY");
        public static readonly Symbol USD_GHS = new Symbol(@"USD/GHS");
        public static readonly Symbol USD_KES = new Symbol(@"USD/KES");
        public static readonly Symbol USD_NGN = new Symbol(@"USD/NGN");
        public static readonly Symbol USD_RON = new Symbol(@"USD/RON");
        public static readonly Symbol USD_UGX = new Symbol(@"USD/UGX");
        public static readonly Symbol USD_ZMK = new Symbol(@"USD/ZMK");
        public static readonly Symbol ZAR_MXN = new Symbol(@"ZAR/MXN");
        public static readonly Symbol USD_THB = new Symbol(@"USD/THB");
        public static readonly Symbol USD_XVN = new Symbol(@"USD/XVN");
        public static readonly Symbol FTS_GBP = new Symbol(@"FTS/GBP");
        public static readonly Symbol DJI_USD = new Symbol(@"DJI/USD");
        public static readonly Symbol SPX_USD = new Symbol(@"SPX/USD");
        public static readonly Symbol SPY_USD = new Symbol(@"SPY/USD");
        public static readonly Symbol NDX_USD = new Symbol(@"NDX/USD");
        public static readonly Symbol DAX_EUR = new Symbol(@"DAX/EUR");
        public static readonly Symbol DXY_EUR = new Symbol(@"DXY/EUR");
        public static readonly Symbol CAC_EUR = new Symbol(@"CAC/EUR");
        public static readonly Symbol STX_EUR = new Symbol(@"STX/EUR");
        public static readonly Symbol NIK_JPY = new Symbol(@"NIK/JPY");
        public static readonly Symbol AUS_AUD = new Symbol(@"AUS/AUD");
        public static readonly Symbol XAU_USD = new Symbol(@"XAU/USD");
        public static readonly Symbol XAG_USD = new Symbol(@"XAG/USD");
        public static readonly Symbol XAU_AUD = new Symbol(@"XAU/AUD");
        public static readonly Symbol XAG_AUD = new Symbol(@"XAG/AUD");
        public static readonly Symbol WTI_USD = new Symbol(@"WTI/USD");
        public static readonly Symbol BRE_USD = new Symbol(@"BRE/USD");
        public static readonly Symbol XPT_USD = new Symbol(@"XPT/USD");
        public static readonly Symbol XPD_USD = new Symbol(@"XPD/USD");
        public static readonly Symbol XAU_EUR = new Symbol(@"XAU/EUR");
        public static readonly Symbol GAS_USD = new Symbol(@"GAS/USD");
        public static readonly Symbol YAU_USD = new Symbol(@"YAU/USD");
        public static readonly Symbol YAG_USD = new Symbol(@"YAG/USD");
        public static readonly Symbol GLD_CNH = new Symbol(@"GLD/CNH");
        public static readonly Symbol SLV_CNH = new Symbol(@"SLV/CNH");
        public static readonly Symbol NDY_USD = new Symbol(@"NDY/USD");
        public static readonly Symbol DJY_USD = new Symbol(@"DJY/USD");
        public static readonly Symbol HSI_HKD = new Symbol(@"HSI/HKD");
        public static readonly Symbol IBX_EUR = new Symbol(@"IBX/EUR");
        public static readonly Symbol EUR_RON = new Symbol(@"EUR/RON");
        public static readonly Symbol XBT_USD = new Symbol(@"XBT/USD");
        public static readonly Symbol XET_USD = new Symbol(@"XET/USD");


        private Symbol(string name, string shortName)
        {
            if (_instanceIdsToNames.Count > byte.MaxValue)
                throw new ArgumentOutOfRangeException(
                    string.Format(
                        "Attempt to create symbol [{0}] that is already over the range of underlying byte storage", name));

            this._value = (byte) _instanceIdsToNames.Count;
            if (_value > 0)
            {
                _instanceNamesToIds.Add(name, this._value);
                _instanceShortNamesToIds.Add(shortName, this._value);
                _baseCurrencies.Add((Currency)name.Split('/')[0]);
                _termCurrencies.Add((Currency)name.Split('/')[1]);
                _allSymbols.Add(this);
            }

            _instanceIdsToNames.Add(name);            
            _instanceIdsToShortNames.Add(shortName);
        }

        private Symbol(string name)
            : this(name, name.Replace(@"/", string.Empty))
        { }

        private Symbol(byte value)
        {
            this._value = value;
        }

        public Currency BaseCurrency { get { return _baseCurrencies[this._value - 1]; } }
        public Currency TermCurrency { get { return _termCurrencies[this._value - 1]; } }

        public static bool TryGetSymbol(Currency baseCurrency, Currency quoteCurrency, out Symbol symbol)
        {
            return TryParse(baseCurrency.ToString() + quoteCurrency.ToString(), out symbol);
        }

        public static explicit operator Symbol(string name)
        {
            Symbol result;
            if (!TryParse(name, out result))
            {
                throw new InvalidCastException("Unknown symbol value: " + name);
            }

            return result;
        }

        public static IEnumerable<Currency> UsedCurrencies
        {
            get
            {
                return Symbol.Values.Select(s => s.BaseCurrency)
                             .Union(Symbol.Values.Select(s => s.TermCurrency));
            }
        }

        public static bool TryParse(string name, out Symbol symbol)
        {
            byte code;
            if (_instanceNamesToIds.TryGetValue(name, out code))
            {
                symbol = new Symbol(code);
                return true;
            }
            else if (_instanceShortNamesToIds.TryGetValue(name, out code))
            {
                symbol = new Symbol(code);
                return true;
            }
            else
            {
                symbol = Symbol.NULL;
                return false;
            }
        }

        public static explicit operator Symbol(int value)
        {
            if (_instanceIdsToNames.Count-1 > value)
            {
                return new Symbol((byte) (value + 1));
            }
            else
            {
                throw new InvalidCastException("Unknown Symbol integer value: " + value);
            }
        }

        public static explicit operator String(Symbol symbol)
        {
            return _instanceIdsToNames[symbol._value];
        }

        public static explicit operator int(Symbol symbol)
        {
            return symbol._value - 1;
        }

        public static IEnumerable<string> StringValues
        {
            get { return _instanceNamesToIds.Keys; }
        }

        public static IEnumerable<Symbol> Values
        {
            get { return _allSymbols; }
        }

        public static IEnumerable<Symbol> AllSymbols
        {
            get { return _allSymbols; }
        }

        public static int ValuesCount
        {
            get { return _instanceIdsToNames.Count - 1; }
        }

        public string ShortName
        {
            get { return _instanceIdsToShortNames[this._value]; }
        }

        public override string ToString()
        {
            return _instanceIdsToNames[this._value];
        }

        //Equality members should not be needed - as there are just those predefined instances
        // So default instance equality is sufficient and faster

        //private bool Equals(Symbol other)
        //{
        //    return value == other.value;
        //}

        //public override int GetHashCode()
        //{
        //    return value;
        //}

        //public static bool operator ==(Symbol left, Symbol right)
        //{
        //    return Equals(left, right);
        //}

        //public static bool operator !=(Symbol left, Symbol right)
        //{
        //    return !Equals(left, right);
        //}

        //public override bool Equals(object obj)
        //{
        //    if (ReferenceEquals(null, obj)) return false;
        //    if (ReferenceEquals(this, obj)) return true;
        //    return obj is Symbol && Equals((Symbol) obj);
        //}

        public static bool operator ==(Symbol a, Symbol b)
        {
            return a._value == b._value;
        }

        [Obsolete("Comparison to null is disallowed, compare to Symbol.NULL instead", true)]
        public static bool operator ==(Symbol a, Symbol? b)
        {
            throw new NotImplementedException();
        }

        [Obsolete("Comparison to null is disallowed, compare to Symbol.NULL instead", true)]
        public static bool operator !=(Symbol a, Symbol? b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(Symbol a, Symbol b)
        {
            return a._value != b._value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Symbol))
                return false;

            Symbol curr = (Symbol)obj;
            return curr._value == this._value;
        }

        public override int GetHashCode()
        {
            return this._value;
        }
    }

}

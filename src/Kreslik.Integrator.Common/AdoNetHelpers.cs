﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class AdoNetHelpers
    {
        public static void LogProcedureException(ILogger logger, Exception e, string operationDescription, bool logToConsole, string suffix = null)
        {
            if (IsRecoverableSqlException(e))
            {
                string msg = $"Got recoverable sql Exception or timeout when executing {operationDescription}.{suffix}";
                logger.LogConditionalConsoleException(LogLevel.Error, logToConsole, e, msg);
            }
            else
            {
                logger.LogException(LogLevel.Fatal, e,
                                         "Got unexpected (likely unrecoverable) Exception when executing {0}." + suffix, operationDescription);
            }
        }

        //SELECT * FROM master.sys.messages WHERE language_id = 1033 AND text LIKE '%<...>%'
        private static readonly int[] _RETRIABLE_SQL_ERROR_NUMBERS = new[]
        {
            //Connection Broken
            -1,
            //Connection Timeout
            -2,
            //Sql out of memory
            701,
            //Time-out occurred while waiting for buffer latch
            844, 845, 846, 847,
            //Deadlock
            1204,
            //Deadlock
            1205,
            //Lock timeout
            1222,
            //The marked transaction "%.*ls" failed. A timeout occurred while attempting to place a mark in the log by committing the marked transaction. This can be caused by contention with Microsoft Distributed Transaction Coordinator (MS DTC) transactions or other local marked transaction that have prepared, but not committed or aborted. Try the operation again and if the error persists, determine the source of the contention.
            3928,
            //Bcp timed out
            4876,
            //Lock timeout
            5245,
            //waiting for resource timeout
            8645, 8675,
            //low memory condition
            8651,
            //login timeout
            17197,
            //network timeout
            17830,
            //word breaker timeout
            30053
        };

        private static readonly int[] _DUPLICATE_KEY_SQL_ERROR_NUMBERS = new[]
        {
            //Cannot insert duplicate key row in object '%.*ls' with unique index '%.*ls'. The duplicate key value is %ls.
            2601,
            //Violation of %ls constraint '%.*ls'. Cannot insert duplicate key in object '%.*ls'. The duplicate key value is %ls.
            2627
        };

        //https://stackoverflow.com/questions/24041062/know-when-to-retry-or-fail-when-calling-sql-server-from-c
        //https://docs.microsoft.com/en-us/dotnet/api/system.data.sqlclient.sqlerror.class?view=netframework-4.8#System_Data_SqlClient_SqlError_Class
        //16 applies to string or binary data would be truncated as well
        private static readonly int[] _RETRIABLE_SQL_ERROR_CLASSES = { 13, 17, 18, 19, 20, 21, 22, 24 };

        public static bool IsRecoverableSqlException(Exception e)
        {
            if (e is SqlException)
            {
                SqlException sqlEx = e as SqlException;

                return sqlEx.Errors.Cast<SqlError>().All(CanRetry);
            }

            return e.Message.Contains("Timeout expired");
        }

        public static bool IsDuplicateKeySqlException(Exception e)
        {
            if (e is SqlException)
            {
                SqlException sqlEx = e as SqlException;

                return sqlEx.Errors.Cast<SqlError>().Any(error => _DUPLICATE_KEY_SQL_ERROR_NUMBERS.Contains(error.Number));
            }

            return e.Message.Contains("Cannot insert duplicate");
        }

        public static bool IsSevereError(Exception e)
        {
            return e.Message.Contains("A severe error occurred on the current command");
        }

        private static bool CanRetry(SqlError error)
        {
            // Handle well-known error codes:
            //
            // Use this switch/if if you want to handle only well-known errors,
            // remove it if you want to always retry. A "blacklist" approach may
            // also work: return false when you're sure you can't recover from one
            // error and rely on Class for anything else.
            if (_RETRIABLE_SQL_ERROR_NUMBERS.Contains(error.Number))
            {
                return true;
            }

            // Handle unknown errors with severity 21 or less. 22 or more
            // indicates a serious error that need to be manually fixed.
            // 24 indicates media errors. They're serious errors (that should
            // be also notified) but we may retry...
            return _RETRIABLE_SQL_ERROR_CLASSES.Contains(error.Class);
        }

        private static TimeSpan[] _retryIntervals = new TimeSpan[] { TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(60), TimeSpan.FromMinutes(60), TimeSpan.FromHours(2), TimeSpan.FromHours(6) };

        public const int DB_RETRY_COUNT_FOR_WEEK = 30;
        public const int DB_RETRY_COUNT_FOR_4HOURS = 9;
        public static readonly int? DB_RETRY_COUNT_FOREWER = null;

        //We need non-anonymous method - as yield return is not allowed in anonymous method
        private static IEnumerable<T> NonRetriableSqlReaderOperationInternal<T>(
            SqlCommand getDataCommand,
            Func<SqlDataReader, T> dataPointFactory)
        {
            //commandString = getDataCommand.GetSpExecuteString();
            using (SqlDataReader sqlDataReader = getDataCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    yield return dataPointFactory(sqlDataReader);
                }
            }
        }

        public class RetriableSqlOperationHelper
        {
            private readonly ILogger _logger;
            private readonly int? _maxRetries;
            private int _attempts = 0;

            public bool RetryOnNonRecoverableExceptions { get; }
            public bool LogRetriesToConsole { get; set; }

            public RetriableSqlOperationHelper(ILogger logger, int? maxRetries, bool retryOnNonRecoverableExceptions)
            {
                _logger = logger;
                _maxRetries = maxRetries;
                RetryOnNonRecoverableExceptions = retryOnNonRecoverableExceptions;
            }

            public TimeSpan MaxRetryWaitTime { get; set; } = _retryIntervals.Last();

            public void ClearAttemptsCounter()
            {
                _attempts = 0;
            }

            public bool LogExceptionAndCheckIfAndWhenToContinue(Exception thrownException, string operationDescription)
            {
                bool @continue;

                if (thrownException != null)
                {
                    _attempts++;

                    @continue =
                        //are we allowed to recover from this error
                        (RetryOnNonRecoverableExceptions || IsRecoverableSqlException(thrownException))
                        &&
                        //do we have retries remaining
                        (!_maxRetries.HasValue || _attempts < _maxRetries)
                        &&
                        //If we already failed once (the attempts member is already incremented here) 
                        // and this is attempt to insert duplicate record - the first attempt might have actually succeeded
                        //Same if it's a severe error - no need to retry
                        !((IsDuplicateKeySqlException(thrownException) || IsSevereError(thrownException)) && _attempts > 1);

                    TimeSpan retryInterval = TimeSpan.Zero;

                    //This was attempt #x, will not retry anymore
                    //This was attempt #x, will retry after z
                    string suffix = $"This was attempt #{_attempts}, will ";
                    if (@continue)
                    {
                        retryInterval = TimeSpanEx.Min(MaxRetryWaitTime,
                            _retryIntervals[Math.Min(_attempts - 1, _retryIntervals.Length - 1)]);
                        suffix += $"retry after {retryInterval}";
                    }
                    else
                    {
                        suffix += "not retry anymore.";
                    }

                    LogProcedureException(_logger, thrownException, operationDescription, LogRetriesToConsole, suffix);

                    ExceptionHit?.Invoke(thrownException);

                    //zero sleep is harmless even if we don't want to wait
                    System.Threading.Thread.Sleep(retryInterval);
                }
                else
                {
                    @continue = false;
                }

                return @continue;
            }

            public event Action<Exception> ExceptionHit;

            public T PerformRetriableFunc<T>(Func<T> func, string operationDescription)
            {
                _attempts = 0;
                do
                {
                    try
                    {
                        return func();
                    }
                    catch (Exception e)
                    {
                        if (!this.LogExceptionAndCheckIfAndWhenToContinue(e, operationDescription))
                        {
                            throw;
                        }
                    }
                } while (true);
            }

            public void PerformRetriableAction(Action action, string operationDescription)
            {
                PerformRetriableFunc(() => { action(); return true;}, operationDescription);
            }
        }

        public static IEnumerable<T> RetriableSqlReaderOperation<T>(
            Func<SqlCommand> commandBuilder,
            Func<SqlDataReader, T> dataPointFactory,
            string operationName, ILogger logger, int? maxRetries, bool retryOnNonSqlErrors)
        {
            RetriableSqlOperationHelper retriableSqlOperationHelper = new RetriableSqlOperationHelper(
                logger, maxRetries, retryOnNonSqlErrors);

            bool @continue = true;
            int attempts = 0;
            do
            {
                Exception thrownException = null;
                IEnumerator<T> dataEnumerator = null;
                SqlCommand getDataCommand = null;
                try
                {
                    getDataCommand = commandBuilder();
                    dataEnumerator =
                        NonRetriableSqlReaderOperationInternal<T>(getDataCommand, dataPointFactory).GetEnumerator();
                }
                catch (Exception e)
                {
                    thrownException = e;
                }

                if (thrownException == null)
                {
                    using (dataEnumerator)
                    {
                        while (true)
                        {
                            T ret;
                            try
                            {
                                if (dataEnumerator.MoveNext())
                                {
                                    break;
                                }
                                ret = dataEnumerator.Current;
                            }
                            catch (Exception e)
                            {
                                thrownException = e;
                                break;
                            }
                            yield return ret;
                        }
                    }
                }

                if (thrownException != null)
                {
                    string completeOperationName = operationName + ". Command (attempted to be) executed: " +
                                                   getDataCommand.GetSpExecuteString();
                    @continue = retriableSqlOperationHelper.LogExceptionAndCheckIfAndWhenToContinue(
                        thrownException, completeOperationName);
                }
                else
                {
                    @continue = false;
                }

                getDataCommand?.Dispose();

            } while (@continue);
        }

        public static bool RetrySqlOperation(
            Action sqlOperation, string operationName, ILogger logger, int? maxRetries, bool retryOnNonSqlErrors)
        {
            RetriableSqlOperationHelper retriableSqlOperationHelper = new RetriableSqlOperationHelper(
                logger, maxRetries, retryOnNonSqlErrors)
            {
                MaxRetryWaitTime = TimeSpan.FromMinutes(10)
            };

            bool @continue = true;
            int attempts = 0;
            do
            {
                try
                {
                    logger.Log(LogLevel.Trace, "Running retriable sql operation: " + operationName);
                    sqlOperation();
                    return true;
                }
                catch (Exception e)
                {
                    @continue = retriableSqlOperationHelper.LogExceptionAndCheckIfAndWhenToContinue(
                        e, operationName);
                }

            } while (@continue);

            return false;
        }
    }
}

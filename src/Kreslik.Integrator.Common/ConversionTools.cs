﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class ConversionTools
    {
        // 128 bits precision (decimal):

        public static decimal ConvertPpmToDecimal(decimal ppmValue, decimal refPrice)
        {
            return ppmValue * refPrice / Constants.OneMillionDecimal;
        }

        public static decimal ConvertBasisPointsToPpm(decimal basisPointsValue)
        {
            return basisPointsValue * Constants.OneHundredDecimal;
        }

        public static decimal ConvertPpmToBasisPoints(decimal ppmValue)
        {
            return ppmValue / Constants.OneHundredDecimal;
        }

        public static decimal ConvertBasisPointsToDecimal(decimal basisPointsValue, decimal refPrice)
        {
            return basisPointsValue * refPrice / Constants.TenThousandDecimal;
        }

        public static decimal ConvertDecimalToPpm(decimal decimalVal, decimal refPrice)
        {
            return Constants.OneMillionDecimal * decimalVal / refPrice;
        }

        public static decimal ConvertDecimalToPips(decimal decimalVal, int pointMultiplier)
        {
            return decimalVal * pointMultiplier;
        }

        public static decimal ConvertDecimalToBasisPoints(decimal decimalVal, decimal refPrice)
        {
            return Constants.TenThousandDecimal * decimalVal / refPrice;
        }

        public static decimal ConvertPpmToPips(decimal ppmValue, decimal refPrice, int pointMultiplier)
        {
            return ppmValue * refPrice * pointMultiplier / Constants.OneMillionDecimal;
        }

        public static decimal ConvertPipsToPpm(decimal pipsValue, int pointMultiplier, decimal refPrice)
        {
            return Constants.OneMillionDecimal * pipsValue / pointMultiplier / refPrice;
        }

        public static decimal ConvertPipsToBasisPoints(decimal pipsValue, int pointMultiplier, decimal refPrice)
        {
            return Constants.TenThousandDecimal * pipsValue / pointMultiplier / refPrice;
        }

        public static decimal ConvertPipsToDecimal(decimal pipsValue, int pointMultiplier)
        {
            return pipsValue / pointMultiplier;
        }

        public static decimal ConvertBasisPointsToPips(decimal basisPointsValue, decimal refPrice, int pointMultiplier)
        {
            return basisPointsValue * refPrice * pointMultiplier / Constants.TenThousandDecimal;
        }

        // 64 bits precision (double):

        public static double ConvertPpmToDecimal(double ppmValue, double refPrice)
        {
            return ppmValue * refPrice / Constants.OneMillionDouble;
        }

        public static double ConvertBasisPointsToPpm(double basisPointsValue)
        {
            return basisPointsValue * Constants.OneHundredDouble;
        }

        public static double ConvertPpmToBasisPoints(double ppmValue)
        {
            return ppmValue / Constants.OneHundredDouble;
        }

        public static double ConvertBasisPointsToDecimal(double basisPointsValue, double refPrice)
        {
            return basisPointsValue * refPrice / Constants.TenThousandDouble;
        }

        public static double ConvertDecimalToPpm(double decimalVal, double refPrice)
        {
            return Constants.OneMillionDouble * decimalVal / refPrice;
        }

        public static double ConvertDecimalToPips(double decimalVal, int pointMultiplier)
        {
            return decimalVal * pointMultiplier;
        }

        public static double ConvertDecimalToBasisPoints(double decimalVal, double refPrice)
        {
            return Constants.TenThousandDouble * decimalVal / refPrice;
        }

        public static double ConvertPpmToPips(double ppmValue, double refPrice, int pointMultiplier)
        {
            return ppmValue * refPrice * pointMultiplier / Constants.OneMillionDouble;
        }

        public static double ConvertPipsToPpm(double pipsValue, int pointMultiplier, double refPrice)
        {
            return Constants.OneMillionDouble * pipsValue / pointMultiplier / refPrice;
        }

        public static double ConvertPipsToBasisPoints(double pipsValue, int pointMultiplier, double refPrice)
        {
            return Constants.TenThousandDouble * pipsValue / pointMultiplier / refPrice;
        }

        public static double ConvertPipsToDecimal(double pipsValue, int pointMultiplier)
        {
            return pipsValue / pointMultiplier;
        }

        public static double ConvertBasisPointsToPips(double basisPointsValue, double refPrice, int pointMultiplier)
        {
            return basisPointsValue * refPrice * pointMultiplier / Constants.TenThousandDouble;
        }
    }
}

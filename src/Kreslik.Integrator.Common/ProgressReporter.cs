﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class ProgressReporter
    {
        private readonly ILogger _logger;
        private readonly string _namer;
        private readonly int _recordsCount;
        private readonly TimeSpan _minimumTimeBetweenProgressReports;
        private DateTime _nextProgressReportUtc;
        public bool LogToConsole { get { return _recordsCount < 0 || _recordsCount > 1000; } }
        public int RecordsProcessedSoFar { get; private set; }

        public ProgressReporter(ILogger logger, string namer, int recordsCount)
        {
            _logger = logger;
            _namer = namer;
            _recordsCount = recordsCount;
            _minimumTimeBetweenProgressReports = TimeSpan.FromSeconds(5);
            _nextProgressReportUtc = DateTime.UtcNow + _minimumTimeBetweenProgressReports;

            logger.LogConditionalConsoleInfo(LogToConsole,
                "{0}: {1} Starting to process dataset. Records count: {2}", namer,
                DateTime.UtcNow.ToLongStringWithFractions(),
                _recordsCount < 0 ? "Unknown" : _recordsCount.ToString());
        }

        public void NextRecordProcessed()
        {
            IncrementRecordsProcessed();

            if (IsTimeForNextProgressReport)
            {
                FlushStats(_recordsCount > 0 ? Math.Min(1, ((float)RecordsProcessedSoFar / Math.Max(1, _recordsCount))).ToString("P2") : "Unknown");
            }
        }

        protected void IncrementRecordsProcessed() { RecordsProcessedSoFar++; }
        protected bool IsTimeForNextProgressReport => DateTime.UtcNow >= _nextProgressReportUtc;

        protected void FlushStats(string progressString)
        {
            _nextProgressReportUtc = DateTime.UtcNow + _minimumTimeBetweenProgressReports;
            _logger.LogConditionalConsoleInfo(LogToConsole, "{0}: {1} records processed: {2}. (Estimated portion of dataset: {3})",
                    _namer,
                    DateTime.UtcNow.ToLongStringWithFractions(),
                    RecordsProcessedSoFar,
                    progressString
                    );
        }

        public void ReportProcessingIsDone()
        {
            _logger.LogConditionalConsoleInfo(
                   LogToConsole,
                   "{0}: {1} Finished processing dateset. Actual records processed: {2}",
                   DateTime.UtcNow.ToLongStringWithFractions(), _namer, RecordsProcessedSoFar);

        }
    }

    public class ProgressReporterOfTimelineData: ProgressReporter
    {
        private readonly DateTime _lowerBound;
        private readonly DateTime _upperBound;

        public ProgressReporterOfTimelineData(ILogger logger, string namer, DateTime lowerBound,
            DateTime upperBound) : base(logger, namer, -1)
        {
            _lowerBound = lowerBound;
            _upperBound = upperBound;
        }

        public void NextRecordProcessed(DateTime currentRecordTime)
        {
            IncrementRecordsProcessed();
            if (IsTimeForNextProgressReport)
            {
                string progressString =
                    string.Format("{0:P2}. Currently datapoint with timestamp {1} from range {2} - {3}",
                        (_upperBound - _lowerBound).GetPortionTakenByDuration(currentRecordTime - _lowerBound),
                        currentRecordTime.ToLongStringWithFractions(),
                        _lowerBound.ToLongStringWithFractions(),
                        _upperBound.ToLongStringWithFractions());
                FlushStats(progressString);
            }
        }
    }

    public class FileProcessingReporter
    {
        public FileProcessingReporter(ILogger logger, string namer, string filePath)
            : this(logger, namer, filePath, TimeSpan.FromSeconds(5), false)
        { }

        public FileProcessingReporter(ILogger logger, string namer, string filePath, TimeSpan minimumTimeBetweenProgressReports)
            : this(logger, namer, filePath, minimumTimeBetweenProgressReports, false)
        { }

        protected FileProcessingReporter(ILogger logger, string namer, string filePath, bool canSkipLines)
            : this(logger, namer, filePath, TimeSpan.FromSeconds(5), canSkipLines)
        { }

        private readonly ILogger _logger;
        private readonly string _namer;
        private readonly string _filePath;
        private readonly TimeSpan _minimumTimeBetweenProgressReports;
        private DateTime _nextProgressReportUtc;
        private readonly bool _canSkipLines;
        public readonly int _estimatedLinesCount;
        public bool LogToConsole { get { return _estimatedLinesCount < 0 || _estimatedLinesCount > 1000; } }
        public int LinesReadSoFar { get; private set; }
        protected int _skippedLines;
        protected FileProcessingReporter(ILogger logger, string namer, string filePath, TimeSpan minimumTimeBetweenProgressReports, bool canSkipLines)
        {
            _logger = logger;
            _namer = namer;
            _filePath = string.IsNullOrEmpty(filePath) ? "<Unknown or unspecified file>" : filePath;
            _minimumTimeBetweenProgressReports = minimumTimeBetweenProgressReports;
            _nextProgressReportUtc = DateTime.UtcNow + minimumTimeBetweenProgressReports;
            _canSkipLines = canSkipLines;
            _estimatedLinesCount = EstimateLineCounts(_filePath);

            //Text->Binary cntk converter
            logger.LogConditionalConsoleInfo(LogToConsole,
                "{0}: {1} Starting to process file [{2}]. Estimated lines: {3}", namer,
                DateTime.UtcNow.ToLongStringWithFractions(), _filePath,
                _estimatedLinesCount < 0 ? "Unknown" : _estimatedLinesCount.ToString());
        }

        public static int EstimateLineCounts(string filePath)
        {
            //Not using FileInfo for this check - as it's touchy for special characters
            if (!File.Exists(filePath))
            {
                return -1;
            }
            FileInfo fi = new FileInfo(filePath);
            long firstTwentyLinesByteCount = 0;
            long lineCount = 0;
            foreach (string line in File.ReadLines(filePath).Take(20))
            {
                lineCount++;
                firstTwentyLinesByteCount += line.Length;
            }

            return (int)(fi.Length / firstTwentyLinesByteCount * lineCount);
        }

        protected void NextLineReadSkippedInternal()
        {
            _skippedLines++;
            NextLineReadProcessed();
        }

        public void NextLineReadProcessed()
        {
            LinesReadSoFar++;

            if (DateTime.UtcNow >= _nextProgressReportUtc)
            {
                _logger.LogConditionalConsoleInfo(LogToConsole, "{0}: {1} lines read: {2}{3}. (Estimated portion of file: {4:P2})",
                    _namer,
                    DateTime.UtcNow.ToLongStringWithFractions(),
                    LinesReadSoFar,
                    _canSkipLines ? $" (from that processed: {LinesReadSoFar - _skippedLines})" : string.Empty,
                    _estimatedLinesCount > 0 ? Math.Min(1, ((float)LinesReadSoFar / Math.Max(1, _estimatedLinesCount))).ToString("P2") : "Unknown"
                    );
                _nextProgressReportUtc = DateTime.UtcNow + _minimumTimeBetweenProgressReports;
            }
        }

        public void ReportProcessingIsDone()
        {
            _logger.LogConditionalConsoleInfo(
                   LogToConsole,
                   "{0}: {1} Finished processing file [{1}]. Actual lines processed: {2}",
                   DateTime.UtcNow.ToLongStringWithFractions(), _filePath, LinesReadSoFar);

        }
    }

    public class FileProcessingReporterWithLineSkipping : FileProcessingReporter
    {
        public FileProcessingReporterWithLineSkipping(ILogger logger, string namer, string filePath)
            : base(logger, namer, filePath, true)
        { }

        public FileProcessingReporterWithLineSkipping(ILogger logger, string namer, string filePath, TimeSpan minimumTimeBetweenProgressReports)
            : base(logger, namer, filePath, minimumTimeBetweenProgressReports, true)
        { }

        public void NextLineReadSkipped()
        {
            NextLineReadSkippedInternal();
        }

        public void MarkIncreaseProcessedLines()
        {
            _skippedLines--;
        }
    }
}

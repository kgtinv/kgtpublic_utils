﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Kreslik.Integrator.Common
{
    public static class Extensions
    {
        /// <summary>
        /// Removes all trialing zeros after last significant digit after decimal separator
        ///  Or after decimal separator itself (if there are no decimal places)
        /// </summary>
        /// <param name="value">Number to be normalized</param>
        /// <returns>Normalized result</returns>
        public static decimal Normalize(this decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }

        public static decimal? Normalize(this decimal? value)
        {
            return value.HasValue ? value.Value.Normalize() : value;
        }

        /// <summary>
        /// Indicates whether date is a business day (doesn't consider holidays)
        /// </summary>
        /// <param name="date">date to check</param>
        /// <returns>true if date is a business day (Monday-Friday), false otherwise (Saturday, Sunday)</returns>
        public static bool IsBusinessDay(this DateTime date)
        {
            return
                date.DayOfWeek != DayOfWeek.Saturday &&
                date.DayOfWeek != DayOfWeek.Sunday;
        }

        public static Task ObserveException(this Task task)
        {
            return task.ContinueWith(t =>
                {
                    if (t.Exception != null)
                    {
                        LogFactory.Instance.GetLogger(null)
                                  .LogException(LogLevel.Fatal, "An unobserved task exception catched", t.Exception);
                    }
                }, TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnFaulted);
        }

        public static void PerformActionWithErrorHandling(Action action)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                                  .LogException(LogLevel.Fatal, "An unexpected exception during action", e);
            }
        }

        public static void SafeInvokeSynchronous(this Action action)
        {
            if (action != null)
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    LogFactory.Instance.GetLogger(null)
                        .LogException(LogLevel.Fatal, "Exception during safe invoking of action", e);
                }
        }

        //public static void ClearWhole(this Array array)
        //{
        //    Array.Clear(array, 0, array.Length);
        //}

        public static void ClearWhole<T>(this T[] array) where T: struct
        {
            Array.Clear(array, 0, array.Length);
        }

        public static void ClearWhole<T>(this T[][] array) where T : struct
        {
            for (int idx = 0; idx < array.Length; idx++)
            {
                array[idx].ClearWhole();
            }
        }

        public static void ClearWhole(this string[][] array)
        {
            for (int idx = 0; idx < array.Length; idx++)
            {
                Array.Clear(array[idx], 0, array[idx].Length);
            }
        }

        public static void CopyWholeFrom<T>(this T[] dstArray, T[] srcArray) where T : struct
        {
            if (dstArray.Length != srcArray.Length)
                throw new ArgumentException(
                    string.Format(
                        "CopyWholeFrom: Destination array length ({0}) is unequal to source array length ({1})",
                        dstArray.Length, srcArray.Length));

            Array.Copy(srcArray, dstArray, dstArray.Length);
        }

        public static void CopyWholeFrom<T>(this T[][] dstArray, T[][] srcArray) where T : struct
        {
            if (dstArray.Length != srcArray.Length)
                throw new ArgumentException(
                    string.Format(
                        "CopyWholeFrom([][]): Destination array length ({0}) is unequal to source array length ({1})",
                        dstArray.Length, srcArray.Length));

            for (int idx = 0; idx < dstArray.Length; idx++)
            {
                dstArray[idx].CopyWholeFrom(srcArray[idx]);
            }
        }

        public static Dictionary<T, int> ToDictionaryOfIndexes<T>(this IReadOnlyList<T> srcArray)
        {
            return srcArray.Select((val, idx) => new {Val = val, Idx = idx}).ToDictionary(x => x.Val, x => x.Idx);
        }
    }

    public static class IntExtensions
    {
        public static int DivideByWithCeilingRoundup(this int value, int divisor)
        {
            return (value + divisor - 1) / divisor;
        }

        public static bool IsOdd(this int n)
        {
            return (n & 1) == 1;
        }

        public static bool IsEven(this int n)
        {
            return (n & 1) == 0;
        }

        public static bool IsOdd(this long n)
        {
            return (n & 1) == 1;
        }

        public static bool IsEven(this long n)
        {
            return (n & 1) == 0;
        }
    }

    public static class ReflectionHelper
    {
        public static PropertyInfo PropertyInfo<T>(this Expression<Func<T, object>> property) where T : class
        {
            MemberExpression body = (MemberExpression)property.Body;
            return (PropertyInfo)body.Member;
        }
    }

    public static class StringEx
    {
        public static bool RepresentsInteger(this string str)
        {
            int n;
            return int.TryParse(str, out n);
        }

        public static bool ContainsCaseInsensitive(this string str, string substring)
        {
            return str.IndexOf(substring, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        /// <summary>
        /// Returns a new string in which all occurrences of a specified string in the current instance are replaced with another 
        /// specified string according the type of search to use for the specified string.
        /// </summary>
        /// <param name="str">The string performing the replace method.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string replace all occurrences of <paramref name="oldValue"/>. 
        /// If value is equal to <c>null</c>, than all occurrences of <paramref name="oldValue"/> will be removed from the <paramref name="str"/>.</param>
        /// <param name="comparisonType">One of the enumeration values that specifies the rules for the search.</param>
        /// <returns>A string that is equivalent to the current string except that all instances of <paramref name="oldValue"/> are replaced with <paramref name="newValue"/>. 
        /// If <paramref name="oldValue"/> is not found in the current instance, the method returns the current instance unchanged.</returns>
        [DebuggerStepThrough]
        public static string Replace(this string str,
            string oldValue, string @newValue,
            StringComparison comparisonType)
        {

            // Check inputs.
            if (str == null)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentNullException(nameof(str));
            }
            if (str.Length == 0)
            {
                // Same as original .NET C# string.Replace behavior.
                return str;
            }
            if (oldValue == null)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentNullException(nameof(oldValue));
            }
            if (oldValue.Length == 0)
            {
                // Same as original .NET C# string.Replace behavior.
                throw new ArgumentException("String cannot be of zero length.");
            }

            // Prepare string builder for storing the processed string.
            // Note: StringBuilder has a better performance than String by 30-40%.
            StringBuilder resultStringBuilder = new StringBuilder(str.Length);

            // Analyze the replacement: replace or remove.
            bool isReplacementNullOrEmpty = string.IsNullOrEmpty(@newValue);



            // Replace all values.
            const int valueNotFound = -1;
            int foundAt;
            int startSearchFromIndex = 0;
            while ((foundAt = str.IndexOf(oldValue, startSearchFromIndex, comparisonType)) != valueNotFound)
            {

                // Append all characters until the found replacement.
                int @charsUntilReplacment = foundAt - startSearchFromIndex;
                bool isNothingToAppend = @charsUntilReplacment == 0;
                if (!isNothingToAppend)
                {
                    resultStringBuilder.Append(str, startSearchFromIndex, @charsUntilReplacment);
                }



                // Process the replacement.
                if (!isReplacementNullOrEmpty)
                {
                    resultStringBuilder.Append(@newValue);
                }


                // Prepare start index for the next search.
                // This needed to prevent infinite loop, otherwise method always start search 
                // from the start of the string. For example: if an oldValue == "EXAMPLE", newValue == "example"
                // and comparisonType == "any ignore case" will conquer to replacing:
                // "EXAMPLE" to "example" to "example" to "example" … infinite loop.
                startSearchFromIndex = foundAt + oldValue.Length;
                if (startSearchFromIndex == str.Length)
                {
                    // It is end of the input string: no more space for the next search.
                    // The input string ends with a value that has already been replaced. 
                    // Therefore, the string builder with the result is complete and no further action is required.
                    return resultStringBuilder.ToString();
                }
            }


            // Append the last part to the result.
            int @charsUntilStringEnd = str.Length - startSearchFromIndex;
            resultStringBuilder.Append(str, startSearchFromIndex, @charsUntilStringEnd);


            return resultStringBuilder.ToString();

        }

        private static char GetChar(this StringBuilder sb, int idx, bool ignoreCase)
        {
            char c = sb[idx];
            if (ignoreCase)
            {
                c = char.ToUpperInvariant(c);
            }
            return c;
        }

        private static char GetChar(this string s, int idx, bool ignoreCase)
        {
            char c = s[idx];
            if (ignoreCase)
            {
                c = char.ToUpperInvariant(c);
            }
            return c;
        }

        /// <summary>
        /// Returns the index of the start of the contents in a StringBuilder
        /// </summary>        
        /// <param name="value">The string to find</param>
        /// <param name="startIndex">The starting index.</param>
        /// <param name="ignoreCase">if set to <c>true</c> it will ignore case</param>
        /// <returns></returns>
        public static int IndexOf(this StringBuilder sb, string value, int startIndex, bool ignoreCase)
        {
            int length = value.Length;
            int maxSearchLength = (sb.Length - length) + 1;

            for (int i = startIndex; i < maxSearchLength; ++i)
            {
                var index = 0;
                while ((index < length) && (sb.GetChar(i + index, ignoreCase) == value.GetChar(index, ignoreCase)))
                    ++index;

                if (index == length)
                    return i;
            }

            return -1;
        }

        public static void Replace(this StringBuilder sb, int startIdx, int length, string value)
        {
            sb.Remove(startIdx, length);
            sb.Insert(startIdx, value);
        }

        public static bool EqualsDisregardWhitespaces(this string str1, string str2)
        {
            return str1.RemoveWhitespace().Equals(str2.RemoveWhitespace());
        }

        public static bool ContainsDisregardWhitespaces(this string str1, string subtring)
        {
            return str1.RemoveWhitespace().Contains(subtring.RemoveWhitespace());
        }

        public static string RemoveWhitespace(this string input)
        {
            return new string(input
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }

        public static int FirstDifferIndex(this string s1, string s2)
        {
            int index = 0;
            int min = Math.Min(s1.Length, s2.Length);
            while (index < min && s1[index] == s2[index])
                index++;

            return (index == min && s1.Length == s2.Length) ? -1 : index;
        }

        public static string DifferenceSnippet(this string s1, string s2, int snippetLengthOfEachString)
        {
            const string format = @"Strings differ (from index {0}):{1}...{2}...{1}...{3}...";

            int diffIndex = FirstDifferIndex(s1, s2);

            if (diffIndex == -1)
            {
                return "Strings are same";
            }

            int startIndex = Math.Max(0, diffIndex - snippetLengthOfEachString / 2);
            int length = snippetLengthOfEachString;

            return string.Format(format,
                diffIndex, Environment.NewLine,
                s1.Substring(startIndex, Math.Min(length, s1.Length - startIndex)),
                s2.Substring(startIndex, Math.Min(length, s2.Length - startIndex)));
        }

        public static string RemoveFromEnd(this string s, string suffix)
        {
            if (s.EndsWith(suffix))
            {
                return s.Substring(0, s.Length - suffix.Length);
            }
            else
            {
                return s;
            }
        }

        public static string RemoveFromStart(this string s, string prefix)
        {
            return RemoveFromStart(s, prefix, StringComparison.CurrentCulture);
        }

        public static string RemoveFromStart(this string s, string prefix, StringComparison stringComparison)
        {
            if (s.StartsWith(prefix, stringComparison))
            {
                return s.Substring(prefix.Length, s.Length - prefix.Length);
            }
            else
            {
                return s;
            }
        }

        public static string SubstringOfAMaximumLength(this string s, int maximumLength)
        {
            if (s.Length <= maximumLength)
            {
                return s;
            }
            else
            {
                return s.Substring(0, maximumLength);
            }
        }

        public static string NormalizePath(this string path)
        {
            return Path.GetFullPath(new Uri(path).LocalPath)
                       .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                       .ToUpperInvariant();
        }

        public static byte[] Md5Hash(this string s)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            return md5Hasher.ComputeHash(Encoding.ASCII.GetBytes(s));
        }

        public static Guid ToGUID(this string value)
        {
            return new Guid(value.Md5Hash());
        }

        /// <summary>
        /// Returns hash 10 chars string in base64 chars (from {[A-Z], [a-z], [0-9], '+', '-', '_'})
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToStringHash(this string s)
        {
            //WARNING!: We should not be using GetHasCode - as it has different behavior on 64 and 32 bit platforms, CLR version to version
            // plus it has some cirtical flaws! e.g.: https://blogs.msdn.microsoft.com/ericlippert/2011/07/15/the-curious-property-revealed/
            //int hashInt = s.GetHashCode();

            string hashString = Convert.ToBase64String(s.Md5Hash())
                //all bytes of MD5 has same significance
                .Substring(0, 10)
                //slash is not valid filename character
                .Replace('/', '_')
                //'=' causes errors in csc.exe:
                //https://github.com/dotnet/roslyn/issues/5623
                .Replace('=','-');
            return hashString;
        }

        public static string ToMaxLength(this string s, int maxLength)
        {
            if (s == null || s.Length <= maxLength)
            {
                return s;
            }

            return s.Substring(0, maxLength);
        }

        public static bool IsSurelyInvalidXml(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }

            string trimmedValue = value.TrimStart();

            if (string.IsNullOrEmpty(trimmedValue))
            {
                return true;
            }

            if (trimmedValue[0] != '<')
            {
                return true;
            }

            return false;
        }

        public static bool IsValidXml(this string value)
        {
            string _;
            return IsValidXml(value, out _);
        }

        public static bool IsValidXml(this string value, out string reason)
        {
            if (IsSurelyInvalidXml(value))
            {
                reason = "Given string is empty or doesn't start with xml tag";
                return false;
            }

            try
            {
                reason = null;
                XElement.Parse(value);
                return true;
            }
            catch (Exception e)
            {
                reason = e.Message;
                return false;
            }
        }

        public static bool IsValidXmlFragment(this string value)
        {
            if (IsSurelyInvalidXml(value))
            {
                return false;
            }

            try
            {
                XElement.Parse("<root>" + value + "</root>");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public static class DirectoryEx
    {
        public static void StampDirUsage(string dirpath)
        {
            try
            {
                Directory.SetLastAccessTimeUtc(dirpath, DateTime.UtcNow);
            }
            catch (Exception e)
            {
                //this is it - if opened in explorer cannot stamp
            }
        }

        public static void DeleteRecursiveIncludingReadOnly(string path, bool ignoreErrors = false)
        {
            PerformAction(() => DeleteRecursiveIncludingReadOnlyInternal(path, ignoreErrors), ignoreErrors, 1);
        }

        private static void DeleteRecursiveIncludingReadOnlyInternal(string path, bool ignoreErrors = false)
        {
            if (Directory.Exists(path))
            {
                DeleteFileOrDirectory(new DirectoryInfo(path), ignoreErrors);
            }
        }

        public static void DeleteSubdirsOlderThan(string path, TimeSpan olderThan, string folderNamePattern = "",
            bool ignoreErrors = false)
        {
            PerformAction(() => DeleteSubdirsOlderThanInternal(path, olderThan, folderNamePattern, ignoreErrors), ignoreErrors, 1);
        }

        private static void DeleteSubdirsOlderThanInternal(string path, TimeSpan olderThan, string folderNamePattern = "", bool ignoreErrors = false)
        {
            if (folderNamePattern == null)
                folderNamePattern = string.Empty;

            //no lazy eval - enumeration can change
            var dirs = new DirectoryInfo(path).GetDirectories()
                .Where(
                    di =>
                        DateTime.UtcNow - DateTimeEx.Max(di.LastWriteTimeUtc, di.LastAccessTimeUtc) > olderThan &&
                        di.Name.StartsWith(folderNamePattern)).ToList();
            if (Directory.Exists(path))
            {
                foreach (DirectoryInfo directoryInfo in dirs)
                {
                    DeleteFileOrDirectory(directoryInfo, ignoreErrors);
                }
            }
        }

        private static void PerformAction(Action action, bool ignoreErrors, int retryCountOnError)
        {
            do
            {
                try
                {
                    action();
                    return;
                }
                catch (Exception)
                {
                    if (!ignoreErrors)
                    {
                        throw;
                    }
                }
            } while (retryCountOnError-- > 0);
        }

        private static void DeleteFileOrDirectory(FileSystemInfo fileOrDirectory, bool ignoreErrors)
        {
            var directoryInfo = fileOrDirectory as DirectoryInfo;
            if (directoryInfo != null && fileOrDirectory.Exists)
            {
                //no lazy eval - enumeration can change
                var allFilesAndSubdirectories = directoryInfo.GetFileSystemInfos().ToList();
                foreach (var fileOrSubdirectory in allFilesAndSubdirectories)
                {
                    DeleteFileOrDirectory(fileOrSubdirectory, ignoreErrors);
                }
            }

            try
            {
                fileOrDirectory.Attributes = FileAttributes.Normal;

                Exception e;

                var retryCountDeleteIfUnathorizedAccess = 3;

                do
                {
                    e = null;

                    try
                    {
                        fileOrDirectory.Delete();
                    }
                    catch (System.UnauthorizedAccessException unauthorizedAccessException) // this won't catch any other exceptions
                    {
                        e = unauthorizedAccessException;
                        retryCountDeleteIfUnathorizedAccess--;
                        Thread.Sleep(100);
                    }
                } while (e != null && retryCountDeleteIfUnathorizedAccess > 0);
            }
            catch (Exception)
            {
                if (!ignoreErrors)
                {
                    throw;
                }
            }
        }

        public static long GetDirectorySize(this string dir)
        {
            return GetDirectorySize(new DirectoryInfo(dir));
        }

        public static long GetDirectorySize(this DirectoryInfo dir)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = dir.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = dir.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += GetDirectorySize(di);
            }
            return size;
        }
    }

    public static class FileEx
    {
        public static void MarkFileReadOnly(string filePath)
        {
            FileInfo fInfo = new FileInfo(filePath);
            fInfo.IsReadOnly = true;
        }

        public static bool TryDelete(string filePath)
        {
            try
            {
                File.Delete(filePath);
                return true;
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Trace, e, "Attempt to delete file [{0}] failed", filePath);
                return false;
            }
        }
    }
    
    public static class DecimalEx
    {
        public static decimal CloserToZero(decimal d1, decimal d2)
        {
            return Math.Abs(d1) <= Math.Abs(d2) ? d1 : d2;
        }

        public static bool IsBetween(this decimal d, decimal bound1, decimal bound2, bool inclusive)
        {
            decimal boundsDistance = Math.Abs(bound1 - bound2);
            decimal numberDistance = Math.Max(Math.Abs(bound1 - d), Math.Abs(bound2 - d));

            return inclusive ? numberDistance <= boundsDistance : numberDistance < boundsDistance;
        }

        public static int ToIntSafe(this decimal d)
        {
            if (d >= int.MaxValue)
                return int.MaxValue;

            if (d <= int.MinValue)
                return int.MinValue;

            return (int) d;
        }
    }

    public static class DoubleEx
    {
        public static bool IsWholeNumber(this double d)
        {
            return Math.Abs(d - (int)d) < 100 * double.Epsilon;
        }

        public static decimal ToDecimalWithoutOverflow(this double dbl)
        {
            if (dbl > (double) decimal.MaxValue)
            {
                return decimal.MaxValue;
            }
            else if (dbl < (double) decimal.MinValue)
            {
                return decimal.MinValue;
            }
            else if(double.IsNaN(dbl))
            {
                throw new Exception($"Unable to cast NaN double [{dbl}] to decimal");
            }

            return (decimal)dbl;
        }
    }

    public static class EnumEx
    {
        public static int OwningEnumValuesCount(this Enum @enum)
        {
            return Enum.GetValues(@enum.GetType()).Length;
        }

        public static int OwningEnumValuesCount(this Type enumType)
        {
            return Enum.GetValues(enumType).Length;
        }

        public static IEnumerable<T> GetFlagsEnumIndividualValues<T>(this Enum @enum) where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<Enum>().Where(@enum.HasFlag).Cast<T>();
        }

        public static IEnumerable<Enum> GetFlagsEnumIndividualValues(this Enum @enum)
        {
            return Enum.GetValues(@enum.GetType()).Cast<Enum>().Where(@enum.HasFlag);
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T Parse<T>(string value) where T : struct
        {
            return (T) Enum.Parse(typeof (T), value);
        }

        public static bool CanParse<T>(string value) where T : struct
        {
            T _;
            return Enum.TryParse(value, out _);
        }
    }

    public static class InterlockedEx
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int Read(ref int value)
        {
            return Interlocked.CompareExchange(ref value, 0, 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CheckAndFlipState(ref int value, int newState, int expectedState)
        {
            return Interlocked.CompareExchange(ref value, newState, expectedState) == expectedState;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CheckAndFlipState(ref long value, long newState, long expectedState)
        {
            return Interlocked.CompareExchange(ref value, newState, expectedState) == expectedState;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CheckAndFlipState<T>(ref T value, T newState, T expectedState) where T: class
        {
            return Interlocked.CompareExchange(ref value, newState, expectedState) == expectedState;
        }
    }

    public static class DateTimeEx
    {
        public static DateTime Max(DateTime a, DateTime b)
        {
            return new DateTime(Math.Max(a.Ticks, b.Ticks));
        }

        public static DateTime? Max(DateTime? a, DateTime? b)
        {
            return !a.HasValue ? b : (!b.HasValue ? a : Max(a.Value, b.Value));
        }

        public static DateTime Min(DateTime a, DateTime b)
        {
            return new DateTime(Math.Min(a.Ticks, b.Ticks));
        }

        public static DateTime? Min(DateTime? a, DateTime? b)
        {
            return !a.HasValue ? b : (!b.HasValue ? a : Min(a.Value, b.Value));
        }

        public static DateTime Mid(DateTime a, DateTime b)
        {
            //We do not want to do just (a + b) / 2 to avoid overflow; however we do not want to lose precision
            // 6 / 2 != (3/2) + (3/2) 
            long adjust = a.Ticks.IsOdd() && b.Ticks.IsOdd() ? 1 : 0;
            return new DateTime(a.Ticks / 2 + b.Ticks / 2 + adjust);
        }

        public static DateTime Mid(this Interval<DateTime> interval)
        {
            return Mid(interval.LowBound, interval.HighBound);
        }

        public static readonly TimeSpan MaxTimespanForCalculations = DateTime.MaxValue - DateTime.MinValue;

        public static DateTime SafeAdd(this DateTime a, TimeSpan b)
        {
            if (b > TimeSpan.Zero && (b > MaxTimespanForCalculations || DateTime.MaxValue - b < a))
                return DateTime.MaxValue;
            else if(b < TimeSpan.Zero && (b < -MaxTimespanForCalculations || DateTime.MinValue - b > a))
                return DateTime.MinValue;
            else
                return a + b;
        }

        public const string LongDateTimeStringWithFractionsFormat = "yyyy-MM-dd HH:mm:ss.fffffff";

        public static string ToLongStringWithFractions(this DateTime dt)
        {
            return dt.ToString(LongDateTimeStringWithFractionsFormat);
        }

        public static string ToLongFileStringWithFractions(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd-HH-mm-ss-fffffff");
        }

        public static string ToLongStringWithFractions(this DateTime? dt)
        {
            return dt.HasValue ? dt.Value.ToString(LongDateTimeStringWithFractionsFormat) : string.Empty;
        }

        public static bool Between(this DateTime dt, DateTime lowerBound, DateTime upperBound)
        {
            return Between(dt, lowerBound, upperBound, true);
        }

        public static bool Between(this DateTime dt, DateTime lowerBound, DateTime upperBound, bool includeBounds)
        {
            return includeBounds
                ? dt >= lowerBound && dt <= upperBound
                : dt > lowerBound && dt < upperBound;
        }

        public static bool HaveOverlap(DateTime start1, DateTime end1, DateTime start2, DateTime end2)
        {
            return start1 < end2 && start2 < end1;
        }

        public static int GetBusinessDays(DateTime startD, DateTime endD)
        {
            double calcBusinessDays =
                ((endD.Date - startD.Date).TotalDays * 5 -
                     (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays++;

            return (int) calcBusinessDays;
        }

        public static IEnumerable<DateTime> GetBusinessDaysEnumeration(DateTime startD, DateTime endD)
        {
            endD = endD.Date;
            startD = startD.Date;

            while (endD > startD)
            {
                if (startD.IsBusinessDay())
                {
                    yield return startD;
                }

                startD += TimeSpan.FromDays(1);
            }
        }

        public static DateTime AddDays(this DateTime start, int days, bool skipWeekends)
        {
            return skipWeekends ? start.AddBusinessDays(days) : start.AddDays(days);
        }

        public static DateTime AddBusinessDays(this DateTime start, int businessDays)
        {
            if (businessDays == 0)
                return start;

            int dayOfWeek = (int)start.DayOfWeek;
            double temp = businessDays + dayOfWeek;
            if (start.DayOfWeek == DayOfWeek.Saturday) temp--;
            DateTime end = start.AddDays(
                Math.Floor(temp / 5) * 2 - dayOfWeek + temp
                - 2 * Convert.ToInt32(temp % 5 == 0));

            return end;
        }

        public static int DayOfWeekEuropean(this DateTime dt)
        {
            return dt.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)dt.DayOfWeek;
        }

        public static bool IsWorkDay(this DateTime date)
        {
            return date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday;
        }

        public static bool IsWeekend(this DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }
    }

    public static class TimeSpanEx
    {
        public static TimeSpan Max(TimeSpan a, TimeSpan b)
        {
            return new TimeSpan(Math.Max(a.Ticks, b.Ticks));
        }

        public static TimeSpan Min(TimeSpan a, TimeSpan b)
        {
            return new TimeSpan(Math.Min(a.Ticks, b.Ticks));
        }

        public static TimeSpan Abs(this TimeSpan t)
        {
            return new TimeSpan(Math.Abs(t.Ticks));
        }

        public static TimeSpan Times(this TimeSpan a, int multiplier)
        {
            return new TimeSpan(a.Ticks * multiplier);
        }

        public static TimeSpan DivideBy(this TimeSpan ts, int divisor)
        {
            return new TimeSpan(ts.Ticks / divisor);
        }

        public static double DivideBy(this TimeSpan ts, TimeSpan divisor)
        {
            return divisor.Ticks == 0 ? 0 : ((double)ts.Ticks) / divisor.Ticks;
        }

        public static int WholeDivideBy(this TimeSpan ts, TimeSpan divizor)
        {
            return (int) (ts.Ticks / divizor.Ticks);
        }

        public static TimeSpan WholeDivideReminder(this TimeSpan ts, TimeSpan divizor)
        {
            return new TimeSpan(ts.Ticks % divizor.Ticks);
        }

        public static double GetPortionTakenByDuration(this TimeSpan entireDuration, TimeSpan portionDuration)
        {
            if (portionDuration == TimeSpan.Zero)
            {
                return 0;
            }

            return portionDuration.Ticks / (double) entireDuration.Ticks;
        }

        public static TimeSpan FromHoursWithRandomSpread(double value, double randomSpread)
        {
            return FromIntervalWithRandomSpread(DateInterval.Hours, value, randomSpread);
        }

        public enum DateInterval
        {
            Years,
            Months,
            Weeks,
            Days,
            Hours,
            Minutes,
            Seconds,
            Milliseconds,
            Ticks
        }

        public static TimeSpan FromIntervalWithRandomSpread(DateInterval dateInterval, double value, double randomSpread)
        {
            if (randomSpread < 0 || randomSpread > value * 2.0)
            {
                throw new Exception($"RandomSpread [{randomSpread}] must be nonegative and lower than half of the span value [{value}]");
            }

            if (randomSpread != 0)
            {
                double offset = (new Random()).NextDouble() * randomSpread;
                value = value - randomSpread / 2.0 + offset;
            }

            return TimeSpanEx.FromInterval(dateInterval, value);
        }

        public static TimeSpan FromInterval(DateInterval dateInterval, double value)
        {
            TimeSpan interval;
            switch (dateInterval)
            {
                default:
                    throw new ArgumentException("dateInterval");
                case DateInterval.Days:
                    interval = TimeSpan.FromDays(value); break;
                case DateInterval.Weeks:
                    interval = TimeSpan.FromDays(value); break;
                case DateInterval.Hours:
                    interval = TimeSpan.FromHours(value); break;
                case DateInterval.Minutes:
                    interval = TimeSpan.FromMinutes(value); break;
                case DateInterval.Seconds:
                    interval = TimeSpan.FromSeconds(value); break;
                case DateInterval.Milliseconds:
                    interval = TimeSpan.FromMilliseconds(value); break;
                case DateInterval.Ticks:
                    interval = TimeSpan.FromTicks((long)value); break;
            }

            return interval;
        }

        public static TimeSpan SafeAdd(this TimeSpan a, TimeSpan b)
        {
            if (TimeSpan.MaxValue - b < a)
                return TimeSpan.MaxValue;
            else
                return a + b;
        }

        public static TimeSpan SafeSum(this IEnumerable<TimeSpan> spans)
        {
            TimeSpan result = TimeSpan.Zero;

            foreach (TimeSpan span in spans)
            {
                result = result.SafeAdd(span);
            }

            return result;
        }

        public static bool Between(this DateTime dt, TimeSpan lowerBound, TimeSpan upperBound)
        {
            TimeSpan timeOfDay = dt.TimeOfDay;
            return timeOfDay >= lowerBound && timeOfDay <= upperBound;
        }

        public static bool Between(this TimeSpan timeOfDay, TimeSpan lowerBound, TimeSpan upperBound)
        {
            return timeOfDay >= lowerBound && timeOfDay <= upperBound;
        }

        public static bool IsTimeOfDay(this TimeSpan timeOfDay)
        {
            return timeOfDay.Between(TimeSpan.Zero, TimeSpan.FromDays(1));
        }

        public static bool IsTimeOfWeek(this TimeSpan timeOfWeek)
        {
            return timeOfWeek.Between(TimeSpan.Zero, TimeSpan.FromDays(7));
        }

        public static bool IsOlderThan(this DateTime dt, TimeSpan age)
        {
            return dt <= DateTime.UtcNow.Subtract(age);
        }

        public static bool IsOlderThanDay(this DateTime dt)
        {
            return IsOlderThan(dt, TimeSpan.FromDays(1));
        }
    }

    public static class TaskEx
    {
        //public static void ExecAfterDelayWithErrorHandling(TimeSpan delay, Action action,
        //    TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
        //    TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool isUltraQuick)
        //{
        //    HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(action, delay, taskExecutionPriority,
        //        requiredTimerPrecision, false, isUltraQuick);
        //}

        private static TimeSpan _borderDelayForLowPriTasks = TimeSpan.FromSeconds(3);
        public static Task ExecAfterDelayWithErrorHandling(TimeSpan delay, Action action)
        {
            Task delayedTask = Task.Delay(delay).ContinueWith(t => action());
            delayedTask.ObserveException();
            return delayedTask;

            //HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(action, delay,
            //    delay > _borderDelayForLowPriTasks
            //        ? TimerTaskFlagUtils.WorkItemPriority.Lowest
            //        : TimerTaskFlagUtils.WorkItemPriority.Normal,
            //    TimerTaskFlagUtils.GetTimerPrecisionForDelay(delay), false, false);
        }

        public static void QueueLongRunningWithErrorHandling(Action action)
        {
            Task.Factory.StartNew(action, TaskCreationOptions.LongRunning).ObserveException();
        }

        public static void StartNew(Action action, TimerTaskFlagUtils.WorkItemPriority workItemPriority)
        {
            ThreadPoolEx.Instance.QueueWorkItem(new WorkItem(action, workItemPriority));
        }

        public static void StartNew(Action action)
        {
            ThreadPoolEx.Instance.QueueWorkItem(new WorkItem(action, TimerTaskFlagUtils.WorkItemPriority.Normal));
        }

        public static IEnumerable<T> OrderRandomly<T>(this IList<T> sequence)
        {
            Random rand = new Random(); // TODO: KGTNNCS-662 - do we need it here? 
            return Enumerable.Range(0, sequence.Count).OrderBy(x => rand.Next()).Select(i => sequence[i]);
        }
    }

    public static class Constansts
    {
        public static readonly TimeSpan MinimumSpanBetweenLLandWatchdog = TimeSpan.FromMilliseconds(2);
    }

    public static class MathEx
    {
        public static decimal Min(decimal d1, decimal d2, decimal d3, decimal d4)
        {
            return Math.Min(Math.Min(d1, d2), Math.Min(d3, d4));
        }

        public static float Min(float f1, float f2, float f3)
        {
            return Math.Min(Math.Min(f1, f2), f3);
        }

        public static decimal Min(decimal d1, decimal d2, decimal d3, decimal d4, decimal d5, decimal d6)
        {
            return Math.Min(MathEx.Min(d1, d2, d3, d4), Math.Min(d5, d6));
        }

        public static decimal Max(decimal d1, decimal d2, decimal d3)
        {
            return Math.Max(Math.Max(d1, d2), d3);
        }

        public static int Max(int d1, int d2, int d3)
        {
            return Math.Max(Math.Max(d1, d2), d3);
        }
    }

    public static class XDocumentHelpers
    {
        public static XElement Sort(this XElement element)
        {
            return new XElement(element.Name,
                    element.Attributes(),
                    from child in element.Nodes()
                    where child.NodeType != XmlNodeType.Element
                    select child,
                //We need to use StringComparer.Ordinal - as standard linq operator uses Default, but Serializer uses ordinal
                element.Elements().OrderBy(child => child.Name.ToString(), StringComparer.Ordinal).Select(Sort));
        }

        public static XDocument Sort(this XDocument file)
        {
            return new XDocument(
                    file.Declaration,
                    from child in file.Nodes()
                    where child.NodeType != XmlNodeType.Element
                    select child,
                    Sort(file.Root));
        }

        public static string SortXmlStringElements(this string xmlString)
        {
            return XDocument.Parse(xmlString).Sort().ToString();
        }

        public static string GetInnerText(this XElement el)
        {
            return el.Nodes().Aggregate("", (b, node) => b += node.ToString());
        }
    }

    public static class IEnumerableExtension
    {
        public static bool HasDuplicities<T>(this IEnumerable<T> sequence)
        {
            if (sequence == null)
            {
                return false;
            }

            return sequence.GroupBy(x => x).Any(g => g.Count() > 1);
        }

        public static IEnumerable<T> GetDuplicities<T>(this IEnumerable<T> sequence)
        {
            if (sequence == null)
            {
                return null;
            }

            return sequence.GroupBy(x => x)
                .Where(g => g.Count() > 1)
                .Select(y => y.Key);
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            if (source == null)
            {
                yield break;
            }

            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static int IndexOf<T>(this IEnumerable<T> source, T value)
        {
            return IndexOf(source, EqualityComparer<T>.Default, value);
        }

        public static int IndexOf<T>(this IEnumerable<T> source, IEqualityComparer<T> comparer, T value)
        {
            int index = 0;
            foreach (T item in source)
            {
                if (comparer.Equals(item, value)) return index;
                index++;
            }
            return -1;
        }

        public static string ToCsvString<T>(this IEnumerable<T> source)
        {
            return ToCsvString(source, false);
        }

        public static string ToCsvString<T>(this IEnumerable<T> source, bool useSpace)
        {
            return source == null ? "<NULL>" : String.Join("," + (useSpace ? " " : String.Empty), source);
        }

        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> source)
        {
            return source ?? Enumerable.Empty<T>();
        }

        public static bool AreSame<T>(this IEnumerable<T> first, IEnumerable<T> second, IEqualityComparer<T> comparer)
        {
            if (first == null && second == null)
            {
                return true;
            }

            if (first == null || second == null)
            {
                return false;
            }

            return first.SequenceEqual(second, comparer);
        }

        public static IEnumerable<T> ConcatMayBeNull<T>(this IEnumerable<T> source, IEnumerable<T> other)
        {
            if (source == null)
            {
                return other;
            }

            if (other == null)
            {
                return source;
            }

            return source.Concat(other);
        }

        public static IEnumerable<TResult> ZipMulti<TData, TResult>(this IEnumerable<IEnumerable<TData>> data,
            Func<IEnumerable<TData>, TResult> zipFunction)
        {
            //must be list - so that we do not perform GetEnumerator multiple times
            IReadOnlyList<IEnumerator<TData>> dataEnumerators = data.Select(d => d.GetEnumerator()).ToList();
            while (dataEnumerators.All(enumerator => enumerator.MoveNext()))
            {
                yield return zipFunction(dataEnumerators.Select(enumerator => enumerator.Current));
            }

            bool hasMismatchedItemsCount = dataEnumerators.Any(enumerator => enumerator.MoveNext());

            foreach (IEnumerator<TData> dataEnumerator in dataEnumerators)
            {
                dataEnumerator.Dispose();
            }

            if (hasMismatchedItemsCount)
            {
                throw new Exception("Enumerations given to ZipMulti contain different number of elements");
            }
        }

        public static IEnumerable<TResult> ZipStrictCountControl<TData1, TData2, TResult>(
        this IEnumerable<TData1> first,
        IEnumerable<TData2> second,
        Func<TData1, TData2, TResult> func)
        {
            int numberOfItemsIterated = 0;
            using (var e1 = first.GetEnumerator())
            using (var e2 = second.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext())
                {
                    yield return func(e1.Current, e2.Current);
                    numberOfItemsIterated++;
                }

                bool firstHasMore = e1.MoveNext();
                bool secondHasMore = e2.MoveNext();
                if (firstHasMore || secondHasMore)
                {
                    throw new Exception(
                        string.Format(
                            "Enumerations given to ZipStrictCountControl contain different number of elements ({0} in both, {1} enumeration has more items)",
                            numberOfItemsIterated, firstHasMore ? "first" : "second"));
                }
            }


        }

        public static IEnumerable<TResult> ZipThree<TData1, TData2, TData3, TResult>(
        this IEnumerable<TData1> first,
        IEnumerable<TData2> second,
        IEnumerable<TData3> third,
        Func<TData1, TData2, TData3, TResult> func)
        {
            using (var e1 = first.GetEnumerator())
            using (var e2 = second.GetEnumerator())
            using (var e3 = third.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                    yield return func(e1.Current, e2.Current, e3.Current);
            }
        }

        public static IEnumerable<TData> ZipIntoOne<TData>(
        this IEnumerable<TData> first,
        IEnumerable<TData> second)
        {
            using (var e1 = first.GetEnumerator())
            using (var e2 = second.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext())
                {
                    yield return e1.Current;
                    yield return e2.Current;
                }
            }
        }

        public static IEnumerable<TResult> ZipFour<TData1, TData2, TData3, TData4, TResult>(
        this IEnumerable<TData1> first,
        IEnumerable<TData2> second,
        IEnumerable<TData3> third,
        IEnumerable<TData4> fourth,
        Func<TData1, TData2, TData3, TData4, TResult> func)
        {
            using (var e1 = first.GetEnumerator())
            using (var e2 = second.GetEnumerator())
            using (var e3 = third.GetEnumerator())
            using (var e4 = fourth.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext() && e4.MoveNext())
                    yield return func(e1.Current, e2.Current, e3.Current, e4.Current);
            }
        }

        public static IEnumerable<T> GetRangeEnumerable<T>(this IReadOnlyList<T> indexableCollection, int startIndexInclusive, int endIndexExclusive)
        {
            for (int idx = startIndexInclusive; idx < endIndexExclusive; idx++)
            {
                yield return indexableCollection[idx];
            }
        }

        public static IReadOnlyList<T> GetRange<T>(this IReadOnlyList<T> collection, int startIndexInclusive, int count)
        {
            if (collection.Count - startIndexInclusive < count)
            {
                throw new IndexOutOfRangeException(
                    String.Format("Attempt to create range of size {0} from index {1} of collection with {2} items",
                        count, startIndexInclusive, collection.Count));
            }

            T[] array = new T[count];
            for (int destinationArrayIdx = 0; destinationArrayIdx < count; destinationArrayIdx++)
            {
                array[destinationArrayIdx] = collection[startIndexInclusive + destinationArrayIdx];
            }

            return array;
        }

        public static int FirstIndexOf<T>(this IEnumerable<T> enumerable, T item)
        {
            int index = 0;
            foreach (T enumerableItem in enumerable)
            {
                if (enumerableItem.Equals(item))
                {
                    return index;
                }
                index++;
            }

            throw new Exception(String.Format("Item [{0}] not found in given enumeration", item));
        }

        public static int FirstIndexOf<T>(this IEnumerable<T> enumerable, Predicate<T> condition)
        {
            int index = 0;
            foreach (T enumerableItem in enumerable)
            {
                if (condition(enumerableItem))
                {
                    return index;
                }
                index++;
            }

            throw new Exception("Item meeting given condtition not found in given enumeration");
        }

        public static void ForEach<T>(this IEnumerable<T> value, Action<T> action)
        {
            foreach (T item in value)
            {
                action(item);
            }
        }

        public static IEnumerable<IEnumerable<T>> CrossApply<T>(
            this IEnumerable<T> prefixes,
            IEnumerable<IEnumerable<T>> tails)
        {
            List<T> prefixesList = prefixes.ToList();
            List<IEnumerable<T>> tailsList = tails?.ToList() ?? new List<IEnumerable<T>>();

            foreach (T prefix in prefixesList)
            {
                if (tailsList.Count == 0)
                {
                    yield return new List<T>() { prefix };
                }

                foreach (IEnumerable<T> tail in tailsList)
                {
                    List<T> list = new List<T> { prefix };
                    list.AddRange(tail);
                    yield return list;
                }
            }
        }

        public static IEnumerable<IEnumerable<T>> AllCombinations<T>(
            this IEnumerable<IEnumerable<T>> valuesPerEachClass)
        {
            if (valuesPerEachClass == null)
            {
                return null;
            }

            List<IEnumerable<T>> classes = valuesPerEachClass.ToList();

            if (classes.Count == 1)
            {
                return (IEnumerable<IEnumerable<T>>) new[] {classes[0].Select(v => (IEnumerable<T>) new[] {v})};
            }

            return
                valuesPerEachClass.First().CrossApply(AllCombinations<T>(valuesPerEachClass.Skip(1)));
        }

        public static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences)
        {
            // base case: 
            IEnumerable<IEnumerable<T>> result = new[] { Enumerable.Empty<T>() };
            foreach (var sequence in sequences)
            {
                // don't close over the loop variable (fixed in C# 5 BTW)
                var s = sequence;
                // recursive case: use SelectMany to build 
                // the new product out of the old one 
                result =
                    result.SelectMany(seq => s, (seq, item) => seq.Concat(new[] { item }));
            }
            return result;
        }

        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> collection, int batchSize)
        {
            List<T> nextbatch = new List<T>(batchSize);
            foreach (T item in collection)
            {
                nextbatch.Add(item);
                if (nextbatch.Count == batchSize)
                {
                    yield return nextbatch;
                    nextbatch = new List<T>();
                    // or nextbatch.Clear(); but it can lead to issues if caller calls .ToList()
                }
            }

            if (nextbatch.Count > 0)
                yield return nextbatch;
        }

        public struct EnumeratingItems<T>
        {
            public EnumeratingItems(T current, T next, bool hasNext)
            {
                Current = current;
                Next = next;
                HasNext = hasNext;
            }

            public T Current { get; }
            public T Next { get; }
            public bool HasNext { get; }
        }

        //private IEnumerable<EnumeratingItems<T>> ToOneForwardEnumerable<T>(IEnumerable<T> enumerable)
        //{
        //    bool hasPrevious = false;
        //    T previous = default(T);

        //    foreach (T item in enumerable)
        //    {
        //        if (hasPrevious)
        //        {
        //            yield return new EnumeratingItems<T>(previous, item);
        //        }

        //        previous = item;
        //        hasPrevious = true;
        //    }

        //    if (hasPrevious)
        //    {
        //        yield return new EnumeratingItems<T>(previous, default(T));
        //    }
        //}

        public static IEnumerable<EnumeratingItems<T>> ToOneForwardEnumerable<T>(this IEnumerable<T> enumerable)
        {
            using (var enumerator = enumerable.GetEnumerator())
            {
                bool hasData = enumerator.MoveNext();
                bool hasPrevious = false;

                while (hasData)
                {
                    if (hasPrevious)
                    {
                        T previous = enumerator.Current;
                        T current = default(T);
                        hasData = enumerator.MoveNext();
                        if (hasData)
                        {
                            current = enumerator.Current;
                        }
                        yield return new EnumeratingItems<T>(previous, current, hasData);
                    }

                    hasPrevious = true;
                }
            }
        }

        public static Dictionary<TKey, TElement> ToDictionarySkippingDuplicates<TSource, TKey, TElement>(
            this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            return source.GroupBy(keySelector).ToDictionary(g => g.Key, g => elementSelector(g.First()));
        }

        public static void AddRangeMayBeNull<T>(this List<T> list, IEnumerable<T> data)
        {
            if (data == null)
            {
                return;
            }

            list.AddRange(data);
        }

        public static IEnumerable<TResult> OfTypeEx<TResult>(this IEnumerable source, bool includeSubclasses)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            return includeSubclasses ? OfTypeAndSubtypeIterator<TResult>(source) : OfExactTypeIterator<TResult>(source);
        }

        public static IEnumerable<TResult> OfExactType<TResult>(this IEnumerable source, bool includeSubclasses)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            return OfExactTypeIterator<TResult>(source);
        }

        private static IEnumerable<TResult> OfTypeAndSubtypeIterator<TResult>(IEnumerable source)
        {
            foreach (object obj in source)
            {
                if (obj is TResult)
                    yield return (TResult)obj;
            }
        }

        private static IEnumerable<TResult> OfExactTypeIterator<TResult>(IEnumerable source)
        {
            Type requestedType = typeof(TResult);
            foreach (object obj in source)
            {
                if (obj.GetType() == requestedType)
                    yield return (TResult)obj;
            }
        }
    }

    public static class DictionaryEx
    {
        public static void AddIfNotPresent<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
        }

        public static TValue GetEx<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
        {
            try
            {
                return dictionary[key];
            }
            catch (Exception e)
            {
                throw new Exception($"Given dictionary doesn't contain key [{key}]. Present keys: [{dictionary.Keys.ToCsvString()}]", e);
            }
        }
    }

    public static class ArrayExtensions
    {
        public static int GetIndexOfMinItem<T>(this T[] array)
        {
            return GetIndexOfMinItem(array, Comparer<T>.Default);
        }

        public static bool HasIndex<T>(this T[] array, int idx)
        {
            return idx >= 0 && array != null && idx < array.Length;
        }

        public static int GetIndexOfMinItem<T>(this T[] array, IComparer<T> comparer)
        {
            if (array == null || array.Length == 0)
            {
                return -1;
            }

            int minIndex = 0;
            T minValue = array[0];

            for (int idx = 1; idx < array.Length; idx++)
            {
                if (comparer.Compare(array[idx], minValue) < 0)
                {
                    minIndex = idx;
                }
            }

            return minIndex;
        }
    }

    public static class LoggerEx
    {
        public static void LogConsoleInfo(this ILogger logger, string logEntry)
        {
            LogConsoleInfo(logger, LogLevel.Info, logEntry);
        }

        public static void LogConsoleInfo(this ILogger logger, LogLevel logLevel, string logEntry)
        {
            logger.Log(logLevel, logEntry);
            LogFactory.OriginalConsoleWriter.WriteLine(logEntry);
        }

        public static void LogConsoleInfo(this ILogger logger, string messageFmt, params object[] args)
        {
            string logEntry = string.Format(messageFmt, args);
            LogConsoleInfo(logger, LogLevel.Info, logEntry);
        }

        public static void LogConsoleInfo(this ILogger logger, LogLevel logLevel, string messageFmt, params object[] args)
        {
            string logEntry = string.Format(messageFmt, args);
            LogConsoleInfo(logger, logLevel, logEntry);
        }

        public static void LogConditionalConsoleInfo(this ILogger logger, bool logToConsole, string logEntry)
        {
            logger.Log(LogLevel.Info, logEntry);
            if (logToConsole)
            {
                LogFactory.OriginalConsoleWriter.WriteLine(logEntry);
            }
        }

        public static void LogConditionalConsoleException(this ILogger logger, LogLevel logLevel, bool logToConsole, Exception e, string logEntry)
        {
            logger.LogException(LogLevel.Info, logEntry, e);
            if (logToConsole)
            {
                LogFactory.OriginalConsoleWriter.WriteLine(logEntry + " " + e.Message);
            }
        }

        public static void LogConditionalConsoleInfo(this ILogger logger, bool logToConsole, string messageFmt, params object[] args)
        {
            string logEntry = string.Format(messageFmt, args);
            LogConditionalConsoleInfo(logger, logToConsole, logEntry);
        }
    }

    public static class TypeEx
    {
        /// <summary>
        /// Gets a Type instance matching the specified class name with just non-namespace qualified class name.
        /// </summary>
        /// <param name="className">Name of the class sought.</param>
        /// <returns>Type that have the class name specified. They may not be in the same namespace.</returns>
        public static Type GetTypeByName(string className)
        {
            return GetTypeByName(className, (string) null);
        }

        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }

        public static IEnumerable<Type> GetDeriveTypes(this Type baseType)
        {
            return
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(a => a.GetLoadableTypes())
                    .Where(t => t.IsSubclassOf(baseType));
        }

        private static Type GetTypeByName(string className, string alternateClassName)
        {
            var assembliesOnStack =
                new StackTrace().GetFrames().Select(frame => frame.GetMethod().ReflectedType.Assembly).Distinct();
            //optimize search by first searching just through assemblies on the stack
            Type searchedType = GetTypeByName(className, assembliesOnStack);

            if (searchedType == null)
            {
                searchedType = GetTypeByName(alternateClassName, assembliesOnStack);
            }

            if (searchedType == null)
            {
                searchedType = GetTypeByName(className, AppDomain.CurrentDomain.GetAssemblies());
            }

            if (searchedType == null)
            {
                searchedType = GetTypeByName(alternateClassName, AppDomain.CurrentDomain.GetAssemblies());
            }

            if (searchedType == null)
            {
                throw new Exception(String.Format("No type found in all loaded assemblies that would have short name: {0}", className));
            }

            return searchedType;
        }

        private static Type GetTypeByName(string className, IEnumerable<Assembly> assembliesToSearch)
        {
            Type searchedType = null;

            foreach (Assembly a in assembliesToSearch)
            {
                Type[] assemblyTypes = a.GetTypes();
                for (int j = 0; j < assemblyTypes.Length; j++)
                {
                    if (assemblyTypes[j].Name == className)
                    {
                        if (searchedType != null)
                        {
                            throw new Exception(String.Format("Ambiguous type (given without namespace): {0}, found at least two matching types: {1} and {2}",
                                className, searchedType.FullName, assemblyTypes[j].FullName));
                        }
                        else
                        {
                            searchedType = assemblyTypes[j];
                        }

                    }
                }
            }

            return searchedType;
        }

        public static T ResolveType<T>(string typeClassName)
        {
            Type targetType = typeof (T);
            Type[] genericArgs = targetType.GenericTypeArguments;
            string alternateTypeClassName = null;

            if (genericArgs.Length > 0 && !typeClassName.Contains('`'))
            {
                alternateTypeClassName = typeClassName + '`' + genericArgs.Length.ToString();
            }

            Type type = TypeEx.GetTypeByName(typeClassName, alternateTypeClassName);

            if (type.IsGenericTypeDefinition)
            {
                type = type.MakeGenericType(genericArgs);
            }

            if (targetType.IsAssignableFrom(type))
            {
                return (T)System.Activator.CreateInstance(type);
            }
            else
            {
                throw new Exception(string.Format("Given class name: {0} resolved as [{1}] is not assignable from {2} - unexpected.",
                    typeClassName, type.FullName, typeof(T).Name));
            }
        }
    }

    public static class ActionEx
    {
        public static readonly Action Empty = delegate {};
    }

    public static class ActionEx<T>
    {
        public static readonly Action<T> Empty = delegate { };
    }

    public class GcEx
    {
        public static long GetTotalConsumedMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            return GC.GetTotalMemory(true);
        }

        private long _initialConsumedMemory;
        public GcEx() { }

        public long TotalConsumedBytes { get; private set; }
        public double TotalConsumedMBs { get { return TotalConsumedBytes / Constants.OneMillionDouble; } }

        public void StartObservingMemoryConsumption()
        {
            _initialConsumedMemory = GetTotalConsumedMemory();
        }

        public void StopObservingMemoryConsumption()
        {
            TotalConsumedBytes += GetTotalConsumedMemory() - _initialConsumedMemory;
        }

    }
}

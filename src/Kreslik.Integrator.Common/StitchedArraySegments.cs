﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Kreslik.Integrator.Common
{
    public class StitchedArraySegments<T> : IReadOnlyList<T>
    {
        //Array segment needed - so that CopyTo works correctly
        private readonly IEnumerable<ArraySegment<T>> _segments;

        public StitchedArraySegments(params ArraySegment<T>[] segments)
        {
            _segments = segments;
        }

        public StitchedArraySegments(params T[][] segments)
        {
            _segments = segments.Select(item => new ArraySegment<T>(item));
        }

        public static StitchedArraySegments<T> CreateByItemsCopying(IEnumerable<IReadOnlyList<T>> segments)
        {
            return new StitchedArraySegments<T>(segments);
        }

        public static StitchedArraySegments<T> CreateByItemsCopying(params IReadOnlyList<T>[] segments)
        {
            return new StitchedArraySegments<T>(segments);
        }

        private StitchedArraySegments(IEnumerable<IReadOnlyList<T>> segments)
        {
            _segments = segments.Select(item => new ArraySegment<T>(item.ToArray()));
        }

        public StitchedArraySegments(params StitchedArraySegments<T>[] segments)
            :this((IEnumerable<StitchedArraySegments<T>>) segments)
        { }

        public StitchedArraySegments(IEnumerable<StitchedArraySegments<T>> segments)
        {
            _segments = segments.SelectMany(x => x._segments).ToArray();
        }

        public IEnumerable<T> EnumerateData()
        {
            return _segments.SelectMany(s => s);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.EnumerateData().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count { get { return _segments.Sum(s => s.Count); } }

        public T this[int index]
        {
            get
            {
                int originalIndex = index;

                foreach (ArraySegment<T> subSegment in _segments)
                {
                    if (subSegment.Count > index)
                    {
                        return ((IReadOnlyList<T>)subSegment)[index];
                    }
                    index -= subSegment.Count;
                }

                throw new IndexOutOfRangeException(
                    $"Indexing StitchedArraySegments with index [{originalIndex}], while internal array segments has {this.Count} items in total (items per each segment: {this._segments.Select(s => s.Count).ToCsvString()})");
            }
        }

        //[MethodImpl(MethodImplOptions.InternalCall)]
        //private static extern void CopyToNative(object source, int startIndex, IntPtr destination, int length);

        private static Action<object, int, IntPtr, int> _copyToNativeDelegate = FetchCopyToNativeDelegate();

        private static Action<object, int, IntPtr, int> FetchCopyToNativeDelegate()
        {
            var methodInfo = typeof(Marshal).GetMethod("CopyToNative", BindingFlags.Static | BindingFlags.NonPublic);
            Action<object, int, IntPtr, int> copyToNativeDelegate =
                (Action<object, int, IntPtr, int>)
                Delegate.CreateDelegate(typeof(Action<object, int, IntPtr, int>), methodInfo);
            return copyToNativeDelegate;
        }

        private static void CopyToNative(object source, int startIndex, IntPtr destination, int lengt)
        {
            _copyToNativeDelegate(source, startIndex, destination, lengt);
        }

        public void CopyTo(IntPtr nativeArrayPtr)
        {

            int typeSize = Marshal.SizeOf<T>();

            foreach (ArraySegment<T> subSegment in _segments)
            {
                CopyToNative(subSegment.Array, subSegment.Offset, nativeArrayPtr, subSegment.Count);
                //CAREFUL!! - IntPtr arithmetic is in Bytes
                nativeArrayPtr = IntPtr.Add(nativeArrayPtr, subSegment.Count * typeSize);
            }
        }
    }








    //This has advantage of not using ArraySegment directly and hence can directly accomodate arrays etc.
    // However(!) can bring disadvantages of (un)boxing - as wehn ArraySegment used (which is struct) and accesed
    // via IList pointer - it needs to be boxed
    public class StitchedArraySegmentsNew<T> : IReadOnlyList<T>
    {
        //Array segment needed - so that CopyTo works correctly
        private readonly IEnumerable<IList<T>> _segments;

        public StitchedArraySegmentsNew(params IList<T>[] segments)
        {
            _segments = segments;
        }

        public StitchedArraySegmentsNew(params T[][] segments)
        {
            _segments = segments;
        }

        public static StitchedArraySegmentsNew<T> CreateByItemsCopying(IEnumerable<IReadOnlyList<T>> segments)
        {
            return new StitchedArraySegmentsNew<T>(segments);
        }

        public static StitchedArraySegmentsNew<T> CreateByItemsCopying(params IReadOnlyList<T>[] segments)
        {
            return new StitchedArraySegmentsNew<T>(segments);
        }

        private StitchedArraySegmentsNew(IEnumerable<IReadOnlyList<T>> segments)
        {
            _segments = segments.Select(item => item.ToArray());
        }

        public StitchedArraySegmentsNew(params StitchedArraySegmentsNew<T>[] segments)
            : this((IEnumerable<StitchedArraySegmentsNew<T>>)segments)
        { }

        public StitchedArraySegmentsNew(IEnumerable<StitchedArraySegmentsNew<T>> segments)
        {
            _segments = segments.SelectMany(x => x._segments).ToArray();
        }

        public IEnumerable<T> EnumerateData()
        {
            return _segments.SelectMany(s => s);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.EnumerateData().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count { get { return _segments.Sum(s => s.Count); } }

        public T this[int index]
        {
            get
            {
                int originalIndex = index;

                foreach (IList<T> subSegment in _segments)
                {
                    if (subSegment.Count > index)
                    {
                        return subSegment[index];
                    }
                    index -= subSegment.Count;
                }

                throw new IndexOutOfRangeException(
                    $"Indexing StitchedArraySegments with index [{originalIndex}], while internal array segments has {this.Count} items in total (items per each segment: {this._segments.Select(s => s.Count).ToCsvString()})");
            }
        }

        //[MethodImpl(MethodImplOptions.InternalCall)]
        //private static extern void CopyToNative(object source, int startIndex, IntPtr destination, int length);

        private static Action<object, int, IntPtr, int> _copyToNativeDelegate = FetchCopyToNativeDelegate();

        private static Action<object, int, IntPtr, int> FetchCopyToNativeDelegate()
        {
            var methodInfo = typeof(Marshal).GetMethod("CopyToNative", BindingFlags.Static | BindingFlags.NonPublic);
            Action<object, int, IntPtr, int> copyToNativeDelegate =
                (Action<object, int, IntPtr, int>)
                Delegate.CreateDelegate(typeof(Action<object, int, IntPtr, int>), methodInfo);
            return copyToNativeDelegate;
        }

        private static void CopyToNative(object source, int startIndex, IntPtr destination, int lengt)
        {
            _copyToNativeDelegate(source, startIndex, destination, lengt);
        }

        public void CopyTo(IntPtr nativeArrayPtr)
        {

            int typeSize = Marshal.SizeOf<T>();

            foreach (IList<T> subSegment in _segments)
            {
                int offset = 0;
                object o = subSegment;

                if (subSegment is ArraySegment<T>)
                {
                    offset = ((ArraySegment<T>) subSegment).Offset;
                }
                else if (subSegment is T[])
                {
                    //that is it; offset 0; object same
                }
                else
                {
                    o = subSegment.ToArray();
                }

                CopyToNative(o, offset, nativeArrayPtr, subSegment.Count);
                //CAREFUL!! - IntPtr arithmetic is in Bytes
                nativeArrayPtr = IntPtr.Add(nativeArrayPtr, subSegment.Count * typeSize);
            }
        }
    }
}

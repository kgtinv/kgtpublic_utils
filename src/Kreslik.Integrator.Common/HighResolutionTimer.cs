﻿//#define HIGHRESTIMERS_TRACING
#define THREADPOOLEX_TRACING

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // IMPORTANT:
    //    - We use sorted queue (needed to ensure as few work during tick as possible - as ticking is quite often)
    //    - We also lock (is it possible to have any sorted structure without locking?)
    //    - So queue can get contended if there are many timers and rescheduled often
    //
    //    - We would likely need to: Make safe timer a first class citizen in here (so sync rescheduling performed directly here); 
    //                   improve async rescheduling by getting rid of sorted queue
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////


    public interface IAccurateTimer
    {
        //void Close();
    }

    internal interface IAccurateTimerInternal : IAccurateTimer
    {
        TimerTaskFlagUtils.TimerTaskMultiFlag MultiFlag { get; set; }
    }

    //public enum TimerExecutionType
    //{
    //    FastSynchronousHighPrecison_Cancellable = 0,
    //    FastSynchronousHighPrecison_NonCancellableFasterPooling = 1,
    //    SlowAsynchronousLowPrecision = 2
    //}

    public interface IHighResolutionTimerManager
    {
        /// <summary>
        /// Creates and registers timer
        /// </summary>
        /// <param name="callback">callback to be called</param>
        /// <param name="dueSpan">relative due</param>
        /// <param name="taskExecutionPriority">Priority of the task once due is up and it's going to be entered into ThreadPoolEx</param>
        /// <param name="requiredTimerPrecision">Precision of timer - as it can be coalesced with other timers</param>
        /// <param name="cancelableAndReschedulable_slowerPooling">Indication of interest for Canceling or Rescheduling operation. if false - those operations will not be possible, but timer will have faster pooling</param>
        /// <param name="isUltraQuickTask">Indication of complexity of task. If true, task is going to be executed synchronously</param>
        /// <returns>Handle to cancel timer prematurely</returns>
        IAccurateTimer RegisterTimer(Action callback, TimeSpan dueSpan,
            TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool cancelableAndReschedulable_slowerPooling,
            bool isUltraQuickTask);

        /// <summary>
        /// Creates and registers timer
        /// </summary>
        /// <param name="callback">callback to be called</param>
        /// <param name="due">absolute due</param>
        /// <param name="taskExecutionPriority">Priority of the task once due is up and it's going to be entered into ThreadPoolEx</param>
        /// <param name="requiredTimerPrecision">Precision of timer - as it can be coalesced with other timers</param>
        /// <param name="cancelableAndReschedulable_slowerPooling">Indication of interest for Canceling or Rescheduling operation. if false - those operations will not be possible, but timer will have faster pooling</param>
        /// <param name="isUltraQuickTask">Indication of complexity of task. If true, task is going to be executed synchronously</param>
        /// <returns>Handle to cancel timer prematurely</returns>
        IAccurateTimer RegisterTimer(Action callback, DateTime due,
            TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool cancelableAndReschedulable_slowerPooling,
            bool isUltraQuickTask);

        /// <summary>
        /// Attempts to create and registers timer
        /// </summary>
        /// <param name="callback">callback to be called</param>
        /// <param name="due">absolute due</param>
        /// <param name="taskExecutionPriority">Priority of the task once due is up and it's going to be entered into ThreadPoolEx</param>
        /// <param name="requiredTimerPrecision">Precision of timer - as it can be coalesced with other timers</param>
        /// <param name="cancelableAndReschedulable_slowerPooling">Indication of interest for Canceling or Rescheduling operation. if false - those operations will not be possible, but timer will have faster pooling</param>
        /// <param name="isUltraQuickTask">Indication of complexity of task. If true, task is going to be executed synchronously</param>
        /// <param name="timer">Resulting timer if created</param>
        /// <returns>true if timer was created (due is in future)</returns>
        bool TryRegisterTimerIfInFuture(Action callback, DateTime due,
            TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool cancelableAndReschedulable_slowerPooling,
            bool isUltraQuickTask, out IAccurateTimer timer);

        /// <summary>
        /// Cancels and nullifies the timer (therefore ref); calls on null (and therefore multiple calls) are OK
        /// </summary>
        /// <param name="timer"></param>
        void CancelTimer(ref IAccurateTimer timer);

        IAccurateTimer CreateUnscheduledTimer(Action callback, TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool isUltraQuickTask);

        void RescheduleTimer(IAccurateTimer timer, TimeSpan dueSpan, bool alreadyExecuted_doNotSearchInQueue);

        void RescheduleTimer(IAccurateTimer timer, DateTime dueTime, bool alreadyExecuted_doNotSearchInQueue);

#if HIGHRESTIMERS_TRACING
        event Action<string> OnLogEntry;
#endif
    }

    internal class HighResolutionTimerManager : IHighResolutionTimerManager
    {
        /// <summary>
        /// Comparer for comparing two keys, handling equality as beeing greater
        /// Use this Comparer e.g. with SortedLists or SortedDictionaries, that don't allow duplicate keys
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        internal class DuplicateKeyreverseComparer<TKey>
            :
                IComparer<TKey> where TKey : IComparable
        {
            #region IComparer<TKey> Members

            public int Compare(TKey x, TKey y)
            {
                int result = y.CompareTo(x);

                if (result == 0)
                    return 1; // Handle equality as beeing greater
                else
                    return result;
            }

            #endregion
        }

        //private const int _REPEAT_INTERVAL_MS = 10;
        //private const int _TIMERS_RESOLUTION_THRESHOLD_MS = 1;
        private const int _TIMERS_POOL_SIZE = 10;

        private static TimeSpan _recheckInterval = TimeSpan.FromMilliseconds(Kreslik.Integrator.NMbgDevIO.NMbgTimer._MAX_POLL_INTERVAL_MS);
        //private static TimeSpan _timersResolution = TimeSpan.FromMilliseconds(_TIMERS_RESOLUTION_THRESHOLD_MS);
        private static long _timersResolutionTicks = TimeSpan.FromMilliseconds(Kreslik.Integrator.NMbgDevIO.NMbgTimer._TIMERS_RESOLUTION_THRESHOLD_MS).Ticks;

        internal static TimeSpan PollInterval { get { return _recheckInterval; } }

        private ManualResetEventSlim _nextCheckEvent = new ManualResetEventSlim(false);
        private object _timerQueueLocker = new object();
        private SortedList<DateTime, HighResTimer> _timersSortedQueue = new SortedList<DateTime, HighResTimer>(new DuplicateKeyreverseComparer<DateTime>());
        private DateTime _nextEventDue = DateTime.MaxValue;
        private bool _shutdown = false;

        private Func<DateTime> _getAccurateTimeStampFunc;
        private Func<long> _queryAccurateTimeStampFunc;
        private ThreadPoolEx _threadPoolEx;

        internal HighResolutionTimerManager(Func<long> queryAccurateTimeStampFunc, Func<DateTime> getAccurateTimeStampFunc, ThreadPoolEx threadPoolEx)
        {
            if (HighResTimer.OnTimerAvailableToPool != null)
                throw new Exception("Only a single instance of HighResolutionTimerManager can be created per AppDomain");

            this._queryAccurateTimeStampFunc = queryAccurateTimeStampFunc;
            this._getAccurateTimeStampFunc = getAccurateTimeStampFunc;
            this._threadPoolEx = threadPoolEx;
            HighResTimer.OnTimerAvailableToPool += this.ReturnTimerToPoolSynchronized;
            _timersPool = Enumerable.Repeat(0, _TIMERS_POOL_SIZE).Select(i => new HighResTimer()).ToArray();
            Task.Factory.StartNew(TimerLoop, TaskCreationOptions.LongRunning);

#if HIGHRESTIMERS_TRACING
            throw new Exception("Compiled with HIGHRESTIMERS_TRACING - this will have severe affect on performance. Not to be used in production");
#endif
        }

#if HIGHRESTIMERS_TRACING
        public event Action<string> OnLogEntry;

        private string TimerQueueDump()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("---- Timer Queue at {0:HH:mm:ss.fffffff} -------", _getAccurateTimeStampFunc());
            sb.AppendLine();
            for (int idx = _timersSortedQueue.Count - 1; idx >= 0; idx--)
            {
                sb.AppendLine(_timersSortedQueue.Values[idx].ToString());
            }

            sb.AppendLine("-------------------------------------------------");

            return sb.ToString();
        }

        private string TimerPoolDump()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("-- Timer Pool at {0:HH:mm:ss.fffffff} idx: {1} --", _getAccurateTimeStampFunc(), _timersPoolIndex);
            sb.AppendLine();
            for (int idx = 0; idx < _timersPool.Length; idx++)
            {
                sb.AppendLine(idx.ToString() + " " + (_timersPool[idx] != null ? _timersPool[idx].ToString() : "NULL"));
            }

            sb.AppendLine("---------------------------------------------------");

            return sb.ToString();
        }
#endif

        private void TimerLoop()
        {
            TimeSpan loopWaitInterval = _recheckInterval;

            while (!_shutdown)
            {
                //this incurs implicit mem. barrier so we should have fresh caches
                _nextCheckEvent.Wait(loopWaitInterval);
                loopWaitInterval = _recheckInterval;
                long nowTicks = _queryAccurateTimeStampFunc();
                long ticksToNextTimer = _nextEventDue.Ticks - nowTicks;

                if (ticksToNextTimer <= _timersResolutionTicks)
                {
                    lock (_timerQueueLocker)
                    {
#if HIGHRESTIMERS_TRACING
                        this.OnLogEntry(this.TimerQueueDump());
#endif

                        bool done = false;

                        do
                        {
                            if (_timersSortedQueue.Count == 0)
                            {
                                _nextEventDue = DateTime.MaxValue;
                                done = true;
                            }
                            else
                            {
                                HighResTimer topTimer =
                                _timersSortedQueue.Values[_timersSortedQueue.Count - 1];
                                if (topTimer.EarliestDue.Ticks - nowTicks <= _timersResolutionTicks)
                                {
                                    _timersSortedQueue.RemoveAt(_timersSortedQueue.Count - 1);
#if HIGHRESTIMERS_TRACING
                                    this.OnLogEntry("Executing timer " + topTimer.ToString());
#endif
                                    this.ExecuteTimerUnsynchronized(topTimer);
#if HIGHRESTIMERS_TRACING
                                    this.OnLogEntry(this.TimerQueueDump());
#endif
                                }
                                else
                                {
                                    _nextEventDue = this.GetNextCoallescedDueUnsynchronized();
                                    done = true;
                                }
                            }
                        } while (!done);
#if HIGHRESTIMERS_TRACING
                        this.OnLogEntry("Next due (after exec): " + _nextEventDue.ToString("HH:mm:ss.fffffff"));
#endif
                    }

                    //just for sure obtain the accurate stamp again
                    // As executing callbacks might have taken some time
                    nowTicks = this._getAccurateTimeStampFunc().Ticks;
                    ticksToNextTimer = _nextEventDue.Ticks - nowTicks;
                }

                if (ticksToNextTimer < _recheckInterval.Ticks)
                    loopWaitInterval = new TimeSpan(Math.Max(0, ticksToNextTimer));
            }
        }

        public IAccurateTimer CreateUnscheduledTimer(Action callback,
            TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool isUltraQuickTask)
        {
            var timer = new HighResTimer();
            timer.OverrideContent(callback, DateTime.MaxValue,
                TimerTaskFlagUtils.GetTimerTaskMultiFlag(taskExecutionPriority,
                    requiredTimerPrecision, true, isUltraQuickTask));
            return timer;
        }

        private void ExecuteTimerUnsynchronized(HighResTimer timer)
        {
            if (TimerTaskFlagUtils.IsUltraFastTask(timer.MultiFlag))
            {
                timer.Execute();
                if (!TimerTaskFlagUtils.IsCancellableTimer(timer.MultiFlag))
                {
                    timer.Close();
                    this.ReturnTimerToPoolUnsynchronized(timer);
                }
            }
            else
            {
                this._threadPoolEx.QueueWorkItem(timer);
            }

        }

        private DateTime GetNextCoallescedDueUnsynchronized()
        {
            //For empty queue there is no next due
            DateTime nextEventDue = DateTime.MaxValue;

            // While next timer due range is at least partially in, 
            //     keep pushing the next due forward to coalesce as much timers in one due as possible

            int coalescedCnt = 0;
            for (int idx = _timersSortedQueue.Count - 1;
                idx >= 0 && nextEventDue > _timersSortedQueue.Values[idx].EarliestDue;
                coalescedCnt++, idx--)
            {
                nextEventDue = DateTimeEx.Min(nextEventDue, _timersSortedQueue.Values[idx].LatestDue);
            }

            //however if no coalescing happen then plan timer exactly on time (no need to delay it)
            if (coalescedCnt == 1)
                nextEventDue = _timersSortedQueue.Values[_timersSortedQueue.Count - 1].Due;

            return nextEventDue;
        }

        /// <summary>
        /// Creates and registers timer
        /// </summary>
        /// <param name="callback">callback to be called</param>
        /// <param name="dueSpan">relative due</param>
        /// <param name="taskExecutionPriority">Priority of the task once due is up and it's going to be entered into ThreadPoolEx</param>
        /// <param name="requiredTimerPrecision">Precision of timer - as it can be coalesced with other timers</param>
        /// <param name="cancelableAndReschedulable_slowerPooling">Indication of interest for Canceling or Rescheduling operation. if false - those operations will not be possible, but timer will have faster pooling</param>
        /// <param name="isUltraQuickTask">Indication of complexity of task. If true, task is going to be executed synchronously</param>
        /// <returns>Handle to cancel timer prematurely</returns>
        public IAccurateTimer RegisterTimer(Action callback, TimeSpan dueSpan, TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool cancelableAndReschedulable_slowerPooling,
            bool isUltraQuickTask)
        {
            return this.RegisterTimerInternal(callback, dueSpan, _getAccurateTimeStampFunc() + dueSpan,
                TimerTaskFlagUtils.GetTimerTaskMultiFlag(taskExecutionPriority, requiredTimerPrecision,
                    cancelableAndReschedulable_slowerPooling, isUltraQuickTask));
        }

        /// <summary>
        /// Creates and registers timer
        /// </summary>
        /// <param name="callback">callback to be called</param>
        /// <param name="due">absolute due</param>
        /// <param name="taskExecutionPriority">Priority of the task once due is up and it's going to be entered into ThreadPoolEx</param>
        /// <param name="requiredTimerPrecision">Precision of timer - as it can be coalesced with other timers</param>
        /// <param name="cancelableAndReschedulable_slowerPooling">Indication of interest for Canceling or Rescheduling operation. if false - those operations will not be possible, but timer will have faster pooling</param>
        /// <param name="isUltraQuickTask">Indication of complexity of task. If true, task is going to be executed synchronously</param>
        /// <returns>Handle to cancel timer prematurely</returns>
        public IAccurateTimer RegisterTimer(Action callback, DateTime due, TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool cancelableAndReschedulable_slowerPooling,
            bool isUltraQuickTask)
        {
            return this.RegisterTimerInternal(callback, due - _getAccurateTimeStampFunc(), due,
                TimerTaskFlagUtils.GetTimerTaskMultiFlag(taskExecutionPriority, requiredTimerPrecision,
                    cancelableAndReschedulable_slowerPooling, isUltraQuickTask));
        }

        /// <summary>
        /// Attempts to create and registers timer
        /// </summary>
        /// <param name="callback">callback to be called</param>
        /// <param name="due">absolute due</param>
        /// <param name="taskExecutionPriority">Priority of the task once due is up and it's going to be entered into ThreadPoolEx</param>
        /// <param name="requiredTimerPrecision">Precision of timer - as it can be coalesced with other timers</param>
        /// <param name="cancelableAndReschedulable_slowerPooling">Indication of interest for Canceling or Rescheduling operation. if false - those operations will not be possible, but timer will have faster pooling</param>
        /// <param name="isUltraQuickTask">Indication of complexity of task. If true, task is going to be executed synchronously</param>
        /// <param name="timer">Resulting timer if created</param>
        /// <returns>true if timer was created (due is in future)</returns>
        public bool TryRegisterTimerIfInFuture(Action callback, DateTime due, TimerTaskFlagUtils.WorkItemPriority taskExecutionPriority,
            TimerTaskFlagUtils.TimerPrecision requiredTimerPrecision, bool cancelableAndReschedulable_slowerPooling,
            bool isUltraQuickTask, out IAccurateTimer timer)
        {
            TimeSpan dueSpan = due - _getAccurateTimeStampFunc();

            if (dueSpan > TimeSpan.Zero)
                timer = this.RegisterTimerInternal(callback, dueSpan, due,
                    TimerTaskFlagUtils.GetTimerTaskMultiFlag(taskExecutionPriority, requiredTimerPrecision,
                        cancelableAndReschedulable_slowerPooling, isUltraQuickTask));
            else
                timer = null;

            return dueSpan > TimeSpan.Zero;
        }

//        private IAccurateTimer RegisterTimerInternal(Action callback, TimeSpan dueSpan, DateTime due,
//            TimerExecutionType timerType)
//        {
//            if (timerType == TimerExecutionType.SlowAsynchronousLowPrecision)
//            {
//                dueSpan = dueSpan > TimeSpan.Zero ? dueSpan : TimeSpan.Zero;
//                //Task.Delay(dueSpan).ContinueWith(t => HighResTimer.ExecuteInternalStatic(callback));
//                return new TimerWrapper(callback, dueSpan);
//            }

//            lock (_timerQueueLocker)
//            {
//                HighResTimer timer = this.CreateTimer(callback, due, timerType);
//#if HIGHRESTIMERS_TRACING
//                this.OnLogEntry("Registering timer " + timer.ToString());
//#endif
//                _timersSortedQueue.Add(timer.Due, timer);
//#if HIGHRESTIMERS_TRACING
//                this.OnLogEntry(this.TimerQueueDump());
//                this.OnLogEntry(this.TimerPoolDump());
//#endif
//                if (timer.Due < _nextEventDue)
//                {
//                    _nextEventDue = timer.Due;
//                    if (dueSpan < _recheckInterval)
//                        _nextCheckEvent.Set();
//                }
//#if HIGHRESTIMERS_TRACING
//                this.OnLogEntry("Next due (after register): " + _nextEventDue.ToString("HH:mm:ss.fffffff"));
//#endif
//                //if this is noncancellable timer than we should not even return handle
//                if (timerType == TimerExecutionType.FastSynchronousHighPrecison_NonCancellableFasterPooling)
//                    return null;
//                else
//                    return timer;
//            }
//        }

        private IAccurateTimer RegisterTimerInternal(Action callback, TimeSpan dueSpan, DateTime due,
            TimerTaskFlagUtils.TimerTaskMultiFlag timerTaskMultiFlag)
        {
            lock (_timerQueueLocker)
            {
                HighResTimer timer = this.CreateTimer(callback, due, timerTaskMultiFlag);

                this.RegisterTimerInternalUnsynchronized(dueSpan, timer);

                //if this is noncancellable timer than we should not even return handle
                if (!TimerTaskFlagUtils.IsCancellableTimer(timerTaskMultiFlag))
                    return null;
                else
                    return timer;
            }
        }

        private void RegisterTimerInternalUnsynchronized(TimeSpan dueSpan, HighResTimer timer)
        {
#if HIGHRESTIMERS_TRACING
                this.OnLogEntry("Registering timer " + timer.ToString());
#endif
                _timersSortedQueue.Add(timer.Due, timer);
#if HIGHRESTIMERS_TRACING
                this.OnLogEntry(this.TimerQueueDump());
                this.OnLogEntry(this.TimerPoolDump());
#endif
                if (timer.Due < _nextEventDue)
                {
                    _nextEventDue = this.GetNextCoallescedDueUnsynchronized();
                    if (dueSpan < _recheckInterval)
                        _nextCheckEvent.Set();
                }
#if HIGHRESTIMERS_TRACING
                this.OnLogEntry("Next due (after register): " + _nextEventDue.ToString("HH:mm:ss.fffffff"));
#endif
        }

        public void CancelTimer(ref IAccurateTimer timer)
        {
            //timer.Close();

            HighResTimer highResTimer = timer as HighResTimer;
            if (highResTimer != null)
            {
                if (!TimerTaskFlagUtils.IsCancellableTimer(highResTimer.MultiFlag))
                    throw new Exception("Attempt to cancel timer which was created as noncancellable");

                lock (_timerQueueLocker)
                {
#if HIGHRESTIMERS_TRACING
                    this.OnLogEntry("Canceling timer " + timer.ToString());
#endif
                    //prevent duble return to pool
                    //also - someone could nullify ref parameter externaly - if that would happen it is a good chance
                    //     that it was parallel invocation of CancelTimer that got inside lock region earlier and nullified timer afterwards
                    if (timer != null && highResTimer.Close())
                    {
                        if (!highResTimer.Executed)
                        {
                            //revrse order - as more often we cancel recent timers
                            for (int idx = _timersSortedQueue.Count - 1; idx >= 0; idx--)
                            {
                                if (_timersSortedQueue.Values[idx] == timer)
                                {
                                    //Removing top timer - need to update the next due
                                    if (idx == _timersSortedQueue.Count - 1)
                                    {
                                        if (idx - 1 >= 0)
                                        {
                                            _nextEventDue = _timersSortedQueue.Values[idx - 1].Due;
                                        }
                                        else
                                        {
                                            _nextEventDue = DateTime.MaxValue;
                                        }
                                    }
                                    _timersSortedQueue.RemoveAt(idx);
                                    break;
                                }
                            }
                        }

                        this.ReturnTimerToPoolUnsynchronized(highResTimer);
                    }
#if HIGHRESTIMERS_TRACING
                    this.OnLogEntry(this.TimerPoolDump());
#endif
                }
            }

            timer = null;
        }

        public void RescheduleTimer(IAccurateTimer timer, TimeSpan dueSpan, bool alreadyExecuted_doNotSearchInQueue)
        {
            DateTime dueTime;
            if (dueSpan == Timeout.InfiniteTimeSpan || dueSpan == TimeSpan.MaxValue)
                dueTime = DateTime.MaxValue;
            else
                dueTime = _getAccurateTimeStampFunc() + dueSpan;

            this.RescheduleTimerInternal(timer, dueSpan, dueTime, alreadyExecuted_doNotSearchInQueue);
        }

        public void RescheduleTimer(IAccurateTimer timer, DateTime dueTime, bool alreadyExecuted_doNotSearchInQueue)
        {
            TimeSpan dueSpan;
            if (dueTime == DateTime.MinValue)
                dueSpan = TimeSpan.Zero;
            else
                dueSpan = dueTime - _getAccurateTimeStampFunc();

            this.RescheduleTimerInternal(timer, dueSpan, dueTime, alreadyExecuted_doNotSearchInQueue);
        }

        private void RescheduleTimerInternal(IAccurateTimer timer, TimeSpan dueSpan, DateTime dueTime,
            bool alreadyExecuted_doNotSearchInQueue)
        {
            //timer.Close();

            HighResTimer highResTimer = timer as HighResTimer;
            if (highResTimer != null)
            {
                if (!TimerTaskFlagUtils.IsCancellableTimer(highResTimer.MultiFlag))
                    throw new Exception("Attempt to reschedule timer which was created as non-cancellable and non-reschedulable");

                if (!alreadyExecuted_doNotSearchInQueue)
                {
                    lock (_timerQueueLocker)
                    {
#if HIGHRESTIMERS_TRACING
                    this.OnLogEntry("Canceling timer " + timer.ToString());
#endif

                        if (!highResTimer.Executed)
                        {
                            for (int idx = 0; idx < _timersSortedQueue.Count; idx++)
                            {
                                if (_timersSortedQueue.Values[idx] == timer)
                                {
                                    //Removing top timer - need to update the next due
                                    if (idx == _timersSortedQueue.Count - 1)
                                    {
                                        if (idx - 1 >= 0)
                                        {
                                            _nextEventDue = _timersSortedQueue.Values[idx - 1].Due;
                                        }
                                        else
                                        {
                                            _nextEventDue = DateTime.MaxValue;
                                        }
                                    }
                                    _timersSortedQueue.RemoveAt(idx);
                                    break;
                                }
                            }
                        }

#if HIGHRESTIMERS_TRACING
                    this.OnLogEntry(this.TimerPoolDump());
#endif
                    }
                }

                //Resort instead? - unfortunately this option doesn't exist

                if (dueSpan == Timeout.InfiniteTimeSpan || dueSpan == TimeSpan.MaxValue)
                    return;

                highResTimer.OverrideDue(dueTime);

                lock (_timerQueueLocker)
                {
                    this.RegisterTimerInternalUnsynchronized(dueSpan, highResTimer);
                }
            }
        }

        private void ReturnTimerToPoolUnsynchronized(HighResTimer timer)
        {
            if (_timersPoolIndex > 0)
            {
                _timersPool[--_timersPoolIndex] = timer;
            }
        }
        private void ReturnTimerToPoolSynchronized(HighResTimer timer)
        {
            lock (_timerQueueLocker)
            {
                this.ReturnTimerToPoolUnsynchronized(timer);
            }
        }

        private HighResTimer[] _timersPool;
        private int _timersPoolIndex = 0;
        private HighResTimer CreateTimer(Action callback, DateTime dueTime, TimerTaskFlagUtils.TimerTaskMultiFlag timerTaskMultiFlag)
        {
            HighResTimer timer;

            if (_timersPoolIndex >= _TIMERS_POOL_SIZE)
            {
                timer = new HighResTimer();
            }
            else
            {
                timer = _timersPool[_timersPoolIndex];
                _timersPool[_timersPoolIndex++] = null;
            }

            timer.OverrideContent(callback, dueTime, timerTaskMultiFlag);

            return timer;
        }

        public enum TimerPrecision
        {
            HighestAvailable,
            UpTo100ms,
            UpToSecond,
            UpToMinute
        }

        private class HighResTimer : IAccurateTimerInternal, IWorkItem
        {
#if HIGHRESTIMERS_TRACING || THREADPOOLEX_TRACING
            private static int _allIstancesCnt;
            private int _instanceCnt = Interlocked.Increment(ref _allIstancesCnt);

            public int Id { get { return _instanceCnt; } }
#endif
            public static Action<HighResTimer> OnTimerAvailableToPool;

            internal void OverrideContent(Action action, DateTime dueTime, TimerTaskFlagUtils.TimerTaskMultiFlag multiFlag)
            {
                this._action = action;
                this.Due = dueTime;
                this.Executed = false;
                this.MultiFlag = multiFlag;
            }

            internal void OverrideDue(DateTime dueTime)
            {
                this.Executed = false;
                this.Due = dueTime;
            }

            private Action _action;
            public TimerTaskFlagUtils.TimerTaskMultiFlag MultiFlag { get; set; }
            public DateTime Due { get; private set; }

            public DateTime EarliestDue
            {
                get { return Due - TimerTaskFlagUtils.GetTimerPrecisionOffset(this.MultiFlag); }
            }

            public DateTime LatestDue
            {
                get { return Due + TimerTaskFlagUtils.GetTimerPrecisionOffset(this.MultiFlag); }
            }

            public bool Executed { get; private set; }

            public DateTime QueuedTime { get; set; }

            internal bool Close()
            {
                bool wasClosed = this._action == null;
                this._action = null;
                return !wasClosed;
            }

            public void Execute()
            {
                //set inactive so that queue removing calls on the same thread do not attempt to remove from queue
                this.Executed = true;
                //this can attempt to reschedule timer - that is OK, since Execute flag is already set and will not 
                // overwrite whatever rescheduling sets
                this._action.SafeInvokeSynchronous();
            }

            private void ExecuteAsync()
            {
                this.Execute();
                //if noncancellable - return immediately
                if (!TimerTaskFlagUtils.IsCancellableTimer(this.MultiFlag) && OnTimerAvailableToPool != null)
                {
                    this.Close();
                    OnTimerAvailableToPool(this);
                }
            }

            ~HighResTimer()
            {
                //Subject to GC here => so nobody is holding reference (not even the pool or timer queue) - so it's safe to try to recycle
                if (OnTimerAvailableToPool != null)
                {
                    this.Close();
                    OnTimerAvailableToPool(this);
                }
            }

#if HIGHRESTIMERS_TRACING
            public override string ToString()
            {
                return string.Format("Timer [{0}], Due: {1:HH:mm:ss.fffffff}, Executed: {2}, Closed; {3}",
                    _instanceCnt, Due, Executed, this._action == null);
            }
#endif
            public Action Action { get { return this.ExecuteAsync; } }

            public TimerTaskFlagUtils.WorkItemPriority Priority
            {
                get { return TimerTaskFlagUtils.GetWorkItemPriority(this.MultiFlag); }
            }
        }
    }


    public static class TimerTaskFlagUtils
    {
        public enum WorkItemPriority
        {
            Lowest = 0,
            //BelowNormal,
            Normal = 1,
            //AboveNormal,
            Highest = 2,
        }

        public enum TimerPrecision
        {
            Highest = 0,
            UpTo100ms = 1,
            UpTo1sec = 2,
            UpTo1min = 3
        }

        internal static TimeSpan[] TimerPrecisionOffsets = new TimeSpan[]
        {
            TimeSpan.FromMilliseconds(Kreslik.Integrator.NMbgDevIO.NMbgTimer._TIMERS_RESOLUTION_THRESHOLD_MS),
            TimeSpan.FromMilliseconds(100),
            TimeSpan.FromSeconds(1),
            TimeSpan.FromMinutes(1)
        };

        [Flags]
        internal enum TimerTaskMultiFlag
        {
            Prec_Highest = 0,
            Prec_UpTo100ms = 1,
            Prec_UpTo1sec = 2,
            Prec_UpTo1min = 3,
            Cancellable = 4,
            UltraFast = 8,
            Pri_Lowest = 0,
            Pri_Normal = 16,
            Pri_Highest = 32,

            Prec_Mask = 3,
            Cancellable_Mask = 4,
            UltraFast_Mask = 8,
            Pri_Mask = 32 + 16
        }

        internal static TimerTaskMultiFlag GetTimerTaskMultiFlag(WorkItemPriority pri, TimerPrecision prec, bool isCancellableTimer,
            bool isUltraFastTask)
        {
            TimerTaskMultiFlag me = (TimerTaskMultiFlag)(((int)pri) << 16) | (TimerTaskMultiFlag)prec;
            if (isCancellableTimer)
                me |= TimerTaskMultiFlag.Cancellable;
            if (isUltraFastTask)
                me |= TimerTaskMultiFlag.UltraFast;

            return me;
        }

        internal static WorkItemPriority GetWorkItemPriority(TimerTaskMultiFlag me)
        {
            return (WorkItemPriority)((int)me >> 16);
        }

        internal static TimerTaskMultiFlag SetWorkItemPriority(this TimerTaskMultiFlag me, WorkItemPriority newPriority)
        {
            return (me & ~TimerTaskMultiFlag.Pri_Mask) | (TimerTaskMultiFlag)(((int)newPriority) << 16);
        }

        internal static TimerPrecision GetTimerPrecision(TimerTaskMultiFlag me)
        {
            return (TimerPrecision)(me & TimerTaskMultiFlag.Prec_Mask);
        }

        internal static TimerTaskMultiFlag SetTimerPrecision(this TimerTaskMultiFlag me, TimerPrecision newPrecision)
        {
            return (me & ~TimerTaskMultiFlag.Prec_Mask) | (TimerTaskMultiFlag)(newPrecision);
        }

        public static TimerPrecision GetTimerPrecisionForDelay(TimeSpan delay)
        {
            for (int idx = TimerPrecisionOffsets.Length - 1; idx >= 0; idx--)
            {
                if (TimerPrecisionOffsets[idx] < delay)
                    return (TimerPrecision) idx;
            }

            return TimerPrecision.Highest;
        }

        internal static TimeSpan GetTimerPrecisionOffset(TimerTaskMultiFlag me)
        {
            return TimerPrecisionOffsets[(int)(me & TimerTaskMultiFlag.Prec_Mask)];
        }

        internal static bool IsUltraFastTask(TimerTaskMultiFlag me)
        {
            return (me & TimerTaskMultiFlag.UltraFast_Mask) == TimerTaskMultiFlag.UltraFast_Mask;
        }

        internal static TimerTaskMultiFlag SetIsUltraFastTask(this TimerTaskMultiFlag me, bool newUltraFastFlag)
        {
            return (me & ~TimerTaskMultiFlag.UltraFast_Mask) |
                   (newUltraFastFlag ? TimerTaskMultiFlag.UltraFast_Mask : 0);
        }

        internal static bool IsCancellableTimer(TimerTaskMultiFlag me)
        {
            return (me & TimerTaskMultiFlag.Cancellable_Mask) == TimerTaskMultiFlag.Cancellable_Mask;
        }

        //internal static TimerTaskMultiFlag SetIsCancellableTimer(this TimerTaskMultiFlag me, bool newIsCancellableFlag)
        //{
        //    return (me & ~TimerTaskMultiFlag.Cancellable_Mask) |
        //           (newIsCancellableFlag ? TimerTaskMultiFlag.Cancellable_Mask : 0);
        //}

        //////// Test Only Methods /////

        private static bool TestMultienum(TimerTaskFlagUtils.WorkItemPriority pri, TimerTaskFlagUtils.TimerPrecision prec, bool isCancellable,
            bool isUltraFast)
        {
            TimerTaskFlagUtils.TimerTaskMultiFlag me = TimerTaskFlagUtils.GetTimerTaskMultiFlag(pri, prec, isCancellable, isUltraFast);
            if (TimerTaskFlagUtils.GetTimerPrecision(me) != prec)
                return false;
            if (TimerTaskFlagUtils.GetWorkItemPriority(me) != pri)
                return false;
            if (TimerTaskFlagUtils.IsCancellableTimer(me) != isCancellable)
                return false;
            if (TimerTaskFlagUtils.IsUltraFastTask(me) != isUltraFast)
                return false;

            return true;
        }

        internal static void TestSelf()
        {
            foreach (TimerTaskFlagUtils.WorkItemPriority pri in Enum.GetValues(typeof(TimerTaskFlagUtils.WorkItemPriority)))
            {
                foreach (TimerTaskFlagUtils.TimerPrecision prec in Enum.GetValues(typeof(TimerTaskFlagUtils.TimerPrecision)))
                {
                    if (!TestMultienum(pri, prec, true, true))
                        throw new Exception("TimerTaskMultiFlag corruption");
                    if (!TestMultienum(pri, prec, true, false))
                        throw new Exception("TimerTaskMultiFlag corruption");
                    if (!TestMultienum(pri, prec, false, true))
                        throw new Exception("TimerTaskMultiFlag corruption");
                    if (!TestMultienum(pri, prec, false, false))
                        throw new Exception("TimerTaskMultiFlag corruption");
                }
            }

            if (TimerPrecisionOffsets.Length != TimerPrecision.Highest.OwningEnumValuesCount())
                throw new Exception("TimerPrecisionOffsets corruption");
        }

        /////////////// Test Only Methods  /////////////////////
    }
}

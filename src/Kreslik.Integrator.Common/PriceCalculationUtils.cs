﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class PriceCalculationUtils
    {
        /// <summary>
        /// Returns true if the counterparty's price is the same or better than the price requested by our order.
        /// </summary>
        public static bool IsSameOrBetterPriceThanOurOrder(this decimal counterpartyPrice, decimal ourOrderPrice, DealDirection ourOrderDirection)
        {
            switch (ourOrderDirection)
            {
                case DealDirection.Buy: return counterpartyPrice <= ourOrderPrice; // we want to buy, so we want this or better (lower) price
                case DealDirection.Sell: return counterpartyPrice >= ourOrderPrice; // we want to sell, so we want this or better (higher) price
                default:
                    throw new ArgumentOutOfRangeException(nameof(ourOrderDirection), ourOrderDirection, null);
            }
        }

        public static decimal GetRelaxedOrderPrice(decimal originalOrderPrice, DealDirection ourOrderDirection, decimal relaxBp)
        {
            decimal priceToleranceDecimal = ConversionTools.ConvertBasisPointsToDecimal(relaxBp, originalOrderPrice);

            switch (ourOrderDirection)
            {
                case DealDirection.Buy:
                    return originalOrderPrice + priceToleranceDecimal; // we are ok to buy higher
                case DealDirection.Sell:
                    return originalOrderPrice - priceToleranceDecimal; // we are ok to sell lower
                default:
                    throw new ArgumentOutOfRangeException(nameof(ourOrderDirection), ourOrderDirection, null);
            }
        }

        public static decimal BpToDecimal(decimal bpValue, decimal medianPrice)
        {
            return (bpValue * medianPrice / 10000m).Normalize();
        }

        public static decimal BetterPrice(decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return Math.Min(priceA, priceB);
            else
                return Math.Max(priceA, priceB);
        }

        public static bool IsBetterPrice(this decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA < priceB;
            else
                return priceA > priceB;
        }

        public static bool IsBetterPriceEvenAfterDeteriorating(this decimal priceA, decimal priceB, decimal priceBImprovement, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA < priceB - priceBImprovement;
            else
                return priceA > priceB + priceBImprovement;
        }

        public static bool IsBetterPrice(this decimal priceA, decimal priceB, PriceSide side)
        {
            if (side == PriceSide.Ask)
                return priceA < priceB;
            else
                return priceA > priceB;
        }

        public static bool IsBetterOrEqualPrice(this decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA <= priceB;
            else
                return priceA >= priceB;
        }

        public static bool IsWorseOrEqualPrice(this decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA >= priceB;
            else
                return priceA <= priceB;
        }

        public static bool IsWorsePrice(this decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA > priceB;
            else
                return priceA < priceB;
        }

        public static decimal WorsePrice(decimal priceA, decimal priceB, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return Math.Max(priceA, priceB);
            else
                return Math.Min(priceA, priceB);
        }

        public static decimal ImprovePrice(this decimal priceA, decimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA - increment;
            else
                return priceA + increment;
        }

        public static decimal DeterioratePrice(this decimal priceA, decimal increment, DealDirection integratorDealDirection)
        {
            if (integratorDealDirection == DealDirection.Buy)
                return priceA + increment;
            else
                return priceA - increment;
        }

        public static decimal CalculateSpreadInBasisPoints(decimal ask, decimal bid)
        {
            return (ask - bid) / ((ask + bid)/2) * 10000m;
        }
    }
}

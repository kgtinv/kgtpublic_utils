﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class ConsoleColoringHelper : IDisposable
    {
        private readonly ConsoleColor _originalForeground = Console.ForegroundColor;
        private readonly ConsoleColor _originalBackground = Console.BackgroundColor;

        public void SetColorsBack()
        {
            Console.BackgroundColor = _originalBackground;
            Console.ForegroundColor = _originalForeground;
        }

        public static void SetColorsToSuccess()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
        }

        public static void SetColorsToFailure()
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Yellow;
        }

        public void Dispose()
        {
            this.SetColorsBack();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class CustomEqualityComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> _equalityComparerFunc;
        private readonly Func<T, int> _hashProvider;
        private static readonly Func<T, int> _DefaultHashProvider = a => 1;

        public CustomEqualityComparer(Func<T, T, bool> equalityComparerFunc)
            : this(equalityComparerFunc, _DefaultHashProvider)
        { }

        public CustomEqualityComparer(Func<T, T, bool> equalityComparerFunc, Func<T, int> hashProvider)
        {
            this._equalityComparerFunc = equalityComparerFunc;
            _hashProvider = hashProvider;
        }

        public bool Equals(T x, T y)
        {
            if (x == null || y == null)
            {
                return false;
            }

            return _equalityComparerFunc(x, y);
        }

        public int GetHashCode(T obj)
        {
            return _hashProvider(obj);
        }
    }
}

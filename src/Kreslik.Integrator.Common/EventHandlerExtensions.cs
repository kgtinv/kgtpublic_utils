﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class EventHandlerExtensions
    {
        public static void SafeInvoke(this Delegate evt, ILogger logger, params object[] args)
        {
            if (evt == null)
                return;

            logger?.Log(LogLevel.Info, "Starting to executing subscriber methods of event: {0}", evt);

            Parallel.ForEach(evt.GetInvocationList(), subscriberAction =>
            {
                string methodName = "<unknown>";
                if (subscriberAction != null)
                {
                    if (subscriberAction.Method != null)
                    {
                        methodName = subscriberAction.Method.ToString();
                    }
                    else
                    {
                        methodName = subscriberAction.ToString();
                    }
                }

                try
                {
                    //This way of invoking involves reflection and so is approximately 700x slower then direct invoke
                    // See e.g. http://stackoverflow.com/questions/12858340/difference-between-invoke-and-dynamicinvoke
                    subscriberAction.DynamicInvoke(args);
                }
                catch (Exception e)
                {
                    (logger ?? LogFactory.Instance.GetLogger(null)).LogException(LogLevel.Fatal, e,
                                              "Subscriber method: {0} experienced unhandled exception during invocation.",
                                              methodName);
                }
            });

            logger?.Log(LogLevel.Info, "Finished executing subscriber methods of event: {0}", evt);
        }

        public static void TryPerformAction(this Action action, ILogger logger)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Fatal, "Failed during safe invoking action", e);
            }
        }

        public static void TryPerformAction(this Action action)
        {
            TryPerformAction(action, false);
        }

        public static void TryPerformAction(this Action action, bool rethrow)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Failed during safe invoking action", e);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public static T TryPerformFunc<T>(this Func<T> func, bool rethrow)
        {
            try
            {
                return func();
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null).LogException(LogLevel.Fatal, "Failed during safe invoking action", e);
                if (rethrow)
                {
                    throw;
                }
                else
                {
                    return default(T);
                }
            }
        }

        public static void TryPerformDelegate(this Delegate evt, ILogger logger, params object[] args)
        {
            Action action = () => evt.DynamicInvoke(args);
            TryPerformAction(action, logger);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kreslik.Integrator.Common
{
    /// <summary>
    /// Exception thrown when error occured during deserialization of xml config file
    /// </summary>
    [Serializable()]
    public class ConfigurationDeserializationException : Exception
    {
        /// <summary>
        /// Initalizes new instance of ConfigurationDeserializationException
        /// </summary>
        public ConfigurationDeserializationException() : base() { }

        /// <summary>
        /// Initalizes new instance of ConfigurationDeserializationException
        /// </summary>
        /// <param name="message">Message about the cause of error</param>
        public ConfigurationDeserializationException(string message) : base(message) { }

        /// <summary>
        /// Initalizes new instance of ConfigurationDeserializationException
        /// </summary>
        /// <param name="message">Message about the cause of error</param>
        /// <param name="inner">Reference to the exception that caused error or null</param>
        public ConfigurationDeserializationException
                        (string message, System.Exception inner)
            : base(message, inner) { }

        /// <summary>
        /// Constructor needed for serialization when exception propagates 
        /// from a remoting server to the client.
        /// </summary>
        /// <param name="info">parameter intenaly used for deserialization</param>
        /// <param name="context">parameter intenaly used for deserialization</param>
        protected ConfigurationDeserializationException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class ProcessRunnerSettings
    {
        public string ProcessName { get; set; }
        public string Arguments { get; set; }
        public string KillCommand { get; set; }
        public int InstancesCount { get; set; }
    }

    public static class MultiProcessRunnerHelper
    {
        public static void RunAndManageSubProcesses(
            IEnumerable<ProcessRunnerSettings> settingsList, 
            CancellationToken shutdownCancellationToken,
            Action doneCallback,
            ILogger logger)
        {
            Task.Factory.StartNew(
                () =>
                {
                    IEnumerable<ProcessRunnerSettings> settingsUnwrapped =
                        settingsList.ToList()
                            .Select(sett => Enumerable.Range(1, sett.InstancesCount).Select(i => sett))
                            .SelectMany(sett2 => sett2);

                    Parallel.ForEach(settingsUnwrapped, processRunnerSettings =>
                    {
                        SingleProcessRunner spr = new SingleProcessRunner(processRunnerSettings.ProcessName,
                            processRunnerSettings.Arguments, processRunnerSettings.KillCommand,
                            shutdownCancellationToken,
                            logger);
                        spr.RunProcess();
                    });

                    logger.Log(LogLevel.Info, "All processes gracefully exited");

                    if (doneCallback != null)
                    {
                        doneCallback();
                    }
                });
        }
    }

    public class SingleProcessRunner
    {
        private string _processFileName;
        private string _arguments;
        private string _killCommand;
        private CancellationToken _shutdownCancellationToken;
        //private Dictionary<int, string> _friendlyNames = new Dictionary<int, string>();
        private Process _process = null;
        private ILogger _logger;

        public SingleProcessRunner(string processFileName, string arguments, string killCommand, CancellationToken shutdownCancellationToken, ILogger logger)
        {
            this._arguments = arguments;
            this._processFileName = processFileName;
            this._killCommand = killCommand;
            this._shutdownCancellationToken = shutdownCancellationToken;
            this._shutdownCancellationToken.Register(SendStopRequestToProcess);
            this._logger = logger;
        }

        public void RunProcess()
        {
            while (!_shutdownCancellationToken.IsCancellationRequested)
            {
                _process = new Process();
                _process.StartInfo.FileName = _processFileName;
                _process.StartInfo.Arguments = _arguments;
                _process.StartInfo.UseShellExecute = false;
                _process.StartInfo.RedirectStandardOutput = true;
                _process.StartInfo.RedirectStandardError = true;
                _process.StartInfo.RedirectStandardInput = true;
                _process.StartInfo.CreateNoWindow = true;

                _process.OutputDataReceived += ProcessOnOutputDataReceived;
                _process.ErrorDataReceived += ProcessOnOutputDataReceived;

                try
                {
                    _process.Start();
                }
                catch (Exception e)
                {
                    _process = null;
                    _logger.LogException(LogLevel.Fatal, e, "Experienced exception during attempt to start subprocess. WILL NOT RETRY TO START IT. Process details: [{0} {1}]", _processFileName, _arguments);
                    return;
                }

                _logger.Log(LogLevel.Info, "Subprocess started. Process details: [{0} {1}]", _processFileName, _arguments);

                _process.BeginOutputReadLine();
                _process.BeginErrorReadLine();

                _process.WaitForExit();

                _process.OutputDataReceived -= ProcessOnOutputDataReceived;
                _process.ErrorDataReceived -= ProcessOnOutputDataReceived;

                _logger.Log(LogLevel.Info, "Process exit detected. Will attempt to restart it after short pause (preventing poison storm) if cancel was not requested. Process details: [{0} {1}]", _processFileName, _arguments);
                _shutdownCancellationToken.WaitHandle.WaitOne(TimeSpan.FromSeconds(5));
            }

            _logger.Log(LogLevel.Info, "Exiting the process keeping loop for process as shutdown was requested. Process details: [{0} {1}]", _processFileName, _arguments);

        }

        private void SendStopRequestToProcess()
        {
            Process process = _process;

            if (process == null)
                return;

            _logger.Log(LogLevel.Info, "Detected shutdown signal - sending it to subprocess. Process details: [{0} {1}]", _processFileName, _arguments);

            try
            {
                if (process.StandardInput.BaseStream == null)
                {
                    _logger.Log(LogLevel.Fatal, "Cannot signal shutdown - as process has no active Std Input");
                }

                //\x3 is ctrl-C
                process.StandardInput.WriteLine(string.IsNullOrEmpty(_killCommand) ? "\x3" : _killCommand);
                process.StandardInput.Close();
            }
            catch (Exception e)
            {
                _logger.LogException(LogLevel.Fatal, e, "Error when attempting to signal exit");
            }
        }

        private static void ProcessOnOutputDataReceived(object sendingProcess, DataReceivedEventArgs dataReceivedEventArgs)
        {
            Console.WriteLine(((Process)sendingProcess).Id.ToString("00000") + " " + dataReceivedEventArgs.Data);
        }
    }
}

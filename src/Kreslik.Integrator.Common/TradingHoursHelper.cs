﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    [Serializable]
    public enum TimeZoneCode
    {
        UTC = 0,
        CET = 1,
        ET = 2,
        NZT = 3,
        BT = 4
    }

    public enum SystemsTradingControlScheduleAction
    {
        ActivateGoFlatOnly,
        ActivateKillSwitch,
        RestartIntegrator,
        NotifyClients,
        LogFatal
    }

    public interface ITradingHoursHelper
    {
        TimeZoneInfo GetTimeZoneInfo(TimeZoneCode timezoneCode);
        bool AreKgtTradingHours { get; }
        bool AreKgtTradingHoursInWorkWeek { get; }
        bool AreKgtTradingHoursInWorkWeekAtUtcTime(DateTime utcTime);
        bool IsWeekend { get; }
        bool IsSpecificTimeWeekend(DateTime timeUtc);
        DateTime EtNow { get; }
        DateTime CetNow { get; }
        DateTime NztNow { get; }
        event Action KgtTradingHoursStarting;
        event Action KgtTradingHoursEnding;
        event Action MarketRollingOver;
        //20 seconds before MarketRollingOver
        event Action MarketRolloverApproaching;
        //20 second after MarketRollingOver
        event Action MarketRolloverDone;
        /// <summary>
        /// 30 seconds before NZDRollingOver (07:00 Auckland)
        /// </summary>
        event Action NZDRolloverApproaching;
        /// <summary>
        /// 07:00 Auckland
        /// </summary>
        event Action NZDRollingOver;
        /// <summary>
        /// 20 second after 07:00 Auckland
        /// </summary>
        event Action NZDRolloverDone;
        event Action UtcMidnight;
        event Action EveryMinuteCheck;
        DateTime ConvertFromUtcToEt(DateTime utcTime);
        DateTime ConvertFromEtToUtc(DateTime etTime);
        DateTime ConvertFromNzToUtc(DateTime nzTime);
        DateTime ConvertFromUtcToCet(DateTime utcTime);
        DateTime ConvertFromCetToUtc(DateTime cetTime);
        DateTime ConvertFromCetToEt(DateTime cetTime);
        DateTime ConvertFromEtToCet(DateTime etTime);
        DateTime ConvertFromUtcToNzt(DateTime utcTime);
        DateTime ConvertToUtc(DateTime time, TimeZoneCode timeZone);
        DateTime ConvertFromUtc(DateTime time, TimeZoneCode timeZone);
        //TODO: event TradingWeekStarts, TradingWeekEnds

        bool IsInRolloverNow(Symbol symbol);
        bool IsInRollover(Symbol symbol, DateTime utcTime);

        DateTime LastCurrentlyUnsettledSettlementDateNZ { get; }
        DateTime LastCurrentlyUnsettledSettlementDateNY { get; }
        DateTime GetCounterpartyRolloverTimeInUtc(DateTime rolloverTimeLocal, Symbol symbol, Counterparty counterparty);
        DateTime GetCentralBankRolloverTimeInUtc(DateTime rolloverTimeLocal, Symbol symbol);
    }

    public class TradingHoursHelper : ITradingHoursHelper
    {
        private static readonly TimeSpan _approachingTime = TimeSpan.FromSeconds(30);
        private static readonly TimeSpan _eventCooldownTime = TimeSpan.FromSeconds(20);

        private static readonly TimeZoneInfo _easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
        private static readonly TimeZoneInfo _centralEuropeTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time");
        private static readonly TimeZoneInfo _nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
        private static readonly TimeZoneInfo _utcTimeZone = TimeZoneInfo.FindSystemTimeZoneById("UTC");
        private static readonly TimeZoneInfo _londonTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

        private static readonly TimeSpan _kgtTradingHoursStartCet = new TimeSpan(6, 30, 0); //new DateTime(0, 0, 0, 6, 0, 0, DateTimeKind.Utc).TimeOfDay;
        private static readonly TimeSpan _kgtTradingHoursEndCet = new TimeSpan(22, 0, 0); //new DateTime(0, 0, 0, 19, 0, 0, DateTimeKind.Utc).TimeOfDay;
        private static readonly TimeSpan _utcMidnight = new TimeSpan(0, 0, 0);
        //private static TimeSpan _rollOverTimeEt = new TimeSpan(17, 0, 0);
        private static readonly TimeSpan _rollOverApproachingTimeEt = new TimeSpan(17, 0, 0).Subtract(_approachingTime);
        //private static TimeSpan _nzdRollOverTimeNZT = new TimeSpan(07, 0, 0);
        private static readonly TimeSpan _nzdRollOverApproachingTimeNZT = new TimeSpan(07, 0, 0).Subtract(_approachingTime);

        private readonly SafeTimer<TradingEvents> _kgtTradingHoursStartTimer;
        private readonly SafeTimer<TradingEvents> _kgtTradingHoursEndTimer;
        private readonly SafeTimer<TradingEvents> _utcMidnightTimer;
        private readonly SafeTimer<TradingEvents> _rollOverTimeTimer;
        private readonly SafeTimer<TradingEvents> _nzdRolloverTimeTimer;
        private readonly SafeTimer _everyMinuteTimer;


        private ILogger _logger;

        private static ITradingHoursHelper _instance = new TradingHoursHelper(LogFactory.Instance.GetLogger(null));

        public static ITradingHoursHelper Instance
        {
            get { return _instance; }
        }

        private TradingHoursHelper(ILogger logger)
        {
            this._logger = logger;
            this._kgtTradingHoursStartTimer = new SafeTimer<TradingEvents>(OnEventConditionTriggered, TradingEvents.KgtTradingHoursStarting, SpanTillNextTimeOfDay(CetNow.TimeOfDay, _kgtTradingHoursStartCet), TimeSpan.FromDays(1))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._kgtTradingHoursEndTimer = new SafeTimer<TradingEvents>(OnEventConditionTriggered, TradingEvents.KgtTradingHoursEnding, SpanTillNextTimeOfDay(CetNow.TimeOfDay, _kgtTradingHoursEndCet), TimeSpan.FromDays(1))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._utcMidnightTimer = new SafeTimer<TradingEvents>(OnEventConditionTriggered, TradingEvents.UtcMidnight, SpanTillNextTimeOfDay(HighResolutionDateTime.UtcNow.TimeOfDay, _utcMidnight), TimeSpan.FromDays(1))
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._rollOverTimeTimer = new SafeTimer<TradingEvents>(OnEventConditionTriggered, TradingEvents.MarketRollingOver, SpanTillNextTimeOfDay(EtNow.TimeOfDay, _rollOverApproachingTimeEt), Timeout.InfiniteTimeSpan)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._nzdRolloverTimeTimer = new SafeTimer<TradingEvents>(OnEventConditionTriggered, TradingEvents.NZDRollingOver, SpanTillNextTimeOfDay(NztNow.TimeOfDay, _nzdRollOverApproachingTimeNZT), Timeout.InfiniteTimeSpan)
            {
                RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo100ms,
                TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
            };
            this._everyMinuteTimer = new SafeTimer(OnEveryMinute, TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1), true);
        }

        private void OnEventConditionTriggered(TradingEvents evt)
        {
            this._logger.Log(LogLevel.Info, "Executing event: {0}", evt);

            Action action = GetEvent(evt);

            if (UseSafeInvoke(evt))
                action.SafeInvoke(this._logger);
            else if (action != null)
                action();
        }

        private DateTime _lastMinuteHandlerInvokedUtc = HighResolutionDateTime.UtcNow;
        private void OnEveryMinute()
        {
            ThreadPoolEx.Instance.LogWaitingTime(HighResolutionDateTime.UtcNow - _lastMinuteHandlerInvokedUtc - TimeSpan.FromMinutes(1), ".net ThreadPool");
            this.EveryMinuteCheck.SafeInvoke(null);
            _lastMinuteHandlerInvokedUtc = HighResolutionDateTime.UtcNow;
        }

        private static TimeSpan SpanTillNextTimeOfDay(TimeSpan now, TimeSpan timeOfDay)
        {
            TimeSpan tillNextTimeInSameDay = timeOfDay - now;
            return tillNextTimeInSameDay > TimeSpan.Zero
                       ? tillNextTimeInSameDay
                       : tillNextTimeInSameDay.Add(TimeSpan.FromDays(1));
        }

        private enum TradingEvents
        {
            KgtTradingHoursStarting,
            KgtTradingHoursEnding,
            MarketRollingOver,
            NZDRollingOver,
            UtcMidnight
        }

        private void HandleMarketRolloverApproaching()
        {
            DateTime eventInvoked = HighResolutionDateTime.UtcNow;
            this.MarketRolloverApproaching.SafeInvoke(this._logger);
            TimeSpan delayTillRollover = _approachingTime - (HighResolutionDateTime.UtcNow - eventInvoked);
            if (delayTillRollover > TimeSpan.Zero)
                Thread.Sleep(delayTillRollover);
            this.MarketRollingOver.SafeInvoke(this._logger);
            TimeSpan delayTillAfterRollover = _approachingTime + _eventCooldownTime - (HighResolutionDateTime.UtcNow - eventInvoked);
            if(delayTillAfterRollover > TimeSpan.Zero)
                Thread.Sleep(delayTillAfterRollover);
            this.MarketRolloverDone.SafeInvoke(this._logger);

            //technically this is not 100% correct as Sunday after DST we will be shifted by one hour - but just for that one day
            // so we are OK with that
            _rollOverTimeTimer.Change(SpanTillNextTimeOfDay(EtNow.TimeOfDay, _rollOverApproachingTimeEt), Timeout.InfiniteTimeSpan);
        }

        private void HandleNZDMarketRolloverApproaching()
        {
            DateTime eventInvoked = HighResolutionDateTime.UtcNow;
            this.NZDRolloverApproaching.SafeInvoke(this._logger);
            TimeSpan delayTillRollover = _approachingTime - (HighResolutionDateTime.UtcNow - eventInvoked);
            if (delayTillRollover > TimeSpan.Zero)
                Thread.Sleep(delayTillRollover);
            this.NZDRollingOver.SafeInvoke(this._logger);
            TimeSpan delayTillAfterRollover = _approachingTime + _eventCooldownTime - (HighResolutionDateTime.UtcNow - eventInvoked);
            if (delayTillAfterRollover > TimeSpan.Zero)
                Thread.Sleep(delayTillAfterRollover);
            this.NZDRolloverDone.SafeInvoke(this._logger);

            //technically this is not 100% correct as Sunday after DST we will be shifted by one hour - but just for that one day
            // so we are OK with that
            _nzdRolloverTimeTimer.Change(SpanTillNextTimeOfDay(NztNow.TimeOfDay, _nzdRollOverApproachingTimeNZT), Timeout.InfiniteTimeSpan);
        }

        private bool UseSafeInvoke(TradingEvents tradingEvent)
        {
            switch (tradingEvent)
            {
                case TradingEvents.MarketRollingOver:
                case TradingEvents.NZDRollingOver:
                    return false;
                case TradingEvents.KgtTradingHoursStarting:
                case TradingEvents.KgtTradingHoursEnding:
                case TradingEvents.UtcMidnight:
                default:
                    return true;
            }
        }

        private Action GetEvent(TradingEvents tradingEvent)
        {
            switch (tradingEvent)
            {
                case TradingEvents.KgtTradingHoursStarting:
                    return this.KgtTradingHoursStarting;
                case TradingEvents.KgtTradingHoursEnding:
                    return this.KgtTradingHoursEnding;
                case TradingEvents.MarketRollingOver:
                    return this.HandleMarketRolloverApproaching;
                case TradingEvents.NZDRollingOver:
                    return this.HandleNZDMarketRolloverApproaching;
                case TradingEvents.UtcMidnight:
                    return this.UtcMidnight;
                default:
                    this._logger.Log(LogLevel.Fatal, "Unexpected enumeration value: {0}", tradingEvent);
                    return null;
            }
        }

        public TimeZoneInfo GetTimeZoneInfo(TimeZoneCode timezoneCode)
        {
            switch (timezoneCode)
            {
                case TimeZoneCode.UTC:
                    return _utcTimeZone;
                case TimeZoneCode.CET:
                    return _centralEuropeTimeZone;
                case TimeZoneCode.ET:
                    return _easternTimeZone;
                case TimeZoneCode.NZT:
                    return _nzTimeZone;
                case TimeZoneCode.BT:
                    return _londonTimeZone;
                default:
                    throw new ArgumentOutOfRangeException("timezoneCode");
            }
        }

        public bool AreKgtTradingHours
        {
            get
            {
                TimeSpan currentCetTimeOfDay = CetNow.TimeOfDay;
                return currentCetTimeOfDay > _kgtTradingHoursStartCet && currentCetTimeOfDay < _kgtTradingHoursEndCet;
            }
        }

        public bool IsWeekend
        {
            get { return IsSpecificTimeWeekend(DateTime.UtcNow); }
        }

        public bool IsSpecificTimeWeekend(DateTime timeUtc)
        {
            DayOfWeek day = ConvertFromUtcToCet(timeUtc).DayOfWeek;
            return day == DayOfWeek.Saturday || day == DayOfWeek.Sunday;
        }

        public bool AreKgtTradingHoursInWorkWeek
        {
            get { return this.AreKgtTradingHoursInWorkWeekAtUtcTime(HighResolutionDateTime.UtcNow); }
        }

        public bool AreKgtTradingHoursInWorkWeekAtUtcTime(DateTime utcTime)
        {
            DateTime cetTime = this.ConvertFromUtcToCet(utcTime);
            TimeSpan cetTimeOfDay = cetTime.TimeOfDay;
            DayOfWeek day = cetTime.DayOfWeek;

            return cetTimeOfDay > _kgtTradingHoursStartCet &&
                   cetTimeOfDay < _kgtTradingHoursEndCet &&
                   day != DayOfWeek.Saturday &&
                   day != DayOfWeek.Sunday;
        }

        public event Action KgtTradingHoursStarting;

        public event Action KgtTradingHoursEnding;

        public event Action MarketRollingOver;

        public event Action MarketRolloverApproaching;

        public event Action NZDRolloverApproaching;

        public event Action NZDRollingOver;

        public event Action MarketRolloverDone;

        public event Action NZDRolloverDone;
        public event Action UtcMidnight;

        public event Action EveryMinuteCheck;

        //TODO: event TradingWeekStarts, TradingWeekEnds

        public DateTime ConvertFromUtcToEt(DateTime utcTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(utcTime, _easternTimeZone);
        }

        public DateTime ConvertFromEtToUtc(DateTime etTime)
        {
            return TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(etTime, DateTimeKind.Unspecified), _easternTimeZone);
        }

        public DateTime ConvertFromNzToUtc(DateTime nzTime)
        {
            return TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(nzTime, DateTimeKind.Unspecified), _nzTimeZone);
        }

        public DateTime ConvertFromUtcToNzt(DateTime utcTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(utcTime, _nzTimeZone);
        }

        public DateTime NztNow
        {
            get { return ConvertFromUtcToNzt(HighResolutionDateTime.UtcNow); }
        }

        public DateTime EtNow
        {
            get { return ConvertFromUtcToEt(HighResolutionDateTime.UtcNow); }
        }

        public DateTime ConvertFromUtcToCet(DateTime utcTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(utcTime, _centralEuropeTimeZone);
        }

        public DateTime CetNow
        {
            get { return ConvertFromUtcToCet(HighResolutionDateTime.UtcNow); }
        }

        public DateTime ConvertFromCetToUtc(DateTime cetTime)
        {
            return TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(cetTime, DateTimeKind.Unspecified), _centralEuropeTimeZone);
        }

        public DateTime ConvertFromCetToEt(DateTime cetTime)
        {
            return ConvertFromUtcToEt(ConvertFromCetToUtc(cetTime));
        }

        public DateTime ConvertFromEtToCet(DateTime etTime)
        {
            return ConvertFromUtcToCet(ConvertFromEtToUtc(etTime));
        }

        public DateTime ConvertToUtc(DateTime time, TimeZoneCode timeZone)
        {
            return TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(time, DateTimeKind.Unspecified), this.GetTimeZoneInfo(timeZone));
        }

        public DateTime ConvertFromUtc(DateTime time, TimeZoneCode timeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(time, DateTimeKind.Unspecified),
                this.GetTimeZoneInfo(timeZone));
        }

        public DateTime LastCurrentlyUnsettledSettlementDateNZ
        {
            get
            {
                DateTime nztNow = this.NztNow;
                if (nztNow.TimeOfDay < _nzdRollOverApproachingTimeNZT.Add(_approachingTime))
                {
                    return nztNow.Date;
                }
                else
                {
                    return nztNow.Date.AddDays(1);
                }
            }
        }

        public DateTime LastCurrentlyUnsettledSettlementDateNY
        {
            get
            {
                DateTime etNow = this.EtNow;
                if (etNow.TimeOfDay < _rollOverApproachingTimeEt.Add(_approachingTime))
                {
                    return etNow.Date;
                }
                else
                {
                    return etNow.Date.AddDays(1);
                }
            }
        }

        public bool IsInRolloverNow(Symbol symbol)
        {
            return this.IsInRollover(symbol, HighResolutionDateTime.UtcNow);
        }

        public bool IsInRollover(Symbol symbol, DateTime utcTime)
        {
            TimeSpan etTimeOfDay = this.ConvertFromUtcToEt(utcTime).TimeOfDay;

            if (etTimeOfDay >= _rollOverApproachingTimeEt &&
                etTimeOfDay <= _rollOverApproachingTimeEt + _approachingTime + _eventCooldownTime)
                return true;


            if (symbol.BaseCurrency == Currency.NZD || symbol.TermCurrency == Currency.NZD)
            {
                TimeSpan nzTimeOfDay = this.ConvertFromUtcToNzt(utcTime).TimeOfDay;

                if (nzTimeOfDay >= _nzdRollOverApproachingTimeNZT &&
                    nzTimeOfDay <= _nzdRollOverApproachingTimeNZT + _approachingTime + _eventCooldownTime)
                    return true;
            }

            return false;
        }

        public DateTime GetCounterpartyRolloverTimeInUtc(DateTime rolloverTimeLocal, Symbol symbol, Counterparty counterparty)
        {
            if (rolloverTimeLocal == System.Data.SqlTypes.SqlDateTime.MinValue.Value)
                return System.Data.SqlTypes.SqlDateTime.MinValue.Value;

            if (symbol.BaseCurrency == Currency.NZD || symbol.TermCurrency == Currency.NZD)
            {
                //Hotspot settles NZD some 2-3 hours later (so the earlies next 17:00 ET after the correct 7:00AM Auckland time)
                if (counterparty.TradingTargetType == TradingTargetType.Hotspot)
                    return
                        this.ConvertFromEtToUtc(rolloverTimeLocal.Date + _rollOverApproachingTimeEt + _approachingTime).AddDays(-1);
                else
                    return
                        this.ConvertFromNzToUtc(rolloverTimeLocal.Date + _nzdRollOverApproachingTimeNZT + _approachingTime);
            }
            else
            {
                return
                    this.ConvertFromEtToUtc(rolloverTimeLocal.Date + _rollOverApproachingTimeEt + _approachingTime);
            }

        }

        public DateTime GetCentralBankRolloverTimeInUtc(DateTime rolloverTimeLocal, Symbol symbol)
        {
            if (rolloverTimeLocal == System.Data.SqlTypes.SqlDateTime.MinValue.Value)
                return System.Data.SqlTypes.SqlDateTime.MinValue.Value;

            if (symbol.BaseCurrency == Currency.NZD || symbol.TermCurrency == Currency.NZD)
            {
                return
                    this.ConvertFromNzToUtc(rolloverTimeLocal.Date + _nzdRollOverApproachingTimeNZT + _approachingTime);
            }
            else
            {
                return
                    this.ConvertFromEtToUtc(rolloverTimeLocal.Date + _rollOverApproachingTimeEt + _approachingTime);
            }
        }
    }
}

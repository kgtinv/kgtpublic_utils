﻿#define THREADPOOLEX_TRACING

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface IWorkItem
    {
#if THREADPOOLEX_TRACING
        int Id { get; }
#endif
        Action Action { get; }
        TimerTaskFlagUtils.WorkItemPriority Priority { get; }

        DateTime QueuedTime { get; set; }
    }

    //It is actually overkill to pool this - as we should not have such an extreme number of workitems
    // We cannot pool GC-freely without locking (see comments in GCFreeObjectPool) - this would ruin the benefit
    // Of GC-free ThreadPoolEx
    public class WorkItem : IWorkItem
    {
        public WorkItem(Action action, TimerTaskFlagUtils.WorkItemPriority priority)
        {
            this.Action = action;
            this.Priority = priority;
        }

        public void OverrideContent(Action action, TimerTaskFlagUtils.WorkItemPriority priority)
        {
            this.Action = action;
            this.Priority = priority;
        }

        public DateTime QueuingTime { get; set; }

        public Action Action { get; private set; }
        public TimerTaskFlagUtils.WorkItemPriority Priority { get; private set; }

        public DateTime QueuedTime { get; set; }
#if THREADPOOLEX_TRACING

        private static int _allIstancesCnt;
        private readonly int _instanceCnt = -Interlocked.Increment(ref _allIstancesCnt);
        public int Id { get { return _instanceCnt; } }
#endif
    }

    public class DummyThreadPoolExPerformanceCounters : IThreadPoolExPerformanceCounters
    {
        private long _actualThreadsNum;
        private long _availableThreadsNum;
        private long _queuedItems;

        public void Initialize(int initialThreadsNum)
        {
            this._actualThreadsNum = initialThreadsNum;
            this._availableThreadsNum = initialThreadsNum;
            this._queuedItems = 0;
        }

        public long IncrementQueuedTasks()
        {
            return Interlocked.Increment(ref this._queuedItems);
        }

        public void DecrementQueuedTasks(TimeSpan spanTheTaskWasQueued)
        {
            Interlocked.Decrement(ref this._queuedItems);
        }

        public long QueuedTasksCount
        {
            get { return this._queuedItems; }
        }

        public long AvailableThreadsCount
        {
            get { return this._availableThreadsNum; }
        }

        public void IncrementAvailableThreads()
        {
            Interlocked.Increment(ref this._availableThreadsNum);
        }

        public void DecrementAvailableThreads()
        {
            Interlocked.Decrement(ref this._availableThreadsNum);
        }

        public long IncrementTotalThreads()
        {
            return Interlocked.Increment(ref this._actualThreadsNum);
        }

        public void DecrementTotalThreads()
        {
            Interlocked.Decrement(ref this._actualThreadsNum);
        }

        public long TotalThreadsCount
        {
            get { return Interlocked.Read(ref this._actualThreadsNum); }
        }
    }

    public class ThreadPoolEx
    {
        private ConcurrentQueue<IWorkItem>[] _priorityQueues =
            Enumerable.Repeat(0, TimerTaskFlagUtils.WorkItemPriority.Lowest.OwningEnumValuesCount())
                .Select(i => new ConcurrentQueue<IWorkItem>())
                .ToArray();

        private readonly int _numThreadsReservedForHighestPriorityTask;
        private readonly int _minimumThreadsNum;
        private readonly int _maximumThreadsNum;
        //private int _actualThreadsNum;
        //private int _availableThreadsNum;
        //private int _queuedItems;
        private readonly IThreadPoolExPerformanceCounters _counters;
        //need to GC root threads
        private List<Thread> _threads = new List<Thread>();
        private readonly SemaphoreSlim[] _workerNeededSemaphores = { new SemaphoreSlim(0), new SemaphoreSlim(0) };
        private readonly WaitHandle[] _workerNeededSemaphoresHandles;
        private ILogger _logger;

        public static ThreadPoolEx Instance { get; private set; }

        static ThreadPoolEx()
        {
            Instance = new ThreadPoolEx(IntegratorPerformanceCounter.Instance, LogFactory.Instance.GetLogger("ThreadPoolEx"));
        }

        internal ThreadPoolEx(IThreadPoolExPerformanceCounters counters, ILogger logger)
            :this(1, Environment.ProcessorCount * 4, counters, logger)
        { }

        internal ThreadPoolEx(int threadsReservedForHighestPriorityTask, int maximumThreadsNum, IThreadPoolExPerformanceCounters counters, ILogger logger)
        {
            if (threadsReservedForHighestPriorityTask == maximumThreadsNum)
                throw new Exception("Invalid ThreadPoolEx initialization");

            this._counters = counters;
            this._logger = logger;
            this._numThreadsReservedForHighestPriorityTask = threadsReservedForHighestPriorityTask;
            this._maximumThreadsNum = maximumThreadsNum;
            this._minimumThreadsNum = threadsReservedForHighestPriorityTask + 1;
            this._workerNeededSemaphoresHandles = _workerNeededSemaphores.Select(s => s.AvailableWaitHandle).ToArray();

            for (int i = 0; i < _minimumThreadsNum; i++)
            {
                this.CreateThread(i);
            }
            counters.Initialize(_minimumThreadsNum);
            //Cannot execute this in ctor context - as it would create circular dependency
            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(15),
                () => TradingHoursHelper.Instance.EveryMinuteCheck += InstanceOnEveryMinuteCheck);
        }

        public void LogWaitingTime(TimeSpan longestWaitingTask, string poolName)
        {
            if (longestWaitingTask > TimeSpan.FromMilliseconds(20))
            {
                _logger.Log(
                    longestWaitingTask < TimeSpan.FromSeconds(1)
                        ? LogLevel.Trace
                        : (longestWaitingTask < TimeSpan.FromSeconds(10)
                            ? LogLevel.Info
                            : (longestWaitingTask < TimeSpan.FromMinutes(1) ? LogLevel.Error : LogLevel.Fatal)),
                    "Currently waiting time in {0}: {1}", poolName, longestWaitingTask);
            }
        }

        
        private void InstanceOnEveryMinuteCheck()
        {
            TimeSpan waitingTime = GetLongestWaitingTime();
            LogWaitingTime(GetLongestWaitingTime(), "ThreadPoolEx");

            if (waitingTime > TimeSpan.FromMinutes(1))
            {
                long actualThreadNumLocal = this._counters.IncrementTotalThreads();
                this.CreateThread(actualThreadNumLocal - 1);
                this._counters.IncrementAvailableThreads();
            }
        }

        private Thread CreateThread(long threadOrdinalId)
        {
            var thread = new Thread(ProcessQueuedItems);
            thread.Name = "ThreadPoolEx thread #" + threadOrdinalId;
            thread.IsBackground = true;
            lock (_threads)
                _threads.Add(thread);
            thread.Start(threadOrdinalId);

            return thread;
        }

        private void SignalNextWorkitem(TimerTaskFlagUtils.WorkItemPriority priority)
        {
            _workerNeededSemaphores[priority == TimerTaskFlagUtils.WorkItemPriority.Highest ? 0 : 1].Release();
        }

        private void WaitForNextWorkitem(TimerTaskFlagUtils.WorkItemPriority priority)
        {
            if (priority == TimerTaskFlagUtils.WorkItemPriority.Highest)
            {
                _workerNeededSemaphores[0].Wait();
                return;
            }

            //WaitAny doesn't decrement the Semaphores CurrentCount - so we need to test the signaled semaphore explicitly
            while (!this._workerNeededSemaphores[WaitHandle.WaitAny(this._workerNeededSemaphoresHandles)].Wait(0))
            { }
        }

        private TimeSpan GetLongestWaitingTime()
        {
            //from lowest priority - as those items will wait longest
            for (int i = 0; i >= (int)_priorityQueues.Length - 1; i++)
            {
                IWorkItem workItem;
                if (_priorityQueues[0].TryPeek(out workItem))
                {
                    return HighResolutionDateTime.UtcNow - workItem.QueuedTime;
                }
            }

            return TimeSpan.Zero;
        }

        public void QueueWorkItem(IWorkItem workItem)
        {
#if THREADPOOLEX_TRACING
            this._logger.Log(LogLevel.Trace, "Queuing WorkItem {0} [{1}]", workItem.Id, workItem.Priority);
#endif

            workItem.QueuedTime = HighResolutionDateTime.UtcNow;
            _priorityQueues[(int)workItem.Priority].Enqueue(workItem);

            if (!TryEnsureThreadsNum(workItem.Priority) &&
                workItem.Priority == TimerTaskFlagUtils.WorkItemPriority.Highest)
            {
                this._logger.Log(LogLevel.Error, "ThreadPoolEx could not immediately facilitate highest priority item. Not doing anything about it now.");

                //This is too dangerous - it can cause deadlocks - even on timer thread 
                //    (e.g. timer executing order timeout which needs matching lock, but that can be held by thread waiting on timer lock to enter order timer)

                //if (this.TryPopNextQueuedItem(TimerTaskFlagUtils.WorkItemPriority.Highest, true, out workItem))
                //{
                //    this._logger.Log(LogLevel.Error, "ThreadPoolEx could not immediately facilitate highest priority item. Executing it synchronously.");
                //    //Execute synchronously
                //    this.ExecuteWorkItem(workItem);
                //}
            }
        }

        private bool TryEnsureThreadsNum(TimerTaskFlagUtils.WorkItemPriority queuedItemPriority)
        {
            long queuedTasksNum = this._counters.IncrementQueuedTasks();
            long availableThreadsCnt = queuedItemPriority == TimerTaskFlagUtils.WorkItemPriority.Highest
                ? this._counters.AvailableThreadsCount
                : this._counters.AvailableThreadsCount - this._numThreadsReservedForHighestPriorityTask;

            //Signal only here, as otherwise signal can unblock waiting thread that picks the item and that decrements number of available threads
            this.SignalNextWorkitem(queuedItemPriority);

            if (queuedTasksNum <= availableThreadsCnt)
                return true;

            if ((int)queuedItemPriority < (int)TimerTaskFlagUtils.WorkItemPriority.Normal
                    && queuedTasksNum <= availableThreadsCnt * 2)
                return true;

            long actualThreadNumLocal = this._counters.IncrementTotalThreads();

            if (actualThreadNumLocal > this._maximumThreadsNum)
            {
                this._counters.DecrementTotalThreads();
                this._logger.Log(LogLevel.Info, "ThreadPoolEx could not immediately facilitate new item.");
                return false;
            }

            this.CreateThread(actualThreadNumLocal-1);
            this._counters.IncrementAvailableThreads();
            return true;
        }

        private void ProcessQueuedItems(object state)
        {
            long currentThreadOrdinalId = (long) state;
            TimerTaskFlagUtils.WorkItemPriority minPriorityToProcess = currentThreadOrdinalId < _numThreadsReservedForHighestPriorityTask
                ? TimerTaskFlagUtils.WorkItemPriority.Highest
                : TimerTaskFlagUtils.WorkItemPriority.Lowest;

            this._logger.Log(LogLevel.Trace, "ThreadPoolEx starting thread [{0}]", Thread.CurrentThread.Name);

            try
            {
                while (true)
                {
                    this._counters.DecrementAvailableThreads();

                    IWorkItem workItem;
                    while (this.TryPopNextQueuedItem(minPriorityToProcess, false, out workItem))
                    {
                        this.ExecuteWorkItem(workItem);
                    }

                    this._counters.IncrementAvailableThreads();

                    if (currentThreadOrdinalId > _minimumThreadsNum)
                    {
                        //give this thread some time to show if it's needed
                        if (_workerNeededSemaphores[1].Wait(TimeSpan.FromSeconds(120)))
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    this.WaitForNextWorkitem(minPriorityToProcess);
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Unhandled exception in ThreadPoolEx during processing items", e);
                throw;
            }

            this._counters.DecrementTotalThreads();
            this._counters.DecrementAvailableThreads();
            this._logger.Log(LogLevel.Info, "{0} is finishing it's execution", Thread.CurrentThread.Name);
        }

        private void ExecuteWorkItem(IWorkItem workItem)
        {
#if THREADPOOLEX_TRACING
            this._logger.Log(LogLevel.Trace, "[{0}] Executing WorkItem {1}", Thread.CurrentThread.Name, workItem.Id);
#endif

            try
            {
                workItem.Action();
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "{0} thread receiving exception when executing workitem",
                    Thread.CurrentThread.Name);
            }
        }

        public bool RegisterHandlerForDelaysInQueue(TimeSpan minimumDelayToInformAbout,
            Action<IWorkItem, TimeSpan> onWorkItemDelayed)
        {
            this._minDelayToWatchFor = minimumDelayToInformAbout;
            this._onWorkItemDelayed = onWorkItemDelayed;
            return true;
        }

        private TimeSpan _minDelayToWatchFor;
        private Action<IWorkItem, TimeSpan> _onWorkItemDelayed;
        protected Action OnPopingNextItem = null;

        private bool TryPopNextQueuedItem(TimerTaskFlagUtils.WorkItemPriority minimumPriority, bool calledDuringEnqueuing, out IWorkItem result)
        {
            if (!calledDuringEnqueuing)
            {
                //This is here for unit testing - so that we can inherit and then inject waiting behavior
                Action onPopingNextItemLocal = this.OnPopingNextItem;
                if (onPopingNextItemLocal != null)
                {
                    onPopingNextItemLocal();
                }
            }

            for (int i = _priorityQueues.Length - 1; i >= (int) minimumPriority; i--)
            {
                if (_priorityQueues[i].TryDequeue(out result))
                {
                    TimeSpan queuedSpan = HighResolutionDateTime.UtcNow - result.QueuedTime;
                    this._counters.DecrementQueuedTasks(queuedSpan);
                    if (queuedSpan >= _minDelayToWatchFor && _onWorkItemDelayed != null)
                    {
                        _onWorkItemDelayed(result, queuedSpan);
                    }
                    return true;
                }
            }

            result = null;
            return false;
        }
    }

    //internal class MyBlockColl
    //{
    //    ConcurrentQueue<string> cq = new ConcurrentQueue<string>();
    //    //SemaphoreSlim 

    //    public MyBlockColl()
    //    {
    //        cq.Enqueue("a");
    //    }
    //}
}

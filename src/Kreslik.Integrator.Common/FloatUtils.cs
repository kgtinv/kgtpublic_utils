﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public static class FloatUtils
    {
        public static string FloatAsHex(float value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            StringBuilder sb = new StringBuilder("0x");

            foreach (byte by in bytes)
                sb.Append(by.ToString("X2"));

            return sb.ToString();
        }

        private static byte[] GetBytesFromHextString(string hexString, bool isInLittleEndianOrder)
        {
            if (hexString.Length < 4 || hexString[0] != '0' || hexString[1] != 'x' || hexString.Length % 2 != 0)
            {
                throw new ArgumentException("Wrong input to GetBytesFromHextString: " + hexString);
            }

            int bytesLength = (hexString.Length - 2)/2;
            byte[] bytes = new byte[bytesLength];

            for (int hexIdx = 2; hexIdx < bytesLength * 2 + 2; hexIdx += 2)
            {
                bytes[hexIdx / 2 - 1] = Convert.ToByte(hexString.Substring(hexIdx, 2), 16);
            }

            if (isInLittleEndianOrder != BitConverter.IsLittleEndian)
            {
                bytes = bytes.Reverse().ToArray();
            }

            return bytes;
        }

        public static float FloatFromHex(string hexString, bool isInLittleEndianOrder)
        {
            //0xcdcccc3d
            if (hexString.Length != 10)
                throw new ArgumentException("Wrong input to FloatFromHex: " + hexString);

            return BitConverter.ToSingle(GetBytesFromHextString(hexString, isInLittleEndianOrder), 0);
        }

        public static float FloatFromHex(string hexString)
        {
            //0xcdcccc3d
            if (hexString.Length != 10)
                throw new ArgumentException("Wrong input to FloatFromHex: " + hexString);

            return BitConverter.ToSingle(GetBytesFromHextString(hexString, BitConverter.IsLittleEndian), 0);
        }

        private static string[] _pythonFloatStringPrefixes = new string[] { "Float_LittleEndian", "Float_BigEndian" };
        public static float FloatFromPythonString(string pythonDumpedString)
        {
            //Float_LittleEndian:0xcdcccc3d
            string[] parts = pythonDumpedString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                throw new ArgumentException("Wrong input to FloatFromPythonString: " + pythonDumpedString);

            int endiannessIdx = Array.IndexOf(_pythonFloatStringPrefixes, parts[0]);

            if (endiannessIdx < 0)
                throw new ArgumentException("Wrong input to FloatFromPythonString: " + pythonDumpedString);

            return FloatFromHex(parts[1], endiannessIdx == 0);
        }

        public static double DoubleFromHex(string hexString, bool isInLittleEndianOrder)
        {
            //0xb96a37ad01d69636
            if (hexString.Length != 18)
            {
                //TODO: temporary workaround!
                hexString = string.Format("0x{0}{1}",
                    new String(Enumerable.Repeat('0', 18 - hexString.Length).ToArray()), hexString.Substring(2));

                //throw new ArgumentException("Wrong input to DoubleFromHex: " + hexString);
            }

            return BitConverter.ToDouble(GetBytesFromHextString(hexString, isInLittleEndianOrder), 0);
        }

        private static string[] _pythonDoubleStringPrefixes = new string[] { "Double_LittleEndian", "Double_BigEndian" };
        public static double DoubleFromPythonString(string pythonDumpedString)
        {
            //Double_LittleEndian:0xb96a37ad01d69636
            string[] parts = pythonDumpedString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                throw new ArgumentException("Wrong input to DoubleFromPythonString: " + pythonDumpedString);

            int endiannessIdx = Array.IndexOf(_pythonDoubleStringPrefixes, parts[0]);

            if (endiannessIdx < 0)
                throw new ArgumentException("Wrong input to DoubleFromPythonString: " + pythonDumpedString);

            return DoubleFromHex(parts[1], endiannessIdx == 0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public class DictionaryArraysBuilder<TKey, TValue>
    {
        private Dictionary<TKey, List<TValue>> _dict = new Dictionary<TKey, List<TValue>>();

        public void Add(TKey key, TValue value)
        {
            if (_dict.ContainsKey(key))
            {
                _dict[key].Add(value);
            }
            else
            {
                _dict[key] = new List<TValue>() { value };
            }
        }

        public TValue this[TKey key]
        {
            set { this.Add(key, value); }
        }

        public Dictionary<TKey, TValue[]> ToDictionary()
        {
            return _dict.ToDictionary(pair => pair.Key, pair => pair.Value.ToArray());
        }

        public Dictionary<TKey, TValue[]> ToDictionary(IEnumerable<TKey> keysToSelect)
        {
            return keysToSelect.Where(k => _dict.ContainsKey(k)).ToDictionary(k => k, k => _dict[k].ToArray());
        }
    }
}

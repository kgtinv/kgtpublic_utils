﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface IExecutionHelper
    {
        void StartAndWaitTillDone(bool failOnErrorReturn);
        string StandartErrorContent { get; }
        string StandartOutputContent { get; }
    }

    public interface IProcessCommunicationWrapper: IDisposable
    {
        void StartAsyncCommunication();
        bool WaitForAsyncComunicationDone(TimeSpan maxWaitTime);
    }

    public class RedirectedProcessHelper : IExecutionHelper
    {
        private ILogger _logger;
        private string _filename;
        private string _arguments;
        private IProcessCommunicationWrapper _processCommunicationWrapper;

        public RedirectedProcessHelper(ILogger logger, string filename, string arguments)
            : this(logger, filename, arguments, null)
        { }

        public RedirectedProcessHelper(ILogger logger, string filename, string arguments, IProcessCommunicationWrapper processCommunicationWrapper)
        {
            _logger = logger;
            _filename = filename;
            _arguments = arguments;
            _processCommunicationWrapper = processCommunicationWrapper;
        }

        public RedirectedProcessHelper(ILogger logger, string filename)
            : this(logger, filename, null)
        { }

        public string WorkingDirectory { get; set; }

        public bool LogToConsole { get; set; } = false;
        public bool LogToConsoleWithoutInfoPrefix { get; set; } = false;

        public void StartAndWaitTillDone(bool failOnErrorReturn)
        {
            Process process = new Process();
            process.StartInfo.FileName = _filename;
            process.StartInfo.Arguments = _arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.WorkingDirectory = this.WorkingDirectory;

            if (!process.Start())
            {
                throw new Exception(string.Format("Could not start process [{0}] args:[{1}]", _filename, _arguments));
            }
            //here we make sure that this process will be killed by OS as soon as our process dies
            JobObjectManager.RegisterProcess(process);

            ProcessId = process.Id;
            StreamReader stdOut = process.StandardOutput;
            StreamReader stdErr = process.StandardError;

            _standartOutputBuilder = new SharedStringBuilder();
            _standartErrorBuilder = new SharedStringBuilder();
            //those tasks are potentially long running and should not block and be blocked by thread pool pressure
            Task stdOutCollectingTask =
                Task.Factory.StartNew(
                    () => this.OutputReadingLoop(ProcessId, stdOut, _standartOutputBuilder, NewStdoutOutput,  "stdout"), TaskCreationOptions.LongRunning);

            Task stdErrCollectingTask =
                Task.Factory.StartNew(
                    () => this.OutputReadingLoop(ProcessId, stdErr, _standartErrorBuilder, NewStderrOutput, "stderr"), TaskCreationOptions.LongRunning);

            _processCommunicationWrapper?.StartAsyncCommunication();
            process.WaitForExit();
            int exitCode = process.ExitCode;
            process.Dispose();
            _logger.Log(LogLevel.Trace, "{0} (pid: {1}) process Finished", Path.GetFileNameWithoutExtension(_filename), ProcessId);
            //It can happen that during main process start there is such a pressure on ThreadPool, that the scheduled tasks
            // take a looong time to event start (and hence also exit)
            for (int attempt = 1; attempt <= 5; attempt++)
            {
                if (!Task.WaitAll(new[] { stdOutCollectingTask, stdErrCollectingTask }, TimeSpan.FromSeconds(120)))
                {
                    bool isLastAttempt = attempt == 5;

                    _logger.Log(LogLevel.Fatal, "ThreadPool pressure detected. Retrying delayed tasks. If any fail they will be reported separately");
                    _logger.Log(isLastAttempt ? LogLevel.Fatal : LogLevel.Error,
                        "{0} process Finished however failed to wait for finalization of async stdout/stderr reading. Attempt #{1}",
                        Path.GetFileNameWithoutExtension(_filename), attempt);
                }
            }
            

            if (_processCommunicationWrapper != null &&
                !_processCommunicationWrapper.WaitForAsyncComunicationDone(TimeSpan.FromSeconds(1)))
            {
                _logger.Log(LogLevel.Fatal,
                    "{0} process Finished however failed to wait for finalization of async data communication with process",
                    Path.GetFileNameWithoutExtension(_filename));
            }
            _processCommunicationWrapper?.Dispose();

            //Since we wait for task with timeout - they might be still running now - so locking StringBuilder
            // to prevent exception internal to sb code
            _standartOutputBuilder.FinishBuilding();
            _standartErrorBuilder.FinishBuilding();

            this.HasError = exitCode != 0;

            if (failOnErrorReturn && HasError)
            {
                throw new Exception(
                    string.Format("Process exited with error code [{0}] and failing on error exit was requested",
                        exitCode));
            }
        }

        //This is the attempt to workaround OS redirected outputs buffering - for more info see e.g. http://stackoverflow.com/questions/3844267/how-to-disable-output-buffering-in-process-standardoutput
        // however it's still not fully successful; as there must be some other buffering going on - see: http://stackoverflow.com/questions/40890443/why-is-process-standarderror-buffered
        private void OutputReadingLoop(
            int processId, 
            StreamReader outputReader, 
            SharedStringBuilder outputBuilder, 
            Action<string> outputEvent,
            string outputDescriptor)
        {
            char[] chars = new char[1024];
            int charsRead = 0;

            string lineFormat = string.Format("{{0}} UTC|{0}[pid:{1:00000}] {2}:{{1}}",
                Path.GetFileNameWithoutExtension(_filename), processId, outputDescriptor);

            StringBuilder currentOutputBuilder = new StringBuilder();
            while (0 != (charsRead = outputReader.Read(chars, 0, chars.Length)))
            {
                bool wasEmpty;
                lock (currentOutputBuilder)
                {
                    wasEmpty = currentOutputBuilder.Length <= 0;
                    currentOutputBuilder.Append(chars, 0, charsRead);
                }

                if (wasEmpty)
                {
                    DateTime receivedUtc = HighResolutionDateTime.UtcNow;
                    //wait for next content that might be part of current line

                    Task.Factory.StartNew(() =>
                    {
                        Thread.Sleep(20);
                        lock (currentOutputBuilder)
                        {
                            if (currentOutputBuilder.Length > 0)
                            {
                                string currentOutput = currentOutputBuilder.ToString();
                                currentOutputBuilder.Clear();

                                _logger.LogConditionalConsoleInfo(
                                    LogToConsole,
                                    LogToConsoleWithoutInfoPrefix ? currentOutput.Trim() :
                                    string.Format(lineFormat,
                                        receivedUtc.ToLongStringWithFractions(),
                                        // We need to trim, especially trailing newlines - as we read everything from output (including newlines)
                                        //  so those would be doubled since logger is also newlining
                                        currentOutput.Trim()));
                                outputBuilder.SafeAppend(currentOutput);
                                outputEvent?.Invoke(currentOutput);
                            }
                        }
                    }, 
                    //we need to attach to parent so that we are sure that the output collection task doesn't end prior collecting all output
                    TaskCreationOptions.AttachedToParent)
                        .ContinueWith(
                            t => _logger.LogException(LogLevel.Fatal, "Error in process starting task", t.Exception),
                            TaskContinuationOptions.OnlyOnFaulted);
                }

                //outputReader.BaseStream.Flush();
            }
        }


        private SharedStringBuilder _standartOutputBuilder;
        private SharedStringBuilder _standartErrorBuilder;
        public string StandartErrorContent => _standartErrorBuilder?.Content;
        public string StandartOutputContent => _standartOutputBuilder?.Content;
        public event Action<string> NewStdoutOutput;
        public event Action<string> NewStderrOutput;
        public bool HasError { get; private set; }
        public int ProcessId { get; private set; }

        private class SharedStringBuilder
        {
            private readonly StringBuilder _builder = new StringBuilder();
            private bool _isDone;
            private string _content;

            public string Content
            {
                get
                {
                    if (_isDone)
                    {
                        return _content;
                    }
                    else
                    {
                        lock (_builder)
                        {
                            return _builder.ToString();
                        }
                    }
                }
            }
            
            public void SafeAppend(string str)
            {
                lock (_builder)
                {
                    _builder.Append(str);
                }
            }

            public void FinishBuilding()
            {
                _content = Content;
                _isDone = true;
            }
        }

        public string RunProcessAndGetOutput(out bool success)
        {
            try
            {
                WorkingDirectory = Path.GetDirectoryName(_filename);

                this.StartAndWaitTillDone(false);
                success = !this.HasError;

                return this.StandartOutputContent;
            }
            catch (Exception e)
            {
                success = false;
                return "ERROR running process: " + e.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kreslik.Integrator.Common
{
    public class CommonSettings
    {
        public bool AttemptToUseMeinbergClocks { get; set; }
        public LoggingSettings Logging { get; set; }
        public AutomailerSettings Automailer { get; set; }

        [System.Xml.Serialization.XmlIgnore]
        public static CommonSettings CommonBehavior
        {
            get
            {
                if (_commonBehavior == null)
                {
                    //We need to be carefull about unit test environemnts (and other minimal auto-deploy envs)
                    if (SettingsInitializator.Instance.EnvironmentName == null)
                    {
                        _commonBehavior = new CommonSettings
                            {
                                Logging = new LoggingSettings {AvailableDiskSpaceThresholdToStopMDLogging_GB = 15},
                                Automailer =
                                    new AutomailerSettings
                                        {
                                            SendEmailsOnlyToLocalFolder = true,
                                            DirectoryForLocalEmails = "LocalEmails",
                                            SmtpUsername = "automailer@kgtinv.com"
                                        }
                            };
                    }
                    else
                    {
                        _commonBehavior =
                            SettingsInitializator.Instance.ReadSettings<CommonSettings>(
                                @"Kreslik.Integrator.Common.dll",
                                Properties.Resources.Kreslik_Integrator_Common_dll);
                    }
                }

                return _commonBehavior;
            }
        }

        private static CommonSettings _commonBehavior;

        public class LoggingSettings
        {
            public int AvailableDiskSpaceThresholdToStopMDLogging_GB { get; set; }

            public string FatalErrorsEmailTo { get; set; }
            public string FatalErrorsEmailCc { get; set; }

            private List<TimeSpan> _fatalErrorsEmailMinDelaysBetweenEmails;

            [System.Xml.Serialization.XmlIgnore]
            public List<TimeSpan> FatalErrorsEmailMinDelaysBetweenEmails
            {
                get
                {
                    if (_fatalErrorsEmailMinDelaysBetweenEmails == null)
                    {
                        _fatalErrorsEmailMinDelaysBetweenEmails = FatalErrorsEmailMinDelaysLevelsBetweenEmailsXml == null
                                              ? new List<TimeSpan>()
                                              : FatalErrorsEmailMinDelaysLevelsBetweenEmailsXml.Select(seconds => TimeSpan.FromSeconds(seconds)).ToList();
                    }

                    return _fatalErrorsEmailMinDelaysBetweenEmails;
                }

                set
                {
                    _fatalErrorsEmailMinDelaysBetweenEmails = value;
                    FatalErrorsEmailMinDelaysLevelsBetweenEmailsXml = value == null ? new List<int>() : value.Select(span => (int)span.TotalSeconds).ToList();
                }
            }

            [XmlArray(ElementName = "FatalErrorsEmailMinDelaysLevelsBetweenEmails")]
            [XmlArrayItem(ElementName = "Delay_Seconds")]
            public List<int> FatalErrorsEmailMinDelaysLevelsBetweenEmailsXml
            {
                get;
                set;
            }


        }

        public class AutomailerSettings
        {
            public string SmtpServer { get; set; }
            public int SmtpPort { get; set; }
            public string ImapServer { get; set; }
            public int ImapPort { get; set; }
            public bool BypassCertificationValidation { get; set; }
            public string SmtpUsername { get; set; }
            public string SmtpPassword { get; set; }
            public string DirectoryForLocalEmails { get; set; }
            public bool SendEmailsOnlyToLocalFolder { get; set; }
        }
    }
}

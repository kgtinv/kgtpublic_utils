﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.Common
{
    public interface ISettingsInitializator
    {
        void SetProcessProcessType(IntegratorProcessType processType);
        IntegratorProcessType IntegratorProcessType { get; }
        string InstanceDescription { get; }
        string EnvironmentName { get; }
        string InstanceName { get; }
        bool IsUat { get; }
        bool IsTradingInstance { get; }
        bool IsIntegratorProcess { get; }
        T ReadSettings<T>(string settingName, string xsdConfigurationUri) where T : class;
        bool HasSettings(string settingName);
        string UpdateCredentialsInConnectionString(string originalConnectionString);
    }
}
